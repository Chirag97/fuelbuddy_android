package com.wemonde.fuelbuddy.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.MapActivity;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AddAssetRequestVo;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.AssetIdentifier;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.models.DistrictArray;
import com.wemonde.fuelbuddy.models.FuelTypeVo;
import com.wemonde.fuelbuddy.models.MyLoc;
import com.wemonde.fuelbuddy.models.State;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.GPSTracker;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.base.Apis.URL_ADD_ASSET;
import static com.wemonde.fuelbuddy.base.Apis.URL_ASSET_IDENTIFIER;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_GET_DISTRICT_BY_STATE;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ASSETS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_ADD_ASSET;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_FUEL_TYPES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ALL_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSET_IDENTIFIER;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DISTRICT;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_LOCATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;

public class AddAssetActivity extends BaseActivity {


   // private Spinner spn_asset;
    private Spinner spn_fuel_type;

    private int fuelId = -1;
    private int assetId = -1;
    private int buildId = -1;
    private int stateId = -1;
    private int districtId = -1;


   // private EditText et_manufacture;
   // private EditText et_model;
    private AutoCompleteTextView et_capacity;
    private AutoCompleteTextView et_asset_id;
    private EditText et_address;
    private Spinner spn_buildings;
    private Button btn_add;
    private Asset asset;

    private EditText et_lat;
    private EditText et_lng;
    private EditText et_pin;
   // private Button getloc;

    private Spinner spn_states;
    private Spinner spn_district;
    private Spinner spn_location;

    private int aid = 0;

    private int mySpinner_selectedId=1;
    private MyLoc myLoca;
    private static  int flagcheckupdateoradd = 0;

    String[] ProgLanguages = { "Genset", "JCB", "Office", "Location", "Vehicle", "Honda",
            "Jackson", "Mahindra", "Generator", "Tank", "Home", "Farmhouse" };


    String[] capcityarray = { "40", "50", "60", "70", "80", "90",
            "100", "150", "200", "250", "300", "350","400","450","500","550","600","700","800","900","1000","1500","2000","2500","3000","3500","4000","4500" };


    String loca="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_add_asset);
        asset = (Asset)getIntent().getSerializableExtra("asset");

        myLoca = new MyLoc();
        flagcheckupdateoradd= 0;

        initViews();
        setLocationTypeSpinnner();


        if (states!= null){
            setStateSpinner();
        }else {
            getStates();
        }


        fuelTypes = getFuelTypes();
        if (fuelTypes != null) {
          //  setFuelTypeSpinner();




            fuelId = 1;

            ArrayList<String> assetLabels1 = new ArrayList<>();
            assetLabels1.add(" Diesel ");
            SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
            spn_fuel_type.setAdapter(assetAdapter1);
        }

      //  getBuildings();
        if(asset == null){
           // setBasicToolBar("Add Asset");
            flagcheckupdateoradd= 1;
        }else{
          //  setBasicToolBar("Edit Asset");
            setAsset();
          //  btn_add.setText("Update");
            flagcheckupdateoradd = 2 ;
        }


        if (buildings == null) {
            getBuildings();
        }else if (flagcheckupdateoradd == 1){
            setBuildingSpinnernew();
        }else if (flagcheckupdateoradd == 2){

            setBuildingSpinnernew();
           // setBuildingSpinner();
        }

        assetId = 1;
//        if (identifiers == null) {
//            assetId = 1;
//
//            ArrayList<String> assetLabels1 = new ArrayList<>();
//            assetLabels1.add(" Immovable Asset ");
//            SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
//            spn_asset.setAdapter(assetAdapter1);
//          //  getAssetIdentifier();
//        }else {
//            setAssetSpinner();
//        }

        if(asset == null){
            setBasicToolBar("Add Asset");
            flagcheckupdateoradd= 1;
        }else{
            setBasicToolBar("Edit Asset");
            setAsset();
            btn_add.setText("Update");
            flagcheckupdateoradd = 2 ;
        }




        //changing the color programmatically

        toolbar.setBackgroundColor(Color.parseColor("#45b879"));


    }

    @Override
    protected void onResume() {
        super.onResume();
//        getGpsLoc();

    }



    protected void getdistrictfromstate (String districtid) {
        //  trustEveryone();

        String url = URL_FOR_GET_DISTRICT_BY_STATE.replace("<UID>", districtid);
        volleyGetHit(url , REQUEST_GET_DISTRICT);

        try{
            spn_district.setSelection(0);
        }catch (Exception e){

        }

        //
        // URL_FOR_GET_DISTRICT_BY_STATE
    }

    private void setAsset(){
        aid = asset.getAssetID();



        spn_fuel_type.setSelection(mySpinner_selectedId);


     //   mySpinner_selectedId = spn_asset.getSelectedItemPosition();
      //  spn_asset.setSelection(mySpinner_selectedId);

       //et_manufacture.setText(""+asset.getAssetManufacturer() );
       // et_model.setText(""+asset.getAssetModel() );
        et_pin.setText(""+asset.getPINCode());

        et_address.setText( ""+asset.getAssetLocation());
        et_capacity.setText(""+asset.getAssetFuelCapacity());
        et_asset_id.setText(""+asset.getAssetS_ID());
        et_lat.setText(""+ asset.getLatitude());
        et_lng.setText(""+ asset.getLongitude());



        int posFuel = 0;
        if(fuelTypes != null) {
            fuelId = 1;

            ArrayList<String> assetLabels1 = new ArrayList<>();
            assetLabels1.add(" Diesel ");
            SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
            spn_fuel_type.setAdapter(assetAdapter1);
//            for (int i = 0; i < fuelTypes.size(); i++) {
//                if (fuelTypes.get(i).getFuelName().equalsIgnoreCase("" + asset.getFuelName())) {
//                    posFuel = i+1;
//                }
//            }
        }

        String build = asset.getBuildName();
        int posBuild = 0;
        if(buildings != null) {
            for (int i = 0; i < buildings.size(); i++) {
                try {
                    if (buildings.get(i).getBuildName().equalsIgnoreCase(build)) {
                        posBuild = i + 1;
                    }
                }catch (Exception e){

                }
            }
        }

        int posAsset = 0;
        if(identifiers != null) {
            assetId = 1;

            ArrayList<String> assetLabels1 = new ArrayList<>();
            assetLabels1.add(" Immovable Asset ");
            SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
           // spn_asset.setAdapter(assetAdapter1);
//            for (int i = 0; i < identifiers.size(); i++) {
//
//                if (identifiers.get(i).getAssetIdentifierName().equalsIgnoreCase("" + asset.getAssetIdentifierName())) {
//                    posAsset = i+1;
//                }
//            }
        }

        int state = 0;
        {
            if (states != null) {
                for (int i = 0; i < states.size(); i++) {


                    if (asset.getStateID() == states.get(i).getStateID() ) {
                        state = i+1 ;

                        if (i <= 0 ){
                            Slog.d("build id :-- " + stateId);
                            // getdistrictfromstate(String.valueOf(stateId));
                        }else {
                            getdistrictfromstate(String.valueOf(stateId));
                            Slog.d("build id :-- " + stateId);
                        }


                      //  getdistrictfromstate(String.valueOf(state));
                        // showToast(states.get(posFuel).getStateName());

                    }
                }
            }
        }



        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    // getGpsLoc();
                    int district = 0;
                    if(districtarray != null) {
                        for (int i = 0; i < districtarray.size(); i++) {


                            if (asset.getDistrictId() ==  districtarray.get(i).getDistrictId()) {
                                //dealertypeid= dealercompleteinfo.get(0).getDealerRankID();
                                district = i+1;
//                                    if (dealertypeid == 3){
//                                        showdialognew ();
//                                    }
                            }
                        }
                    }

                    spn_district.setSelection(district);
                    //  imagedownloading ();
                    hideProgress();
                }

            };
            h.postDelayed(r, 1000);
            // showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }






        spn_states.setSelection(state);

        spn_fuel_type.setSelection(posFuel);
        spn_buildings.setSelection(posBuild);

    }

    private void initViews() {


        spn_buildings = (Spinner) findViewById(R.id.spn_buildings);

       // spn_asset = (Spinner) findViewById(R.id.spn_asset);
        btn_add = (Button) findViewById(R.id.btn_add);
        spn_fuel_type = (Spinner) findViewById(R.id.spn_fuel_type);
      //  et_manufacture = (EditText) findViewById(R.id.et_manufacture);
       // et_model = (EditText) findViewById(R.id.et_model);
        et_capacity = (AutoCompleteTextView) findViewById(R.id.et_capacity);
        et_asset_id = (AutoCompleteTextView) findViewById(R.id.et_asset_id);
        et_address = (EditText) findViewById(R.id.et_address);

        et_lat = (EditText) findViewById(R.id.et_lat);
        et_lng = (EditText) findViewById(R.id.et_lng);
        et_pin = (EditText) findViewById(R.id.et_pin);

       // getloc = (Button)findViewById(R.id.getloc);


        spn_district = (Spinner)findViewById(R.id.spn_district_assets);
        spn_location = (Spinner)findViewById(R.id.location_type);


        spn_states = (Spinner) findViewById(R.id.spn_states_assets);


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.autocompletetextview, capcityarray);
        //Used to specify minimum number of
        //characters the user has to type in order to display the drop down hint.
        et_capacity.setThreshold(1);
        //Setting adapter
        et_capacity.setAdapter(arrayAdapter);





        ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(this,
                R.layout.autocompletetextview, ProgLanguages);
        //Used to specify minimum number of
        //characters the user has to type in order to display the drop down hint.
        et_asset_id.setThreshold(1);
        //Setting adapter
        et_asset_id.setAdapter(arrayAdapter1);

//        spn_asset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i > 0)
//                    assetId = identifiers.get(i - 1).getAssetIdentifierID();
//                Slog.d("asset id :-- " + assetId);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//            }
//        });


//        getloc.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gotolocation();
//            }
//        });


        spn_buildings.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i > 0)
                    buildId = buildings.get(i -1).getBuildID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
        spn_fuel_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    fuelId = fuelTypes.get(i - 1).getFuelID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spn_states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) stateId = states.get(i - 1).getStateID();

                getdistrictfromstate(String.valueOf(stateId));
                Slog.d("build id :-- " + stateId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });


        spn_district.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) districtId = districtarray.get(i - 1).getDistrictId();
                Slog.d("build id :-- " + stateId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }



    @Override
    protected void onResponse(int request_code, String response, String data) {
//        super.onResponse(request_code, response, data);

        Gson gson = new Gson();
        switch (request_code) {



            case REQUEST_GET_STATES:
                try {
                    Type type = new TypeToken<ArrayList<com.wemonde.fuelbuddy.models.State>>() {}.getType();
                    states = gson.fromJson(new JSONObject(data).get("assets").toString(), type);




                    //  setStateSpinner();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case REQUEST_GET_ASSET_IDENTIFIER:
                try {
                    Type type = new TypeToken<ArrayList<AssetIdentifier>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    identifiers = gson.fromJson(jobj.get("assets").toString(), type);
                    setAssetSpinner();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_FUEL_TYPES:
                try {
                    SharedPrefrenceHelper.setFuels(bContext, data);
                    FuelTypeVo assetVo = gson.fromJson(data, FuelTypeVo.class);
                    fuelTypes = assetVo.getAssets();
                    setFuelTypeSpinner();
                } catch (Exception e) {
                    Slog.e("exception in assets -====--- fuel type   " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case REQUEST_ADD_ASSET:
                if (flagcheckupdateoradd ==1 ) {
                    showToast("Asset Added Successfully");
                }else if (flagcheckupdateoradd == 2 ){
                    showToast("Asset Updated Successfully");
                }
                finish();
                break;






            case REQUEST_GET_BUILDING:
                try {
//                    SharedPrefrenceHelper.setFuels(bContext, data);
                    Type type = new TypeToken<ArrayList<Building>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    buildings = gson.fromJson(jobj.get("assets").toString(), type);
                    setBuildingSpinner();
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    showLogToast("unable to parse buildings");
                    e.printStackTrace();
                }
                break;


            case  REQUEST_GET_DISTRICT :

                try {
//                    SharedPrefrenceHelper.setFuels(bContext, data);
                    Type type = new TypeToken<ArrayList<DistrictArray>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    districtarray = gson.fromJson(jobj.get("assets").toString(), type);
                    setDistrictSpinner();
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    showLogToast("unable to parse district");
                    e.printStackTrace();
                }
                break;


        }
    }

    @Override
    protected void canGetLocation(GPSTracker gps) {
//        super.canGetLocation(gps);
        et_lat.setText("" + gps.getLatitude());
        et_lng.setText("" + gps.getLongitude());
    }

    private String prepareJson() {
        if (AppUtils.isEmpty(et_lat)) {
            showToast("Unable to get location");
//            getGpsLoc();
            return "";
        }

        if (AppUtils.isEmpty(et_lng)) {
            showToast("Unable to get location");
//            getGpsLoc();
            return "";
        }

        //not mandatory field
//        if (AppUtils.isEmpty(et_manufacture)) {
//            showToast("Enter Manufacturer");
//            return "";
//        }

        //not mandatory
//        if (AppUtils.isEmpty(et_model)) {
//            showToast("Enter Model");
//            return "";
//        }

        if (AppUtils.isEmpty(et_asset_id)) {
            showToast("Enter Asset Name");
            return "";
        }

        if (AppUtils.isEmpty(et_address)) {
            showToast("Enter Address");
            return "";
        }
        int a = Integer.parseInt(et_capacity.getText().toString().trim());
        if (a <= 0) {
            showToast("Enter Valid Capacity");
            return "";
        }



        if (assetId <= 0) {
            showToast("Select Asset Identifier");
            return "";
        }

        if (fuelId <= 0) {
            showToast("Select Fuel Type");
            return "";
        }

        if (stateId <= 0) {
            showToast("Please Select State");
            return "";
        }

        if (buildId <= 0) {
            showToast("Select Building ");
            return "";
        }

//        if (districtId <= 0) {
//            showToast("Select District ");
//            return "";
//        }


        if (AppUtils.isEmpty(et_pin)) {
            showToast("Please Enter Pincode");
            return "";
        }

        try {
            AddAssetRequestVo avo = new AddAssetRequestVo();
//            avo.setAssetModel("" + et_model.getText().toString().trim());
            avo.setAssetManufacturer(" ");
            avo.setAssetID(aid);
            avo.setAssetIdentifierID(assetId);
            //avo.setAssetLocationType(loca);
//            avo.setAssetManufacturer("" + et_manufacture.getText().toString().trim());
            avo.setAssetModel(" ");
            avo.setAssetS_ID("" + et_asset_id.getText().toString());
            avo.setLatitude(et_lat.getText().toString().trim());
            avo.setLongitude(et_lng.getText().toString().trim());
            avo.setPINCode(et_pin.getText().toString().trim());
            avo.setAssetFuelCapacity(a);
            avo.setFuelID(fuelId);
            avo.setAssetIsActive(1);
            avo.setAssetLocation("" + et_address.getText().toString().trim());
            avo.setBuildID(buildId);
            avo.setDistrictId(String.valueOf(districtId));
            avo.setStateID(stateId + "");

            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 209 addassetactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public void clickAdd(View v) {
        String json = prepareJson();
        if (json.length() <= 0) return;
        String url = URL_ADD_ASSET;
        volleyBodyHit(url, json, REQUEST_ADD_ASSET);

    //    showdialogmaterial("");
       // gotoMyAsset();
    }

    private boolean setAssetSpinner() {
        try {

            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Asset");
            for (int i = 0; i < identifiers.size(); i++) {
                assetLabels.add(identifiers.get(i).getAssetIdentifierName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            //spn_asset.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean setFuelTypeSpinner() {
        try {
            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Fuel");
            for (int i = 0; i < fuelTypes.size(); i++) {
                assetLabels.add(fuelTypes.get(i).getFuelName());
            }

            SpinnerAdapter fuelTypeAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);

//            ArrayAdapter<String> fuelTypeAdapter = new ArrayAdapter<String>(bContext, android.R.layout.simple_dropdown_item_1line, res.getStringArray(R.array.fuel_array));

            spn_fuel_type.setAdapter(fuelTypeAdapter);


            return true;
        } catch (Exception e) {
            Slog.e("exception in fuel type spinner");
            e.printStackTrace();
            return false;
        }
    }

    private boolean setBuildingSpinner() {
        try {
            if (buildings == null) {
                showToast("No Buildings Found");
                return false;
            }

            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Building");



            for (int i = 0; i < buildings.size(); i++) {
                assetLabels.add(buildings.get(i).getBuildName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_buildings.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private boolean setBuildingSpinnernew() {
        try {
            if (buildings == null) {
                showToast("No Buildings Found");
                return false;
            }

            ArrayList<String> assetLabels = new ArrayList<>();
           // assetLabels.add("Select Building");



            for (int i = 0; i < buildings.size(); i++) {
                assetLabels.add(buildings.get(i).getBuildName());
            }
            buildId = buildings.get(0).getBuildID();
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_buildings.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    private boolean setStateSpinner() {
        try {
            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select State");
            for (int i = 0; i < states.size(); i++) {
                assetLabels.add(states.get(i).getStateName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_states.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    private boolean setDistrictSpinner() {
        try {
            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select District");
            for (int i = 0; i <districtarray.size(); i++) {
                assetLabels.add(districtarray.get(i).getDistrictName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_district.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void setLocationTypeSpinnner(){
        ArrayList<String> locationtypess= new ArrayList<>();
        locationtypess.add("Select Location Type");
        locationtypess.add("BaseMent");
        locationtypess.add("Ground");
        locationtypess.add("Terrrace");


        for(int i=0;i<locationtypess.size();i++){

        }

    SpinnerAdapter adapter = new SpinnerAdapter(bContext, R.layout.item_spinner, locationtypess);
        //ArrayAdapter<String> adapter= new ArrayAdapter<>(bContext,android.R.layout.simple_spinner_dropdown_item,locationtypess);
        spn_location.setAdapter(adapter);
        spn_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 loca= parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_GET_LOCATION){
                Slog.d("result from map");

                MyLoc mloc = (MyLoc)data.getSerializableExtra("mloc");
                try{
                    if(mloc != null) {
                        Slog.d("lat lng  : " + mloc.getLat() + "," + mloc.getLng());
                        et_lat.setText("" + mloc.getLat());
                        et_lng.setText("" + mloc.getLng());
                        et_address.setText("" + mloc.getAddress());

                        et_pin.setText(""+mloc.getPincode());
                    }
                }catch (Exception e){}
            }
        }
    }



    public void showdialogmaterial (String msg) {

        new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher_app)
                .limitIconToDefaultSize()
                .title("FuelBuddy")
                .content(" Do you wish to configure your Tank parameters for TankBuddy (The Automatic level measurement system) ")
                .positiveText("Allow")
                .negativeText("Deny")
                .onAny(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        showToast("Want to Know More : " + dialog.isPromptCheckBoxChecked());
                    }
                })
                .checkBoxPromptRes(R.string.TankBuddy, false, null)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog dialog, DialogAction which) {
                // TODO
            }
        })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        showdialogmaterialnew ();
                    }
                })
                .show();
    }

    public void showdialogmaterialnew (){
        boolean wrapInScrollView = true;
        new MaterialDialog.Builder(this)
                .title(R.string.app_name)
                .iconRes(R.mipmap.ic_launcher_app)
                .customView(R.layout.custom_view, wrapInScrollView)
                .positiveText(R.string.done)
                .show();





    }
}