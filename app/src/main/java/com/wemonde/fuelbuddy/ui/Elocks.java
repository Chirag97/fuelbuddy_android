package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AssetVo;
import com.wemonde.fuelbuddy.utils.Slog;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_LOCK;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_UNLOCK_ELOCK;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_LOCK_ELOCK;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_UNLOCK_ELOCK;

public class Elocks extends BaseActivity {


    private TextView unlock,lock;
    private ImageView lockimage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_elocks);

        setBasicToolBar("E-LOCK");

        lock=(TextView)findViewById(R.id.elock);
        unlock = (TextView)findViewById(R.id.unlock);
        lockimage=(ImageView)findViewById(R.id.lockimage);


        lock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                lockelocks();

                lockimage.setBackgroundResource(R.drawable.lockimage);
                //lockimage.setba

            }
        });

        unlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                unlockelocks();

                lockimage.setBackgroundResource(R.drawable.unlockimage);
            }
        });



    }

    protected void lockelocks() {
        //  trustEveryone();
        volleyGetHit(URL_FOR_LOCK, REQUEST_GET_LOCK_ELOCK);
    }

    protected void unlockelocks() {
        //  trustEveryone();
        volleyGetHit(URL_FOR_UNLOCK_ELOCK, REQUEST_GET_UNLOCK_ELOCK);
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        switch (request_code) {
            case REQUEST_GET_LOCK_ELOCK:
                try {
                    showToast("E-LOCK LOCKED ");
                    //HomeActivity.assetsdetails = assets;
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case REQUEST_GET_UNLOCK_ELOCK:
                try {
                    showToast("E-LOCK UNLOCKED ");
                    //HomeActivity.assetsdetails = assets;
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }
                break;
        }
    }
}
