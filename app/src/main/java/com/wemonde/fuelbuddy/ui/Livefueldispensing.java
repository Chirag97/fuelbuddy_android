package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.os.Handler;

import com.github.anastr.speedviewlib.SpeedView;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

import static com.wemonde.fuelbuddy.R.anim.animation_entry_right;

public class Livefueldispensing extends BaseActivity{

    private   Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setscreenLayout(R.layout.activity_livedispensing);
        setBasicToolBar("Live FuelDispensing");


        final SpeedView speedometer = (SpeedView) findViewById(R.id.speedView);

// move to 50 Km/s
//        try {
//            handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//
//                    // showToast(newceck);
//
//                    for (int i = 0 ; i<=5000;i++){
//                        speedometer.speedTo(i);
//                    }
//
//                   // hideProgress();
//                    //Do something after 20 seconds
//                    handler.postDelayed(this, 10);
//                }
//            }, 20);  //the time is in miliseconds
//
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }


       // speedometer.setLowSpeedPercent(75);
        speedometer.setMinMaxSpeed(0,5000);
       speedometer.setLowSpeedPercent(25);
       speedometer.setMediumSpeedPercent(50);

       speedometer.speedTo(5000, 10000);
        //speedometer.setAnimation(animation_entry_right);
//        speedometer.setStartDegree(180);
//        speedometer.setEndDegree(270);
        speedometer.setUnit("Litres");

    }
    }
