package com.wemonde.fuelbuddy.ui;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.TimePickerFragment;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;

/**
 * Created by Devendra Pandey on 12/2/2017.
 */

public class SchedulingActivity extends BaseActivity implements TimePickerFragment.TimeDialogListener{
    ArrayList<HashMap<String,String>> list = new ArrayList<HashMap<String,String>>();
    private SimpleAdapter sa;

    private TextView dateofschedule;
    private TextView outputtime;
    private TextView cost;
    private Button time;
    private int mYear, mMonth, mDay, mHour, mMinute;
    TimePickerDialog timePickerDialog;
    private static final String DIALOG_TIME = "SchedulingActivity.TimeDialog";
    private Button done;
    private static Date date_of_schedule;
    private static String time_of_schedule= null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.scheduleprofile);
        setBasicToolBar("Schedule Order");



// textView is the TextView view that should display it


        dateofschedule = (TextView)findViewById(R.id.textviewschedule);
        cost = (TextView)findViewById(R.id.cost);
        time = (Button)findViewById(R.id.btnClick) ;
        outputtime = (TextView)findViewById(R.id.output);
        done = (Button)findViewById(R.id.btn_scheduling_done);
        Date d=new Date();
        SimpleDateFormat sdf=new SimpleDateFormat("hh:mm a");
        String currentDateTimeString = sdf.format(d);
        outputtime.setText(currentDateTimeString);

        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

/** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .build();


       // cost.setText();
        dateofschedule.setText("Delivery on  " +String.format("%1$tY %1$tb %1$td", startDate) + "by...");

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {
                dateofschedule.setText("Delivery on  " + String.format("%1$td  %1$tb , %1$tY", date)+ " by...");

                date_of_schedule = date;

            }

            @Override
            public void onCalendarScroll(HorizontalCalendarView calendarView,
                                         int dx, int dy) {

            }

            @Override
            public boolean onDateLongClicked(Date date, int position) {
                return true;
            }
        });

//        HashMap<String,String> item;
//        for(int i=0;i<StatesAndCapitals.length;i++){
//            item = new HashMap<String,String>();
//            item.put( "line1", StatesAndCapitals[i][0]);
//            item.put( "line2", StatesAndCapitals[i][1]);
//            item.put( "line3", StatesAndCapitals[i][2]);
//            list.add( item );
//        }
//        sa = new SimpleAdapter(this, list,
//                R.layout.list_single_check,
//                new String[] { "line1","line2","line3" },
//                new int[] {R.id.title, R.id.artist,R.id.cost});
//        ((ListView)findViewById(R.id.list)).setAdapter(sa);


        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                gettime();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gettheschedule();
            }
        });


    }

    private String prepareJson() {

        Date d = new Date();
        String returnnew = null;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyy / MM / dd ");
        String strDate = String.format("%1$td  %1$tb , %1$tY", d);
        ;
        if (date_of_schedule.equals(strDate)) {


            SimpleDateFormat sdf=new SimpleDateFormat("HH:mm a");
            String currentDateTimeString = sdf.format(d);
            String pattern = "HH:mm";

            try {
                Date date1 = sdf.parse("19:28");
                Date date2 = sdf.parse("21:13");


                // Outputs -1 as date1 is before date2
                if (time_of_schedule.compareTo(currentDateTimeString) == -1)
                {

                    returnnew= "";
                }else {
                    returnnew= "yes";
                }

                // Outputs 1 as date1 is after date1


            } catch (ParseException e) {
                // Exception handling goes here
            }

//            getGpsLoc();

        }


        return returnnew;
    }

    private void gettheschedule() {


        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, 0);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.FRANCE);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
     //   SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.FRANCE);


        Date date = new Date();


        String strDate = dateFormat.format(date);
        if (strDate.equals(dateFormat.format(date_of_schedule))) {

            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentDateTimeString = sdf.format(date);
            String pattern = "HH:mm";

            try {


                // Outputs -1 as date1 is before date2
                if (time_of_schedule.compareTo(currentDateTimeString) < 0) {

                    showToast("Please select appropriate time");
                    // returnnew= "";
                } else {

                    String final_schedule = String.valueOf(dateFormat.format(date_of_schedule))+"T"+time_of_schedule;
                    // showToast(final_schedule);

                    SharedPrefrenceHelper.setscheduledate(bContext, final_schedule);
                  //  showToast(final_schedule);

                    finish();
                  //  showToast("correct time");
                    //  returnnew= "yes";
                }
                //showToast("good going");
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            String final_schedule = String.valueOf(dateFormat.format(date_of_schedule))+"T"+time_of_schedule;
            // showToast(final_schedule);

            SharedPrefrenceHelper.setscheduledate(bContext, final_schedule);
           // showToast(final_schedule);

            finish();

        }



        //2017-06-15








       // SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);

    }

    private String[][] StatesAndCapitals =
            {{"4:30 PM","Request by 10 AM","FREE"},
                    {"12:00 PM","Request by 10 AM",""+RSymbol+" 150"},
                    {"4:30 PM","Request by 1 PM",""+RSymbol+" 150"},
                    {"New one hour rush delivery ","Request by 2:30 PM"," "+RSymbol+ " 500" },

                   };


    private void gettime(){

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SchedulingActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {



                SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
                SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm:ss");
                try {
                    String time24 = outFormat.format(inFormat.parse(updateTime(selectedHour,selectedMinute)));
                    time_of_schedule = time24;
                    // showToast("Time  "+ time24);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                outputtime.setText( updateTime(selectedHour,selectedMinute));


            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();


        // Get Current Time
//        TimePickerFragment dialog = new TimePickerFragment();
//        dialog.show(getSupportFragmentManager(), DIALOG_TIME);







    }


    @Override
    public void onFinishDialog(String time) {


        SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm:ss");
        try {
            String time24 = outFormat.format(inFormat.parse(time));
            time_of_schedule = time24;
           // showToast("Time  "+ time24);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        outputtime.setText(time);

    }


    private String updateTime(int hours, int mins) {

        String timeSet = "";
        if (hours > 12) {
            hours -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hours += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String minutes = "";
        if (mins < 10)
            minutes = "0" + mins;
        else
            minutes = String.valueOf(mins);

        String myTime = new StringBuilder().append(hours).append(':')
                .append(minutes).append(" ").append(timeSet).toString();

        return myTime;
    }
}
