package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.BuildVerRecAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.models.State;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.wemonde.fuelbuddy.base.Apis.URL_GET_BUILDS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_BUILDS_ALL;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ALL_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;

public class MyBuildingActivity extends BaseActivity {

    private RecyclerView rv_build_ver;
    private ArrayList<Building> builds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_my_building);

        //changing the color programmatically





        setBasicToolBar("My Buildings");

        toolbar.setBackgroundColor(Color.parseColor("#45b879"));



        rv_build_ver = (RecyclerView) findViewById(R.id.rv_build_ver);
        rv_build_ver.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_build_ver.setNestedScrollingEnabled(false);
        rv_build_ver.setHasFixedSize(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPMBuildings();
    }

    private void getPMBuildings() {
        String url = URL_GET_BUILDS_ALL.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_ALL_BUILDING);
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
//        super.onResponse(request_code, response, data);
        Gson gson = new Gson();
        switch (request_code) {
            case REQUEST_GET_ALL_BUILDING:
                try {
                    Type type = new TypeToken<ArrayList<Building>>() {}.getType();
                    builds = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
                    setBuildAdapter();
                } catch (Exception e) {
                    showToast("No Building found!");
                    e.printStackTrace();
                }
                break;

        }
    }

    @Override
    protected void onFailed(int request_code, VolleyError error) {
//        super.onFailed(request_code, error);
        switch (request_code) {
            case REQUEST_GET_ALL_BUILDING:
                showToast("No Building Found!");
               // finish();
                break;
        }
    }

    private void setBuildAdapter() {
        if (builds == null) {
            showToast("No Building Found!");
           // finish();
        }

        BuildVerRecAdapter mAdapter = new BuildVerRecAdapter(bContext, builds);
        rv_build_ver.setAdapter(mAdapter);

    }

    public void clickAddBuilding(View v) {
        gotoAddBuilding();
    }
}