package com.wemonde.fuelbuddy.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.ChangePasswordRequast;
import com.wemonde.fuelbuddy.models.NewUserRegistration;
import com.wemonde.fuelbuddy.utils.Slog;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_NEW_USER_REGISTRATION;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_RESET_PASSWORD;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_NEW_USER_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_RESET_PASSWORD_CHANGE;

/**
 * Created by Devendra Pandey on 1/4/2018.
 */

public class UserRegistrationActivity extends BaseActivity {

    private EditText et_corporate_name;
    private EditText et_email_id;
    private EditText et_user_name;
    private EditText et_password;
    private EditText et_phonenumber;
    private Button et_new_user_register;
    private static String new_corporate = null;
    public static Context myContext;

    public static Activity usa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_new_user_registration);

        usa = this;


        initViews();


        //  trustEveryone();


    }

    private void initViews() {

        et_corporate_name = (EditText) findViewById(R.id.et_corporate_name);
        et_email_id = (EditText) findViewById(R.id.et_email_id);
        et_phonenumber = (EditText) findViewById(R.id.et_phone_number);
        et_user_name = (EditText) findViewById(R.id.et_user_name);
        et_password = (EditText) findViewById(R.id.et_user_password);
        et_new_user_register = (Button) findViewById(R.id.btn_register_new_user);


        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            String j =(String) b.get("Mobno");
            et_phonenumber.setText(j);
        }


        et_new_user_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkfornull();
            }
        });

        et_phonenumber.setEnabled(false);


        BroadcastReceiver broadcast_reciever = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                String action = intent.getAction();
                if (action.equals("finish_activity")) {
                    showToast("User Registered Sucessfully !");
                    finish();
                    // DO WHATEVER YOU WANT.
                }
            }
        };
        registerReceiver(broadcast_reciever, new IntentFilter("finish_activity"));
    }

    private void checkfornull() {
        new_corporate = et_corporate_name.getText().toString().trim();
        String ed_text_email_id = et_email_id.getText().toString().trim();
        String ed_phone_number = et_phonenumber.getText().toString().trim();
        String ed_user_name = et_user_name.getText().toString().trim();
        String ed_password = et_password.getText().toString().trim();

        //ed_text_email_id.isEmpty() || ed_text_email_id.length() == 0 || ed_text_email_id.equals("") || ed_text_email_id == null

        if (ed_text_email_id.isEmpty() || ed_text_email_id.length() == 0 || ed_text_email_id.equals("") || ed_text_email_id == null) {
            showToast("Please enter Email id");
            return;
        } else if (ed_phone_number.isEmpty() || ed_phone_number.length() == 0 || ed_phone_number.equals("") || ed_phone_number == null || ed_phone_number.length() <= 9) {
            showToast("Enter valid mobile number");
            return;
        } else if (ed_user_name.isEmpty() || ed_user_name.length() == 0 || ed_user_name.equals("") || ed_user_name == null) {
            showToast("Please enter User name");
            return;
        } else if (ed_password.isEmpty() || ed_password.length() == 0 || ed_password.equals("") || ed_password == null) {
            showToast("Please enter Password");
            return;
        }
//        else if (isValidMobile(ed_phone_number)== false){
//            showToast("Enter valid mobile number");
//            return;
//        }else if (isValidMail(ed_text_email_id)== false){
//            showToast("Enter valid Email ID");
//            return;
//        }


        else {
            String json = preparejson();
            if (json.length() <= 0) return;
            String url = URL_FOR_NEW_USER_REGISTRATION;
            // SharedPrefrenceHelper.setUpass(context,newpssword);
            volleyBodyHit(url, json, REQUEST_NEW_USER_REGISTRATION);



        }
    }

    public void goback() {
        try {
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //  this.overridePendingTransition(R.anim.stay_still, R.anim.animation_exit_right);

    }

    public static void finishnew() {
        try {
            //  (UserRegistrationActivity)myContext).finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Intent i = getIntent();
        String easyPuzzle = i.getStringExtra("finish");
        if (easyPuzzle.equals("finish_activity")) {
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

//         intent.getBundleExtra("finish_activity");
//
//        String easyPuzzle = i.getStringExtra("finish");
//        if (easyPuzzle.equals("finish_activity")){
//            finish();
//        }

        // setIntent(intent);
    }


    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
            et_email_id.setError("Not Valid Email");
        }
        return check;
    }


    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                et_phonenumber.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }


    private String preparejson() {
        try {


            if (new_corporate.isEmpty() || new_corporate.length() == 0 || new_corporate.equals("") || new_corporate == null) {

                new_corporate = "Company" + "_" + et_user_name.getText().toString().trim();
//            showToast("Please enter corporate name");
//            return;


            } else {
                new_corporate = et_corporate_name.getText().toString().trim();
            }


            NewUserRegistration avo = new NewUserRegistration();
            avo.setCompanyName(new_corporate);

            avo.setEmail(et_email_id.getText().toString().trim());

            avo.setPhoneNo(et_phonenumber.getText().toString().trim());
            avo.setUserName(et_user_name.getText().toString().trim());
            avo.setUserPassword(et_password.getText().toString().trim());


            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        if (request_code == REQUEST_NEW_USER_REGISTRATION) {
            try {



                shownewdialog ("User registered sucessfully, you can login with your user name password provided ");


//            Intent intent = new Intent("finish_activity");
//            sendBroadcast(intent);


            } catch (Exception e) {

            }


        }





    }

    public void shownewdialog (String msg){


        final PrettyDialog dialog = new PrettyDialog(this);
        dialog
                .setTitle("FuelBuddy ")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)

                .addButton(
                        "Okay",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {

                                // onResume();
                                // Dismiss

                                finish();
                                dialog.dismiss();
                                // Do what you gotta do
                            }
                        }
                )
                .show();

    }
}
