package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
//import  org.apache.commons.lang3.text.WordUtils;

import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.Environment;
import com.citrus.sdk.logger.CitrusLogger;
import com.citrus.sdk.login.CitrusAccount;
import com.citrus.sdk.response.CitrusError;
import com.citruspay.citrusbrowser.core.CitrusBrowserConfig;
import com.citruspay.sdkui.ui.utils.CitrusFlowManager;
import com.citruspay.sdkui.ui.utils.PPConfig;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.BookOrderRequestVoDynamicAddress;
import com.wemonde.fuelbuddy.models.BookingVo;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.payment.AppEnvironment;
import com.wemonde.fuelbuddy.payment.AppPreference;
import com.wemonde.fuelbuddy.payment.MainActivity;
import com.wemonde.fuelbuddy.utils.JsonUtils;
import com.wemonde.fuelbuddy.utils.Slog;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.commons.lang3.*;
import static com.wemonde.fuelbuddy.base.Apis.URL_BOOK_ORDER;
import static com.wemonde.fuelbuddy.payment.MainActivity.returnUrlLoadMoney;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_BOOK_ORDER;

public class OrderSummaryActivity extends BaseActivity   {

    private TextView tv_amount_top;
    private TextView tv_delivery_to;
    private TextView tv_est_time;
 //   private TextView tv_reg_no;
 //   private TextView tv_asset_type;
    private TextView tv_asset_detail;
    private TextView tv_amount_total;
    private TextView tv_calc;
    private TextView tv_fee;
    private TextView tv_amount;
    private TextView tv_fuel_quantity;
    private TextView tv_fuel_price;
    private TextView tv_fuel_type;
    private Button make_payment;
    private BookOrderRequestVo borv;
    private BookOrderRequestVoDynamicAddress borv1;
    private static  String total_amount;
    private AppEnvironment appEnvironment;
    private AppPreference app_stored_pref;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_order_summary);

        app_stored_pref = new AppPreference();
        //changing the color programmatically
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0097a7")));
        setBasicToolBar("Order Confirmation");
        toolbar.setBackgroundColor(Color.parseColor("#45b879"));
        if (HomeActivity.flagfordynamicbooking == 0){
            borv = (BookOrderRequestVo) getIntent().getSerializableExtra("borv");
        }else if (HomeActivity.flagfordynamicbooking == 1){
            borv = (BookOrderRequestVo) getIntent().getSerializableExtra("borv");
           // borv1 = (BookOrderRequestVoDynamicAddress) getIntent().getSerializableExtra("borv");
        }
       // borv = (BookOrderRequestVo) getIntent().getSerializableExtra("borv");
       // borv1 = (BookOrderRequestVoDynamicAddress) getIntent().getSerializableExtra("borv");
        initViews();
        confirmBooking();
    }

    private void initViews(){
        tv_amount_top = (TextView)findViewById(R.id.tv_amount_top);
        tv_delivery_to = (TextView)findViewById(R.id.tv_delivery_to);
        tv_est_time = (TextView)findViewById(R.id.tv_est_time);
       // tv_reg_no = (TextView)findViewById(R.id.tv_reg_no);
        tv_asset_detail = (TextView)findViewById(R.id.tv_asset_detail);
       // tv_asset_type = (TextView)findViewById(R.id.tv_asset_type);
        tv_amount_total = (TextView)findViewById(R.id.tv_amount_total);
        tv_fee = (TextView)findViewById(R.id.tv_fee);
        tv_amount = (TextView)findViewById(R.id.tv_amount);
        tv_calc = (TextView)findViewById(R.id.tv_calc);
        tv_fuel_quantity = (TextView)findViewById(R.id.tv_fuel_quantity);
        tv_fuel_price = (TextView)findViewById(R.id.tv_fuel_price);
        tv_fuel_type = (TextView)findViewById(R.id.tv_fuel_type);
        make_payment=(Button)findViewById(R.id.make_new_payment);





        LoginVo lvo = getLoginVo();
        int corp = lvo.getCorpID();



             if (  corp == 2272 ) {
                 make_payment.setVisibility(View.GONE);

             }else {
                 makenewpayment ();

                 make_payment.setOnClickListener(new View.OnClickListener() {
                     @Override
                     public void onClick(View v) {
                         startShoppingFlow();


                     }
                 });
             }


    }

    private void startShoppingFlow() {
        CitrusClient.getInstance(this).isUserSignedIn(new Callback<Boolean>() {
            @Override
            public void success(Boolean success) {
                if (success) {
                    initQuickPayFlow();
//                    if (isDifferentUser(userMobile)) {
//                        showUserLoggedInDialog(true);
//                    } else {
//                        initQuickPayFlow();
//                    }
                } else {
                    initQuickPayFlow();
                }
            }

            @Override
            public void error(CitrusError error) {

            }
        });

    }

    private void initQuickPayFlow() {
        //amount


            CitrusFlowManager.startShoppingFlowStyle(OrderSummaryActivity.this,
                    "adnan.kidwai@fuelbuddy.in", "9654000101",
                    "1",
                    R.style.AppTheme_Green, false);

    }


    private void confirmBooking(){

        String json = null;
        Gson gson = new Gson();
        if (HomeActivity.flagfordynamicbooking == 0){
             json = gson.toJson(borv);
        }else if (HomeActivity.flagfordynamicbooking == 1){
            json = gson.toJson(borv);
           //  json = gson.toJson(borv1);
        }

        String url = URL_BOOK_ORDER;
        volleyBodyHit(url, json, REQUEST_BOOK_ORDER);

    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);
        Gson gson = new Gson() ;

        try {
            JSONArray assetArr = JsonUtils.getJsonArrayFromJSON(new JSONObject(data), "assets");
//            for (int i = 0; i < assetArr.length(); i++) {
                JSONObject assetObj = assetArr.getJSONObject(0);
                BookingVo bookingVo = gson.fromJson(assetObj.toString(), BookingVo.class);
//            }

            setData(bookingVo);
        }catch (Exception e){

            Slog.e("exception in parsing" + e.getMessage());
            e.printStackTrace();
        }
    }


    public void makenewpayment () {



//        {
//            "access_key":"YEWUL3N11FPVDNO7QZAC",
//                "merchantClientId":"jt2iu8vpi9-sdk-merchant",
//                "merchantClientSecret":"49e0fce69a1046717a3fdde21a286459",
//                "consumerClientId":"jt2iu8vpi9-sdk-consumer",
//                "consumerClientSecret":"2117e89430871930465d48e467292e17",
//                "vanity":"fuelbuddy"
//        }hange the environment to PRODUCTION while going live.
//





//        First Parameter – SignUp Key (merchantClientId)
//                Second Parameter – SignUp Secret (merchantClientSecret)
//                Third Parameter – SignIn Key (consumerClientId)
//                Fourth Parameter – SignIn Secret  (consumerClientSecret)
//                Fifth Parameter – Vanity
//        Sixth Parameter – Environment. (SANDBOX or PRODUCTION).

        https://www.fuelbuddyapp.com/payment/GenerateBill.aspx?UserID=1&OrderItemID=1&
//
//        "access_key":"YEWUL3N11FPVDNO7QZAC",
//                "merchantClientId":"jt2iu8vpi9-sdk-merchant",
//                "merchantClientSecret":"49e0fce69a1046717a3fdde21a286459",
//                "consumerClientId":"jt2iu8vpi9-sdk-consumer",
//                "consumerClientSecret":"2117e89430871930465d48e467292e17",
//                "vanity":"fuelbuddy"



        CitrusFlowManager.initCitrusConfig("jt2iu8vpi9-sdk-merchant",
               "49e0fce69a1046717a3fdde21a286459", "jt2iu8vpi9-sdk-consumer",
                "2117e89430871930465d48e467292e17", ContextCompat.getColor(this, R.color.white),
                //https://www.fuelbuddyapp.com/payment/GenerateBill.aspx?UserID=1&OrderItemID=123
                OrderSummaryActivity.this, Environment.SANDBOX, "fuelbuddy"," https://www.fuelbuddyapp.com/payment/GenerateBill.aspx?UserID=1&OrderItemID=123",
                "https://www.fuelbuddyapp.com/payment/returnData.aspx");

        //To Set the Log Level of Core SDK & Plug & Play
        PPConfig.getInstance().setLogLevel(this, CitrusLogger.LogLevel.DEBUG);

        PPConfig.getInstance().disableWallet(true);


        //To Set the User details
        CitrusUser.Address customAddress = new CitrusUser.Address("Street1", "Street2", "City", "State", "Country", "411045");
        PPConfig.getInstance().setUserDetails("Custom_FName", "Custom_LName", customAddress);




//        CitrusFlowManager.initCitrusConfig(appEnvironment.getSignUpId(),
//                appEnvironment.getSignUpSecret(), appEnvironment.getSignInId(),
//                appEnvironment.getSignInSecret(), ContextCompat.getColor(this, R.color.white),
//                OrderSummaryActivity.this, appEnvironment.getEnvironment(), appEnvironment.getVanity(), appEnvironment.getBillUrl(),
//                returnUrlLoadMoney);
    }

    public void confirmBookpay(View v) {

//        CitrusFlowManager.initCitrusConfig("test-signup",
//                "c78ec84e389814a05d3ae46546d16d2e", "test-signin",
//                "52f7e15efd4208cf5345dd554443fd99",
//                getResources().getColor(R.color.white),bContext,
//                Environment.SANDBOX, "jt2iu8vpi9","https://www.fuelbuddyapp.com/payment/GenerateBill.aspx" , "");

      //  gotoCardpaymenui(total_amount);

     //   CitrusFlowManager.startShoppingFlow(OrderSummaryActivity.this, "dpandey1989@gmail.com", "9911683358", "5",true);
    }

    private void setData(BookingVo bookingVo){
        total_amount= bookingVo.getItemValue();
        tv_amount_top        .setText(RSymbol + " "+ bookingVo.getItemValue());
        tv_delivery_to       .setText(""+ bookingVo.getAssetLocation());
        tv_est_time          .setText("NA");
        // tv_reg_no            .setText(""+ bookingVo.getAssetIdentifierID());
        //  tv_asset_type        .setText(""+ bookingVo.getAssetIdentifierName());
        //  WordUtils.capitalize("your string") ;
        tv_asset_detail      .setText(""+ WordUtils.capitalize (bookingVo.getAssetModel()) + "   " + WordUtils.capitalize ( bookingVo.getAssetS_ID()));
        tv_amount_total      .setText(RSymbol + " "+ bookingVo.getItemValue());
        tv_calc              .setText(""+ RSymbol+" "+ bookingVo.getFuelPrice() + " X "  + bookingVo.getQty() + " = " );
        tv_amount            .setText(RSymbol+" "+ bookingVo.getItemValue());
        tv_fuel_quantity     .setText(""+ bookingVo.getQty());
        tv_fuel_price        .setText( RSymbol + "" + bookingVo.getFuelPrice());
        tv_fuel_type         .setText(""+ bookingVo.getFuelName());
        tv_fee                 .setText( RSymbol + " " +"0.0");

    }
}