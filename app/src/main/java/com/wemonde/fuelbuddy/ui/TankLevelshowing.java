package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.gelitenight.waveview.library.WaveView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.adapters.WaveHelper;
import com.wemonde.fuelbuddy.adapters.WavehelperNew;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AssetIdentifier;
import com.wemonde.fuelbuddy.models.Assettankdetails;
import com.wemonde.fuelbuddy.models.Dashboard;
import com.wemonde.fuelbuddy.models.TankBuddyConsumption;
import com.wemonde.fuelbuddy.models.Tankbuddynew;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_DAILY_TANKCONSUMPTION;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_TANK_DATA;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TANK_BUDDY_CURRENT_CONSUMPTION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TANK_DETAILS;

/**
 * Created by Devendra Pandey on 1/30/2018.
 */



public class TankLevelshowing extends BaseActivity {

    private static String  message = null;
    private WavehelperNew mWaveHelper;
    private static String assetname;
    private static String assetid;
    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private static int fuldepth;
    private static double tankdepth;
    private static double   percentagethreshold;
    private static double realraeding;

    private TextView capacity;

    private TextView temperature,dailyconsumption,dailyfilling;
    private TextView reading,dailycon,dailyfillingdate,monthlyconsumption,monthlyfilling,monthlyconnew,monthlyfillingnew;

    private   Handler handler;
    private   Handler handler1;
    private ImageView backbtn;
    private  WaveView waveView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tank_level);
        //setBasicToolBar("TankBuddy");

        Toolbar androidtool = (Toolbar)findViewById(R.id.toolbaar) ;





      //  Bundle bundle = getIntent().getExtras();
      //  message = bundle.getString("borv");
      //  String msg = message;

       // setAssetSpinnernewtanklevel(msg);


       // gettankdetails();



        reading = (TextView)findViewById(R.id.progress_reading_new);
        temperature = (TextView)findViewById(R.id.progress_reading_temp_new);
        capacity = (TextView)findViewById(R.id.progress_reading_cap_new);
        backbtn = (ImageView)findViewById(R.id.backbtn);

        dailycon = (TextView)findViewById(R.id.progress_reading_daily_consumption) ;
        dailyfillingdate=(TextView)findViewById(R.id.progress_reading_daily_filling) ;

        dailyconsumption= (TextView)findViewById(R.id.progress_reading_daily_consumption_new);
        dailyfilling = (TextView)findViewById(R.id.progress_reading_daily_filling_new);

        monthlyconsumption= (TextView)findViewById(R.id.progress_reading_monthly_consumption);
        monthlyfilling = (TextView)findViewById(R.id.progress_reading_monthly_filling);

        monthlyconnew = (TextView)findViewById(R.id.progress_reading_monthly_consumption_new);

        monthlyfillingnew= (TextView)findViewById(R.id.progress_reading_monthly_filling_new);
        Calendar c = Calendar.getInstance();


        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());

        String[]monthName={"January","February","March", "April", "May", "June", "July",
                "August", "September", "October", "November",
                "December"};
        String month=monthName[c.get(Calendar.MONTH)];


        dailycon.setText("Today's Consumption" +"\n"+ formattedDate);
        dailyfillingdate.setText("Today's Filling" +"\n"+ formattedDate);

        monthlyconsumption.setText("Monthly Consumption"+"\n"+ month);
        monthlyfilling.setText("Monthly Filling"+"\n"+ month);

         waveView = (WaveView)findViewById(R.id.wave);


        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();


                try {

                    handler.removeCallbacksAndMessages(null);
                    handler1.removeCallbacksAndMessages(null);

                }catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }

    public void clickPlaceorder(View v){

        finish();

        try {

            handler.removeCallbacksAndMessages(null);
            handler1.removeCallbacksAndMessages(null);

        }catch (Exception e) {
            e.printStackTrace();
        }

        gotoPlaceOrder();
       // gotoPlaceOrdernew(assetname,assetid);

    }


    @Override
    protected void onResume() {
        super.onResume();


        try {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    gettankdetailsnew();



                    waveView.setWaveColor(
                            Color.parseColor("#ffffff"),
                            Color.parseColor("#0c2f33"));
                    mBorderColor = Color.parseColor("#0c2f33");
                    waveView.setBorder(5, mBorderColor);
                    waveView.setShapeType(WaveView.ShapeType.SQUARE);
                    mWaveHelper = new WavehelperNew(waveView);
                    mWaveHelper.start();


                    hideProgress();
                    //Do something after 20 seconds
                    handler.postDelayed(this, 10000);
                }
            }, 10);  //the time is in miliseconds


        }catch (Exception e){
            e.printStackTrace();
        }





        try {


            handler1 = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    getconsumptions();
                    //Do something after 100ms
                }
            }, 100);

        }catch (Exception e){
            e.printStackTrace();
        }

//        try {
//            handler1 = new Handler();
//            handler1.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    getconsumptions();
//
//                    hideProgress();
//                    //Do something after 20 seconds
//                    handler1.postDelayed(this, 100);
//                }
//            }, 20);  //the time is in miliseconds
//
//
//        }catch (Exception e){
//            e.printStackTrace();
//        }




    }


    private void getconsumptions(){

        String url = URL_FOR_DAILY_TANKCONSUMPTION.replace("<UID>","TB00001");
        volleyGetHit(url, REQUEST_TANK_BUDDY_CURRENT_CONSUMPTION);


    }



    public void placelevel(){
        LinearLayout layout = (LinearLayout)findViewById(R.id.actual_reading_new);
        int paddingPixel = 780;

        int a = (int) Math.round(realraeding);
        layout.setPadding(0,a*20,0,0);


        LinearLayout layoutthreshold = (LinearLayout)findViewById(R.id.threshhold_reading_new);
        int paddingPixelnew = 100;


//        int b = (int) Math.round(percentagethreshold);
//
//        layoutthreshold.setPadding(0,0,0,b);
//
//        showToast("  "+a);
    }

    private void setAssetSpinnernewtanklevel(String message) {

        // spn_asset.setSelection(1);
        String[] separated = message.split("@&");
        //  separated[0]; // this will contain "Fruit"

        assetname = separated[0].trim();





        ArrayList<String> assetLabels12 = new ArrayList<>();
        assetLabels12.add(WordUtils.capitalize (assetname));
       // SpinnerAdapter assetAdapter12 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels12);
        //spn_asset.setAdapter(assetAdapter12);
         assetid = String.valueOf(Integer.parseInt(separated[1].trim())) ;
//        showToast(separated[1].trim());

        //assetId = asset_id;
        // showToast(""+assetId);
        // selectedassetinarray =1;





    }


    public void tankdetailsshow (){



         fuldepth = tankdetails.get(0).getFuelDepth();
         percentagethreshold = tankdetails.get(0).getTankDepthPercentageThreshold();
         tankdepth = tankdetails.get(0).getTankDepth();



    //   showToast(""+ tankdepth);



        realraeding = (fuldepth/tankdepth)*100;


      //  placelevel();
      //    showToast(""+ realraeding);



    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();




        try {

            handler.removeCallbacksAndMessages(null);
            handler1.removeCallbacksAndMessages(null);

        }catch (Exception e) {
            e.printStackTrace();
        }

        finish();

    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        switch (request_code) {
            case REQUEST_TANK_DETAILS:
              //  setAssetAdapter();
                try {
                    if (newtankbuddy == null) {
                        newtankbuddy = new ArrayList<>();
                    }
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Tankbuddynew>>() {}.getType();

                    newtankbuddy = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
                    // JSONObject jobj = new JSONObject(data);
                   // newtankbuddy = gson.fromJson(data.toString(), type);



                    capacity.setText( " \n"+  newtankbuddy.get(0).getCapacity() + " litres");

                    reading.setText(  "\n"+newtankbuddy.get(0).getTank_depth() +  " litres");

                    temperature.setText( "\n"+ "TB00001");



                    realraeding =( Double.parseDouble(newtankbuddy.get(0).getTank_depth())/  Double.parseDouble(newtankbuddy.get(0).getCapacity()));

                    double roundOff = Math.round(realraeding * 10.0) / 10.0;

                    SharedPrefrenceHelper.setFbid(bContext,String.valueOf(roundOff));


                   // realraeding = 80;
                           // (fuldepth/tankdepth)*100;


                 //  placelevel();




                   // tankdetailsshow ();


                }catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }
                break;

            case REQUEST_TANK_BUDDY_CURRENT_CONSUMPTION:
                try {


                    Gson gson1 = new Gson();
                    Type type1 = new TypeToken<ArrayList<TankBuddyConsumption>>() {}.getType();
                    Type type2 = new TypeToken<ArrayList<TankBuddyConsumption>>() {}.getType();

                    JSONObject jsonObject = new JSONObject(data);

                    JSONObject getassetarray = jsonObject.getJSONObject("assets");

                    consumptiondaily = gson1.fromJson(new JSONObject(getassetarray.toString()).get("CurrentDate").toString(), type1);

                    consumptionmonthly = gson1.fromJson(new JSONObject(getassetarray.toString()).get("CurrentMonth").toString(), type2);
//this to uncomment
                    dailyfilling.setText(consumptiondaily.get(0).getColumn2() + " litres");
                    dailyconsumption.setText(consumptiondaily.get(0).getColumn1()+ " litres");


                    monthlyconnew.setText(consumptionmonthly.get(0).getColumn1() + " litres");
                    monthlyfillingnew.setText(consumptionmonthly.get(0).getColumn2()+ " litres");


  //uncomment


//                    Gson gson = new Gson();
//                    Type type = new TypeToken<ArrayList<TankBuddyConsumption>>() {}.getType();
//                    JSONObject jobj = new JSONObject(data);
//                    consumption = gson.fromJson(jobj.get("assets").toString(), type);
//
//
//                    dailyfilling.setText(consumption.get(0).getColumn2() + " litres");
//                    dailyconsumption.setText(consumption.get(0).getColumn1()+ " litres");
                    // setAssetSpinner();
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }
    }


}
