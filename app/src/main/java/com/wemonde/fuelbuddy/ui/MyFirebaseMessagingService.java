package com.wemonde.fuelbuddy.ui;

import android.app.*;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.internal.zzm;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.ui.HomeActivity;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;

import java.util.Set;

import me.leolin.shortcutbadger.ShortcutBadger;

import static android.R.attr.action;
import static com.google.firebase.messaging.RemoteMessage.*;
import static com.wemonde.fuelbuddy.adapters.OrderRecVerAdapter.orderid;

/**
 * Created by Devendra
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static String title = "";
    private static int badgeCount1 = 1;
    private static int badgeCount = 0;
    private static RemoteMessage.Notification newnot;
    private Context context;
    // private RemoteMessage.Notification ;
    private NotificationManager mNotificationManager;

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
//    @Override
//    public void zzm(Intent intent) {
//        Log.i("uniqbadge", "zzm");
//       // badgeCount= badgeCount+badgeCount1;
//
//        String numberofcount = String.valueOf(SharedPrefrenceHelper.getNotificationCount(BaseActivity.getInstance()));
//
//
//        badgeCount = Integer.parseInt(numberofcount);
//        badgeCount = badgeCount+badgeCount1;
//        String numberAsString = String.valueOf(badgeCount);
////        if (badgeCount == 1){
////
////            badgeCount = 1;
////        }else {
////            badgeCount++;
////        }
//
//
//
//
//
//        SharedPrefrenceHelper.setNotificationCount(BaseActivity.getInstance(), numberAsString);
//
//
//
//       // ShortcutBadger.applyNotification(getApplicationContext(), newnot, badgeCount);
//      //
//        Log.i("uniq", " badge count " + badgeCount);
//        ShortcutBadger.applyCount(this, badgeCount);
//         //notification;
//
//        Notification.Builder builder = new Notification.Builder(getApplicationContext())
//                .setContentTitle("")
//                .setContentText("")
//                .setSmallIcon(R.drawable.ic_aadhar);
//        Notification notification = builder.build();
//        ShortcutBadger.applyNotification(getApplicationContext(), notification, badgeCount);
//      //  mNotificationManager.notify(notificationId, notification);
//
//
//      //  ShortcutBadger.applyNotification(this, newnot, badgeCount);
//       // ShortcutBadger.with(getApplicationContext()).count(badgeCount); //for 1.1.3
//
////        ShortcutBadger.applyNotification(getApplicationContext(), notification, badgeCount);
//        Log.i("uniq", " " + "end");
//        Set<String> keys = intent.getExtras().keySet();
////        for (String key : keys) {
////            //gcm.notification.title
////            //badge
////            try {
////                Log.i("uniq", " " + key + " " + intent.getExtras().get(key));
////                if (key.equals("gcm.notification.title")) {
////                   // String cnt =   intent.getExtras().get(key).toString();
////                    //int badgeCount = Integer.valueOf(cnt);
////                    badgeCount++;
////                    Log.i("uniq", " badge count " + badgeCount);
////                    ShortcutBadger.applyCount(this, badgeCount);
////                    Log.i("uniq", " " + "end");
////                }
////            } catch (Exception e) {
////                Log.i("uniqbadge", "zzm Custom_FirebaseMessagingService" + e.getMessage());
////            }
////        }
//
//        super.zzm(intent);
//    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
         title = remoteMessage.getNotification().getTitle();

         if (title.equalsIgnoreCase("Order Completed")){
             String intValue = remoteMessage.getNotification().getBody().replaceAll("[^0-9]", ""); // returns 123
             orderid = Integer.parseInt(intValue) ;
             sendnewNotification(remoteMessage.getNotification().getBody());

         }else {
             sendNotification(remoteMessage.getNotification().getBody());
         }




        //Calling method to generate notification

    }

    //This method is only generating push notification
    //It is same as we did in earlier posts






        private void sendnewNotification(String messageBody) {
            if (messageBody.equalsIgnoreCase("you have been logged in from a new device")){

            }




            Intent intent = new Intent(this, OrderCompletionFinal.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_ONE_SHOT);





            int messageCount = 3;

//        badgeCount++;


            //  ShortcutBadger.applyCount(this, badgeCount1); //for 1.1.4+

            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher_app)
                    .setContentTitle(title)

                    .setContentText(messageBody)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    ;







//        NotificationCompat.InboxStyle inboxStyle =
//                new NotificationCompat.InboxStyle();
//
//        String[] events = new String[15];
//
//
//// Sets a title for the Inbox in expanded layout
//// Sets a title for the Inbox in expanded layout
//        inboxStyle.setBigContentTitle("FuelBuddy");
//        inboxStyle.setSummaryText(messageBody);
//
//// Moves events into the expanded layout
//        for (int i=0; i < events.length; i++) {
//            inboxStyle.addLine(events[i]);
//        }
//// Moves the expanded layout object into the notification object.
//        notificationBuilder.setStyle(inboxStyle);

            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));


            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



            notificationManager.notify(0, notificationBuilder.build());
        }




    private void sendNotification(String messageBody) {
        if (messageBody.equalsIgnoreCase("you have been logged in from a new device")){

        }

        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);





        int messageCount = 3;

//        badgeCount++;


      //  ShortcutBadger.applyCount(this, badgeCount1); //for 1.1.4+

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher_app)
                .setContentTitle(title)

                .setContentText(messageBody)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                ;







//        NotificationCompat.InboxStyle inboxStyle =
//                new NotificationCompat.InboxStyle();
//
//        String[] events = new String[15];
//
//
//// Sets a title for the Inbox in expanded layout
//// Sets a title for the Inbox in expanded layout
//        inboxStyle.setBigContentTitle("FuelBuddy");
//        inboxStyle.setSummaryText(messageBody);
//
//// Moves events into the expanded layout
//        for (int i=0; i < events.length; i++) {
//            inboxStyle.addLine(events[i]);
//        }
//// Moves the expanded layout object into the notification object.
//        notificationBuilder.setStyle(inboxStyle);

        notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);



        notificationManager.notify(0, notificationBuilder.build());
    }
}