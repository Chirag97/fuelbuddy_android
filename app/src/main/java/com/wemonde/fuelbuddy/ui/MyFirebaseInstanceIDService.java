package com.wemonde.fuelbuddy.ui;

/**
 * Created by Devendra on 8/11/2017.
 */

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import com.wemonde.fuelbuddy.base.BaseActivity;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;


//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        SharedPrefrenceHelper.setTOKEN(BaseActivity.getInstance(), refreshedToken.toString());

//        Handler handler = new Handler(Looper.getMainLooper());
//        handler.post(new Runnable() {
//
//            @Override
//            public void run() {
//                Toast.makeText(getApplicationContext(),
//                        "new toast "+ refreshedToken,
//                        Toast.LENGTH_LONG).show();
//            }
//        });


        //Toast.makeText(BaseActivity.getInstance(),"Refreshed Token : "+ refreshedToken,Toast.LENGTH_LONG ).show();
//        Toast.makeText(getApplicationContext(),
//                "Refreshed Token : "+ refreshedToken,
//                Toast.LENGTH_SHORT).show();


        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
    }
}