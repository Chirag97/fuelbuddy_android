package com.wemonde.fuelbuddy.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import com.github.mikephil.charting.animation.ChartAnimator;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.renderer.PieChartRenderer;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.utils.TransformerHorizontalBarChart;
import com.github.mikephil.charting.utils.Utils;

import com.wemonde.fuelbuddy.R;

/**
 * Created by Devendra Pandey on 12/23/2017.
 */

public class CustomPieChartRenderer extends PieChartRenderer {
    private Context context;

    public CustomPieChartRenderer(PieChart chart, ChartAnimator animator,
                                  ViewPortHandler viewPortHandler) {
        super(chart, animator, viewPortHandler);
        context = chart.getContext();
    }

    @Override
    public void drawExtras(Canvas c) {
        super.drawExtras(c);
        drawImage(c);
    }

    private void drawImage(Canvas c) {
     //   MPPointF  center = mChart.getCenterCircleBox();

        Drawable d =  ResourcesCompat.getDrawable(context.getResources(), R.mipmap.ic_launcher_app, null);

        if(d != null) {
            float halfWidth = d.getIntrinsicWidth() / 2;
            float halfHeight = d.getIntrinsicHeight() / 2;

            d.setBounds((int) (mChart.getCenterCircleBox().x - halfWidth), (int) (mChart.getCenterCircleBox().y - halfHeight), (int) (mChart.getCenterCircleBox().x + halfWidth), (int) (mChart.getCenterCircleBox().y + halfHeight) );
            d.draw(c);
        }
    }
}