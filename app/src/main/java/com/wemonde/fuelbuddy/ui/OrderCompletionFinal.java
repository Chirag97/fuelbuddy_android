package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.view.animation.BounceInterpolator;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.google.android.gms.maps.model.LatLng;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;

import static com.wemonde.fuelbuddy.adapters.OrderRecVerAdapter.quantity;

/**
 * Created by Devendra Pandey on 1/23/2018.
 */

public class OrderCompletionFinal extends BaseActivity {

    private static float density ;
    private static float temperature;
    private static double volumedespensed;
    private TextView denssity;
    private TextView temp;
    private TextView quantity;
    private SimpleRatingBar myRatingBar;
    private ImageView qualitycheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.ordercompletionfinal);
        setBasicToolBar("Order Summary ");

        myRatingBar = (SimpleRatingBar) findViewById(R.id.ratingView);
        denssity =(TextView)findViewById(R.id.density);
        temp = (TextView)findViewById(R.id.temperature_new);
        quantity = (TextView)findViewById(R.id.volume_dispensed);
        qualitycheck=(ImageView)findViewById(R.id.sppedometer);




        SimpleRatingBar.AnimationBuilder builder = myRatingBar.getAnimationBuilder()
                .setRatingTarget(0)
                .setDuration(2000)
                .setInterpolator(new BounceInterpolator());
        builder.start();
        myRatingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {

                // showToast("Star rating is "+ String.valueOf(rating));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            if (sensors.size() != 0) {

                sensors.size();

                // assetsdetails.size();
                // showToast("Size is" + assets.size());



                // volumedespensed = orderstatusdetails.get(0).getQty();
                //SharedPrefrenceHelper.getscheduledate(bContext);
                quantity.setText(" "+SharedPrefrenceHelper.getCompanyID(bContext)+"  "+ "litres" );

                for (int i = 0; i <= sensors.size(); i++) {
                    String sensorname = sensors.get(i).getSensorName();
                    if (sensorname.equals("Density"))
                    {

                        int indexsensor = sensors.get(i).getSensorIndex();
                        if (indexsensor == 0){
                            density =  sensors.get(i).getSensorValue();

                            denssity.setText(" "+String.valueOf(density) + "  "+ "g/cm³" );

                            if (density >= 0.68){
                                qualitycheck.setImageResource(R.drawable.speedometergreen);
                            }else if (density > 0.60 && density < 0.63){
                                qualitycheck.setImageResource(R.drawable.speedometeryellow);

                            }else {

                                qualitycheck.setImageResource(R.drawable.speedometerred);
                            }
                        }
                    }else if (sensorname.equals("Temperature")){
                        int indexsensor = sensors.get(i).getSensorIndex();
                        if (indexsensor == 0){
                            temperature =  sensors.get(i).getSensorValue();

                            temp.setText(" "+String.valueOf(temperature)+"  " +"°C");
                        }
                    }
                }




                // LatLng ll = new LatLng(assets.get(i).getLatitude(), assets.get(i).getLongitude());

            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
