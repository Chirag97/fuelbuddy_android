package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_OTP_REQUEST;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_VERIFY_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_OTP_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_RESET_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_VERIFY_OTP_REGISTRATION;

public class OtpRegistrationPhone extends BaseActivity implements OnPermissionCallback {

    private BroadcastReceiver receiver;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT = 100;
    private SharedPreferences permissionStatusreadsms;
    private boolean sentToSettings = false;
    private EditText mobilenumber;
    private EditText otp;
    private Button btn_sign_in;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        setContentView(R.layout.activity_newuser_registration);

        mobilenumber = (EditText)findViewById(R.id.mobile_number);
        btn_sign_in = (Button)findViewById(R.id.btn_signin);
        permissionStatusreadsms = getSharedPreferences("permissionStatussms",MODE_PRIVATE);
       // checkpermission();
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(getApplicationContext(),"Send OTP is "  , Toast.LENGTH_SHORT).show();
                gethit();

            }
        });

        receiver  = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                otp.setText(message);
                                showToast("Otp is valid for 60 sec only");

                            }catch (Exception e ){
                                e.printStackTrace();
                            }

                            //Do something after 100ms
                        }
                    }, 100);

                    //Do whatever you want with the code here
                }
            }
        };
    }

    private void checkpermission() {
        if (ActivityCompat.checkSelfPermission(OtpRegistrationPhone.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(OtpRegistrationPhone.this, Manifest.permission.READ_SMS)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(OtpRegistrationPhone.this);
                builder.setTitle("Need SMS Permission");
                builder.setMessage("This app needs SMS permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(OtpRegistrationPhone.this, new String[]{Manifest.permission.CAMERA}, EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatusreadsms.getBoolean(Manifest.permission.READ_SMS,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(OtpRegistrationPhone.this);
                builder.setTitle("Need Read SMS Permission");
                builder.setMessage("This app needs Read SMS permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant READ SMS Permission", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(OtpRegistrationPhone.this, new String[]{Manifest.permission.READ_SMS}, EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatusreadsms.edit();
            editor.putBoolean(Manifest.permission.READ_SMS,true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.
            // proceedAfterPermission();
        }
    }


    @Override
    public void onResume() {
        //trustEveryone();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();




    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }


    private void gethit(){

        String ed_text = mobilenumber.getText().toString().trim();


        if (ed_text.isEmpty() || ed_text.length() == 0 || ed_text.equals("") || ed_text == null ||ed_text.length() <= 9){
            showToast("Please enter phone number");
            return;


        }

        String url = URL_FOR_OTP_REGISTRATION.replace("<UID>",mobilenumber.getText().toString().trim());
        volleyGetHit(url, REQUEST_GET_OTP_REGISTRATION);


    }


    private void gethitotpverify(){

        String ed_text = mobilenumber.getText().toString().trim();
        String ed_otp = otp.getText().toString().trim();


        if (ed_text.isEmpty() || ed_text.length() == 0 || ed_text.equals("") || ed_text == null){
            showToast("Please enter phone number");
            return;


        }else  if(ed_otp.isEmpty() || ed_otp.length() == 0 || ed_otp.equals("") || ed_otp == null ||ed_otp.length() < 4 ) {
            showToast("Enter valid OTP");
            return;
        }

        String url = URL_FOR_VERIFY_OTP_REGISTRATION.replace("<UID>", mobilenumber.getText().toString().trim()).replace("<OTP>",otp.getText().toString().trim());
        volleyGetHit(url, REQUEST_VERIFY_OTP_REGISTRATION);


    }


    public  void otpdialog (){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.otp_layout_new);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        otp =  dialog.findViewById(R.id.otp_textbox);

        Button dialogBtn_cancel = dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gethit();
            }
        });

        Button dialogBtn_okay =  dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String imgSett = otp.getText().toString().trim();
                if(!imgSett.isEmpty()){
                    gethitotpverify();
                    gotoLogin();
                }else {
                    showToast("Please enter otp !");
                    return;
                }

                dialog.cancel();
            }
        });
        Button finishbutton = dialog.findViewById(R.id.btn_finish);
        finishbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



    @Override
    public void onPermissionGranted(String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }


    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        if(request_code == REQUEST_GET_OTP_REGISTRATION){
            otpdialog();

        }else if(request_code == REQUEST_VERIFY_OTP_REGISTRATION){

            gotoNewUserRegistration(mobilenumber.getText().toString().trim());
            finish();
        }
    }

}
