package com.wemonde.fuelbuddy.ui;

import android.content.Intent;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kyleduo.switchbutton.SwitchButton;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.wemonde.fuelbuddy.models.BookingVo;
import com.wemonde.fuelbuddy.models.Dashboard;
import com.wemonde.fuelbuddy.utils.Slog;
//import com.github.mikephil.charting.utils.Highlight;
//import com.github.mikephil.charting.utils.PercentFormatter;
import lecho.lib.hellocharts.view.PieChartView;

import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;

import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_GET_DASHBOARD_DETAILS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_Accepted;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_Declined;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_OPEN;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS__delivered;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DASHBOARD_DETAILS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ORDERS;

/**
 * Created by Devendra Pandey on 12/18/2017.
 */


public class Dashboardnewui extends BaseActivity {


    private PieChart mChart;
    private BarChart bChart;

    private static float[] yData;
    private static String[] xData;
    private CoordinatorLayout coordinatorLayout;
    //    private float[] yData = { 5, 10, 15, 30, 40 };
    // WordUtils.capitalize
//    private String[] xData = { "Genset 1", "Genset 2", "Office Gen", "Jackson", "Honda" };
    private SwitchButton openorder;

    private SwitchButton acceptedorder;
    private SwitchButton Completeorder;
    private SwitchButton Declinedorder;
    private Button btn;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.newdashboardui);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);


//        List<PieEntry> entries = new ArrayList<>();
//
//        entries.add(new PieEntry(18.5f, "Green"));
//        entries.add(new PieEntry(26.7f, "Yellow"));
//        entries.add(new PieEntry(24.0f, "Red"));
//        entries.add(new PieEntry(30.8f, "Blue"));
//
//        PieDataSet set = new PieDataSet(entries, "Election Results");
//        PieData data = new PieData(set);
//        pieChart.setData(data);
//        pieChart.invalidate(); // refresh

        setBasicToolBar("DashBoard ");

        //   getdashboardnew();


        getdata();


        mChart = new PieChart(this);
        bChart=new BarChart(this);

        mChart = (PieChart) findViewById(R.id.chart);
        bChart=findViewById(R.id.chart12);
        openorder = (SwitchButton) findViewById(R.id.sb_default_open);
        acceptedorder = (SwitchButton) findViewById(R.id.sb_default_Accepted);
        Completeorder = (SwitchButton) findViewById(R.id.sb_default_Complete);
        Declinedorder = (SwitchButton) findViewById(R.id.sb_default_Declined);
        btn=findViewById(R.id.paymenthistory);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoPaymentHistory();

            }
        });
        // add pie chart to main layout
//        mainLayout.addView(mChart);
//        mainLayout.setBackgroundColor(Color.parseColor("#55656C"));

        // configure pie chart
        mChart.setUsePercentValues(true);
        mChart.setDescription(" ");
        // enable hole and configure
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColorTransparent(true);
        mChart.setHoleRadius(15);
        mChart.setTransparentCircleRadius(20);

        mChart.setCenterTextColor(Color.BLACK);
//        mChart.setCenterTextSize(15f);
//        mChart.setCenterText("Asset Fuel ordered graph");

//        mChart.setRenderer(new CustomPieChartRenderer(
//                mChart,
//                mChart.getAnimator(),
//                mChart.getViewPortHandler()
//        ));

// When setting a new renderer, we need to invalidate the old one
        mChart.invalidate();


        // enable rotation of the chart by touch
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(true);


        // set a chart value selected listener
        mChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                // display msg when value selected
                if (e == null)
                    return;

                //WordUtils.capitalize

                // Math.round()
                showsnackbar(WordUtils.capitalize(xData[e.getXIndex()]) + " ordered  " + Math.round(e.getVal()) + " litres");


//                Toast.makeText(Dashboardnewui.this,
//                        xData[e.getXIndex()] + " = " + e.getVal() + " litres", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected() {

            }
        });



        // add data
        addData();



        if (openorder != null) {
            openorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    String url = URL_GET_ORDERS_OPEN.replace("<UID>", uid);

                    gotoOrderHistory("Open Orders", URL_GET_ORDERS_OPEN);
                    acceptedorder.setCheckedNoEvent(false);
                    Completeorder.setCheckedNoEvent(false);
                    Declinedorder.setCheckedNoEvent(false);
//                    mDefaultSb.setEnabled(isChecked);
//                    mSB.setEnabled(isChecked);
                }
                //  disableNoEventSb.setCheckedImmediatelyNoEvent(false);
            });
        }


        if (acceptedorder != null) {
            acceptedorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    String url = URL_GET_ORDERS_Accepted.replace("<UID>", uid);

                    gotoOrderHistory("Accepted Orders", URL_GET_ORDERS_Accepted);
                    openorder.setCheckedNoEvent(false);
                    Completeorder.setCheckedNoEvent(false);
                    Declinedorder.setCheckedNoEvent(false);

                    //   openStartupnew();
//                    mFlymeSb.setEnabled(isChecked);
//                    mMIUISb.setEnabled(isChecked);
//                    mCustomSb.setEnabled(isChecked);
//                    mDefaultSb.setEnabled(isChecked);
//                    mSB.setEnabled(isChecked);
                }
                //  disableNoEventSb.setCheckedImmediatelyNoEvent(false);
            });
        }

        if (Completeorder != null) {
            Completeorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    String url = URL_GET_ORDERS__delivered.replace("<UID>", uid);

                    gotoOrderHistory("Completed Orders", URL_GET_ORDERS__delivered);
                    openorder.setCheckedNoEvent(false);
                    acceptedorder.setCheckedNoEvent(false);
                    Declinedorder.setCheckedNoEvent(false);
//                    mDefaultSb.setEnabled(isChecked);
//                    mSB.setEnabled(isChecked);
                }
                //  disableNoEventSb.setCheckedImmediatelyNoEvent(false);
            });
        }


        if (Declinedorder != null) {
            Declinedorder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    String url = URL_GET_ORDERS_Declined.replace("<UID>", uid);

                    gotoOrderHistory("Declined Orders", URL_GET_ORDERS_Declined);
                    openorder.setCheckedNoEvent(false);
                    Completeorder.setCheckedNoEvent(false);
                    acceptedorder.setCheckedNoEvent(false);
//                    mDefaultSb.setEnabled(isChecked);
//                    mSB.setEnabled(isChecked);
                }
                //  disableNoEventSb.setCheckedImmediatelyNoEvent(false);
            });
        }

        // customize legends
//        Legend l = mChart.getLegend();
//        l.setPosition(LegendPosition.RIGHT_OF_CHART);
//        l.setXEntrySpace(7);
//        l.setYEntrySpace(5);
    }


//    public void  gotoOrderHistorynew () {
//
//        Intent intent = new Intent(this, OrderListActivity.class);
////        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//    };


    @Override
    protected void onResume() {
        super.onResume();
        //  openorder.setChecked(false);
    }


    protected void getdata() {
        try {

            if (dashboarddetails.size() != 0) {

                dashboarddetails.size();

                // Float yData[];
                ArrayList<Float> yEntries = new ArrayList<>();
                ArrayList<String> xEntries = new ArrayList<>();

                try {


                    for (int i = 0; i < dashboarddetails.size(); i++) {
                        float f1 = Float.parseFloat(dashboarddetails.get(i).getQty());

                        yEntries.add(i, f1);
                        // yData.add(f1);

                    }
                    for (int i = 0; i < dashboarddetails.size(); i++) {
                        String assetname = dashboarddetails.get(i).getAssetS_ID();

                        xEntries.add(i, assetname);
                        // xData[i]=assetname;
                        //xEntries.add( new String(dashboarddetails.get(i).getAssetS_ID(), i));
                    }

                    //   yData = new F[mStringList.size()];


                    yData = new float[yEntries.size()];
                    int i = 0;

                    for (Float f : yEntries) {
                        yData[i++] = (f != null ? f : Float.NaN); // Or whatever default you want.
                    }


                    xData = xEntries.toArray(new String[0]);


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //        chart = (PieChartView).findViewById(R.id.chart);
//        chart.setOnValueTouchListener(new ValueTouchListener());


    private void addData() {
        try {

            //My side

            ArrayList<BarEntry> yvals1= new ArrayList<>();

            for (int i = 0; i < yData.length; i++)
                yvals1.add(new BarEntry(yData[i], i));

            ArrayList<String> xvals = new ArrayList<String>();

            for (int i = 0; i < xData.length; i++)
                xvals.add(WordUtils.capitalize(xData[i]));

            BarDataSet barDataSet= new BarDataSet(yvals1,"");
            BarData barData= new BarData(xvals,barDataSet);
            barDataSet.setColor(R.color.colorPrimary);
            barData.setGroupSpace(3);
            barData.setValueFormatter(new Valueformatter());
            barData.setValueTextColor(R.color.black);
            barData.setValueTextSize(11f);

            barDataSet.setColor(getResources().getColor(R.color.colorPrimary));
            barDataSet.setBarShadowColor(getResources().getColor(R.color.colorPrimary));
            bChart.setData(barData);
            bChart.setScaleEnabled(true);
            bChart.setDragEnabled(true);
            bChart.setDescription("");
            bChart.setDrawingCacheBackgroundColor(getResources().getColor(R.color.colorPrimary));
            bChart.setBorderColor(getResources().getColor(R.color.colorPrimary));


//            bChart.setBackgroundColor(getResources().getColor(R.color.colorPrimary));



            // undo all highlights
            bChart.highlightValues(null);
            // update pie chart
            bChart.invalidate();

            ArrayList<Entry> yVals1 = new ArrayList<Entry>();
            //  ArrayList entries = new ArrayList();
            //  entries.add(new PieEntry(valueFloat, String.valueOf(valueFloat).concat(" units")));

            for (int i = 0; i < yData.length; i++)
                yVals1.add(new Entry(yData[i], i));

            ArrayList<String> xVals = new ArrayList<String>();

            for (int i = 0; i < xData.length; i++)
                xVals.add(WordUtils.capitalize(xData[i]));


            // create pie data set
            PieDataSet dataSet = new PieDataSet(yVals1, "");
            dataSet.setSliceSpace(3);
            dataSet.setSelectionShift(10);






            final int[] MY_COLORS = {Color.rgb(54, 179, 222), Color.rgb(198, 236, 183), Color.rgb(255, 192, 0),
                    Color.rgb(127, 127, 127), Color.rgb(146, 208, 80), Color.rgb(0, 176, 80), Color.rgb(79, 129, 189)};
            ArrayList<Integer> colors = new ArrayList<Integer>();

            for (int c : MY_COLORS) colors.add(c);

            dataSet.setColors(colors);


//            dataSet.setValueLinePart1Length(1f);
//            dataSet.setValueLinePart2Length(.2f);
//            dataSet.setValueTextColor(Color.BLACK);
//            dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//
            // add many colors
//        ArrayList<Integer> colors = new ArrayList<Integer>();
//
//
//        for (int c : ColorTemplate.VORDIPLOM_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.JOYFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.COLORFUL_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.LIBERTY_COLORS)
//            colors.add(c);
//
//        for (int c : ColorTemplate.PASTEL_COLORS)
//            colors.add(c);
//
//        //colors.add(ColorTemplate.getHoloBlue());
//        dataSet.setColors(colors);

            // instantiate pie data object now
            PieData data = new PieData(xVals, dataSet);
//        data.a



            data.setValueFormatter(new PercentFormatter());
            data.setValueTextSize(11f);
            data.setValueTextColor(Color.BLACK);


            mChart.setData(data);


            // undo all highlights
            mChart.highlightValues(null);

            // update pie chart
            mChart.invalidate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getdashboardnew() {
        String url = URL_FOR_GET_DASHBOARD_DETAILS.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_DASHBOARD_DETAILS);
    }


    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        if (request_code == REQUEST_GET_DASHBOARD_DETAILS) {

            try {
                Gson gson = new Gson();

                //  Tracking track = gson.fromJson(data, Tracking.class);
                //assets = assetVo.getAssets();

                Type type = new TypeToken<ArrayList<Dashboard>>() {
                }.getType();


                JSONObject jsonObject = new JSONObject(data);
//                JSONArray events = jsonObject.getJSONArray("assets");
//                String jsonString = events.toString();


                JSONObject getassetarray = jsonObject.getJSONObject("assets");


                // String innerJson = gson.toJson(new JSONObject(data).get("assets"));


                dashboarddetails = gson.fromJson(new JSONObject(getassetarray.toString()).get("Table").toString(), type);

                // gotoTracking();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showsnackbar(String msg) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);

        snackbar.show();
    }
}
