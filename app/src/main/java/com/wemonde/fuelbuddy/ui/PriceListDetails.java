package com.wemonde.fuelbuddy.ui;


import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.ConstantClass;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.AssetVerRecAdapter;
import com.wemonde.fuelbuddy.adapters.Movie;
import com.wemonde.fuelbuddy.adapters.MoviesAdapter;
import com.wemonde.fuelbuddy.adapters.PriceListAdapter;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.DistrictArray;
import com.wemonde.fuelbuddy.models.State;
import com.wemonde.fuelbuddy.utils.Slog;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_GET_DISTRICT_BY_STATE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DISTRICT;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;

public class PriceListDetails extends BaseActivity {


    private MoviesAdapter mAdapter;
    private Button Done;
    private RecyclerView pricerecyler;
    private Spinner listofprice;
    private LinearLayout  header;
    private LinearLayout dateon;
    private TextView dated;

    private int stateId;
    private static List<com.wemonde.fuelbuddy.adapters.Movie> movieList;

    private static Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setscreenLayout(R.layout.activity_layout_price_details);
        setBasicToolBar("Price Details");


        Done = (Button) findViewById(R.id.btn_order_now);
        pricerecyler = (RecyclerView) findViewById(R.id.recycler_view_new);
        listofprice = (Spinner) findViewById(R.id.spn_product);
        header = (LinearLayout)findViewById(R.id.header_layout);
        dateon = (LinearLayout)findViewById(R.id.dateon);
        dated = (TextView)findViewById(R.id.pricedate);


      //  movie = new Movie();


       // mAdapter = new MoviesAdapter(movieList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        pricerecyler.setLayoutManager(mLayoutManager);
        pricerecyler.setItemAnimator(new DefaultItemAnimator());




        pricerecyler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        pricerecyler.setNestedScrollingEnabled(false);
        pricerecyler.setHasFixedSize(true);
        pricerecyler.addItemDecoration(new SimpleDividerItemDecoration(bContext));

        ContextCompat.getDrawable(bContext,R.drawable.line_divider);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy , h:mm a");
        String formattedDate = df.format(c);
        dated.setText(formattedDate);
        //dated.setText(ConstantClass.dateTimeFormat(Calendar.getInstance().getTime()));
        // pricerecyler.setAdapter(mAdapter);


        listofprice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) stateId = states.get(i - 1).getStateID();

                if (i <= 0 ){
                    Slog.d("build id :-- " + stateId);
                   // getdistrictfromstate(String.valueOf(stateId));
                }else {
                    getdistrictfromstate(String.valueOf(stateId));
                    Slog.d("build id :-- " + stateId);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        if (states != null) {
            setStateSpinner();
        } else {


            getStates();
        }


    }


    protected void getdistrictfromstate(String districtid) {
        //  trustEveryone();



            // pricerecyler.clear();



        String url = URL_FOR_GET_DISTRICT_BY_STATE.replace("<UID>", districtid);
        volleyGetHit(url, REQUEST_GET_DISTRICT);

        header.setVisibility(View.GONE);
        pricerecyler.setVisibility(View.GONE);
        dateon.setVisibility(View.GONE);




        //
        // URL_FOR_GET_DISTRICT_BY_STATE
    }

    private boolean setStateSpinner() {
        try {
            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select State");
            for (int i = 0; i < states.size(); i++) {
                assetLabels.add(states.get(i).getStateName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            listofprice.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        Gson gson = new Gson();
        switch (request_code) {


            case REQUEST_GET_STATES:
                try {
                    Type type = new TypeToken<ArrayList<com.wemonde.fuelbuddy.models.State>>() {
                    }.getType();
                    states = gson.fromJson(new JSONObject(data).get("assets").toString(), type);


                    //  setStateSpinner();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case REQUEST_GET_DISTRICT:

                try {


                    if (districtarray == null) {
                        districtarray = new ArrayList<>();
                    }
//                    SharedPrefrenceHelper.setFuels(bContext, data);
                    Type type = new TypeToken<ArrayList<DistrictArray>>() {
                    }.getType();
                    JSONObject jobj = new JSONObject(data);
                    districtarray = gson.fromJson(jobj.get("assets").toString(), type);

                        showpricelist ();

                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    showLogToast("unable to parse district");
                    e.printStackTrace();
                }
                break;
          //  PriceListAdapter
        }
    }
    private void showpricelist () {
        if (districtarray== null){
            header.setVisibility(View.GONE);
            pricerecyler.setVisibility(View.GONE);
            dateon.setVisibility(View.GONE);
            PriceListAdapter mAdapter = new PriceListAdapter(bContext, districtarray);
            mAdapter.clear();

        }else {
            header.setVisibility(View.VISIBLE);
            pricerecyler.setVisibility(View.VISIBLE);
            dateon.setVisibility(View.VISIBLE);
            PriceListAdapter mAdapter = new PriceListAdapter(bContext, districtarray);
            pricerecyler.setAdapter(mAdapter);
        }
    }
}
