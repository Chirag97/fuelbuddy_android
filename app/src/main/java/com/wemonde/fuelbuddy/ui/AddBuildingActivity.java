package com.wemonde.fuelbuddy.ui;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AddBuildRequestVo;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.models.MyLoc;
import com.wemonde.fuelbuddy.models.State;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.GPSTracker;
import com.wemonde.fuelbuddy.utils.Slog;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.wemonde.fuelbuddy.base.Apis.URL_ADD_BUILD;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_ADD_ASSET;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_ADD_BUILD;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_LOCATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;

public class AddBuildingActivity extends BaseActivity {

  //  private ArrayList<State> states;
    private int stateId = -1;
    private int buildId = 0;

    private Spinner spn_states;
    private EditText et_name;
    private EditText et_lat;
    private EditText et_lng;
    private EditText et_pin;
    private EditText et_address;
    private Button btn_add;

    private Building build;
    private static  int flagcheckupdateoradd = 0 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_add_build);
        //changing the color programmatically
        flagcheckupdateoradd = 0 ;

        build = (Building) getIntent().getSerializableExtra("build");
        initViews();

        if (states!= null){
            setStateSpinner();
        }else {
            getStates();
        }

        if (build == null) {
            setBasicToolBar("Add Building");
            toolbar.setBackgroundColor(Color.parseColor("#45b879"));
            flagcheckupdateoradd = 1 ;
        } else {
            setBasicToolBar("Edit Building");
            setBuildData();
            btn_add.setText("Update");
            toolbar.setBackgroundColor(Color.parseColor("#45b879"));
            flagcheckupdateoradd = 2 ;
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
//        getGpsLoc();
    }

    private void initViews() {
        spn_states = (Spinner) findViewById(R.id.spn_states);
        et_name = (EditText) findViewById(R.id.et_name);
        btn_add = (Button) findViewById(R.id.btn_add);

        et_lat = (EditText) findViewById(R.id.et_lat);
        et_lng = (EditText) findViewById(R.id.et_lng);
        et_pin = (EditText) findViewById(R.id.et_pin);
        et_address = (EditText) findViewById(R.id.et_address);

        spn_states.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) stateId = states.get(i - 1).getStateID();
                Slog.d("build id :-- " + stateId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
    }

    private void setBuildData() {
        buildId = build.getBuildID();
        et_name.setText("" + build.getBuildName());
        et_lat.setText("" + build.getLatitude());
        et_lng.setText("" + build.getLongitude());
        et_pin.setText("" + build.getPINCode());
        et_address.setText(""+ build.getBuildLocation());



                int posFuel = 0;
        {
            if (states != null) {
                for (int i = 0; i < states.size(); i++) {


                    if (build.getStateID() == states.get(i).getStateID() ) {
                        posFuel = i+1 ;
                       // showToast(states.get(posFuel).getStateName());

                    }
                }
            }
        }
        spn_states.setSelection(posFuel);
    }

//    private void getStates() {
//        volleyGetHit(URL_GET_STATES, REQUEST_GET_STATES);
//    }

    @Override
    protected void canGetLocation(GPSTracker gps) {
//        super.canGetLocation(gps);
        et_lat.setText("" + gps.getLatitude());
        et_lng.setText("" + gps.getLongitude());
    }

    @Override
    protected void onFailed(int request_code, VolleyError error) {
        switch (request_code) {
            case REQUEST_GET_STATES:
                showToast("Unable to get states. Please try again");
                finish();
                break;
        }
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
//        super.onResponse(request_code, response, data);

        Gson gson = new Gson();
        switch (request_code) {
            case REQUEST_GET_STATES:
                try {
                    Type type = new TypeToken<ArrayList<State>>() {}.getType();
                    states = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
                  //  setStateSpinner();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case REQUEST_ADD_BUILD:
                if (flagcheckupdateoradd == 1) {
                    showToast("Building Added Successfully");
                }else if (flagcheckupdateoradd == 2){
                    showToast("Building Updated Successfully");
                }
                finish();
                break;

            case REQUEST_ADD_ASSET:
                showToast("Asset Added Successfully");
                finish();
                break;
        }
    }

    private String prepareJson() {

//        {"BuildID":0,
//          "BuildName":"B12345",
//          "BuildLocation":"B1LO",
//          "StateID":5,
//          "PINCode":"803330",
//          "Latitude":1.000,
//          "Longitude":0.991,
//          "CreatedByPMgrID":2}

        if (AppUtils.isEmpty(et_lat)) {
            showToast("Unable to get location");
            getGpsLoc();
            return "";
        }

        if (AppUtils.isEmpty(et_lng)) {
            showToast("Unable to get location");
            getGpsLoc();
            return "";
        }

        if (AppUtils.isEmpty(et_name)) {
            showToast("Enter name");
            return "";
        }


        if (AppUtils.isEmpty(et_pin)) {
            showToast("Please Enter Pincode");
            return "";
        }

        if (AppUtils.isEmpty(et_address)) {
            showToast("Enter Location");
            return "";
        }

        if (stateId <= 0) {
            showToast("Select State");
            return "";
        }

        try {
            AddBuildRequestVo avo = new AddBuildRequestVo();
            avo.setBuildID("" + buildId);
            avo.setBuildName(et_name.getText().toString().trim());
            avo.setBuildLocation(et_address.getText().toString().trim());
            avo.setCreatedByPMgrID(uid);
            avo.setLatitude(et_lat.getText().toString().trim());
            avo.setLongitude(et_lng.getText().toString().trim());
            avo.setPINCode(et_pin.getText().toString().trim());
            avo.setStateID(stateId + "");

            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public void clickAdd(View v) {
        String json = prepareJson();
        if (json.length() <= 0) return;
        String url = URL_ADD_BUILD;
        volleyBodyHit(url, json, REQUEST_ADD_BUILD);
        //  gotoBuilding();
    }

    private boolean setStateSpinner() {
        try {
            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select State");
            for (int i = 0; i < states.size(); i++) {
                assetLabels.add(states.get(i).getStateName());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_states.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_GET_LOCATION){
                Slog.d("result from map");
                MyLoc mloc = (MyLoc)data.getSerializableExtra("mloc");
                try{
                    if(mloc != null){
                        Slog.d("lat lng  : " + mloc.getLat()+ ","+ mloc.getLng());
                        et_lat.setText(""+ mloc.getLat());
                        et_lng.setText(""+ mloc.getLng());
                        et_address.setText(""+ mloc.getAddress());
                        et_pin.setText(""+ mloc.getPincode());
                    }
                }catch (Exception e){}
            }
        }
    }
}