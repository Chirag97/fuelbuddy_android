package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import static android.view.View.*;
import static com.wemonde.fuelbuddy.R.id.map;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_POSTING_TOKEN;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_Accepted;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;

import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.BuildConfig;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.LazyAdapter;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AddTokenRequest;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.Dashboard;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.utils.GPSTracker;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;

import org.apache.commons.lang3.text.WordUtils;
import org.json.JSONObject;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DASHBOARD_DETAILS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TOKEN_REGISTRATION;

public class HomeActivity extends BaseActivity implements OnMapReadyCallback, OnPermissionCallback, PlaceSelectionListener, GoogleMap.OnMarkerClickListener, TextToSpeech.OnInitListener, GoogleApiClient.ConnectionCallbacks, OnConnectionFailedListener, LocationListener,
        ResultCallback<LocationSettingsResult> {

    private static final int REQUEST_CODE_PERMISSION = 2;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    final String PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    final String DIALOG_TITLE = "Access Location";
    final String DIALOG_MESSAGE = "We need to access your Location to show you proper distance to the centers.";
    //  String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    PermissionHelper permissionHelper;

    public static int assetId = -1;
    public static float assetfuelcapacity = -1;
    public static int fuelId = -1;
    public static String assetselected = null;
    public static int selectedassetinarray = -1;

    private CoordinatorLayout coordinatorLayout;

    public static int selectedassetmapbooking = -1;
    public static int mappointer = -1;

    public static int locationcheck = 1;
    private TextToSpeech tts;

    private ActionBarDrawerToggle toggle;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private TextView nv_name;
    private ImageView nv_dp;
    private AutoCompleteTextView tv_location;
    private MapFragment mapFragment;
    public static PlaceAutocompleteFragment autocompleteFragment;
    private RelativeLayout rl_add_booking;
    private LinearLayout ll_add_booking;
    private Spinner spn_asset;
    private Spinner spn_fuel_type;
    private TextView tv_address;
    private EditText tv_quantity;
    private SeekBar sb_quantity;
    private int quantity;
    private int new_quantity;
    public static BottomNavigationView bottomNavigationView;
    private static ListView list;
    private static boolean flobtn = false;

    private static boolean Directbooking = false;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    private static boolean onresumeview = false;
    private static FloatingActionButton fab;
    private static String address;
    private static String pincode;
    private static String state;
    private static int stateid;
    LocationManager mLocationManager;

    private AlertDialog.Builder alertDialog1;

    public static int checkthefloatbutton = 0;

    public static int flagfordirectbooking = 0;
    public static String Addressofassetdirectorder = null;

    public static double Latitude;
    public static double Longitude;
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private List<Asset> dataListofaSSETS;
    private static double assetlat;
    private static double assetlong;
    private static AlertDialog alertDialog;
    private static AlertDialog alertdynamicbooking;

    public static int flagfordynamicbooking = 0;

    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    private static final int PERMISSION_CALLBACK_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private final int REQ_CODE_SPEECH_INPUT = 100;

    private static RelativeLayout back_dim_layout;

    private int currentapiVersion;

    protected Location mLastLocation;

    private static String keyidtoken = "";
    boolean checkGPS = false;
    boolean checkNetwork = false;
    protected LocationManager locationManager;

    private String[] web = {};

    // Integer[] imageId

    private Integer[] imageId = {};
    private PrettyDialog dialognewone;

    public final static int REQUEST_CODE = 10101;

    protected GoogleApiClient mGoogleApiClient;
    LinearLayout floatinglayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_home);
        setBasicToolBar(R.string.app_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        //  showToast("set path" + getLoginVo().getPMgrImagePath()) ;


        // showalertreorder();

        selectedassetmapbooking = -1;


        currentapiVersion = android.os.Build.VERSION.SDK_INT;

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .placeSnackBar);


        //Floatin action buttonItems
        web = new String[]{
                "My DashBoard",
                "My Profile",
//                "Track order",
                "My Assets",
                "Contact Us",
                "Price list",
                //"Feedback",
                "Log Out",

        };
        web = new String[]{
                "Offer Zone",
                "My Profile",
                "My Assest",
                "My Dashboard",
                "Price List",
                "Products",
                "Help",
                "Contact us",
                "Log Out"
        };


        imageId = new Integer[]{
                //for offer zone
                R.drawable.track_fuel,
                R.drawable.profile_new_one_p,
                R.drawable.asset_new_one,
                R.drawable.dashboards,
                R.drawable.pricing,
                //For procts space
                R.drawable.track_fuel,
                //For help
                R.drawable.track_fuel,
                R.drawable.contact_new_one,
                R.drawable.logout_new,

        };


        LoginVo lvo = getLoginVo();
        int approved = lvo.getIsFBApproved();
        if (approved == 0) {

            showdialogmaterial("Thank you for registering with FuelBuddy. You can use the circular menu to create your assets and experience hassle free fuel ordering. Thank you for providing us with an opportunity to serve you," + "\n" + " Team FuelBuddy ");
//            showsnackbarnew ("Greetings from FuelBuddy! \n" +
//                    "Thanks for registering , you can use around menu to create your assets and have the experience of hassel free fuel ordering. We would like to communicate with you so that we can have oppourtunity to serve you. \n" +  "Thank you, FuelBuddy ");
//            showToast("Thank you for registering with FuelBuddy.\n" +
//                    "Your dedicated buddy will be contacting you soon to verify your details and get you on your way to a new and convenient way of ordering fuel.");
            //return;
        }


        int corp = lvo.getCorpID();
//3388
        if (corp == 3388) {

            web = new String[]{

//                    "My DashBoard",
//                    "My Profile",
////                    "Track order",
//                    "My Assets",
//                    "Contact Us",
//                    "Price list",
//
//                    "TankBuddy",
//                    //   "ELocks",
//                    "Log Out",

                    "Offer Zone",
                    "My Profile",
                    "My Assest",
                    "My Dashboard",
                    "Price List",
                    "Products",
                    "Help",
                    "Contact us",
                    "Log Out"

            };


            imageId = new Integer[]{
//                    R.drawable.dashboards,
//                    R.drawable.profile_new_one_p,
////                    R.drawable.track_fuel_xxh,
//                    R.drawable.asset_new_one,
//                    R.drawable.contact_new_one,
//                    R.drawable.pricing,
//                    R.mipmap.ic_launcher_app,
//                    //   R.mipmap.ic_launcher_app,
//
//                    R.drawable.logout_new,
                    //for offer zone
                    R.drawable.track_fuel,
                    R.drawable.profile_new_one_p,
                    R.drawable.asset_new_one,
                    R.drawable.dashboards,
                    R.drawable.pricing,
                    //For procts space
                    R.drawable.track_fuel,
                    //For help
                    R.drawable.track_fuel,
                    R.drawable.contact_new_one,
                    R.drawable.logout_new,


            };

            // tv_quantity.setText("40");


        } else {


            web = new String[]{

                    "Offer Zone",
                    "My Profile",
                    "My Assest",
                    "My Dashboard",
                    "Price List",
                    "Products",
                    "Help",
                    "Contact us",
                    "Log Out"

//                    "My DashBoard",
//                    "My Profile",
////                    "Track order",
//                    "My Assets",
//                    "Contact Us",
//
//                    "Price list",
//
//                    //   "ELocks",
//                    "Log Out",

            };


            imageId = new Integer[]{


                    R.drawable.newassests,
                    R.drawable.newprofile1,
                    R.drawable.newassests,
                    R.drawable.newdashboard,
                    //for pricing
                    R.drawable.newassests,
                    //For procts space
                    R.drawable.newassests,
                    //for Help
                    R.drawable.newassests,
                    R.drawable.newcontactus,
                    R.drawable.newlogout,

//
//                    //for offer zone
//                    R.drawable.track_fuel,
//                    R.drawable.profile_new_one_p,
//                    R.drawable.asset_new_one,
//                    R.drawable.dashboards,
//                    R.drawable.pricing,
//                    //For procts space
//                    R.drawable.track_fuel,
//                    //For help
//                    R.drawable.track_fuel,
//                    R.drawable.contact_new_one,
//                    R.drawable.logout_new,


            };

        }


        if (checkDrawOverlayPermission()) {
            //  startService(new Intent(this, PowerButtonService.class));
        }


        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        checkpermission();


        tts = new TextToSpeech(this, this);

        keyidtoken = SharedPrefrenceHelper.getTOKEN(bContext);


        //int uuid = Integer.parseInt(uid);
        String uuid = uid;

        String json = preparejson();
        if (json.length() <= 0) return;
        String url = URL_FOR_POSTING_TOKEN;
        volleyBodyHit(url, json, REQUEST_TOKEN_REGISTRATION);


        // Create an instance of GoogleAPIClient.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                    .addOnConnectionFailedListener((OnConnectionFailedListener) this)
                    .addApi(LocationServices.API)
                    .build();
        }

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, 0 /* clientId */, null)
//                .addApi(Places.GEO_DATA_API)
//                .build();

        //changing the color programmatically

        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#36a670")));

        bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation1);

        bottomNavigationView.setVisibility(View.GONE);

        // turnGPSOn();

        permissionHelper = PermissionHelper.getInstance(this);
        permissionHelper.setForceAccepting(false).request(PERMISSION);

//        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
//                .addLocationRequest(mLocationRequestHighAccuracy)
//                .addLocationRequest(mLocationRequestBalancedPowerAccuracy);
//        //CertPathBuilder builder;
//        PendingResult<LocationSettingsResult> result =
//                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());

        // getGpsLoc();
        initViews();
        // initNavigationDrawer();
        bottombar();


//        try {
//            Handler h = new Handler();
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    // getMgrAssets();
////                    getFuelTypes();
//                    // getdashboard();
//                    getBuildings();
//                    //getAssetIdentifier();
//                  //  getStates();
//                    // gettrackingdetails();
//                    hideProgress();
//                }
//            };
//            h.postDelayed(r, 10);
//            showProgress();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    // getMgrAssets();
                    getFuelTypes();
                    // getdashboard();
                    //
                    //getAssetIdentifier();
                    getStates();
                    // gettrackingdetails();
                    hideProgress();
                }
            };
            h.postDelayed(r, 100);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (currentapiVersion <= 24) {
            // Do something for 14 and above versions

            try {
                //  getGpsLoc();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            // do something for phones running an SDK before 14
        }


        try {

            isInternetOn();
        } catch (Exception e) {


        }


        if (corp == 3388) {
            oncreatefloatingbuttonnew();
        } else {
            oncreatefloatingbutton();
        }


        checkinggpsenabledornot();


        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;

        if (versionCode < 1000045) {
            showoverlaydialogplaystore("New version of the App with more features is availabe ! Please update the App");
        }


        try {
            setMyButton();
        } catch (Exception e) {
            Log.e("error", "gpserror");
        }


    }


    public void showoverlaydialogplaystore(String msg) {


        final PrettyDialog dialog = new PrettyDialog(this);
        dialog
                .setTitle("FuelBuddy ")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)


                .addButton(
                        "Update",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {

                                // onResume();
                                // Dismiss

                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.wemonde.fuelbuddy"); // missing 'http://' will cause crashed
                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                startActivity(intent);
                                //finish();
                                dialog.dismiss();
//                                try {
//                                    UserRegistrationActivity.usa.finish();
//                                }catch (Exception e) {
//                                    e.printStackTrace();
//                                }


                                // Do what you gotta do
                            }
                        }
                )
                .show();

    }


    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    public void oncreatefloatingbutton() {

        fab = findViewById(R.id.fab_btn);


        list = findViewById(R.id.list);
        floatinglayout = findViewById(R.id.floatinglayout);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        fab.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));


//        floatinglayout.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(flobtn== true) {
//                    fab.startAnimation(rotate_backward);
//                    final Handler handler = new Handler();
//                    handler.postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            // Do something after 5s = 5000ms
//                            list.setVisibility(GONE);
//                            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
//                        }
//                    }, 0);
//                    flobtn = false;
//                }
//                // fab.startAnimation(rotate_forward);
//
//
//            }
//        });
        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //   showToast("Under Development");


                try {
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            getdashboard();

                            // getMgrAssets();
                            // getStates();
                            // gettrackingdetails();
                            hideProgress();
                        }
                    };
                    h.postDelayed(r, 1);
                    showProgress();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (!flobtn) {

                    fab.startAnimation(rotate_forward);
                    //fab.startAnimation(rotate_backward);
//                    fab1.startAnimation(fab_close);
//                    fab2.startAnimation(fab_close);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            floatinglayout.setBackgroundColor(Color.parseColor("#BF000000"));

                            list.setVisibility(VISIBLE);
                        }
                    }, 200);

                    LazyAdapter adapter = new
                            LazyAdapter(HomeActivity.this, web, imageId);

                    list.setAdapter(adapter);


                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


//                            "Offer Zone",
//                                    "My Profile",
//                                    "My Assest",
//                                    "My Dashboard",
//                                    "Price List",
//                                    "Products",
//                                    "Help",
//                                    "Contact us",
//                                    "Log Out"


                            switch (position) {
                              /*  case 0:
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    Toast.makeText(getApplicationContext(),"Need Offer Zone",Toast.LENGTH_SHORT).show();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton = 0;
                                    break;
*/
                                case 1:
                                    gotoProfile();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton = 0;
                                    break;

                                case 2:
                                    gotoMyAsset();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton = 0;
                                    break;

                                case 3:
                                    gotonewDashboard();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton = 0;
                                    break;

                                case 4:
                                    gotopricedetails();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton = 0;
                                    break;

                                case 5:
                                    gotoTracking();
                                    Toast.makeText(getApplicationContext(),"Need Prodcuts page",Toast.LENGTH_SHORT).show();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton=0;
                                    break;

                                case 6:
                                    Toast.makeText(getApplicationContext(),"Need HELP page",Toast.LENGTH_SHORT).show();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton=0;
                                    break;

                                case 7:
                                    gotoContact();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    checkthefloatbutton=0;
                                    break;


                                case 8: // Sixth one of the list.
                                    logout();
                                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                                    break;


                            }


                        }
                    });


                    flobtn = true;
//                fab1.setVisibility(View.VISIBLE);
//                fab2.setVisibility(View.VISIBLE);
                    // Click action
//                Intent intent = new Intent(MainActivity.this, NewMessageActivity.class);
//                startActivity(intent);
                } else {

                    fab.startAnimation(rotate_backward);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                            list.setVisibility(GONE);
                            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                        }
                    }, 50);

                    // fab.startAnimation(rotate_forward);
                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                    flobtn = false;
                }
            }
        });


    }

    public void oncreatefloatingbuttonnew() {

        fab = (FloatingActionButton) findViewById(R.id.fab_btn);

        list = (ListView) findViewById(R.id.list);


        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        fab.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));


        fab.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //   showToast("Under Development");


                try {
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            getdashboard();
                            // getMgrAssets();
                            // getStates();
                            // gettrackingdetails();
                            hideProgress();
                        }
                    };
                    h.postDelayed(r, 1);
                    showProgress();
                } catch (Exception e) {
                    e.printStackTrace();
                }


                if (!flobtn) {


                    fab.startAnimation(rotate_forward);
                    //fab.startAnimation(rotate_backward);
//                    fab1.startAnimation(fab_close);
//                    fab2.startAnimation(fab_close);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            list.setVisibility(VISIBLE);
                        }
                    }, 300);

                    LazyAdapter adapter = new
                            LazyAdapter(HomeActivity.this, web, imageId);

                    list.setAdapter(adapter);


                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                            switch (position) {
                                case 0: // first one of the list
                                    gotoMyAsset();
                                    // gotoBuilding();
                                    checkthefloatbutton = 0;
                                    break;
                                case 1: // second one of the list.

                                    gotoProfile();
                                    checkthefloatbutton = 0;
                                    break;
                                case 2: // second one of the list.

                                    //gotoOrderHistory();

                                    gotoOrderHistory("Track Orders", URL_GET_ORDERS_Accepted);

                                    // snackbarshow ("Under Development");
                                    //showToast("Under Development");
                                    checkthefloatbutton = 0;
                                    break;
                                case 3: // third one of the list.

                                    gotoContact();
                                    checkthefloatbutton = 0;
                                    break;
                                case 4: // fourth one of the list.

                                    // showToast("Under Development");
                                    gotonewDashboard();
                                    // gotoDashboard();
                                    checkthefloatbutton = 0;
                                    break;


                                case 5: // fifth one of the list.

                                    // gotoTracking();
                                    //gotoTankLevelsenseing();

                                    gotopricedetails();

                                    // logout();
                                    //   gotofeedbackactivity();
                                    // gotoDashboard();
                                    checkthefloatbutton = 0;
                                    break;


                                case 6: // Sixth one of the list.

                                    gotoTankLevelsenseing();
                                    checkthefloatbutton = 0;

//                                    logout();
                                    break;


//                                case 7: // Sixth one of the list.
//
//                                    gotoelocksactivity();
//                                    checkthefloatbutton = 0;
//
////                                    logout();
//                                    break;
                                case 7: // Sixth one of the list.


                                    logout();
                                    break;
                            }

                        }
                    });


                    flobtn = true;
//                fab1.setVisibility(View.VISIBLE);
//                fab2.setVisibility(View.VISIBLE);
                    // Click action
//                Intent intent = new Intent(MainActivity.this, NewMessageActivity.class);
//                startActivity(intent);
                } else {

                    fab.startAnimation(rotate_backward);
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            list.setVisibility(GONE);
                            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
                        }
                    }, 300);

                    // fab.startAnimation(rotate_forward);
                    floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));

                    flobtn = false;
                }
            }
        });


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Slog.i("onRequestPermissionsResult() ", Arrays.toString(permissions)
                + "\nRequest Code: " + requestCode
                + "\nGrand Results: " + grantResults);

        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    private void initViews() {

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        rl_add_booking = (RelativeLayout) findViewById(R.id.rl_add_booking);

        tv_location = (AutoCompleteTextView) findViewById(R.id.tv_location);
        tv_address = (TextView) findViewById(R.id.tv_address);
        tv_quantity = (EditText) findViewById(R.id.tv_quantity);
        spn_fuel_type = (Spinner) findViewById(R.id.spn_fuel_type);
        spn_asset = (Spinner) findViewById(R.id.spn_asset);
        sb_quantity = (SeekBar) findViewById(R.id.sb_quantity);
        back_dim_layout = (RelativeLayout) findViewById(R.id.share_bac_dim_layout);

        sb_quantity.setProgress(30);


        mapFragment = (MapFragment) getFragmentManager().findFragmentById(map);
        mapFragment.getMapAsync(this);


//        tv_quantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_NONE) {
//
//                    tv_quantity.setTextColor(Color.parseColor("#000000"));
//
//                    return true;
//                }
//                return false;
//            }
//        });


////
        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);


        // placeAutocompleteFragment - is my PlaceAutocompleteFragment instance
        ((EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(14.0f);


        //address = String.valueOf(((EditText)autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).getText());

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_ADMINISTRATIVE_AREA_LEVEL_1)
                .setCountry("IN")
                .build();

        autocompleteFragment.setFilter(autocompleteFilter);
        //    autocompleteFragment.setText (BaseActivity.AddressFromGoogleApi);

        FrameLayout.LayoutParams params
                = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        params.setMargins(0, 100, 0, 0);
        //autocompleteFragment.getView().setscreenLayoutParams(params);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                locationcheck = 0;

                //  place.getAddress();


                showToast((String) place.getAddress());
                double workLatitude = place.getLatLng().latitude;
                double workLongitude = place.getLatLng().longitude;

                address = (String) place.getAddress();
                // address = getCompleteAddressString(workLatitude, workLongitude);
                if (address != null && address.length() > 0) {
                    autocompleteFragment.setText(address);

                    //autocompleteFragment.setText(getCompleteAddressString(workLatitude, workLongitude));
                    //  autocompleteFragment.setText ("Hi");
                } else {
                    autocompleteFragment.setText(BaseActivity.AddressFromGoogleApi);
                    address = BaseActivity.AddressFromGoogleApi;
                }


                // Toast.makeText(getApplicationContext(),"Your number is " + workLatitude + workLongitude , Toast.LENGTH_SHORT).show();


                try {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(workLatitude, workLongitude)));
                    marker = addPointToMap(new LatLng(workLatitude, workLongitude));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Toast.makeText(get)
                //Log.i(TAG, "Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                //  Log.i(TAG, "An error occurred: " + status);
            }
        });


        sb_quantity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
//                quantity = progress;
                tv_quantity.setText("" + progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        spn_asset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    assetId = assets.get(i - 1).getAssetID();

                //  assetfuelcapacity = assets.get(i - 1).getAssetFuelCapacity();


                selectedassetinarray = i;
//                if (selectedassetmapbooking == 0){
//                  //  selectedassetmapbooking = 0 ;
//                    selectedassetinarray = 1;
//                    showToast( "selectedassetmapbooking = "+ selectedassetmapbooking);
//                    showToast( "selectedassetinarray = "+ selectedassetinarray);
//
//                }else {
//                    selectedassetmapbooking = 0;
//
//                    showToast( "else selectedassetmapbooking = "+ selectedassetmapbooking);
//                    showToast( "else selectedassetinarray = "+ selectedassetinarray);
//                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        spn_fuel_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0)
                    fuelId = fuelTypes.get(i - 1).getFuelID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        //   bottomNavigationView.set();

        // trustEveryone();


        flagfordynamicbooking = 0;


//        ShortcutBadger.removeCount(this); //for 1.1.4+
//        SharedPrefrenceHelper.setNotificationCount(BaseActivity.getInstance(), "0");


        //  getMgrAssets();

        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    getMgrAssets();
                    // getMgrAssets();

                    // gettrackingdetails();
                    ///  hideProgress();
                }
            };
            h.postDelayed(r, 5);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }


            if (googleMap != null) {

                googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        Slog.d("Map clicked");
                        locationcheck = 1;


                        // markernew.remove();
                        updateAddress(latLng);
                        marker = addPointToMap(latLng);
                        fab.startAnimation(rotate_backward);
                        list.setVisibility(GONE);
                        flobtn = false;

                        floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));

                    }
                });


                googleMap.setMyLocationEnabled(true);
                //   googleMap.setTrafficEnabled(true);


                googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {

                        if (currentapiVersion <= 15) {
                            // Do something for 14 and above versions

                            try {
                                getGpsLoc();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {

                            if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return false;
                            }
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                    mGoogleApiClient);
                            if (mLastLocation != null) {
//                        showToast(String.valueOf(mLastLocation.getLatitude()));
//                        showToast(String.valueOf(mLastLocation.getLongitude()));

                                LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                updateAddress(DELHI6);
                                marker = addPointToMap(DELHI6);
                            }
                            // do something for phones running an SDK before 14

                        }


//
//                    double current_lat = mLastLocation.getLatitude();
//                    double current_longi = mLastLocation.getLongitude();
//
//                    String numberAsString = Double.toString(current_lat);
//                    showToast(numberAsString);
                        // getGpsLoc();
                        return false;
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (fab.getVisibility() == VISIBLE){
//            fab.setVisibility(GONE);
//        }
        if (checkthefloatbutton == 1 && rl_add_booking.getVisibility() == VISIBLE) {
            fab.setVisibility(GONE);
        } else {
            fab.setVisibility(VISIBLE);
            fab.startAnimation(rotate_backward);
            list.setVisibility(GONE);
            flobtn = false;
            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
        }


        // getGpsLoc();
        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (currentapiVersion <= 24) {
                        // Do something for 14 and above versions

                        try {

                            getGpsLoc();
//                            final Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    getGpsLoc();
//                                    //Do something after 1000ms
//                                }
//                            }, 1000);

                            // showToast("hi ");
//                            for(int i=0;i<2;i++){
//                                getGpsLoc();
//                            }

                            //  onMapReady(googleMap);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

//                        if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            // TODO: Consider calling
//                            //    ActivityCompat#requestPermissions
//                            // here to request the missing permissions, and then overriding
//                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                            //                                          int[] grantResults)
//                            // to handle the case where the user grants the permission. See the documentation
//                            // for ActivityCompat#requestPermissions for more details.
//                           // return false;
//                        }
//                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
//                                mGoogleApiClient);
//                        if (mLastLocation != null) {
////                        showToast(String.valueOf(mLastLocation.getLatitude()));
////                        showToast(String.valueOf(mLastLocation.getLongitude()));
//
//
//                            LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
//                            updateAddress(DELHI6);
//
//
//
//                             addPointToMap(DELHI6);
//                        }
                        // do something for phones running an SDK before 14


                    }
                    // getMgrAssets();
                    // getGpsLoc();
                    hideProgress();
                }
            };
            h.postDelayed(r, 100);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ArrayList<Double> allat = new ArrayList<Double>();
        ArrayList<Double> allong = new ArrayList<Double>();

        //  currentlocationnew();

//        try {
//
//
//            if (assets.size() != 0) {
//
//                assets.size();
//
//                // assetsdetails.size();
//                showToast("Size is" + assets.size());
//                for (int i = 0; i <= assets.size(); i++) {
//                    LatLng ll = new LatLng(assets.get(i).getLatitude(), assets.get(i).getLongitude());
//
//                    //  drawMarker(ll);
//
//
//                    final Marker marker = googleMap.addMarker(new MarkerOptions()
//                            .position(new LatLng(assets.get(i).getLatitude(), assets.get(i).getLongitude()))
//                            .title(assets.get(i).getAssetIdentifierName() + "-" + assets.get(i).getAssetS_ID())
//                            .snippet(String.valueOf(assets.get(i).getAssetID()))
//
//
//                    );
//
//                    Addressofassetdirectorder = assets.get(i).getAssetLocation();
//
//                    if ((assets.get(i).getAssetIdentifierName()).equals("Movable Asset")) {
//                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.movable_asset_location));
//                    } else {
//                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.immovable_asset_location));
//                    }
//
//
//                    googleMap.setOnMarkerClickListener(this);
//
//
//                }
//            }else {
//                showToast("Size is null");
//            }
//           // googleMap.setOnMarkerClickListener(this);
//
//
//
//
//        }catch (Exception e){
//            e.printStackTrace();
//
//        }
/*
for(int i = 0 ; i < allat.size(); i++){
    LatLng DELHI6 = new LatLng(allat.get(i),allong.get(i));

    googleMap.moveCamera(CameraUpdateFactory.newLatLng(DELHI6));

}*/

        //  LatLng DELHI6 = new LatLng(assetlat,assetlong);

//        try {
//           // googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(assetlat, assetlong)));
//            //this is the line to show ok
//           onMapReady(googleMap);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }


    public void checkinggpsenabledornot() {
        LocationManager lm = (LocationManager) bContext.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {

            //https://github.com/googlesamples/android-play-location/blob/8163f9f91bbb1ca6ed6470f5458b7c6c7e6f7ef7/LocationSettings/app/src/main/java/com/google/android/gms/location/sample/locationsettings/MainActivity.java
            displayLocationSettingsRequest(this);
            // notify user
            if (currentapiVersion > 24) {
                //  showSettingsAlert();
            } else {
                //   showSettingsAlert();

            }
        } else if (gps_enabled) {


        }
    }


    public void currentlocationnew() {
        try {


            if (assets.size() != 0) {

                assets.size();

                // assetsdetails.size();
                // showToast("Size is" + assets.size());
                for (int i = 0; i <= assets.size(); i++) {
                    LatLng ll = new LatLng(assets.get(i).getLatitude(), assets.get(i).getLongitude());


                    //  drawMarker(ll);


                    final Marker marker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(assets.get(i).getLatitude(), assets.get(i).getLongitude()))
                            .title(assets.get(i).getAssetIdentifierName() + "-" + assets.get(i).getAssetS_ID())
                            .snippet(String.valueOf(assets.get(i).getAssetID()))


                    );

                    Addressofassetdirectorder = assets.get(i).getAssetLocation();
                    //  assetfuelcapacity = assets.get(i).getAssetFuelCapacity();

                    if ((assets.get(i).getAssetIdentifierName()).equals("Movable Asset")) {
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.movable_asset_location));
                    } else {
                        marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.immovable_asset_location));
                    }


                    googleMap.setOnMarkerClickListener(this);


                }
            } else {
                showToast("No Assets To Show !");
            }
            // googleMap.setOnMarkerClickListener(this);


        } catch (Exception e) {
            e.printStackTrace();

        }

    }


    @Override
    public boolean onMarkerClick(final Marker marker) {

        // showToast(" YOU have Selected " + marker.getTitle());

        //  showToast(Addressofassetdirectorder);
        marker.getPosition();
        // showToast(""+marker.getPosition());
        updateAddress(marker.getPosition());

        marker.showInfoWindow();

        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                if ((marker.getTitle()).equals("My Location")) {
                    //Your Location mk


                } else {

                    showalert(marker.getTitle(), marker.getSnippet());

                }


            }
        });


        return true;

    }

    public void showalert(final String assetidentifier, final String assetname) {

        final AlertDialog.Builder alertDialogBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertDialogBuilder = new AlertDialog.Builder(this);
        } else {
            alertDialogBuilder = new AlertDialog.Builder(this);
        }


        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setTitle("FuelBuddy")
                .setMessage("Do you want to place order for : " + WordUtils.capitalize(assetidentifier))
                .setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
//                                String assetidentifier = assetidentifier;
//                                String assetname = marker.getSnippet();
                                gotoPlaceOrdernew(assetidentifier, assetname);
                                // showBookingSheetfordirectdelivery(assetidentifier, assetname);


//                                selectedassetinarray = 1;
//                                assetId = 2;
                                // showBookingSheet();


                            }
                        });

        alertDialogBuilder.setNegativeButton("No",


                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        selectedassetmapbooking = -1;
                        dismiss();

                        // onDialogNo();
                        //  finish();
                    }
                }).setIcon(R.mipmap.ic_launcher_app_round);


        alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void dismiss() {
        alertDialog.dismiss();
    }


    @Override
    public void onPause() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
                fab.setVisibility(VISIBLE);
                fab.startAnimation(rotate_backward);
            }
        }, 300);

        //getaddressnew();


        if (currentapiVersion > 24) {
            mappointer = 1;
            getaddressnew();
            //HomeActivity.this.finish();
        } else {
            getGpsLoc();
        }

        // getGpsLoc();


        try {
            mapFragment.onPause();
        } catch (Exception e) {
            e.printStackTrace();
            Slog.e("exception in map fragment " + e.getMessage());
        }
        super.onPause();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        // currentlocationnew();

    }


    private void displayLocationSettingsRequest(Context context) {
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(10000 / 2);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //     Log.i(TAG, "All location settings are satisfied.");
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Log.i(TAG, "Location settings are not satisfied. Show the user a dialog to upgrade location settings ");

                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result
                            // in onActivityResult().
                            status.startResolutionForResult(HomeActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            //   Log.i(TAG, "PendingIntent unable to execute request.");
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        //   Log.i(TAG, "Location settings are inadequate, and cannot be fixed here. Dialog not created.");
                        break;
                }
            }
        });
    }


    @Override
    public void onDestroy() {
        try {

            if (tts != null) {
                tts.stop();
                tts.shutdown();
            }
            mapFragment.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
            Slog.e("exception in map fragment " + e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        try {
            mapFragment.onLowMemory();
        } catch (Exception e) {
            e.printStackTrace();
            Slog.e("exception in map fragment " + e.getMessage());
        }
    }

    ;

    // create an action bar button
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);

        return super.onCreateOptionsMenu(menu);
    }

    // handle button activities
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//
//
//        if (id == R.id.mybutton) {
//            showToast("YOU Are here");
//
//           // speakOut();
//
//         //   promptSpeechInput();
//
////            if (tts != null) {
////                tts.stop();
////                tts.shutdown();
////            }
//         //   gotoBuilding();
//        }
//           // launchmail();
////        }else if (id == R.id.buttoncall) {
////            callphone();
////        }
//
//        // buttoncall
//        return super.onOptionsItemSelected(item);
//    }


    public void bottombar() {


        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:


                                if (rl_add_booking.getVisibility() == GONE) {
                                    try {
                                        selectedassetmapbooking = -1;
                                        showBookingSheet();
                                    } catch (Exception e) {

                                    }

                                    //  bottomNavigationView.setVisibility(View.GONE);
                                    list.setVisibility(GONE);
                                    ;


                                }

                                // showBookingSheet();


                                //  selectedFragment = ItemOneFragment.newInstance();
                                break;
                            case R.id.action_item2:
                                gotoMyAsset();


                                //  selectedFragment = ItemTwoFragment.newInstance();
                                break;
                            case R.id.action_item3:
                                // bottomNavigationView.setVisibility(true

                                //gotoOrderHistory();


                                //  selectedFragment = ItemThreeFragment.newInstance();
                                break;

//                            case R.id.action_item4:
//                                // bottomNavigationView.setVisibility(true
//
//                                gotoOrderHistory();
//
//
//                                //  selectedFragment = ItemThreeFragment.newInstance();
//                                break;
                        }
//                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.frame_layout, selectedFragment);
//                        transaction.commit();
                        return true;
                    }
                });

        //Manually displaying the first fragment - one time only
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        final FragmentTransaction replace = transaction.replace(R.id.frame_layout, ItemOneFragment.newInstance());
//        transaction.commit();

        //Used to select an item programmatically
        //bottomNavigationView.getMenu().getItem(2).setChecked(true);
    }


    public void initNavigationDrawer() {

        toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_add_asset:
                        //do nothing;
                        gotoAddAsset();
                        break;
                    case R.id.nav_my_asset:
                        //do nothing;
                        gotoMyAsset();
                        break;
                    case R.id.nav_building:
                        //do nothing;
                        gotoBuilding();
                        break;
                    case R.id.nav_refil:
                        if (rl_add_booking.getVisibility() == GONE) {
                            showBookingSheet();
                        }
                        break;
                    case R.id.nav_delivery_history:
                        //gotoOrderHistory();
                        break;
                    case R.id.nav_address:
                        break;
                    case R.id.nav_pricing:
                        break;
                    case R.id.nav_contact:
                        gotoContact();
                        break;
                    case R.id.nav_logout:
                        logout();
                        break;
                }

                drawerLayout.closeDrawers();
                toggle.syncState();
                return true;
            }
        });

        View navHeader = navigationView.getHeaderView(0);

        nv_name = (TextView) navHeader.findViewById(R.id.nv_name);
        nv_dp = (ImageView) navHeader.findViewById(R.id.nv_dp);

        nv_name.setText(getLoginVo().getPMgrFName());

        navHeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoProfile();
                drawerLayout.closeDrawers();
                toggle.syncState();
            }
        });

    }

    public void clickBook(View v) {
        gotoPlaceOrder();
//        if (rl_add_booking.getVisibility() == GONE) {
//            showBookingSheet();
//        }
    }

    public void confirmBook(View v) {


        tv_quantity.setTextColor(Color.parseColor("#000000"));

        try {
            assetfuelcapacity = assets.get(selectedassetinarray - 1).getAssetFuelCapacity();
        } catch (Exception e) {

        }
        if (selectedassetmapbooking == -1) {

            if (assetId == -1 || selectedassetinarray == 0) {
                // selectedassetmapbooking = 0;
                showToast("Please select asset");
                return;

            }
        }
        if (fuelId == -1) {
            showToast("Please select fuel");
            return;
        }
        try {
            new_quantity = Integer.parseInt(tv_quantity.getText().toString());
        } catch (Exception e) {
            showToast("Please enter quantity");
            return;
        }

        if (assetfuelcapacity < new_quantity) {
            showToast("Ordered value is greater than asset's capacity");
            tv_quantity.setTextColor(Color.parseColor("#FF0000"));

            // setTextColor(Color.parseColor("#8B008B")

            return;
        }


        int uuid = Integer.parseInt(uid);
        try {
            quantity = Integer.parseInt(tv_quantity.getText().toString());
            if (quantity < 1) {
                showToast("Please enter quantity");
                tv_quantity.setTextColor(Color.parseColor("#000000"));
                //    speakOut("Please enter quantity");
                return;
            } else if (quantity < 30) {
                showToast("Minimum  order quantity is 30 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else if (quantity > 5000) {
                showToast("Maximum order is 5000 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else {
                checkthefloatbutton = 0;
            }

        } catch (Exception e) {
        }


//            BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity,1,"275 /22 gali no 3 sadar bazar",);
//            gotoOrderSummary(borv);

        //  rl_add_booking.setVisibility(View.GONE);
        //  checkthefloatbutton = 0;
        //  showToast("Thank You ! for Ordering, Soon You Will Be Receiving Your Order ");


        try {

            if (flagfordirectbooking == 1) {
//                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity);
//                gotoOrderSummary(borv);
                selectedassetmapbooking = -1;
                Toast.makeText(HomeActivity.this, "Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ", Toast.LENGTH_LONG).show();
                // speakOut( "Thank you for ordering with Fuel  buddy! You will  be receiving your order Soon ");
                flagfordirectbooking = 0;
                rl_add_booking.setVisibility(GONE);
                // sb_quantity.setProgress(30);
                try {
                    tv_quantity.setTextColor(Color.parseColor("#000000"));

                } catch (Exception e) {

                }


            } else {
                showalertdynamic(address, pincode);
                // sb_quantity.setProgress(30);

                try {
                    tv_quantity.setTextColor(Color.parseColor("#000000"));

                } catch (Exception e) {

                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showalertdynamic(final String address, final String pincode) {


        assetselected = assets.get(selectedassetinarray - 1).getAssetS_ID();

        final AlertDialog.Builder alertDialogBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertDialogBuilder = new AlertDialog.Builder(this);
        } else {
            alertDialogBuilder = new AlertDialog.Builder(this);
        }


        alertDialogBuilder.setCancelable(true);
        //setMessage("Do You Want To  Order For "+  WordUtils.capitalize(assetselected)+ " on Your selected Location : "+ WordUtils.capitalize(address) + " or Your Saved "+ WordUtils.capitalize(assetselected )+ "Address " )
        //   speakOut("Do Want To  Order For "+ assetselected+ " on Your selected Location : "+ address + " State:"+state+ " or Your Default   : "+assetselected+ "    Address " );
        alertDialogBuilder.setTitle("FuelBuddy")
                .setMessage("Do you want to confirm order  ")
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
//                                String assetidentifier = assetidentifier;
//                                String assetname = marker.getSnippet();
                                flagfordynamicbooking = 1;

                                int uuid = Integer.parseInt(uid);


//                                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity);
//                                gotoOrderSummary(borv);

//                                    BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uuid,assetId,fuelId,quantity,stateid,address,Latitude,Longitude,pincode);
//
//                                    gotoOrderSummaryfordynamicaddress(borv);
                                rl_add_booking.setVisibility(GONE);

                                Toast.makeText(HomeActivity.this, "Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ", Toast.LENGTH_LONG).show();


                                //     speakOut( "Thank you for ordering with Fuel  buddy! You will  be receiving your order Soon ");
                                assetselected = null;
                                //flagfordynamicbooking =0 ;

                                //  showBookingSheetfordirectdelivery(assetidentifier,assetname);
                                // showBookingSheet();


                            }
                        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                //               int uuid = Integer.parseInt(uid);
//                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity);
//                gotoOrderSummary(borv);
                // Toast.makeText(HomeActivity.this, "Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ", Toast.LENGTH_LONG).show();


                //   rl_add_booking.setVisibility(View.GONE);
                flagfordynamicbooking = 0;
                assetselected = null;
                // dismiss1();

                // onDialogNo();
                //  finish();
            }
        }).setIcon(R.mipmap.ic_launcher_app_round);


        alertDialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                assetId = -1;
                fuelId = -1;
                assetfuelcapacity = -1;
                assetselected = null;
            }
        });

        alertdynamicbooking = alertDialogBuilder.create();
        alertdynamicbooking.show();
    }


    public void dismiss1() {
        alertdynamicbooking.dismiss();
    }


    public void closeBookingWindow(View v) {
        bottomNavigationView.setVisibility(GONE);
        rl_add_booking.setVisibility(GONE);
        fab.setVisibility(VISIBLE);
        selectedassetmapbooking = -1;
    }

    private void showBookingSheet() {
        rl_add_booking.setVisibility(VISIBLE);
        fab.setVisibility(GONE);
        bottomNavigationView.setVisibility(GONE);

        sb_quantity.setProgress(30);
        tv_quantity.setText("30");


        if (list.getVisibility() == VISIBLE) {
            list.setVisibility(GONE);

            fab.startAnimation(rotate_backward);
            flobtn = false;
            floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));
        }


        checkthefloatbutton = 1;


        //  if (tv_location.getText() == null || tv_location.getText().length() <= 0)
//
//        if (address == null || address.length() <= 0 ){
//            showToast("Please Select a location");
//
//            rl_add_booking.setVisibility(View.GONE);
//            //list.setVisibility(View.GONE);
//            return;
//        } else {
//            list.setVisibility(View.GONE);
//
//        }

        // tv_address.setText("" + tv_location.getText());
        // tv_address.setText("Place Order");
        // Resources passed to adapter to get image


        //formaking Diesel fuel
        fuelId = 1;

        ArrayList<String> assetLabels1 = new ArrayList<>();
        assetLabels1.add(" Diesel ");
        SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
        spn_fuel_type.setAdapter(assetAdapter1);

        //setting diesel direct otherwise this statement to be written
        // if (!setAssetSpinner() || !setFuelTypeSpinner()) {
        if (!setAssetSpinner()) {
            rl_add_booking.setVisibility(GONE);
            list.setVisibility(GONE);
            return;
        }
    }


    public boolean checkDrawOverlayPermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            return false;
        } else {
            return true;
        }
    }


    public void showBookingSheetfordirectdelivery(String assetidentifier, String assetidselected) {

        gotoPlaceOrdernew(assetidentifier, assetidselected);

        flagfordirectbooking = 1;
        tv_quantity.setText("30");
        rl_add_booking.setVisibility(VISIBLE);
        fab.setVisibility(GONE);
        bottomNavigationView.setVisibility(GONE);

        sb_quantity.setProgress(30);


        selectedassetmapbooking = 0;


        checkthefloatbutton = 1;


        //  if (tv_location.getText() == null || tv_location.getText().length() <= 0)

//        if (address == null || address.length() <= 0 ){
//            showToast("Please Select a location");
//
//            rl_add_booking.setVisibility(View.GONE);
//            //list.setVisibility(View.GONE);
//            return;
//        } else {
//            list.setVisibility(View.GONE);
//
//        }


        // tv_address.setText("" + tv_location.getText());
        //  tv_address.setText(address);
        // Resources passed to adapter to get image
        ArrayList<String> assetLabels = new ArrayList<>();
        assetLabels.add(assetidentifier);
        SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
        spn_asset.setAdapter(assetAdapter);
        int asset_id = Integer.parseInt(assetidselected);

        assetId = asset_id;
        fuelId = 1;

        ArrayList<String> assetLabels1 = new ArrayList<>();
        assetLabels1.add(" Diesel ");
        SpinnerAdapter assetAdapter1 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels1);
        spn_fuel_type.setAdapter(assetAdapter1);


//        if ( !setFuelTypeSpinner()) {
//            rl_add_booking.setVisibility(View.GONE);
//            list.setVisibility(View.GONE);
//            return;
//        }
    }


    private boolean setFuelTypeSpinner() {
        try {

            if (fuelTypes == null) {
//                gotoAddAsset();
                return false;
            }

            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Fuel Type");
            for (int i = 0; i < fuelTypes.size(); i++) {
                assetLabels.add(fuelTypes.get(i).getFuelName());
            }

            SpinnerAdapter fuelTypeAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);

//            ArrayAdapter<String> fuelTypeAdapter = new ArrayAdapter<String>(bContext, android.R.layout.simple_dropdown_item_1line, res.getStringArray(R.array.fuel_array));

            spn_fuel_type.setAdapter(fuelTypeAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean setAssetSpinner() {
        try {
            if (assets == null) {
                showToast("Please Add Assets");
                bottomNavigationView.setVisibility(GONE);
                fab.setVisibility(VISIBLE);
                return false;
            }

            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Asset");
            for (int i = 0; i < assets.size(); i++) {
                assetLabels.add(assets.get(i).getAssetIdentifierName() + "-" + assets.get(i).getAssetS_ID());
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_asset.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);
        switch (request_code) {
            case REQUEST_GET_ASSETS:
                setAssetAdapternew();
                break;

            case REQUEST_GET_DASHBOARD_DETAILS:

                try {
                    Gson gson1 = new Gson();

                    //  Tracking track = gson.fromJson(data, Tracking.class);
                    //assets = assetVo.getAssets();

                    Type type1 = new TypeToken<ArrayList<Dashboard>>() {
                    }.getType();


                    JSONObject jsonObject = new JSONObject(data);
//                JSONArray events = jsonObject.getJSONArray("assets");
//                String jsonString = events.toString();


                    JSONObject getassetarray = jsonObject.getJSONObject("assets");


                    // String innerJson = gson.toJson(new JSONObject(data).get("assets"));


                    dashboarddetails = gson1.fromJson(new JSONObject(getassetarray.toString()).get("Table").toString(), type1);

                    //  showToast("Scuess");
                    // gotoTracking();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void setAssetAdapternew() {

        if (assets == null) {
            assets = new ArrayList<>();
        }
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() { // Tried new Handler(Looper.myLopper()) also
                @Override
                public void run() {
                    currentlocationnew();
                    //  mAdapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//    @Override
//    public void onMarkerClick(final Marker marker) {
//
//      showToast("Marker clicked");
//
//
//        //return true;
//    }

    @Override
    public void onMapReady(GoogleMap gmap) {
        this.googleMap = gmap;
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(DELHI));
//here ill paass that array like thik at eixlal mple rite ?yes


        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());


        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String str = sdf.format(new Date());

        String[] time = str.split(":");
        int hour = Integer.parseInt(time[0].trim());


        //  outputtime.setText(time);
        // showToast(""+hour);

        if (hour >= 18 || hour < 6) {
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.mapstyle_night);
            gmap.setMapStyle(style);


            fab.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));

            // fab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            //fab_btn.setbac
        } else {
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.mapstyle_retro);
            gmap.setMapStyle(style);

        }


//        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
//                .getString(R.string.style_json)));
//
//        if (!success) {
//          //  Log.e(TAG, "Style parsing failed.");
//        }


        gmap.addMarker(new MarkerOptions()
                .position(new LatLng(assetlat, assetlong))
                .title("Hello world"));


        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(DELHI)      // Sets the center of the map to Mountain View
                .zoom(zoom)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());


        if (status != ConnectionResult.SUCCESS) {
            ///if play services are not available
            int requestCode = 10;
//            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
//            dialog.show();
        } else {
            // Enabling MyLocation Layer of Google Map
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                googleMap.setMyLocationEnabled(true);
                // googleMap.setTrafficEnabled(true);
                googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        //  marker.setVisible(false);

                        if (currentapiVersion <= 24) {
                            // Do something for 14 and above versions

                            try {
                                if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    // return TODO;
                                }
                                googleMap.setMyLocationEnabled(true);
                                getGpsLoc();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {

                            if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return false;
                            }
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                    mGoogleApiClient);
                            if (mLastLocation != null) {
//                            showToast(String.valueOf(mLastLocation.getLatitude()));
//                            showToast(String.valueOf(mLastLocation.getLongitude()));


                                LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                updateAddress(DELHI6);

                                marker = addPointToMap(DELHI6);
//
//                                if (markernew != null) {
//                                    markernew.remove();
//                                }
                            }
                            // do something for phones running an SDK before 14

                        }


//
//                    double current_lat = mLastLocation.getLatitude();
//                    double current_longi = mLastLocation.getLongitude();
//
//                    String numberAsString = Double.toString(current_lat);
//                    showToast(numberAsString);
                        // getGpsLoc();


                        //  getGpsLoc();
                        return false;
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

//            googleMap.moveCamera(CameraUpdateFactory.newLatLng(DELHI));
            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            setMyButton();
//            Handler h = new Handler();
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    getGpsLoc();
//                    hideProgress();
//                }
//            };
//
//            h.postDelayed(r, 1000);
//            showProgress();
        }

        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
//                Slog.d("map moving");
            }
        });


        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Slog.d("Map clicked");
                locationcheck = 1;

                // markernew.remove();
                marker = addPointToMap(latLng);
                updateAddress(latLng);
                fab.startAnimation(rotate_backward);
                list.setVisibility(GONE);
                flobtn = false;
                floatinglayout.setBackgroundColor(Color.parseColor("#00000000"));

            }
        });
    }


    @Override
    protected void canGetLocation(GPSTracker gps) {

        if (locationcheck == 1) {
            // super.canGetLocation(gps);
            setMyButton();
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();

            LatLng DELHI6 = new LatLng(latitude, longitude);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(DELHI6)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom
                    .build();
            try {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                marker = addPointToMap(new LatLng(latitude, longitude));
            } catch (Exception e) {
                e.printStackTrace();
            }
            showLogToast("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

        }
    }

    private void setMyButton() {
        try {
            View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

            // and next place it, for exemple, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
            //locationButton.setPadding(10,200,900,140);

            //locationButton.setPadding(10,200,700,140);
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
//            rlp.setMargins(50, 50, 35, 145);

            //position o right top side

//            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//            rlp.setMargins(0, 180, 180, 0);

            // position left 20,900,180,60
            rlp.addRule(RelativeLayout.ALIGN_PARENT_END, 0);
            rlp.addRule(RelativeLayout.ALIGN_END, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            rlp.setMargins(20, 650, 180, 60);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void updateAddress(LatLng loc) {
        super.updateAddress(loc);
        // ((EditText)autocompleteFragment.getView().findViewById(R.id.autocomplete_fragment)).setText(getCompleteAddressString(loc.latitude, loc.longitude));
        // tv_location.setText(getCompleteAddressString(loc.latitude, loc.longitude));

        // marker = addPointToMap(loc);

        Latitude = loc.latitude;
        Longitude = loc.longitude;


        address = " ";
        pincode = " ";
        state = " ";


        //  new JSONParse().execute();
        address = getCompleteAddressString(loc.latitude, loc.longitude);
        pincode = getcompletepincode(loc.latitude, loc.longitude);
        state = getcompletestateaddress(loc.latitude, loc.longitude);


        stateid = getstateid(state);

        //showToast(state);


        if (pincode != null && pincode.length() > 0) {

            //autocompleteFragment.setText(getCompleteAddressString(loc.latitude, loc.longitude));
            //  autocompleteFragment.setText ("Hi");
        } else {
            // autocompleteFragment.setText (BaseActivity.AddressFromGoogleApi);
            pincode = BaseActivity.AddressFromGoogleApi;
        }


        if (address != null && address.length() > 0) {

            autocompleteFragment.setText(getCompleteAddressString(loc.latitude, loc.longitude));
            //  autocompleteFragment.setText ("Hi");
        } else {
            // autocompleteFragment.setText (BaseActivity.AddressFromGoogleApi);
            address = BaseActivity.AddressFromGoogleApi;
        }
        //  getAddress(loc.latitude,loc.longitude);


        // getAddress1
    }

    @Override
    public void onPermissionGranted(String[] permissionName) {
        Slog.i("onPermissionGranted() ", "" + Arrays.toString(permissionName));
        showToast(Arrays.toString(permissionName));
        getGpsLoc();
    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {
        Slog.i("onPermissionDeclined() ", "" + Arrays.toString(permissionName));
    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {
        Slog.i("onPermissionPreGranted() ", permissionsName);
    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {
        Slog.i("onPermissionNeedExplanation() ", permissionName);

        /*
        Show dialog here and ask permission again. Say why
         */

        showAlertDialog(DIALOG_TITLE, DIALOG_MESSAGE, PERMISSION);

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {
    }

    @Override
    public void onNoPermissionNeeded() {

    }

    private void


    showAlertDialog(String title, String message, final String permission) {

        AlertDialog settingDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Request", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionHelper.requestAfterExplanation(permission);
                        dialog.dismiss();
                    }
                })
                .create();
        settingDialog.show();
    }


    public void onDialogNo() {
        if (rl_add_booking.getVisibility() == VISIBLE) {
            rl_add_booking.setVisibility(GONE);
            checkthefloatbutton = 0;
            return;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        list.setVisibility((GONE));
        bottomNavigationView.setVisibility(VISIBLE);
        // showExitDialog();
        fab.setVisibility(VISIBLE);
        fab.startAnimation(rotate_backward);

    }

    @Override
    public void onBackPressed() {

        if (tts != null) {
            tts.stop();
            // tts.shutdown();
        }


        flagfordirectbooking = 0;
        if (rl_add_booking.getVisibility() == VISIBLE) {
            rl_add_booking.setVisibility(GONE);
            bottomNavigationView.setVisibility(GONE);
            fab.setVisibility(VISIBLE);

            checkthefloatbutton = 0;
            return;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        list.setVisibility((GONE));
        bottomNavigationView.setVisibility(GONE);
        showExitDialog();
        fab.setVisibility(VISIBLE);
        fab.startAnimation(rotate_backward);

    }


    @Override
    public void onPlaceSelected(Place place) {

        double workLatitude = place.getLatLng().latitude;
        double workLongitude = place.getLatLng().longitude;


        final LatLng DELHINEW = new LatLng(workLatitude, workLongitude);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(DELHINEW)      // Sets the center of the map to Mountain View
                .zoom(zoom)                   // Sets the zoom
                .build();

        //  Toast.makeText(getApplicationContext(),"Your number is " + workLatitude + workLongitude , Toast.LENGTH_SHORT).show();


        try {
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(workLatitude, workLongitude)));
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            marker = addPointToMap(new LatLng(workLatitude, workLongitude));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public int getstateid(String state) {


        if (state != null) {


            if (state.equalsIgnoreCase("ANDAMAN AND NICOBAR ISLANDS") || state.equalsIgnoreCase(" ANDAMAN AND NICOBAR ISLANDS")) {
                return 1;
            } else if (state.equalsIgnoreCase("ANDHRA PRADESH") || state.equalsIgnoreCase(" ANDHRA PRADESH") || state.equalsIgnoreCase("ANDHRA PRADESH")) {
                return 2;
            } else if (state.equalsIgnoreCase("ARUNACHAL PRADESH") || state.equalsIgnoreCase(" ARUNACHAL PRADESH") || state.equalsIgnoreCase(" ARUNACHAL PRADESH")) {
                return 3;
            } else if (state.equalsIgnoreCase("ASSAM") || state.equalsIgnoreCase(" ASSAM") || state.equalsIgnoreCase("ASSAM")) {
                return 4;
            } else if (state.equalsIgnoreCase("BIHAR") || state.equalsIgnoreCase(" BIHAR") || state.equalsIgnoreCase(" BIHAR")) {
                return 5;
            } else if (state.equalsIgnoreCase("CHANDIGARH") || state.equalsIgnoreCase(" CHANDIGARH") || state.equalsIgnoreCase("CHANDIGARH")) {
                return 7;
            } else if (state.equalsIgnoreCase("CHATTISGARH") || state.equalsIgnoreCase(" CHATTISGARH") || state.equalsIgnoreCase(" CHATTISGARH")) {
                return 6;
            } else if (state.equalsIgnoreCase("DADRA AND NAGAR HAVELI") || state.equalsIgnoreCase(" DADRA AND NAGAR HAVELI") || state.equalsIgnoreCase("DADRA AND NAGAR HAVELI")) {
                return 10;
            } else if (state.equalsIgnoreCase("DAMAN AND DIU") || state.equalsIgnoreCase(" DAMAN AND DIU") || state.equalsIgnoreCase(" DAMAN AND DIU")) {
                return 8;
            } else if (state.equalsIgnoreCase("DELHI") || state.equalsIgnoreCase(" DELHI") || state.equalsIgnoreCase("NEW DELHI")) {
                return 9;
            } else if (state.equalsIgnoreCase("GOA") || state.equalsIgnoreCase(" GOA") || state.equalsIgnoreCase(" GOA")) {
                return 11;
            } else if (state.equalsIgnoreCase("GUJARAT") || state.equalsIgnoreCase(" GUJARAT") || state.equalsIgnoreCase(" GUJARAT")) {
                return 12;
            } else if (state.equalsIgnoreCase("HARYANA") || state.equalsIgnoreCase(" HARYANA") || state.equalsIgnoreCase("HARYANA")) {
                return 14;
            } else if (state.equalsIgnoreCase("HIMACHAL PRADESH") || state.equalsIgnoreCase(" HIMACHAL PRADESH") || state.equalsIgnoreCase(" HIMACHAL PRADESH")) {
                return 13;
            } else if (state.equalsIgnoreCase("JAMMU AND KASHMIR") || state.equalsIgnoreCase(" JAMMU AND KASHMIR") || state.equalsIgnoreCase(" JAMMU AND KASHMIR")) {
                return 15;
            } else if (state.equalsIgnoreCase("JHARKHAND") || state.equalsIgnoreCase(" JHARKHAND") || state.equalsIgnoreCase(" JHARKHAND")) {
                return 16;
            } else if (state.equalsIgnoreCase("KARNATAKA") || state.equalsIgnoreCase(" KARNATAKA") || state.equalsIgnoreCase("KARNATAKA ")) {
                return 18;
            } else if (state.equalsIgnoreCase("KERALA") || state.equalsIgnoreCase(" KERALA") || state.equalsIgnoreCase("KERALA ")) {
                return 17;
            } else if (state.equalsIgnoreCase("LAKSHADWEEP") || state.equalsIgnoreCase(" LAKSHADWEEP") || state.equalsIgnoreCase("LAKSHADWEEP ")) {
                return 19;
            } else if (state.equalsIgnoreCase("MADHYA PRADESH") || state.equalsIgnoreCase(" MADHYA PRADESH") || state.equalsIgnoreCase("MADHYA PRADESH ")) {
                return 23;
            } else if (state.equalsIgnoreCase("MAHARASHTRA") || state.equalsIgnoreCase(" MAHARASHTRA") || state.equalsIgnoreCase("MAHARASHTRA ")) {
                return 21;
            } else if (state.equalsIgnoreCase("MANIPUR") || state.equalsIgnoreCase(" MANIPUR") || state.equalsIgnoreCase("MANIPUR ")) {
                return 22;
            } else if (state.equalsIgnoreCase("MEGHALAYA") || state.equalsIgnoreCase(" MEGHALAYA") || state.equalsIgnoreCase("MEGHALAYA ")) {
                return 20;
            } else if (state.equalsIgnoreCase("MIZORAM") || state.equalsIgnoreCase(" MIZORAM") || state.equalsIgnoreCase("MIZORAM ")) {
                return 24;
            } else if (state.equalsIgnoreCase("MANIPUR") || state.equalsIgnoreCase(" MANIPUR") || state.equalsIgnoreCase("MANIPUR ")) {
                return 22;
            } else if (state.equalsIgnoreCase("MEGHALAYA") || state.equalsIgnoreCase(" MEGHALAYA") || state.equalsIgnoreCase("MEGHALAYA ")) {
                return 20;
            } else if (state.equalsIgnoreCase("MIZORAM") || state.equalsIgnoreCase(" MIZORAM") || state.equalsIgnoreCase("MIZORAM ")) {
                return 24;
            } else if (state.equalsIgnoreCase("NAGALAND") || state.equalsIgnoreCase(" NAGALAND") || state.equalsIgnoreCase("NAGALAND ")) {
                return 25;
            } else if (state.equalsIgnoreCase("ORISSA") || state.equalsIgnoreCase(" ORISSA") || state.equalsIgnoreCase("ORISSA ")) {
                return 26;
            } else if (state.equalsIgnoreCase("PONDICHERRY") || state.equalsIgnoreCase(" PONDICHERRY") || state.equalsIgnoreCase("PONDICHERRY ")) {
                return 28;
            } else if (state.equalsIgnoreCase("PUNJAB") || state.equalsIgnoreCase(" PUNJAB") || state.equalsIgnoreCase("PUNJAB ")) {
                return 27;
            } else if (state.equalsIgnoreCase("RAJASTHAN") || state.equalsIgnoreCase(" RAJASTHAN") || state.equalsIgnoreCase("RAJASTHAN ")) {
                return 29;
            } else if (state.equalsIgnoreCase("SIKKIM") || state.equalsIgnoreCase(" SIKKIM") || state.equalsIgnoreCase("SIKKIM ")) {
                return 30;
            } else if (state.equalsIgnoreCase("TAMIL NADU") || state.equalsIgnoreCase(" TAMIL NADU") || state.equalsIgnoreCase("TAMIL NADU ")) {
                return 31;
            } else if (state.equalsIgnoreCase("TRIPURA") || state.equalsIgnoreCase(" TRIPURA") || state.equalsIgnoreCase("TRIPURA ")) {
                return 32;
            } else if (state.equalsIgnoreCase("UTTAR PRADESH") || state.equalsIgnoreCase(" UTTAR PRADESH") || state.equalsIgnoreCase("UTTAR PRADESH ")) {
                return 34;
            } else if (state.equalsIgnoreCase("UTTARAKHAND") || state.equalsIgnoreCase(" UTTARAKHAND") || state.equalsIgnoreCase("UTTARAKHAND ")) {
                return 33;
            } else if (state.equalsIgnoreCase("WEST BENGAL") || state.equalsIgnoreCase(" WEST BENGAL") || state.equalsIgnoreCase("WEST BENGAL ")) {

                return 35;
            } else {
                return 0;
            }

        } else {
            showToast("Error in Getting State, please click on Map to get Your Location");
            return 0;
        }


    }


    @Override
    public void onError(Status status) {

    }


    public void checkpermission() {
        if (ActivityCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Need Location  Permission");
                builder.setMessage("This app needs Location permission");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.ACCESS_FINE_LOCATION, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle("Need Location  Permission");
                builder.setMessage("This app needs Location permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Location", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.ACCESS_FINE_LOCATION, true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.
            // proceedAfterPermission();
        }
    }


    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
    }


    @Override
    public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);
            tts.setPitch((float) 1);
            tts.setSpeechRate((float) 1);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.e("TTS", "This Language is not supported");
            } else {
                // btnSpeak.setEnabled(true);
                //   speakOut("Click On The Mic On Right Left Corner ,n Say Order Now ... For Quick Order");
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

    }
//    private void speakOut(String text) {
//
//       // text = "Thank You ! for Ordering, Soon You Will Be Receiving Your Order";
//              // txtText.getText().toString();
//
//        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
//    }

//    private void promptSpeechInput() {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
//                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
//                getString(R.string.speech_prompt));
//        try {
//            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
//        } catch (ActivityNotFoundException a) {
//            Toast.makeText(getApplicationContext(),
//                    getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
//        }
//    }

    /**
     * Receiving speech input
     */
    @TargetApi(Build.VERSION_CODES.M)

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQ_CODE_SPEECH_INPUT: {
                if (resultCode == RESULT_OK && null != data) {

                    ArrayList<String> result = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    showToast(result.get(0));


                    String sentence = result.get(0);
                    String search = "order";
                    String desel = "diesel";
                    String fuuu = "fuel";

                    if (sentence.toLowerCase().indexOf(search.toLowerCase()) != -1 || sentence.toLowerCase().indexOf(desel.toLowerCase()) != -1 || sentence.toLowerCase().indexOf(fuuu.toLowerCase()) != -1) {

                        showBookingSheet();
                        //  speakOut("Please Select Asset... From the Drop Downlist (For Which You Want To order), And Quantity in Litres..., n  Press Confirm Order");

                    } else {

                        //   speakOut("Didn't Get , Please try again...");
                        System.out.println("not found");

                    }
                    // txtSpeechInput.setText(result.get(0));
                }
                break;
            }
            case REQUEST_CODE: {
                if (Settings.canDrawOverlays(this)) {
                    //   startService(new Intent(this, PowerButtonService.class));
                }
            }

        }
    }


    private String preparejson() {
        try {
            AddTokenRequest avo = new AddTokenRequest();
            avo.setKeyID(uid);
            avo.setDeviceRegID(keyidtoken);
            avo.setOSIndex("0");


            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }


    private void turnGPSOn() {
        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(bContext);


        alertDialog.setTitle("GPS Settings");

        alertDialog.setMessage("Please enable GPS by going to settings");


        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                bContext.startActivity(intent);
            }
        });


        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        alertDialog.show();
    }


    protected Map<String, String> getParams() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("KeyID", "fakeEmail");//
        params.put("DeviceRegID", "fakePassword");
        params.put("OSIndex", "fakeEmail");//
        return params;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
//           showToast(String.valueOf(mLastLocation.getLatitude()));
//            showToast(String.valueOf(mLastLocation.getLongitude()));

            LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            updateAddress(DELHI6);

            marker = addPointToMap(DELHI6);
            //addPointToMap(DELHI6);
        }
    }

    public void getaddressnew() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            googleMap.setMyLocationEnabled(true);
            //   googleMap.setTrafficEnabled(true);


            if (currentapiVersion <= 24) {
                // Do something for 14 and above versions

                try {
                    getGpsLoc();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else {


                // marker.remove();
                if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    //return false;
                }
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
                if (mLastLocation != null) {
//                        showToast(String.valueOf(mLastLocation.getLatitude()));
//                        showToast(String.valueOf(mLastLocation.getLongitude()));

                    LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    updateAddress(DELHI6);

                    marker = addPointToMap(DELHI6);

                    //
                    //
                    //
                    //
                    //
                    //
                    // markernew = addmarkerpoint(DELHI6);


//                            if (mappointer == 1 ){
//                                markernew = addPointToMap(DELHI6);
//                            }
                    //  mapFragment.getMapAsync(HomeActivity.this);

                    // onMapReady(googleMap);
                    // addPointToMap(DELHI6);
                }


                // do something for phones running an SDK before 14

            }


//
//                    double current_lat = mLastLocation.getLatitude();
//                    double current_longi = mLastLocation.getLongitude();
//
//                    String numberAsString = Double.toString(current_lat);
//                    showToast(numberAsString);
            // getGpsLoc();
            // return false;

            // });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onConnectionSuspended(int i) {
        showToast("get location gone ");

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        showToast("get location gone ! , new location if no");
    }


//
//    private void showSnack(String message) {
//
//        int color;
//        color = Color.WHITE;
////        if (isConnected) {
////            message = "Good! Connected to Internet";
////            color = Color.WHITE;
////        } else {
////            message = "Sorry! Not connected to internet";
////            color = Color.RED;
////        }
//
//        Snackbar snackbar = Snackbar
//                .make(findViewById(R.id.fab_btn), message, Snackbar.LENGTH_LONG);
//
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setTextColor(color);
//        snackbar.show();
//
//    }


    public void snackbarshow(String msg) {
        // bottomNavigationView.setVisibility(View.GONE);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

        snackbar.show();
    }

    public final boolean isInternetOn() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager) getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if (connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            // if connected with internet

            // Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED) {

            showSettingsAlertnew();
            //  Toast.makeText(this, " No internet connectivity ! ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


    private void showalertreorder() {


        // Integer x = Integer.valueOf(str);


        //  back_dim_layout.setVisibility(View.VISIBLE);


        final Dialog dialog = new Dialog(bContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.intialalertdialog);
        //  dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));


        // dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        TextView text = (TextView) dialog.findViewById(R.id.text_message);
        text.setText("Go to FuelBuddy or e-FuelBuddy");

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//
//                        BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uid, assetID, fuelid,qty,Stateid,location,Latitude,Longitude,Pincode);
//        ((BaseActivity) context).gotoOrderSummaryfordynamicaddress(borv);


//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();
//        BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetID, fuelid,qty);
//        ((BaseActivity) context).gotoOrderSummary(borv);
    }

    public void showSettingsAlertnew() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(bContext);


        alertDialog.setTitle("Internet Settings");

        alertDialog.setMessage("Please enable internet by going to settings");


        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                bContext.startActivity(intent);
            }
        });


        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        alertDialog.show();
    }

    @Override
    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {

    }


    public void showdialogmaterial(String msg) {


        final PrettyDialog dialog = new PrettyDialog(this);
        dialog
                .setTitle("Greetings !")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)

                .addButton(
                        "Okay",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {

                                // onResume();
                                // Dismiss
                                dialog.dismiss();
                                // Do what you gotta do
                            }
                        }
                )
                .show();


//
//        new MaterialStyledDialog.Builder(this)
//
//
//                .setDescription(msg)
//                .withDialogAnimation(true)
//                .withDarkerOverlay(true)
//                //.withDialogAnimation(true, Duration.SLOW)
//                .show();


//        new MaterialStyledDialog.Builder(this)
//                .setTitle(" Greetings !")
//
//                .setDescription(msg)
//                .withDialogAnimation(true, Duration.SLOW)
//                .setPositiveText("Okay")
//                .setScrollable(false)
//                .show();
    }

//    public void showsnackbarnew (String msg){
//        Snackbar snackbar = Snackbar
//                .make(coordinatorLayout, msg, 18000);
//
//        View sbView = snackbar.getView();
//        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
//        textView.setMaxLines(10);
//        textView.setTextColor(Color.WHITE);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//        } else {
//            textView.setGravity(Gravity.CENTER_HORIZONTAL);
//        }
//
//        snackbar.show();
//    }
//    private void trustEveryone() {
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }});
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, new X509TrustManager[]{new X509TrustManager(){
//                public void checkClientTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public void checkServerTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }}}, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(
//                    context.getSocketFactory());
//        } catch (Exception e) { // should never happen
//            e.printStackTrace();
//        }
//    }
}