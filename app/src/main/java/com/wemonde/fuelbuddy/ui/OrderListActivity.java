package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.OrderRecVerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.BookingVo;
import com.wemonde.fuelbuddy.utils.Slog;
import com.yuan.waveview.WaveView;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_Declined;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ORDERS_OPEN;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ORDERS;

public class OrderListActivity extends BaseActivity {

    private RecyclerView rv_orders;
    private String message;
    private static String url_new;
    private static  String orderstatus;
    private WaveView waveview;

    private CoordinatorLayout coordinatorLayout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_order_list);


        Bundle bundle = getIntent().getExtras();
        message = bundle.getString("borv");
        String msg = message;

        String[] separated = message.split("@&");
        //  separated[0]; // this will contain "Fruit"

        orderstatus = separated[0].trim();


        url_new = separated[1].trim();





        setBasicToolBar(orderstatus);







        //changing the color programmatically
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45b879")));


        coordinatorLayout = (CoordinatorLayout)findViewById(R.id.coordinatorLayout);

        waveview = (WaveView)findViewById(R.id.waveview);



        rv_orders = (RecyclerView) findViewById(R.id.rv_orders);
        rv_orders.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_orders.setNestedScrollingEnabled(false);
        rv_orders.setHasFixedSize(true);

        getOrders();

//
//        waveview.setWaveColor(Color.RED);
//        waveview.setbgColor(Color.WHITE);




//        waveview.setProgressListener(new WaveView.waveProgressListener() {
//            @Override
//            public void onPorgress(boolean isDone, long progress, long max) {
//                if (isDone){
//                    Toast.makeText(OrderListActivity.this,"Loading completed!!!",Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }


    @Override
    protected void onResume() {
        super.onResume();

//        waveview.setMode(WaveView.MODE_RECT);
//        waveview.setSpeed(WaveView.SPEED_FAST);
//
//        waveview.setProgress(80);
//        waveview.setMax(100);
    }

    private void getOrders() {
        String url = url_new.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_ORDERS);
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        if(request_code == REQUEST_GET_ORDERS){

            try {
                Gson gson = new Gson();
                Type type = new TypeToken<ArrayList<BookingVo>>() {
                }.getType();
                JSONObject jobj = new JSONObject(data);
                ArrayList<BookingVo> bookings = gson.fromJson(jobj.get("assets").toString(), type);
                setOrderList(bookings);

//                if(bookings.isEmpty() && orderstatus.equals("Track Orders") )
//                {
//                    // Do something with the empty list here.
//                    showsnackbar(" No orders to Track now !");
//                }
            }catch (Exception e){
                Slog.e("exception : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private void setOrderList(ArrayList<BookingVo> bookings) {
        rv_orders.setAdapter(new OrderRecVerAdapter(bContext, bookings));
    }


    public void showsnackbar (String msg){
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);

        snackbar.show();
    }
}
