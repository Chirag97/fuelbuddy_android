package com.wemonde.fuelbuddy.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.JsonUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_POSTING_TOKEN;
import static com.wemonde.fuelbuddy.base.Apis.URL_LOGIN;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_LOGIN;

public class LoginActivity extends BaseActivity {

    private EditText et_pwd;
    private EditText et_uname;
    private CheckBox mCbShowPwd;
    private CheckBox magreeTermsncondition;
    LoginVo vo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews() {
        et_uname = (EditText) findViewById(R.id.et_uname);
        et_pwd = (EditText) findViewById(R.id.et_pwd);
        mCbShowPwd = (CheckBox)findViewById(R.id.cbShowPwd);
        magreeTermsncondition = (CheckBox)findViewById(R.id.agreetermsn_condition);




        mCbShowPwd.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // checkbox status is changed from uncheck to checked.
                if (!isChecked) {
                    // show password
                    et_pwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    et_pwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }

    });

    }
    private boolean isValidMail(String email) {
        boolean check;
        Pattern p;
        Matcher m;

        String EMAIL_STRING = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        p = Pattern.compile(EMAIL_STRING);

        m = p.matcher(email);
        check = m.matches();

        if(!check) {
          //  txtEmail.setError("Not Valid Email");
        }
        return check;
    }

    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
               // txtPhone.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }






    public void clickSignin(View v) {

       // trustEveryone();


        if(!magreeTermsncondition.isChecked()){
            showToast("Please agree to our terms and privacy policy");
            return;
        }

        if (!AppUtils.isEmpty(et_uname) && !AppUtils.isEmpty(et_pwd)) {
            String url = URL_LOGIN.replace("<UNAME>", et_uname.getText().toString().trim()).replace("<PWD>", et_pwd.getText().toString().trim());
            volleyGetHit(url, REQUEST_LOGIN);
        }else{
            showToast("Please enter username/password");
        }
    }

//    private void trustEveryone() {
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }});
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, new X509TrustManager[]{new X509TrustManager(){
//                public void checkClientTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public void checkServerTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }}}, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(
//                    context.getSocketFactory());
//        } catch (Exception e) { // should never happen
//            e.printStackTrace();
//        }
//    }


    public void gototermsofuse(View v){
        gototermsofuse();

    }

    public void gotoprivacypolicy(View v){
        gotoprivacypolicy();

    }


    @Override
    protected void onResponse(int request_code, String response, String data) {
        if (request_code == REQUEST_LOGIN) {

            try {
                JSONObject dataObj = new JSONObject(data);
                JSONObject assetObj = JsonUtils.getJsonObjFromJSON(dataObj, "assets");

                SharedPrefrenceHelper.setUpass(bContext,et_pwd.getText().toString().trim());

                SharedPrefrenceHelper.setLvo(bContext, assetObj.toString());
                SharedPrefrenceHelper.setIsLogin(bContext, true);


                LoginVo lvo = getLoginVo();
                int approved = lvo.getIsFBApproved();
                openHome();
//                if (approved == 0){
//                    openStartupnew();
//                }else {
//                    openHome();
//                }


               // openHome();
            } catch (Exception e) {
                e.printStackTrace();
                Slog.e("exception in parsing");
                showToast("Unable to login");
            }
        }else {
            showToast("Unable to login");
        }
    }

    public void clickSignup(View v) {
        gotoSignup();
    }

    public void forgetPassword(View v) {
        gotoForget();
    }

    public void gotoForget() {
        gotoSignup();
       // showToast("Please Contact Fuel Buddy Head Office to Get Your ID and Password !");
        showLogToast("forget clicked");
    }

    public void gotoSignup() {
        showLogToast("signup clicked");
        Intent intent = new Intent(bContext, SignupActivity.class);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public boolean validate() {
        boolean valid = true;

//        String email = phoneNumberET.getText().toString();
        String phoneNumber = et_uname.getText().toString();;
        String password = et_pwd.getText().toString();

        if (phoneNumber.isEmpty()) {
            et_uname.setError("Enter a valid User number!");
            valid = false;
        } else {
            et_uname.setError(null);
        }

        if (password.isEmpty() ) {
            et_pwd.setError("Password Should Not be Left Blank!");
            valid = false;
        } else {
            et_pwd.setError(null);
        }

        return valid;
    }

    public void new_user_registration(View view) {
preetydialog();
        //gotoNewUserRegistrationPhone();

    }

    public void preetydialog(){
        final PrettyDialog dialog = new PrettyDialog(this);

        dialog
                .setTitle("FuelBuddy ")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage("What you want")
                .setAnimationEnabled(true)
                .addButton("Corporate", R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary, new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                gotoNewUserRegistrationPhone();
                                dialog.dismiss();
                            }
                        })
                .addButton("Individual", R.color.pdlg_color_white, R.color.colorPrimary, new PrettyDialogCallback() {
                    @Override
                    public void onClick() {
                        gotoindividual();
                        dialog.dismiss();

                    }
                })
                .show();
//
//
//                .addButton(
//                        "Update",     // button text
//                        R.color.pdlg_color_white,  // button text color
//                        R.color.colorPrimary,  // button background color
//                        new PrettyDialogCallback() {  // button OnClick listener
//                            @Override
//                            public void onClick() {
//
//                                // onResume();
//                                // Dismiss
//
//                                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.wemonde.fuelbuddy"); // missing 'http://' will cause crashed
//                                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                                startActivity(intent);
//                                //finish();
//                                dialog.dismiss();
////                                try {
////                                    UserRegistrationActivity.usa.finish();
////                                }catch (Exception e) {
////                                    e.printStackTrace();
////                                }
//
//
//                                // Do what you gotta do
//                            }
//                        }
//                )
//                .show();

    }
}