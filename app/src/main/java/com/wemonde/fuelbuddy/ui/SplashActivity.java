package com.wemonde.fuelbuddy.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.VideoView;

import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.R;

public class SplashActivity extends BaseActivity implements OnPermissionCallback {

   // ImageView iv1;
    AnimationDrawable Anim;
    VideoView videoHolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // setscreenLayout(R.layout.activity_splash);
       setContentView(R.layout.activity_splash);

      //  gotoHome();
     //  iv1 = (ImageView) findViewById(R.id.list_image);
//
//        ImageView myAnimation = (ImageView)findViewById(R.id.list_image);
//        final AnimationDrawable myAnimationDrawable
//                = (AnimationDrawable)myAnimation.getDrawable();
//
//      //  myAnimationDrawable.start();
//
//
//        try {
//
//
//            myAnimation.post(
//
//
//                    new Runnable() {
//
//                        @Override
//                        public void run() {
//                            myAnimationDrawable.start();
//                        }
//                    });
//            Handler handler = new Handler();
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//
//
//                    gotoHome();
//
//                }
//            };
//
//            handler.postDelayed(r, 3000);
//
//        }catch (Exception e ){
//            //TODO
//            gotoHome();
//        }
//


        try{
            videoHolder = new VideoView(this);
            videoHolder = findViewById(R.id.splashVideoView) ;
//            setContentView(videoHolder);


            Uri video = Uri.parse("android.resource://" + getPackageName() + "/"
                    + R.raw.fb_splash);
            videoHolder.setVideoURI(video);

            videoHolder.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                public void onCompletion(MediaPlayer mp) {
                    jump();
                }

            });
            videoHolder.start();
        } catch(Exception ex) {
            jump();
        }


//        iv1.buildDrawingCache();
//        Bitmap frame1 = iv1.getDrawingCache();

//            BitmapDrawable frame1 = (BitmapDrawable) getResources().getDrawable(
//                    R.drawable.fuel_buddy_logo_00003);
//
//
//            BitmapDrawable frame2 = (BitmapDrawable) getResources().getDrawable(
//                    R.drawable.fuel_buddy_logo_00009);
//            BitmapDrawable frame3 = (BitmapDrawable) getResources().getDrawable(
//                    R.drawable.fuel_buddy_logo_00012);
//        BitmapDrawable frame4 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00015);
//        BitmapDrawable frame5 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00019);
//        BitmapDrawable frame6 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00021);
//        BitmapDrawable frame7 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00025);
//        BitmapDrawable frame8 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00028);
//        BitmapDrawable frame9 = (BitmapDrawable) getResources().getDrawable(
//                R.drawable.fuel_buddy_logo_00029);
//
//        Drawable drawableFrame2 = ((AnimationDrawable)iv1.getBackground()).getFrame(2);
//
//            Anim = new AnimationDrawable();
//            Anim.addFrame(drawableFrame2, 1);
//            Anim.addFrame(frame2, 2);
//            Anim.addFrame(frame3, 3);
//        Anim.addFrame(frame4, 4);
//        Anim.addFrame(frame5, 5);
//        Anim.addFrame(frame6, 6);
//        Anim.addFrame(frame7, 7);
//        Anim.addFrame(frame8, 8);
//        Anim.addFrame(frame9, 9);
//           // Anim.setOneShot(false);
//
//
//
//
//        Anim.start();
//        Handler handler = new Handler();
//        Runnable r = new Runnable() {
//            @Override
//            public void run() {
//
//                gotoHome();
//            }
//        };
//
//        handler.postDelayed(r, 2000);

    }


    private void jump() {
//        if(isFinishing())
//            return;
        gotoHome();
       // finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        gotoHome();
    }

    @Override
    protected void onResume() {
        super.onResume();
      //  gotoHome();

        }
    @Override
    public void onPermissionGranted(String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }
}