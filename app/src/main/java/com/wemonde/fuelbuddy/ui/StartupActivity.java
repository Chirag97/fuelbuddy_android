package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.os.Handler;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

/**
 * Created by Devendra Pandey on 1/4/2018.
 */

public class StartupActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {

                    gotoHome();
                    // getMgrAssets();
//                    getFuelTypes();
                    // getdashboard();
                  //  getBuildings();
                    //getAssetIdentifier();
                    //  getStates();
                    // gettrackingdetails();
                   // hideProgress();
                }
            };
            h.postDelayed(r, 10000);
           // showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
