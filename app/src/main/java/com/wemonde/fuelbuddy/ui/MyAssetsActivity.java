package com.wemonde.fuelbuddy.ui;

import android.animation.Animator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.AssetVerRecAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.willowtreeapps.spruce.Spruce;
import com.willowtreeapps.spruce.animation.DefaultAnimations;
import com.willowtreeapps.spruce.sort.LinearSort;

import org.apache.commons.lang3.text.WordUtils;

import java.util.Random;

import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;

public class MyAssetsActivity extends BaseActivity {

    private RecyclerView rv_asset_ver;
    private Paint p = new Paint();
    public  static  int positionorder = -1;
    private static int assetId = -1;
    private static int capacity = -1;
    private AlertDialog alertDialog;
    private EditText et_country;
    private View view;
    private static int quantityselected = -1;
    private   Dialog dialog ;
    private SeekBar sb_quantity;
    AssetVerRecAdapter mAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_my_assets);
        setBasicToolBar("My Assets");
//        if(mAdapter==null){
//            gotoAddAsset();
//        }
//
//        else {
//



        //changing the color programmatically
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45b879")));

        getMgrAssets();

        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    // getMgrAssets();
//                    getFuelTypes();
                    // getdashboard();
                    getBuildings();

                 //   gettankdetails();
                    //getAssetIdentifier();
                    //  getStates();
                    // gettrackingdetails();
                    hideProgress();
                }
            };
            h.postDelayed(r, 5);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }


        rv_asset_ver =  findViewById(R.id.rv_asset_ver);
        rv_asset_ver.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_asset_ver.setNestedScrollingEnabled(false);
        rv_asset_ver.setHasFixedSize(true);
        rv_asset_ver.addItemDecoration(new SimpleDividerItemDecoration(bContext));
        ContextCompat.getDrawable(bContext,R.drawable.line_divider);


//
//}
    }



    @Override
    protected void onResume() {
        super.onResume();
        getMgrAssets();
//        try {
//            Handler h = new Handler();
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    // getMgrAssets();
//                    getBuildings();
//                    // gettrackingdetails();
//                    hideProgress();
//                }
//            };
//            h.postDelayed(r, 10);
//            showProgress();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        switch (request_code) {
            case REQUEST_GET_ASSETS:
                setAssetAdapter();
                break;
        }
    }

    @Override
    protected void onFailed(int request_code, VolleyError error) {
//        super.onFailed(request_code, error);
        switch (request_code) {
            case REQUEST_GET_ASSETS:
              //  showToast("No Assets Found!");
                //finish();
                break;
        }
    }

    private void setAssetAdapter() {
        if (assets == null) {
            gotoAddAsset();
           // showToast("No Assets Found!");
           // finish();
        }

         mAdapter = new AssetVerRecAdapter(bContext, assets);
        rv_asset_ver.setAdapter(mAdapter);


//        mAdapter.setDataChangeListener(new GestureAdapter.OnDataChangeListener<MonthItem>() {
//            @Override
//            public void onItemRemoved(final MonthItem item, final int position) {
//                final Snackbar undoSnack = Snackbar.make(view, "Month removed from position " + position, Snackbar.LENGTH_SHORT);
//                undoSnack.setAction("", new View.OnClickListener() {
//                    @Override
//                    public void onClick(final View v) {
//                       // mAdapter.undoLast();
//                    }
//                });
//                undoSnack.show();
//            }


        initSwipe();
       // initDialog();
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
                new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT ) {
                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder
                            target) {
                        return false;
                    }
                    @Override
                    public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
//                        showToast("yes ! you got");
//                        int position = viewHolder.getAdapterPosition();
//                        showToast("yes ! you got"+ position);
                        // input.remove(viewHolder.getAdapterPosition());
                        // mAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
                    }
                };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        final int swipeFlags = ItemTouchHelper.RIGHT ;
        itemTouchHelper.attachToRecyclerView(rv_asset_ver);


    }



    private void initSwipe(){
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0,  ItemTouchHelper.RIGHT |ItemTouchHelper.LEFT ) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                 positionorder = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT){

                    getMgrAssets();

                  //   gotoAddAsset();
                    //  adapter.removeItem(position);
                } else {

                    AssetVerRecAdapter mAdapternew = new AssetVerRecAdapter(bContext, assets);
                    mAdapternew.Assetdetails(positionorder);

                    // AssetVerRecAdapter.getbooikingdetails(position);
                    final String assetname = AssetVerRecAdapter.assetname;
                    assetId = AssetVerRecAdapter.assetid;
                    capacity = AssetVerRecAdapter.quantityofasset;

                    showalert(assetname,String.valueOf(assetId));
                    //gotoPlaceOrdernew(assetname,String.valueOf(assetId));

                   // initDialog();

                  //  showToast("Ordernow"+  assetname);
                    //  removeView();
//                    edit_position = position;
                    //alertDialog.setTitle("FuelBuddy");
                  //  et_country.setText("30");
                    //alertDialog.show();
                }
            }

            public void showalert(final String assetidentifier, final String assetname) {

                final AlertDialog.Builder alertDialogBuilder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialogBuilder = new AlertDialog.Builder(bContext);
                } else {
                    alertDialogBuilder = new AlertDialog.Builder(bContext);
                }


                alertDialogBuilder.setCancelable(true);

                alertDialogBuilder.setTitle("FuelBuddy")
                        .setMessage("Do you want to place order for : " + WordUtils.capitalize (assetidentifier))
                        .setPositiveButton("yes",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
//                                String assetidentifier = assetidentifier;
//                                String assetname = marker.getSnippet();
                                        gotoPlaceOrdernew(assetidentifier,assetname);
                                        // showBookingSheetfordirectdelivery(assetidentifier, assetname);


//                                selectedassetinarray = 1;
//                                assetId = 2;
                                        // showBookingSheet();


                                    }
                                });

                alertDialogBuilder.setNegativeButton("No",


                        new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {

                               // selectedassetmapbooking = -1;
                                dismiss();
                                getMgrAssets();

                                // onDialogNo();
                                //  finish();
                                //
                            }
                        });


                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }



            public void dismiss() {
                alertDialog.dismiss();
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE){

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if(dX > 0){
                        p.setColor(Color.parseColor("#388E3C"));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.order_fuel_scaled);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width ,(float) itemView.getTop() + width,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                    else {
                        p.setColor(Color.parseColor("#45b879"));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background,p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.transparent);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                        c.drawBitmap(icon,null,icon_dest,p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);

        final int swipeFlags = itemTouchHelper.RIGHT ;
        itemTouchHelper.attachToRecyclerView(rv_asset_ver);
    }
//    private void removeView(){
//        if(view.getParent()!=null) {
//            ((ViewGroup) view.getParent()).removeView(view);
//        }
//    }



    private void initDialog(){



        dialog = new Dialog(this,R.style.CustomDialog);
       // dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customorderingdialog);
       // dialog.st(R.style.DialogAnimation_2, "Up - Down Animation!");
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

       et_country = (EditText) dialog.findViewById(R.id.et_country);
//        if (et_country.hasFocus()){
//            et_country.setCursorVisible(true);
//        }
        et_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_country.setCursorVisible(true);
            }
        });



        // et_country.setSelection(et_country.getText().length());
       // text.setText("Do You Want To confirm Order For : "+ quantity+ "litres , For the Asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
                getMgrAssets();

            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validate();


//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
               // dialog.cancel();
            }
        });

        dialog.show();
//        BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetID, fuelid,qty);
//        ((BaseActivity) context).gotoOrderSummary(borv);
    }
//        alertDialog = new AlertDialog.Builder(this);
//
//
//        view = getLayoutInflater().inflate(R.layout.dialog_layout,null);
//        alertDialog.setView(view);
//        alertDialog.setCancelable(false);
//        et_country = (EditText)view.findViewById(R.id.et_country);
//        et_country.setText("30");
//
//
//        alertDialog.setPositiveButton("Confirm ", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//
//                validate();
//
//
////                if(add){
////                    add =false;
////                    adapter.addItem(et_country.getText().toString());
////                    dialog.dismiss();
////                } else {
////                    countries.set(edit_position,et_country.getText().toString());
////                    adapter.notifyDataSetChanged();
////                    dialog.dismiss();
////                }
//
//            }
//        });
//        alertDialog.setNegativeButton("Back ", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                getMgrAssets();
//                dialog.cancel();
//            }
//        }).setIcon(R.mipmap.ic_launcher_app_round);




    public void validate(){

        try {
            quantityselected = Integer.parseInt(et_country.getText().toString());
        }catch (Exception e){
            showToast("Please enter quantity");
            return;
        }


        if (capacity  < quantityselected){
            showToast("Ordered capacity is greater than Asset's capacity");
            et_country.setTextColor(Color.parseColor("#FF0000"));

            // setTextColor(Color.parseColor("#8B008B")

            return;
        }



        try {
            try {
                quantityselected = Integer.parseInt(et_country.getText().toString());
            }catch (Exception e){
                showToast("Please enter quantity");
                return;
            }
            if (quantityselected < 1) {
                showToast("Please enter quantity");
                et_country.setTextColor(Color.parseColor("#000000"));
                //    speakOut("Please enter quantity");
                return;
            } else if (quantityselected < 30) {
                showToast("Minimum Limit of Ordering is 30 litres, Please Order Within Limit ");
                et_country.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else if (quantityselected > 5000) {
                showToast("Maximum Limit of Ordering is 5000 litres, Please Order Within Limit ");
                et_country.setTextColor(Color.parseColor("#FF0000"));
                return;
            }else  {
                int uuid = Integer.parseInt(uid);


                int quantity = Integer.parseInt(et_country.getText().toString());
                int fuelid = 1;

                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelid, quantity,"");
                gotoOrderSummary(borv);
                dialog.cancel();

                showToast("Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ");

            }
        }catch (Exception e) {
        }
    }

    public void clickAddAsset(View v) {
        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    // getMgrAssets();
                    getBuildings();
                    // gettrackingdetails();
                    hideProgress();
                }
            };
            h.postDelayed(r, 10);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gotoAddAsset();
    }
}