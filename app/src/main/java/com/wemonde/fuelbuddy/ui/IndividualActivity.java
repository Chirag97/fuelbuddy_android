package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

public class IndividualActivity extends BaseActivity {
    EditText name,emailid,username,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSoftKeyboard();
        setContentView(R.layout.activity_individual);
        initviews();
    }
    void initviews(){
        name=findViewById(R.id.et_name);
        emailid=findViewById(R.id.et_email_id);
        username=findViewById(R.id.et_user_name);
        password=findViewById(R.id.et_user_password);
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
}
