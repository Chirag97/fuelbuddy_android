package com.wemonde.fuelbuddy.ui;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.SpinnerAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.AssetVo;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.wemonde.fuelbuddy.ui.HomeActivity.flagfordirectbooking;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;

/**
 * Created by Devendra Pandey on 11/30/2017.
 */


public class PlaceOrder extends BaseActivity  {

    private Spinner spn_asset;
    private SeekBar sb_quantity;
    private EditText tv_quantity;
    public static int assetId = -1;
    private Button confirm_new;
    private static int fuelId = -1;
    public static float assetfuelcapacity = -1;
    public static int selectedassetinarray = -1;
    private int new_quantity;
    private int quantity;
    private static String  message = null;
    private CoordinatorLayout coordinatorLayout;
    ListView lv;
    LoginVo lvo;

    ListView lv_complete;

    private static int positionofarray = -1;

    String[] city= {
            "Diesel",
           // "Biodiesel",

    };
    String[] ordersummary= {
            "Normal delivery",
            "Schedule delivery",

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.placeorder);


        Bundle bundle = getIntent().getExtras();
         message = bundle.getString("borv");
         String msg = message;
       // showToast(message);

        setBasicToolBar("Place Order");



     //   setAssetSpinnernewone(message);

        //check message here







       sb_quantity = (SeekBar)findViewById(R.id.sb_quantity) ;
        confirm_new = (Button)findViewById(R.id.confirm_new);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .new_coordinator);


        spn_asset = (Spinner) findViewById(R.id.spn_asset_place);
         lv = new ListView(this);

        lv = (ListView)findViewById(R.id.android_list) ;
        lv_complete = (ListView)findViewById(R.id.android_list_new);
        tv_quantity = (EditText) findViewById(R.id.tv_quantity);

       // spn_asset = (Spinner) findViewById(R.id.spn_asset);

        //  lv.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,myList));f ofjgfjgjr
       // setContentView(lv);

        // storing string resources into Array

        sb_quantity.incrementProgressBy(10);

//        lv.setItemChecked(0, true);
//        lv_complete.setItemChecked(0, true);

        SharedPrefrenceHelper.setscheduledate(bContext,"");

        fuelId =1;


       // ListView listview= getListView();
        //	listview.setChoiceMode(listview.CHOICE_MODE_NONE);
        //	listview.setChoiceMode(listview.CHOICE_MODE_SINGLE);
        lv.setChoiceMode(lv.CHOICE_MODE_MULTIPLE);

        //--	text filtering
        lv.setTextFilterEnabled(true);



        lv.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_checked,city));




        lv.setItemChecked(0, true);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                switch (position) {
                    case 0: // first one of the list

                     //   showToast("Diesel"+position);
//                        gotoMyAsset();
                        lv.setItemChecked(0, true);
                        lv.setItemChecked(1, false);
                        fuelId =1;
                        lv.setItemChecked(1, false);
//                        // gotoBuilding();
//                        checkthefloatbutton = 0;
                        break;
                    case 1: // second one of the list.

                       // showToast("Bio Diesel"+position);
                        lv.setItemChecked(1, true);
                        lv.setItemChecked(0, false);
                        fuelId =1;
                        lv.setItemChecked(0, false);
//                        gotoProfile();
//                        checkthefloatbutton = 0;
                        break;

//                                case 6: // Sixth one of the list.
//
//
//                                    break;
                    // and so on...
                }

            }
        });


        lv_complete.setChoiceMode(lv.CHOICE_MODE_MULTIPLE);

        //--	text filtering
        lv_complete.setTextFilterEnabled(true);

        lv_complete.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_checked,ordersummary));


        lv_complete.setItemChecked(0, true);
        lv_complete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                switch (position) {
                    case 0: // first one of the list

                        //   showToast("Diesel"+position);
//                        gotoMyAsset();
                       // fuelId =1;
                        if (lv_complete.isItemChecked(0)){
                            lv_complete.setItemChecked(0, true);
                        }
                        lv_complete.setItemChecked(0, true);
                        lv_complete.setItemChecked(1, false);
                        SharedPrefrenceHelper.setscheduledate(bContext,"");

//                        // gotoBuilding();
//                        checkthefloatbutton = 0;
                        break;
                    case 1: // second one of the list.

                        // showToast("Bio Diesel"+position);
                      //  fuelId =1;
//                        if (lv_complete.isItemChecked(1)){
//                            lv_complete.setItemChecked(1, true);
//                        }
                        lv_complete.setItemChecked(1, true);
                        lv_complete.setItemChecked(0, false);
                        gotoScheduling();
//                        gotoProfile();
//                        checkthefloatbutton = 0;
                        break;

//                                case 6: // Sixth one of the list.
//
//
//                                    break;
                    // and so on...
                }

            }
        });


        int step = 1;
        int max = 180;
        int min = 60;

        LoginVo lvo = getLoginVo();
        int corp = lvo.getCorpID();


          if (  corp== 2272 ) {
              tv_quantity.setText("50");
        } else if (  corp== 2253 ) {
              tv_quantity.setText("1");
        }else if (  corp != 2253  ) {
              tv_quantity.setText("30");
        }



        sb_quantity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
//                quantity = progress;

                progress = progress / 10;
                progress = progress * 10;
             //   seekBarValue.setText(String.valueOf(progress));
                tv_quantity.setText("" + progress);
              //  double value = 30 + (progress * 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


            }
        });


        spn_asset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if( i > 0)
                    assetId = assets.get(i - 1).getAssetID();

                //  assetfuelcapacity = assets.get(i - 1).getAssetFuelCapacity();


                selectedassetinarray = i;
//                if (selectedassetmapbooking == 0){
//                  //  selectedassetmapbooking = 0 ;
//                    selectedassetinarray = 1;
//                    showToast( "selectedassetmapbooking = "+ selectedassetmapbooking);
//                    showToast( "selectedassetinarray = "+ selectedassetinarray);
//
//                }else {
//                    selectedassetmapbooking = 0;
//
//                    showToast( "else selectedassetmapbooking = "+ selectedassetmapbooking);
//                    showToast( "else selectedassetinarray = "+ selectedassetinarray);
//                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (!setAssetSpinner()) {
//            rl_add_booking.setVisibility(GONE);
//            list.setVisibility(GONE);
            return;
        }

        confirm_new.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmbooknew();
            }
        });



//        tv_quantity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_NONE) {
//
//                    getWindow().setSoftInputMode(
//                            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//                   // tv_quantity.setTextColor(Color.parseColor("#000000"));
//
//                    return true;
//                }
//                return false;
//            }
//        });


        tv_quantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
                    //do something
                }
            }
        });

//        tv_quantity.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
//            }
//        });


        if (assets!= null && message.equals("") ) {
            setAssetSpinner();
        }
        //myString != null && !myString.isEmpty()
        else if (msg != null && !msg.isEmpty()) {

            setAssetSpinnernewone(msg);

          //  assetcapacity();


        }
        else {
            getMgrAssets();
        }
    }

    private void setAssetSpinnernewone(String message) {

       // spn_asset.setSelection(1);
        String[] separated = message.split("@&");
      //  separated[0]; // this will contain "Fruit"

        String name = separated[0].trim();





        ArrayList<String> assetLabels12 = new ArrayList<>();
        assetLabels12.add(WordUtils.capitalize (name));
        SpinnerAdapter assetAdapter12 = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels12);
        spn_asset.setAdapter(assetAdapter12);
        int asset_id = Integer.parseInt(separated[1].trim());
//        showToast(separated[1].trim());

        assetId = asset_id;
       // showToast(""+assetId);
       // selectedassetinarray =1;





    }

    public void assetcapacity (){
        for (int i = 0; i < assets.size(); i++) {


            int assetidnew = assets.get(i).getAssetID();
            if (assetidnew == assetId){

              //  int fuelcapacity = assets.get(i).getAssetFuelCapacity();
              //   selectedassetinarray = i;

                // showToast(""+assets.get(i+1).getAssetFuelCapacity() + assets.get(i+1).getAssetS_ID());

                assetfuelcapacity = assets.get(i).getAssetFuelCapacity();
                //  assetfuelcapacity =assets.get(i).getAssetFuelCapacity();
               // positionofarray = i;

            }
            //  drawMarker(ll);








        }
      //  assetfuelcapacity = assets.get(positionofarray).getAssetFuelCapacity();
    }

    public void onAttach(Activity activity) {
        //super.onAttach(activity);
    }
    public void onListItemClick(ListView parent, View v,
                                int position, long id) {
        Toast.makeText(this, "You have selected city : " + city[position],
                Toast.LENGTH_LONG).show();
    }
    private boolean setAssetSpinner() {
        try {
            if (assets == null) {
                showToast("Please Add Assets");
               // bottomNavigationView.setVisibility(GONE);
               // fab.setVisibility(VISIBLE);
                return false;
            }

            ArrayList<String> assetLabels = new ArrayList<>();
            assetLabels.add("Select Asset");
            for (int i = 0; i < assets.size(); i++) {
                assetLabels.add(  assets.get(i).getAssetIdentifierName() + " - " + WordUtils.capitalize (assets.get(i).getAssetS_ID()));
            }
            // Create custom adapter object ( see below CustomAdapter.java )
            SpinnerAdapter assetAdapter = new SpinnerAdapter(bContext, R.layout.item_spinner, assetLabels);
            spn_asset.setAdapter(assetAdapter);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//    @Override
//    protected void onResponse(int request_code, String response, String data) {
//        super.onResponse(request_code, response, data);
//        switch (request_code) {
//            case REQUEST_GET_ASSETS:
//
//                try {
//                    Gson gson = new Gson();
//                    AssetVo assetVo = gson.fromJson(data, AssetVo.class);
//                    assets = assetVo.getAssets();
//                    //HomeActivity.assetsdetails = assets;
//                } catch (Exception e) {
//                    Slog.e("exception in assets -====---  " + e.getMessage());
//                    e.printStackTrace();
//                }
//
//
//                setAssetAdapternew();
//                break;
//        }
//    }




    private void setAssetAdapternew() {

        if (assets == null) {
            assets = new ArrayList<>();
        }
        try {
            new Handler(Looper.getMainLooper()).post(new Runnable() { // Tried new Handler(Looper.myLopper()) also
                @Override
                public void run() {
                  //  currentlocationnew();
                    //  mAdapter.notifyDataSetChanged();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
//        try {
//            Handler h = new Handler();
//            Runnable r = new Runnable() {
//                @Override
//                public void run() {
//                    getMgrAssets();
////                    getFuelTypes();
////                    getBuildings();
////                    //getAssetIdentifier();
////                    getStates();
//                    // gettrackingdetails();
//                    hideProgress();
//                }
//            };
//            h.postDelayed(r, 0);
//            showProgress();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    public void confirmbooknew (){

        tv_quantity.setTextColor(Color.parseColor("#000000"));

        try {

            assetfuelcapacity = assets.get(selectedassetinarray - 1).getAssetFuelCapacity();
        } catch (Exception e) {

            assetcapacity ();
//            assetcapacity();
        }
       // if (selectedassetmapbooking == -1) {

            if (assetId == -1 ) {
                // selectedassetmapbooking = 0;
                showToast("Please select asset");
                return;

            }
       // }
        if (fuelId == -1) {
            showToast("Please select fuel");
            return;
        }
        try {
            new_quantity = Integer.parseInt(tv_quantity.getText().toString());
        } catch (Exception e) {
            showToast("Please enter quantity");
            return;
        }

        if (assetfuelcapacity < new_quantity) {
            showToast("Ordered value is greater than asset's capacity");
            tv_quantity.setTextColor(Color.parseColor("#FF0000"));

            // setTextColor(Color.parseColor("#8B008B")

            return;
        }


        int uuid = Integer.parseInt(uid);


        LoginVo lvo = getLoginVo();
        int corp = lvo.getCorpID();
        try {
            quantity = Integer.parseInt(tv_quantity.getText().toString());
            if (quantity < 1) {
                showToast("Please enter quantity");
                tv_quantity.setTextColor(Color.parseColor("#000000"));
                //    speakOut("Please enter quantity");
                return;
            }

            else if (  corp== 2272 &&  quantity < 50) {
                showToast("Minimum  order quantity is 50 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else if (  corp== 2253 &&  quantity < 1) {
                showToast("Minimum  order quantity is 1 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            }else if (  corp != 2253  &&  corp != 2272 &&  quantity < 30) {
                showToast("Minimum  order quantity is 30 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            }

            else if (quantity > 6000) {
                showToast("Maximum order is 6000 litres");
                tv_quantity.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else {
              //  checkthefloatbutton = 0;
            }

        } catch (Exception e) {
        }


        LoginVo lvo1 = getLoginVo();
        int approved = lvo1.getIsFBApproved();
        Log.d("fuel","approved: "+approved);
        Toast.makeText(getApplicationContext(),"approved: "+approved,Toast.LENGTH_SHORT).show();
        if (approved == 0){

            //Thank you for registering with FuelBuddy. Your dedicated Buddy will be contacting you shortly for completing the KYC formalities and get you on your way to a new and convenient way of ordering fuel.
            showdialogmaterial("Thank you for registering with FuelBuddy.\n" +
                    "Your dedicated Buddy will be contacting you shortly for completing the KYC formalities and get you on your way to a new and convenient way of ordering fuel.");

//            showsnackbarnew ("Thank you for registering with FuelBuddy.\n" +
//                    "Your dedicated buddy will be contacting you soon to verify your details and get you on your way to a new and convenient way of ordering fuel.");
//            showToast("Thank you for registering with FuelBuddy.\n" +
//                    "Your dedicated buddy will be contacting you soon to verify your details and get you on your way to a new and convenient way of ordering fuel.");
            //return;
        }else {

//            BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity,1,"275 /22 gali no 3 sadar bazar",);
//            gotoOrderSummary(borv);

            //  rl_add_booking.setVisibility(View.GONE);
            //  checkthefloatbutton = 0;
            //  showToast("Thank You ! for Ordering, Soon You Will Be Receiving Your Order ");


            try {
                SharedPrefrenceHelper.getscheduledate(bContext);
                // if (flagfordirectbooking == 1) {
                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetId, fuelId, quantity, SharedPrefrenceHelper.getscheduledate(bContext));
                gotoOrderSummary(borv);
                finish();
                //  selectedassetmapbooking = -1;
                Toast.makeText(PlaceOrder.this, "Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ", Toast.LENGTH_LONG).show();
                // speakOut( "Thank you for ordering with Fuel  buddy! You will  be receiving your order Soon ");
//                flagfordirectbooking = 0;
//                rl_add_booking.setVisibility(GONE);
                // sb_quantity.setProgress(30);
                try {
                    tv_quantity.setTextColor(Color.parseColor("#000000"));

                } catch (Exception e) {

                }


                // } else {
                // showalertdynamic(address, pincode);
                // sb_quantity.setProgress(30);

//                try {
//                    tv_quantity.setTextColor(Color.parseColor("#000000"));
//
//                } catch (Exception e) {
//
//                }

                // }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    public void showdialogmaterial (String msg) {


        final PrettyDialog dialog =   new PrettyDialog(this);
        dialog
                .setTitle("Greetings !")
                .setIcon(R.mipmap.circularfb)
                .setIconTint( R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)

                .addButton(
                        "Okay",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {

                                // onResume();
                                // Dismiss
                                dialog.dismiss();
                                // Do what you gotta do
                            }
                        }
                )
                .show();


//
//        new MaterialStyledDialog.Builder(this)
//
//
//                .setDescription(msg)
//                .withDialogAnimation(true)
//                .withDarkerOverlay(true)
//                //.withDialogAnimation(true, Duration.SLOW)
//                .show();


//        new MaterialStyledDialog.Builder(this)
//                .setTitle(" Greetings !")
//
//                .setDescription(msg)
//                .withDialogAnimation(true, Duration.SLOW)
//                .setPositiveText("Okay")
//                .setScrollable(false)
//                .show();
    }


    public void showsnackbarnew (String msg){
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, 12000);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        textView.setTextColor(Color.WHITE);




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        } else {
            textView.setGravity(Gravity.CENTER_HORIZONTAL);
        }

        snackbar.show();

    }

}
