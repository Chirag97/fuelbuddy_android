package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.DataModel;
import com.wemonde.fuelbuddy.base.BaseActivity;

import java.util.ArrayList;

public class ContactActivity extends BaseActivity {

    ImageView callus;
    Button call ;
    ArrayList dataModels;
    ListView listView;
    private CustomAdapter adapter;
    private static String subjectofmail;

    protected TextView privacypolicy;
    protected TextView termsofuse;

    private  Button newbutton ;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatuscalling;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT = 100;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setscreenLayout(R.layout.activity_contact);
//        setBasicToolBar("My Assets");
//
//        setContentView(R.layout.activity_contact);

        setBasicToolBar("Contact Us");

        permissionStatuscalling = getSharedPreferences("permissionStatuscalling",MODE_PRIVATE);

        //changing the color programmatically
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45b879")));


        callus = (ImageView)findViewById(R.id.mail);

        newbutton = (Button)findViewById(R.id.mail_new_button);

        privacypolicy =(TextView)findViewById(R.id.privacy_policy);
        termsofuse = (TextView)findViewById(R.id.terms_of_use);

        Animation bottomUp123 = AnimationUtils.loadAnimation(bContext,
                R.anim.bottom_up);
        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
        newbutton.startAnimation(bottomUp123);
       // call = (Button)findViewById(R.id.call);


        Animation bottomUp1234 = AnimationUtils.loadAnimation(bContext,
                R.anim.fade_in);
        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
        callus.startAnimation(bottomUp1234);
        callus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callus();
                checkpermission();

            }
        });




        listView = (ListView) findViewById(R.id.listView);

        dataModels = new ArrayList();

        dataModels.add(new DataModel("Did not receive OTP", false));
        dataModels.add(new DataModel("Cancellation refund/ wrongly charged", false));
        dataModels.add(new DataModel("Change in time scheduled", false));
        dataModels.add(new DataModel("Order delayed", false));
        dataModels.add(new DataModel("Change in quantity", false));
        dataModels.add(new DataModel("Unable to track driver", false));
        dataModels.add(new DataModel("Invoice not received", false));
        dataModels.add(new DataModel("Pricing related query", false));
        dataModels.add(new DataModel("Fuel spill and leakage", false));
        dataModels.add(new DataModel("Excess amount deducted", false));
        dataModels.add(new DataModel("Damage to the vehicle", false));
        dataModels.add(new DataModel("Other Issues", false));
//        dataModels.add(new DataModel("Marshmallow", false));
//        dataModels.add(new DataModel("Nougat", false));

        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView .setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                DataModel dataModel= (DataModel) dataModels.get(position);



//                for (int i = 0; i< dataModels.size();i++){
//                    dataModel(i)
//                }
                if ( dataModel.checked){
                    subjectofmail = "";



                    dataModel.checked = false;








//                    for(dataModels.)
//                    {
//                        planet.setChecked(false);
//                    }

//                    ( (DataModel) ((DataModel) dataModels.get(position)).checked)
                   // (DataModel) dataModels.get(position).name, false);
                }else {
                    dataModel.checked = !dataModel.checked;
                    subjectofmail = ((DataModel) dataModels.get(position)).name;

                    if (subjectofmail.equals("Other Issues")){
                        subjectofmail = "";
                    }


                    for (int i = 0 ; i < dataModels.size();i++){
                        DataModel dataModel1= (DataModel) dataModels.get(i);

                        if (dataModel == dataModel1){
                            dataModel1.checked = true;
                        }else {
                            dataModel1.checked = false;
                        }
                    }


                   // dataModel1.checked = false;


                  //  listView.setItemChecked(listView.getCheckedItemPosition(), true);

                   // listView.setItemChecked(5, true);
                    // listView.getCheckedItemPosition();

                    //  showToast(name);
                }
                adapter.notifyDataSetChanged();


            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                switch(scrollState) {
                    case 2: // SCROLL_STATE_FLING
                        //hide button here
//                        Animation bottomUp = AnimationUtils.loadAnimation(bContext,
//                                R.anim.bottom_down);
//                       // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
//                        newbutton.startAnimation(bottomUp);
//                      //  hiddenPanel.setVisibility(View.VISIBLE);
//
//
//                        newbutton.setVisibility(View.GONE);
                        break;

                    case 1: // SCROLL_STATE_TOUCH_SCROLL
//                        Animation bottomUp1 = AnimationUtils.loadAnimation(bContext,
//                                R.anim.bottom_down);
//                        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
//                        newbutton.startAnimation(bottomUp1);
//                        //hide button here
//                        newbutton.setVisibility(View.GONE);
                        break;

                    case 0: // SCROLL_STATE_IDLE
//                        Animation bottomUp12 = AnimationUtils.loadAnimation(bContext,
//                                R.anim.bottom_up);
//                        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
//                        newbutton.startAnimation(bottomUp12);
//                        //show button here
//                        newbutton.setVisibility(View.VISIBLE);
//                        break;

                    default:
                        //show button here
                        Animation bottomUp123 = AnimationUtils.loadAnimation(bContext,
                                R.anim.bottom_up);
                        // ViewGroup hiddenPanel = (ViewGroup)findViewById(R.id.mail_new_button);
                        newbutton.startAnimation(bottomUp123);
                        newbutton.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });

        newbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mailus ();
            }
        });

        privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoprivacypolicy();
            }
        });

        termsofuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gototermsofuse();
            }
        });

    }



    private void

    mailus (){
        {
            Log.i("Send email", "");
            String[] TO = {"support@fuelbuddy.in"};
           // String[] CC = {"dpandey1989@gmail.com"};
            String[] CC = {"testingtries@gmail.com"};
            Intent emailIntent = new Intent(Intent.ACTION_SEND);

            emailIntent.setData(Uri.parse("mailto:"));
            emailIntent.setType("text/plain");
            emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
            emailIntent.putExtra(Intent.EXTRA_CC, CC);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subjectofmail);
            emailIntent.putExtra(Intent.EXTRA_TEXT, " ");

            try {
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                //finish();
                // Log.i("Finished sending email...", "");
            } catch (android.content.ActivityNotFoundException ex) {
                // Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void callus () {
        {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            //callIntent.setData(Uri.parse("tel:9871094876"));
           // +91 9354021411
            callIntent.setData(Uri.parse("tel:9354021411"));
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //   startActivity(calIntent);


//        Intent callIntent = new Intent(Intent.ACTION_CALL);
//        callIntent.setData(Uri.parse("tel:9871094876"));

            if (ActivityCompat.checkSelfPermission(ContactActivity.this,
                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            startActivity(callIntent);
        }
    }

    private void checkpermission() {
        if (ActivityCompat.checkSelfPermission(ContactActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ContactActivity.this, Manifest.permission.CALL_PHONE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
                builder.setTitle("Need Calling Permission");
                builder.setMessage("This app needs Calling permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(ContactActivity.this, new String[]{Manifest.permission.CALL_PHONE}, EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatuscalling.getBoolean(Manifest.permission.CALL_PHONE,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ContactActivity.this);
                builder.setTitle("Need Calling Permission");
                builder.setMessage("This app needs Calling permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant calling", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(ContactActivity.this, new String[]{Manifest.permission.CALL_PHONE}, EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatuscalling.edit();
            editor.putBoolean(Manifest.permission.CALL_PHONE,true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.
            // proceedAfterPermission();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // gettrackingdetails();

//        if (track!= null)
//            for (int i = 0 ; i<=track.size(); i++){
//                newceck = track.get(i).getAssetS_ID();
//
//            }
//        showToast(newceck);

    }
}
