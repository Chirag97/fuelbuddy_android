package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.LoginVo;

/**
 * Created by Devendra Pandey on 6/27/2017.
 */



public class Dashboardui extends BaseActivity {


    private WebView myWebView;
    LoginVo lvo;

    String historyUrl="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.webviewxml);

        setBasicToolBar("Dash Board");

        //changing the color programmatically
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45b879")));
        myWebView = (WebView) findViewById(R.id.webview);


        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);


        LoginVo lvo = getLoginVo();
        String id = lvo.getPMgrID();
        String url =  "https://www.live.fuelbuddyapp.com/FBSecure/PMDashBoard.aspx?width=600&PMgrID="+id;


        WebBackForwardList mWebBackForwardList = myWebView.copyBackForwardList();
        if (mWebBackForwardList.getCurrentIndex() > 0)
            historyUrl = mWebBackForwardList.getItemAtIndex(mWebBackForwardList.getCurrentIndex()-1).getUrl();
        myWebView.setWebViewClient(new WebViewClient());
        //myWebView.loadUrl("http://fuelbuddyapp.com/corporate/PMDashBoard.aspx?width=600&PMgrID=1"+ shargetUUID);



       // javascript:(function(){document.getElementById('mA').click();})()

//        myWebView.loadUrl("http://fuelbuddyapp.com/corporate/PMDashBoard.aspx?width=600&PMgrID="+id+
//                "e=document.createEvent('HTMLEvents');"+
//                "e.initEvent('click',true,true);"+
//                "l.dispatchEvent(e);"+
//                "})()");
     //   myWebView.loadDataWithBaseURL("http://fuelbuddyapp.com/corporate/PMDashBoard.aspx?width=600&PMgrID="+id, "data", "text/html", "utf-8", null);
       myWebView.loadUrl("https://www.live.fuelbuddyapp.com/FBSecure/PMDashBoard.aspx?width=600&PMgrID="+id);





        // myWebView.setWebViewClient(new MyWebViewClient());

    }







    public void onBackPressed() {
                    if (myWebView.canGoBack()) {
                        myWebView.goBack();
                    } else {
                        finish();
                    }

            }



    }
