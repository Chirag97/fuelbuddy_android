package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.view.View;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

public class LoginSignupActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
    }

    public void clickSignin(View v) {
        gotoLogin();
    }

    public void clickSignup(View v) {
        gotoSignup();
    }
}