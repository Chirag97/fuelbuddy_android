package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.ChangePasswordRequast;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_OTP_REQUEST;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_RESET_PASSWORD;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ASSETS;
import static com.wemonde.fuelbuddy.ui.ProfileActivity.newpssword;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_OTP_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_RESET_PASSWORD_CHANGE;

public class SignupActivity extends BaseActivity implements OnPermissionCallback {

    private EditText username;
    private EditText mobilenumber;
    private EditText otp;
    private EditText newpassword;
    private EditText confirmpassword;
    private BroadcastReceiver receiver;
    private static String otprecevied;
    private static String pass;
    private Button btn_sign_in;

    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT = 100;
    private SharedPreferences permissionStatusreadsms;
    private boolean sentToSettings = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        hideSoftKeyboard();

        username = (EditText)findViewById(R.id.user_name);
        mobilenumber = (EditText)findViewById(R.id.mobile_number);
        btn_sign_in = (Button)findViewById(R.id.btn_signin);

        permissionStatusreadsms = getSharedPreferences("permissionStatussms",MODE_PRIVATE);

       // checkpermission();
        btn_sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                // Toast.makeText(getApplicationContext(),"Send OTP is "  , Toast.LENGTH_SHORT).show();
                gethit();
            }
        });

         receiver  = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equalsIgnoreCase("otp")) {
                    final String message = intent.getStringExtra("message");

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                otp.setText(message);
                                showToast("Otp is valid for 60 sec only");

                            }catch (Exception e ){
                                e.printStackTrace();
                            }

                            //Do something after 100ms
                        }
                    }, 100);

                    //Do whatever you want with the code here
                }
            }
        };
    }





    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                mobilenumber.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }
    public void clickSignup(View v){

        checkpermission();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

       // Toast.makeText(getApplicationContext(),"Send OTP is "  , Toast.LENGTH_SHORT).show();
        gethit();


    }

    public  void otpdialog (){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.otp_layout_new);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        otp = (EditText) dialog.findViewById(R.id.otp_textbox);
      //  final EditText text1 = (EditText) dialog.findViewById(R.id.password);
        //  text.setText("Do You Want To confirm Order For : "+ quantity+ "litres , For the Asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                oldpassord = text.getText().toString();
//                newpssword = text1.getText().toString();
//
//
//
//                String imgSett = SharedPrefrenceHelper.getUPass(context);

               // !userEmail.equals("null")

                String imgSett = otp.getText().toString().trim();
                if(!imgSett.isEmpty()){
                    passwordconfirmdialog();
                }else {
                    showToast("Please enter otp !");
                    return;
                }




//                BookOrderRequestVo borv = new BookOrderRequestVo(uid, assetID, fuelid,qty);
//                ((BaseActivity) context).gotoOrderSummary(borv);
////
////                        BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uid, assetID, fuelid,qty,Stateid,location,Latitude,Longitude,Pincode);
////        ((BaseActivity) context).gotoOrderSummaryfordynamicaddress(borv);
//
//                HomeActivity.flagfordynamicbooking = 1;
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();
    }


    private void passwordconfirmdialog (){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.new_password_enter_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        newpassword = (EditText)dialog.findViewById(R.id.password_new);
        confirmpassword = (EditText) dialog.findViewById(R.id.new_password_confirm);
        //  final EditText text1 = (EditText) dialog.findViewById(R.id.password);
        //  text.setText("Do You Want To confirm Order For : "+ quantity+ "litres , For the Asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


              String  oldpassord = newpassword.getText().toString();
               String newpssword = confirmpassword.getText().toString();

              //  (userEmail != null && !userEmail.isEmpty() && !userEmail.equals("null")


                if((oldpassord.equals(newpssword)) && !oldpassord.equals("null") &&!newpssword.equals("null") ){
                    try {
                        pass =newpssword;
                        sendjson();
                    }catch (Exception e ){

                    }

                }else {
                    showToast("Password didn't match");
                    return;
                }
//
//
//
//                String imgSett = SharedPrefrenceHelper.getUPass(context);


              //  sendjson();


//                BookOrderRequestVo borv = new BookOrderRequestVo(uid, assetID, fuelid,qty);
//                ((BaseActivity) context).gotoOrderSummary(borv);
////
////                        BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uid, assetID, fuelid,qty,Stateid,location,Latitude,Longitude,Pincode);
////        ((BaseActivity) context).gotoOrderSummaryfordynamicaddress(borv);
//
//                HomeActivity.flagfordynamicbooking = 1;
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();

    }

    private void gethit (){





                    String ed_text = mobilenumber.getText().toString().trim();
                    String ed_text_username = username.getText().toString().trim();

        if (ed_text_username.isEmpty() || ed_text_username.length() == 0 || ed_text_username.equals("") || ed_text_username == null){
            showToast("Please enter user name");
            return;


                    }else  if(ed_text.isEmpty() || ed_text.length() == 0 || ed_text.equals("") || ed_text == null ||ed_text.length() <= 9 ) {
            showToast("Enter valid mobile number");
            return;
        }
        else {

            String url = URL_FOR_OTP_REQUEST.replace("<UID>", username.getText().toString().trim()).replace("<PHONE>",mobilenumber.getText().toString().trim());
            volleyGetHit(url, REQUEST_OTP_PASSWORD_CHANGE);


//            final Handler handler = new Handler();
//            handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                    try {
//
//
//                        if (checkotp.equals("yes") || checkotp.equals("") || checkotp == null || checkotp.isEmpty()) {
//                            otpdialog();
//                            checkotp = "no";
//                        } else {
//                            showToast("Please try again");
//                        }
//
//                    }catch (Exception e){
//                        showToast("Something went wrong");
//                    }
//
//                    //Do something after 100ms
//                }
//            }, 2000);
//




            }





                //Do something after 100ms


       //
    }


    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        if(request_code == REQUEST_OTP_PASSWORD_CHANGE){

            otpdialog();

        }else if (request_code == REQUEST_RESET_PASSWORD_CHANGE){
            showoverlaydialogpassword("Password Changed");
        }
    }




    public void showoverlaydialogpassword (String msg){


        final PrettyDialog dialog = new PrettyDialog(this);
        dialog
                .setTitle("FuelBuddy ")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)

                .addButton(
                        "Okay",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {


                                // onResume();
                                // Dismiss

                                finish();
                                dialog.dismiss();
                                // Do what you gotta do
                            }
                        }
                )
                .show();

    }
    private void sendjson (){
        String json = preparejson();
        if (json.length() <= 0) return;
        String url = URL_FOR_RESET_PASSWORD;
       // SharedPrefrenceHelper.setUpass(context,newpssword);
        volleyBodyHit(url, json, REQUEST_RESET_PASSWORD_CHANGE);
      //  showToast("Password Changed");


//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                try {
//
//
//                    goback();
//
//                }catch (Exception e){
//                    showToast("Something Went Wrong");
//                }
//
//                //Do something after 100ms
//            }
//        }, 2000);

    }



    private String preparejson (){
        try {
            ChangePasswordRequast avo = new ChangePasswordRequast();
            avo.setKeyID(username.getText().toString().trim());
            avo.setOldKey(otp.getText().toString().trim());
            avo.setNewKey(pass);


            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public void goback(){
        try {
            finish();
        }catch (Exception e ){
            e.printStackTrace();
        }
      //  this.overridePendingTransition(R.anim.stay_still, R.anim.animation_exit_right);

    }

    @Override
    public void onResume() {
        //trustEveryone();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter("otp"));
        super.onResume();




    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }


    private void checkpermission() {
        if (ActivityCompat.checkSelfPermission(SignupActivity.this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(SignupActivity.this, Manifest.permission.READ_SMS)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                builder.setTitle("Need SMS Permission");
                builder.setMessage("This app needs SMS permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(SignupActivity.this, new String[]{Manifest.permission.CAMERA}, EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatusreadsms.getBoolean(Manifest.permission.READ_SMS,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(SignupActivity.this);
                builder.setTitle("Need Read SMS Permission");
                builder.setMessage("This app needs Read SMS permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant READ SMS Permission", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(SignupActivity.this, new String[]{Manifest.permission.READ_SMS}, EXTERNAL_STORAGE_SMS_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatusreadsms.edit();
            editor.putBoolean(Manifest.permission.READ_SMS,true);
            editor.commit();


        } else {
        //You already have the permission, just go ahead.
        // proceedAfterPermission();
    }
}

    @Override
    public void onPermissionGranted(String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }

//    private void trustEveryone() {
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }});
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, new X509TrustManager[]{new X509TrustManager(){
//                public void checkClientTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public void checkServerTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }}}, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(
//                    context.getSocketFactory());
//        } catch (Exception e) { // should never happen
//            e.printStackTrace();
//        }
//    }
}