package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.images.ImageManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.models.DirectionsJSONParser;
import com.wemonde.fuelbuddy.models.Tracking;
import com.wemonde.fuelbuddy.utils.Slog;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.wemonde.fuelbuddy.base.Apis.URL_GET_BUILDS_ALL;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_TRACKING_DETAILS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ALL_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_TRACKING;

/**
 * Created by Devendra Pandey on 8/4/2017.
 */

public class TrackingActivity extends BaseActivity implements OnMapReadyCallback, OnPermissionCallback {


    private MapFragment mapFragment;

    private double workLatitude;
    private double workLongitude;

    private double destinationLatitude;
    private double destinationLongitude;
    private String newceck;
    private TextView veh_number;
    private TextView driver_name;
    private TextView contact_number;
    private TextView current_address;
    private TextView destination_address;
    private ImageView callingbutton;
    protected static GoogleMap googleMap;
    private   Handler handler;


    private SharedPreferences permissionStatuscalling;
    private boolean sentToSettings = false;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT = 100;


    protected ArrayList markerPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setscreenLayout(R.layout.trackingactivity);
        //  setContentView(R.layout.trackingactivity);

        setBasicToolBar("Track Order");



        permissionStatuscalling = getSharedPreferences("permissionStatuscalling",MODE_PRIVATE);
        //changing the color programmatically
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#45b879")));


        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //googleMap.getMapAsync(this);

        initViews();

        if (track != null)
            for (int i = 0; i < track.size(); i++) {
                newceck = track.get(i).getLocation();
                workLatitude = track.get(i).getFExecCurrLatitude();
                workLongitude = track.get(i).getFExecCurrLongitude();

                destinationLatitude = track.get(i).getLatitude();
                destinationLongitude = track.get(i).getLongitude();

            }
        //   showToast(newceck);

        addtrackingdetails();


        try {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    gettrackingdetails();

                    onMapReady(googleMap);
                    if (track != null)
                        for (int i = 0; i < track.size(); i++) {
                            Toast.makeText(getApplicationContext(),"Track:",Toast.LENGTH_SHORT).show();
                            newceck = track.get(i).getLocation();
                            workLatitude = track.get(i).getFExecCurrLatitude();
                            workLongitude = track.get(i).getFExecCurrLongitude();

                            destinationLatitude = track.get(i).getLatitude();
                            destinationLongitude = track.get(i).getLongitude();

                        }
                    // showToast(newceck);

                    addtrackingdetails();
                    hideProgress();
                    //Do something after 20 seconds
                    handler.postDelayed(this, 10000);
                }
            }, 200);  //the time is in miliseconds


        }catch (Exception e){
            e.printStackTrace();
        }

        //currentlocationnew();

    }


    private void initViews() {


        veh_number = (TextView) findViewById(R.id.veh_number);
        contact_number = (TextView) findViewById(R.id.contact_number);
        driver_name = (TextView) findViewById(R.id.driver_name);
        current_address = (TextView) findViewById(R.id.current_address);

        destination_address = (TextView) findViewById(R.id.destination_address);
        callingbutton = (ImageView) findViewById(R.id.calling_button);
        //  et_lng = (EditText) findViewById(R.id.et_lng);
        // onMapReady(googleMap);
    }

    private void addtrackingdetails() {

        if (track != null)
            for (int i = 0; i < track.size(); i++) {
                newceck = track.get(i).getAssetS_ID();

                veh_number.setText(track.get(i).getFExecVNumber());
                Toast.makeText(getApplicationContext(),track.get(i).getFExecVNumber(),Toast.LENGTH_SHORT).show();
                contact_number.setText(track.get(i).getFExecPhone1());
                driver_name.setText(track.get(i).getFExecName());
                current_address.setText("Delivery Address:"  + track.get(i).getLocation());
                destination_address.setText("Updating Distance and ETA...Please Wait"
                );


            }
        LatLng origin = new LatLng(workLatitude, workLongitude);
        LatLng dest = new LatLng(destinationLatitude, destinationLongitude);


        // Getting URL to the Google Directions API
        String url = getDirectionsUrl(origin, dest);

        DownloadTask downloadTask = new DownloadTask();

// Start downloading json data from Google Directions API
        downloadTask.execute(url);


        callingbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                checkpermission();
                call();
            }
        });

    }

    private void call() {

        String number = contact_number.getText().toString();
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + number));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    private void checkpermission() {
        if (ActivityCompat.checkSelfPermission(TrackingActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(TrackingActivity.this, Manifest.permission.CALL_PHONE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
                builder.setTitle("Need Calling Permission");
                builder.setMessage("This app needs Calling permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(TrackingActivity.this, new String[]{Manifest.permission.CALL_PHONE}, EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatuscalling.getBoolean(Manifest.permission.CALL_PHONE,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(TrackingActivity.this);
                builder.setTitle("Need Calling Permission");
                builder.setMessage("This app needs Calling permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant calling", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(TrackingActivity.this, new String[]{Manifest.permission.CALL_PHONE}, EXTERNAL_STORAGE_CALLING_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatuscalling.edit();
            editor.putBoolean(Manifest.permission.CALL_PHONE,true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.
            // proceedAfterPermission();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        // gettrackingdetails();

//        if (track!= null)
//            for (int i = 0 ; i<=track.size(); i++){
//                newceck = track.get(i).getAssetS_ID();
//
//            }
//        showToast(newceck);

    }

    @Override
    public void onDestroy() {
        try {



            handler.removeCallbacksAndMessages(null);
            mapFragment.onDestroy();

        } catch (Exception e) {

            e.printStackTrace();
//            Slog.e("exception in map fragment " + e.getMessage());
        }
        super.onDestroy();
    }

//    @Override
//    protected void onResponse(int request_code, String response, String data) {
////        super.onResponse(request_code, response, data);
//        Gson gson = new Gson();
//        switch (request_code) {
//            case REQUEST_GET_TRACKING:
//                try {
//                    Type type = new TypeToken<ArrayList<Tracking>>() {}.getType();
//                    track = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
//
//                   // setBuildAdapter();
//                } catch (Exception e) {
//                    showToast("No Building found!");
//                    e.printStackTrace();
//                }
//                break;
//
//        }
//    }
//
//    @Override
//    protected void onFailed(int request_code, VolleyError error) {
////        super.onFailed(request_code, error);
//        switch (request_code) {
//            case REQUEST_GET_TRACKING:
//                showToast("No Tracking Details  Found!");
//                finish();
//                break;
//        }
//    }


    @Override
    public void onPermissionGranted(String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }


    public void currentlocationnew() {
        try {


            if (track.size() != 0) {

                track.size();

                // assetsdetails.size();
                // showToast("Size is" + assets.size());
                for (int i = 0; i < track.size(); i++) {
                    LatLng ll = new LatLng(track.get(i).getFExecCurrLatitude(), track.get(i).getFExecCurrLongitude());

                    //  drawMarker(ll);


                    final Marker marker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(track.get(i).getFExecCurrLatitude(), track.get(i).getFExecCurrLongitude()))
                            .title(track.get(i).getLocation())
                            .snippet(String.valueOf(track.get(i).getPINCode()))


                    );

                    // Addressofassetdirectorder = assets.get(i).getAssetLocation();


                    marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.movable_asset_location));


                    //  googleMap.setOnMarkerClickListener(this);


                }
            }else {
                //  showToast("No Assets To Show !");
            }
            // googleMap.setOnMarkerClickListener(this);




        }catch (Exception e){
            e.printStackTrace();

        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(DELHI));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(DELHI)      // Sets the center of the map to Mountain View
                .zoom(15)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder

        googleMap.moveCamera( CameraUpdateFactory.newLatLngZoom(new LatLng(workLatitude,workLongitude) , 14.0f) );


        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        if (status != ConnectionResult.SUCCESS) {
            ///if play services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            // Enabling MyLocation Layer of Google Map
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            googleMap.setTrafficEnabled(true);
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    getGpsLoc();
                    return false;
                }
            });



            LatLng sydney = new LatLng(workLatitude, workLongitude);
            googleMap.addMarker(new MarkerOptions().position(sydney)

                    .title("Current Field Exec location"));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


            LatLng destination = new LatLng(destinationLatitude, destinationLongitude);
            googleMap.addMarker(new MarkerOptions().position(destination)
                    .title("Delivery Location" ));
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
            //googleMap.getUiSettings().setZoomControlsEnabled(true);
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    getGpsLoc();
                    hideProgress();
                }
            };

            h.postDelayed(r, 1000);
            showProgress();

        }
    }

    private String getDirectionsUrl(LatLng origin,LatLng dest){

// Origin of route
        String str_origin = "origin="+origin.latitude+","+origin.longitude;

// Destination of route
        String str_dest = "destination="+dest.latitude+","+dest.longitude;

// Sensor enabled
        String sensor = "sensor=false";

// Building the parameters to the web service
        String parameters = str_origin+"&"+str_dest+"&"+sensor;

// Output format
        String output = "json";

// Building the url to the web service


        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters + "&key=" + "AIzaSyDNEIQ7Q6zj8VvNfA54zzs8A4Ln6wbPrBs";
       // String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;

        return url;
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

// Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

// Connecting to url
            urlConnection.connect();

// Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while( ( line = br.readLine()) != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
            // Log.d("Exception while downloading url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

// For storing data from web service
            String data = "";

            try{
// Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
// doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

// Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

// Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }







        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if(result.size()<1){
                // Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

// Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                PolylineOptions rectLine = new PolylineOptions();

// Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

// Fetching all the points in i-th route
                for(int j=0;j <path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    if(j==0){ // Get distance from the list
                        distance = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        duration = (String)point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

// Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.BLUE);

            }

            destination_address.setText("Distance:  "+distance + "       ETA : "+duration);

// Drawing polyline in the Google Map for the i-th route
            googleMap.addPolyline(lineOptions);
        }
    }

    @Override
    public void onBackPressed() {
        finish();

    }
}
