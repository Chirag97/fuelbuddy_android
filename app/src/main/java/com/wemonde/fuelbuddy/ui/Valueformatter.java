package com.wemonde.fuelbuddy.ui;

import android.os.DropBoxManager;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.text.DecimalFormat;

/**
 * Created by Devendra Pandey on 12/26/2017.
 */

public class Valueformatter implements ValueFormatter {

    private DecimalFormat mFormat;

    public Valueformatter() {
        mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
    }

//    @Override
//    public String getFormattedValue(float value, DropBoxManager.Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
//        // write your logic here
//        return mFormat.format(value) + " $"; // e.g. append a dollar-sign
//    }



    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        String s = Float.toString(value);
        return s;
    }
}