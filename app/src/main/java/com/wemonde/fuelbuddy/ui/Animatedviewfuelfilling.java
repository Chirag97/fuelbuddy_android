package com.wemonde.fuelbuddy.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.gelitenight.waveview.library.WaveView;
import com.google.gson.Gson;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.WaveHelper;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AddTokenRequest;
import com.wemonde.fuelbuddy.models.Getorderstatus;
import com.wemonde.fuelbuddy.utils.Slog;

import pl.tajchert.waitingdots.DotsTextView;

import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_ORDER_FILLING;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_POSTING_TOKEN;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_ORDER_STATUS_NEW;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TOKEN_REGISTRATION;

/**
 * Created by Devendra Pandey on 1/22/2018.
 */


public class Animatedviewfuelfilling extends BaseActivity {

    private int lastPosition = -1;
    private WaveHelper mWaveHelper;
    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private   Handler handler;
    private Handler h;
    public  TextView progress;

    private static int orderstatus = 0;
    private DotsTextView  dotstextview;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.animated_view);



        Bundle bundle = getIntent().getExtras();
        // message = bundle.getString("borv");
        //  String msg = message;
        // showToast(message);

        setBasicToolBar("Order Filling in Progress");



        progress =(TextView)findViewById(R.id.progress_text_view);

        dotstextview = (DotsTextView)findViewById(R.id.dots);

        dotstextview.showAndPlay();
        final WaveView waveView = (WaveView)findViewById(R.id.wave);
        waveView.setWaveColor(
                Color.parseColor("#ffffff"),
                Color.parseColor("#45b879"));
        mBorderColor = Color.parseColor("#0c2f33");
        waveView.setBorder(5, mBorderColor);
        waveView.setShapeType(WaveView.ShapeType.SQUARE);
        mWaveHelper = new WaveHelper(waveView);
        mWaveHelper.start();



        try {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    String json = preparejson();
                    if (json.length() <= 0) return;
                    String url = URL_FOR_ORDER_FILLING;
                   // gettrackingdetails();
                    volleyBodyHit(url, json, REQUEST_ORDER_STATUS_NEW);

                    mWaveHelper = new WaveHelper(waveView);
                    mWaveHelper.start();


                  //  onMapReady(googleMap);
                    if (orderstatusdetails != null)
                        for (int i = 0; i < orderstatusdetails.size(); i++) {
                            orderstatus = orderstatusdetails.get(i).getOrderStatusID();


                        }

                    // showToast(newceck);



                   // addtrackingdetails();
                    hideProgress();
                    //Do something after 20 seconds



                    handler.postDelayed(this, 3000);
                }
            }, 2000);  //the time is in miliseconds


        }catch (Exception e){
            e.printStackTrace();
        }



    }


    public void waitforsometime (){

        progress.setText("    Fuel Delivery Completed   ");

        dotstextview.hideAndStop();

        h.removeCallbacksAndMessages(null);
        handler.removeCallbacksAndMessages(null);
        getsensors();



        try {
            Handler hand = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {



                    gotoOrdercompletionfinal();


                    // getMgrAssets();

                  //  getsensors();
                    // gettrackingdetails();
                    hideProgress();
                }
            };
            hand.postDelayed(r, 3000);
           // showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

//        if (orderstatus == 9){
//            showToast("Fuel Delivery is in progress ... ");
//        }else {
//            finish();
//        }
//        // gettrackingdetails();
//        hideProgress();




        try {
              h   = new Handler();
        h.postDelayed(new Runnable() {
            @Override
            public void run() {

                        if (orderstatus == 9){
                            waitforsometime ();

                            handler.removeCallbacksAndMessages(null);
                          //  gotoOrdercompletionfinal();
                            h.removeCallbacksAndMessages(null);

           // showToast("Fuel Delivery is in progress ... ");
                            showToast("Fuel Delivery Completed ");
        }else {
            finish();
        }
        // gettrackingdetails();


                hideProgress();
                //Do something after 20 seconds
                h.postDelayed(this, 5000);
            }
        }, 15000);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    private String preparejson() {
        try {


            //    {
//        "PMgrID": 0,
//            "OrderID": 2,
//            "OrderItemID": 0,
//            "OrderStatusID": 0,
//            "CompactDataLevel": 1,
//            "PageNo": 1,
//            "RecordPerPage": 20
//    }




            Getorderstatus avo = new Getorderstatus();
            avo.setPMgrID(uid);
            avo.setOrderID(4);
            avo.setOrderItemID(0);
            avo.setCompactDataLevel(0);
            avo.setPageNo(1);
            avo.setRecordPerPage(20);


            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        h.removeCallbacksAndMessages(null);
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public void onBackPressed() {
        h.removeCallbacksAndMessages(null);
        handler.removeCallbacksAndMessages(null);
        finish();

    }
}
