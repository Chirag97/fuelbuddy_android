package com.wemonde.fuelbuddy.ui;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;
import com.wemonde.fuelbuddy.ConstantClass;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.AssetHorRecAdapter;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.AddTokenRequest;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.AssetVo;
import com.wemonde.fuelbuddy.models.BaseVo;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.ChangePasswordRequast;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.models.MultipartRequest;
import com.wemonde.fuelbuddy.models.State;
import com.wemonde.fuelbuddy.models.User;
import com.wemonde.fuelbuddy.models.VolleyMultipartRequest;
import com.wemonde.fuelbuddy.models.VolleySingleton;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.JsonUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;

import org.apache.commons.lang3.text.WordUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import static android.R.attr.data;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_IMAGE_DOWNLOADING;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_POSTING_IMAGE;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_POSTING_TOKEN;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ASSETS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_BUILDS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_POST_IMAGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TOKEN_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQ_CODE_PICK_IMAGE;

public class ProfileActivity extends BaseActivity  implements OnPermissionCallback {

    private TextView tv_name;
    private TextView tv_phone;
    private Button tv_phone2;
    private TextView tv_email;
    private CircleImageView iv_dp;
    private RecyclerView rv_asset_hor;
    private NestedScrollView scrl_profile;
    BitmapFactory.Options options;
    public  static  InputStream imageStream = null;
    public  static String image = null;
    public static String fpath = null;
    private static  Bitmap imagetobesend = null;

    private ImageView expanded_list_image;
    private RelativeLayout expanded_list_rlt,main_rltv;
    private ProgressBar expanded_image_progress;

    private TextView details;
    private static String new_path_server = null;

    private static String oldpassord = null;
    public static String newpssword = null;
    private Context context;

    private Uri mCropImageUri;

    private static final int REQUEST_PERMISSION_SETTING = 101;
    private static final int EXTERNAL_STORAGE_CAMERA_PERMISSION_CONSTANT = 100;
    private SharedPreferences permissionStatuscamera;
    private boolean sentToSettings = false;
    private static boolean flagvar = false;
    private static String path = null;
    Typeface typeface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setscreenLayout(R.layout.activity_profile);
        //changing the color programmatically

        permissionStatuscamera = getSharedPreferences("permissionStatuscamera",MODE_PRIVATE);

        setBasicToolBar("Profile");


        toolbar.setBackgroundColor((Color.parseColor("#45b879")));
        initViews();
//        User user = new User("sagar", "fundroiding@gmail.com", "9911502888", "http://wallpaper-gallery.net/images/image/image-13.jpg");
        getProfile();

        try {
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    // getMgrAssets();
//                    getFuelTypes();
                    // getdashboard();
                    getBuildings();
                    //getAssetIdentifier();
                    //  getStates();
                    // gettrackingdetails();
                    hideProgress();
                }
            };
            h.postDelayed(r, 5);
            showProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        getMgrAssets();
       // getProfile();FGfFFf
    }

    private void initViews() {
        tv_name = (TextView) findViewById(R.id.tv_name);
        tv_phone = (TextView) findViewById(R.id.tv_phone);
       tv_phone2 = (Button) findViewById(R.id.tv_phone2);
       tv_phone2.setTypeface(ConstantClass.getFont(getApplicationContext()));
        tv_email = (TextView) findViewById(R.id.tv_email);
        iv_dp = (CircleImageView) findViewById(R.id.iv_dp);
        rv_asset_hor = (RecyclerView) findViewById(R.id.rv_asset_hor);
        scrl_profile = (NestedScrollView) findViewById(R.id.scrl_profile);
        details=findViewById(R.id.details);
        details.setTypeface(ConstantClass.getFont(getApplicationContext()));



        tv_phone2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //showToast("change password clicked");
                changepassword ();
            }
        });

    }



    private void changepassword (){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.password_change_dialog);
        //dialog.setContentView(R.layout.change_password_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        final EditText text = (EditText) dialog.findViewById(R.id.username);
        final EditText text1 = (EditText) dialog.findViewById(R.id.password);
      //  text.setText("Do You Want To confirm Order For : "+ quantity+ "litres , For the Asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldpassord = text.getText().toString();
                newpssword = text1.getText().toString();


               // if (userEmail != null && !userEmail.isEmpty() && !userEmail.equals("null"))

                    String imgSett = SharedPrefrenceHelper.getUPass(context);

                if (imgSett.equalsIgnoreCase(oldpassord) ){
                    if (newpssword.equals(imgSett)){
                        showToast("You entered same previous password !");
                        return;
                    }else if (newpssword == null || newpssword.isEmpty() || newpssword.equals("null")){
                        showToast("New password can't be left empty !");
                        return;
                    }

                    else if (!newpssword.equals(imgSett)&& newpssword != null && !newpssword.isEmpty()  ) {
                        sendjson();
                       // showToast("send json");
                    }


                }else {
                    showToast("Previous password is not correct");
                    return;
                }


//                BookOrderRequestVo borv = new BookOrderRequestVo(uid, assetID, fuelid,qty);
//                ((BaseActivity) context).gotoOrderSummary(borv);
////
////                        BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uid, assetID, fuelid,qty,Stateid,location,Latitude,Longitude,Pincode);
////        ((BaseActivity) context).gotoOrderSummaryfordynamicaddress(borv);
//
//                HomeActivity.flagfordynamicbooking = 1;
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();
    }


    private void sendjson (){
        String json = preparejson();
        if (json.length() <= 0) return;
        String url = URL_FOR_PASSWORD_CHANGE;
        SharedPrefrenceHelper.setUpass(context,newpssword);
        volleyBodyHit(url, json, REQUEST_PASSWORD_CHANGE);
        showToast("Password Changed");
    }

    private String preparejson (){
        try {
            ChangePasswordRequast avo = new ChangePasswordRequast();
            avo.setKeyID(uid);
            avo.setOldKey(oldpassord);
            avo.setNewKey(newpssword);


            Gson gson = new Gson();
            return gson.toJson(avo);

        } catch (Exception e) {
            Slog.e("exception e line 161 addbuildingactivity" + e.getMessage());
            e.printStackTrace();
            return "";
        }
    }

    public void clickCam(View v) {

       CropImage.startPickImageActivity(this);
        checkpermission();

//        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Back" };
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Change Profile Pic !");
//        builder.setItems(options, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int item) {
//                if (options[item].equals("Take Photo"))
//                {
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                  //  startActivityForResult(intent, 1);
//                   // Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                   // File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
//                   // intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
//                    //startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);
//                    startActivityForResult(intent, 1);
//                }
//                else if (options[item].equals("Choose from Gallery"))
//                {
//                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    startActivityForResult(intent, 2);
//
//                }
//                else if (options[item].equals("Cancel")) {
//                    dialog.dismiss();
//                }
//            }
//        });
//        builder.show();



//        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//        photoPickerIntent.setType("image/*");
//        photoPickerIntent.putExtra("crop", "true");
//
//        photoPickerIntent.putExtra("aspectX", 10);
//        photoPickerIntent.putExtra("aspectY", 10);
//        photoPickerIntent.putExtra("scale", true);
//        photoPickerIntent.putExtra("return-data", true);
     //   startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);

       // showLogToast("cam clicked");
    }

    public void clickAssetEdit(View v) {
        gotoAddAsset();
        showLogToast("asset edit");
    }

    private void getProfile() {
        LoginVo lvo = getLoginVo();
        setProfileData(lvo);
    }

    private  void clickOnImage(){

       }



//    public void clickOnChangePassword(){
//           showToast("change password");
//       }

    private void pickimage () {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");


        startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);


    }


    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent imageReturnedIntent) {
      //  super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

       // resultCode == Activity.RESULT_OK


        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, imageReturnedIntent);


            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;


//
//                        try {
//                            imageStream = getContentResolver().openInputStream(selectedImage);
                try {
                    imageStream = getContentResolver().openInputStream(mCropImageUri);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap selectedImagenew = null;

                selectedImagenew = BitmapFactory.decodeStream(imageStream);
//
//                         //   imagetobesend = selectedImagenew;
                            imagetobesend=   getResizedBitmap(selectedImagenew, 200);

                //requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(imageReturnedIntent);
            if (resultCode == RESULT_OK) {

                try {
                    imageStream = getContentResolver().openInputStream(result.getUri());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap selectedImagenew = null;

                selectedImagenew = BitmapFactory.decodeStream(imageStream);
//
//                         //   imagetobesend = selectedImagenew;
                imagetobesend=   getResizedBitmap(selectedImagenew, 500);
                iv_dp.setImageBitmap(imagetobesend);
                saveProfileAccount();

               // showToast("Cropping Failed" + result.getError());

                //  ((ImageButton) findViewById(R.id.quick_start_cropped_image)).setImageURI(result.getUri());
              //  Toast.makeText(this, "Cropping successful, Sample: " + result.getSampleSize(), Toast.LENGTH_LONG).show();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast("Cropping Failed" + result.getError());
              //  Toast.makeText(this, "Cropping failed: " + result.getError(), Toast.LENGTH_LONG).show();
            }
        }


//                    if (requestCode == 1) {
////                        File f = new File(Environment.getExternalStorageDirectory().toString());
////                        for (File temp : f.listFiles()) {
////                            if (temp.getName().equals("temp.jpg")) {
////                                f = temp;
////                                break;
////                            }
////                        }
//                        try {
//                           // Bitmap bitmap;
//                            BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
//
//                            Bitmap bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
//
//
////                            bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
////                                    bitmapOptions);
//                            imagetobesend=   getResizedBitmap(bitmap, 200);
//
//                           // imagetobesend = bitmap;
//                            //getResizedBitmap(bitmap, 50);
//                            saveProfileAccount();
//
//                            iv_dp.setImageBitmap(imagetobesend );
//
////                            String path = android.os.Environment
////                                    .getExternalStorageDirectory()
////                                    + File.separator
////                                    + "Phoenix" + File.separator + "default";
////                            f.delete();
////                            OutputStream outFile = null;
////                            File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");
////                            try {
////                                outFile = new FileOutputStream(file);
////                                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
////                                outFile.flush();
////                                outFile.close();
////                            } catch (FileNotFoundException e) {
////                                e.printStackTrace();
////                            } catch (IOException e) {
////                                e.printStackTrace();
////                            } catch (Exception e) {
////                                e.printStackTrace();
////                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } else if (requestCode == 2) {
//
//
//                        Bitmap bitmap = null;
//                        Bitmap selectedImagenew = null;
//                        Uri selectedImage = imageReturnedIntent.getData();
//
//                        try {
//                            imageStream = getContentResolver().openInputStream(selectedImage);
//                           // performCrop(picturePath);
//
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//                        String[] filePathColumn = {MediaStore.Images.Media.DATA};
//
//                        Cursor cursor = getContentResolver().query(
//                                selectedImage, filePathColumn, null, null, null);
//                        cursor.moveToFirst();
//
//                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
//                        String filePath = cursor.getString(columnIndex);
//                        fpath = filePath;
//                        cursor.close();
//
//
//                        try {
//                            // String imageInSD = "/sdcard/UserImages/" + userImageName;
//
//                            String fname = new File(getFilesDir(), "test.png").getAbsolutePath();
//                            //showToast(fname);
//
//                            // postFile(URL_FOR_POSTING_IMAGE.replace("<UID>", uid),fpath,REQUEST_POST_IMAGE);
//
//                            bitmap = BitmapFactory.decodeFile(filePath);
//
//                            // getStringImage(bit)
//
//                            selectedImagenew = BitmapFactory.decodeStream(imageStream);
//
//                         //   imagetobesend = selectedImagenew;
//                            imagetobesend=   getResizedBitmap(selectedImagenew, 200);
//
//                            saveProfileAccount();
//                            image = getStringImage(selectedImagenew);
//
//                            selectedImagenew = getResizedBitmap(selectedImagenew, 50);
//
//
//                            cropAndGivePointedShape(bitmap);
//                            //return bitmap;
//                        } catch (OutOfMemoryError e) {
//                            try {
//                                options = new BitmapFactory.Options();
//                                options.inSampleSize = 2;
//                                bitmap = BitmapFactory.decodeFile(filePath, options);
//                                cropAndGivePointedShape(bitmap);
//                                // return bitmap;
//                            } catch (Exception g) {
//                                showLogToast("image disaappeard");
//                                // Log.e(e);
//                            }
//                        }
//
//
////                    try {
////                        HttpClient httpClient = new DefaultHttpClient();
////                        HttpContext httpContext = new BasicHttpContext();
////                        HttpPost httpPost = new HttpPost(
////                                URL_FOR_POSTING_IMAGE.replace("<UID>", uid));
////                        MultipartEntity entity = getMultipleEntityUpload();
////                        httpPost.setEntity(entity);
////                        HttpResponse httpResponse = httpClient.execute(httpPost,
////                                httpContext);
////                        HttpEntity httpEntity = httpResponse.getEntity();
////                        InputStream is = httpEntity.getContent();
////
////                        String line = "";
////                        StringBuilder total = new StringBuilder();
////                        BufferedReader rd = new BufferedReader(new InputStreamReader(
////                                is));
////
////                        while ((line = rd.readLine()) != null) {
////                            total.append(line);
////                        }
////                        String result =total.toString();
////
////                    }
////                    catch (Exception e) {
////                        showLogToast("exception");
////                        // TODO: handle exception
////                    }
////
////                   // Bitmap yourSelectedImage = BitmapFactory.decodeFile(filePath);
//                        iv_dp.setImageBitmap(imagetobesend);
//////                    String json = preparejson();
//////                    if (json.length() <= 0) return;
//////                    String url = URL_FOR_POSTING_TOKEN;
//////                    volleyBodyHit(url, json, REQUEST_TOKEN_REGISTRATION);
//
//                    }
                }






   // public String upload (){



    private MultipartEntity getMultipleEntityUpload() {
        MultipartEntity entity = new MultipartEntity(
                HttpMultipartMode.BROWSER_COMPATIBLE);
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //imagePic is bitmap of your image
            imagetobesend.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            byte[] arrByteImage = stream.toByteArray();
            try {
                entity.addPart("REQUEST_POST_IMAGE", new ByteArrayBody(
                        arrByteImage, ".jpg"));
            } catch (Exception e) {
            }

        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return entity;
    }



    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }




    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private Bitmap cropAndGivePointedShape(Bitmap originalBitmap)
    {
        Bitmap bmOverlay = Bitmap.createBitmap(originalBitmap.getWidth(),
                originalBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        Paint p = new Paint();
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(originalBitmap, 0, 0, null);
        canvas.drawRect(0, 0, 20, 20, p);

        Point a = new Point(0, 20);
        Point b = new Point(20, 20);
        Point c = new Point(0, 40);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.lineTo(a.x, a.y);
        path.close();

        canvas.drawPath(path, p);

        a = new Point(0, 40);
        b = new Point(0, 60);
        c = new Point(20, 60);

        path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.lineTo(a.x, a.y);
        path.close();

        canvas.drawPath(path, p);

        canvas.drawRect(0, 60, 20, originalBitmap.getHeight(), p);

        return bmOverlay;
    }

    @Override
    protected void onResponse(int request_code, String response, String data) {
        super.onResponse(request_code, response, data);

        switch (request_code){
            case REQUEST_GET_ASSETS:
                setAssetAdapter();
                break;
        }

//        User user = new User("sagar","fundroiding@gmail.com","9911502888", "http://wallpaper-gallery.net/images/image/image-13.jpg");
//        setProfileData(user);
    }

    private void setProfileData(LoginVo lvo) {
        tv_name.setText("" + WordUtils.capitalize (lvo.getPMgrFName()));
        tv_email.setText(""+ lvo.getPMgrEmail());
        tv_phone.setText(""+ lvo.getPMgrPhone1());
      //  tv_phone2.setText(""+ lvo.getPMgrPhone2());
      //  URL_GET_BUILDS.replace("<UID>", uid)

        String new_path = getLoginVo().getPMgrImagePath();



                //SharedPrefrenceHelper.getNotificationCount(bContext);

                //getLoginVo().getPMgrImagePath();

        if (!new_path.equals("") ){
            try {

              //  new_path = profileupdate.get(0).getPMgrImagePath();

//             //   if (new_path ==null || new_path == "" || new_path.isEmpty() || new_path.length() ==0)
//                    if (new_path.equals("")){
//                   // SharedPrefrenceHelper.setNotificationCount(bContext, new_path);
//                }else {
//                    SharedPrefrenceHelper.setNotificationCount(bContext, new_path);
//                    new_path =  SharedPrefrenceHelper.getNotificationCount(bContext);
//                }

               // SharedPrefrenceHelper.setNotificationCount(bContext, new_path);
              //  showToast("path is  :" + new_path);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else if (new_path.equals("")){



            new_path =  SharedPrefrenceHelper.getNotificationCount(bContext);
        }else {
//            SharedPrefrenceHelper.setNotificationCount(bContext, new_path);
//            new_path =  SharedPrefrenceHelper.getNotificationCount(bContext);

        }




        String url = URL_FOR_IMAGE_DOWNLOADING.replace("<UID>", new_path);
        //Picasso.with(bContext).load(url).into(iv_dp);
        AppUtils.setImageUrl(iv_dp,url, R.drawable.def_user);
    }

    private void setAssetAdapter() {
        if (assets == null) {
            assets = new ArrayList<>();
        }

        rv_asset_hor.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_asset_hor.setNestedScrollingEnabled(false);
        rv_asset_hor.setHasFixedSize(true);
        AssetHorRecAdapter mAdapter = new AssetHorRecAdapter(bContext, assets);
        rv_asset_hor.setAdapter(mAdapter);
    }



    public String bitMapToBase64()
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        //add support for jpg and more.
        imagetobesend.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();

        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);

        return encoded;
    }

       public static byte[] getFileDataFromDrawable(VolleyMultipartRequest context, Bitmap bitmap) {
        // Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }



    public static JSONObject postFile(String url, String filePath, int id){
        String result="";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        File file = new File(filePath);
        MultipartEntity mpEntity = new MultipartEntity();
        ContentBody cbFile = new FileBody(file, "image/jpeg");
        StringBody stringBody= null;
        JSONObject responseObject=null;
        try {
            stringBody = new StringBody(id+"");
            mpEntity.addPart("file", cbFile);
            mpEntity.addPart("id",stringBody);
            httpPost.setEntity(mpEntity);
            System.out.println("executing request " + httpPost.getRequestLine());
            HttpResponse response = httpClient.execute(httpPost);
            HttpEntity resEntity = response.getEntity();
            result=resEntity.toString();
            responseObject=new JSONObject(result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return responseObject;
    }

    private void saveProfileAccount() {
        // loading or check internet connection or something...
        // ... then
        String url = URL_FOR_POSTING_IMAGE.replace("<UID>", uid);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                Slog.d("request completed");
                try {
                    JSONObject jobj = new JSONObject(resultResponse);
                    int rs = JsonUtils.getIntFromJSON(jobj, "rs");
                    String msg = JsonUtils.getStringFromJSON(jobj, "msg");
                    String data = JsonUtils.getStringFromJSON(jobj, "data");
                    BaseVo baseVo = new BaseVo(rs, msg, data);


//                   try {
//
//
////                       JSONObject dataObj = new JSONObject(data);
////                       Gson gson = new Gson();
////
////                       Type type = new TypeToken<ArrayList<LoginVo>>() {}.getType();
////                       profileupdate = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
//
//
//
//
//                      // JSONObject assetObj = JsonUtils.getJsonObjFromJSON(dataObj, "assets");
//
//                       // SharedPrefrenceHelper.setUpass(bContext,et_pwd.getText().toString().trim());
//
//                       JSONArray jsonArray = dataObj.getJSONArray("assets");
//                       showToast(jsonArray.toString());
//
//
//                    //   SharedPrefrenceHelper.setLvo(bContext, jsonArray.toString());
//                   }catch (JSONException e){
//                       e.printStackTrace();
//                   }


                    if (baseVo != null) {
                        if (baseVo.getRs() == 0) {

                            Gson gson = new Gson();


                            Type type = new TypeToken<ArrayList<LoginVo>>() {}.getType();
                            profileupdate = gson.fromJson(new JSONObject(data).get("assets").toString(), type);


                            try {

                                new_path_server = profileupdate.get(0).getPMgrImagePath();
                                SharedPrefrenceHelper.setNotificationCount(bContext, new_path_server);

                            }catch (Exception e){
                                e.printStackTrace();
                            }


                          //  profileupdate assetVo = gson.fromJson(data, profileupdate.class);
                           // onResponse(resultResponse, response, data);
//                    if (request_code ==  REQUEST_LOGIN){
//                        showLogToast("Invalid user name password");
//                    }
                        }
                    }





                    JSONObject result = new JSONObject(resultResponse);
                    String status = result.getString("status");
                    String message = result.getString("message");

                    if (status.equals(REQUEST_POST_IMAGE)) {
                        // tell everybody you have succed upload image and post strings
                        showToast("Messsage"+ message);
//                        getLoginVo();
                        Log.i("Messsage", message);
                    } else {
                        Log.i("Unexpected", message);
                        showToast("Unexpected"+ message);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                String errorMessage = "Unknown error";
                if (networkResponse == null) {
                    if (error.getClass().equals(TimeoutError.class)) {
                        errorMessage = "Request timeout";
                    } else if (error.getClass().equals(NoConnectionError.class)) {
                        errorMessage = "Failed to connect server";
                    }
                } else {
                    String result = new String(networkResponse.data);
                    try {
                        JSONObject response = new JSONObject(result);
                        String status = response.getString("status");
                        String message = response.getString("message");

                        Log.e("Error Status", status);
                        Log.e("Error Message", message);

                        if (networkResponse.statusCode == 404) {
                            errorMessage = "Resource not found";
                        } else if (networkResponse.statusCode == 401) {
                            errorMessage = message+" Please login again";
                        } else if (networkResponse.statusCode == 400) {
                            errorMessage = message+ " Check your inputs";
                        } else if (networkResponse.statusCode == 500) {
                            errorMessage = message+" Something is getting wrong";
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.i("Error", errorMessage);
                error.printStackTrace();
            }
        }) {


            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("cover",new DataPart("dev.png", getFileDataFromDrawable(this, imagetobesend), "image/png"));
               // params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mCoverImage.getDrawable()), "image/jpeg"));

                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(multipartRequest);
    }





    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(this);

//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
//                .setMultiTouchEnabled(true)
//                .start(this);
    }

    private void checkpermission() {
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.CAMERA)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("Need Camera Permission");
                builder.setMessage("This app needs Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, EXTERNAL_STORAGE_CAMERA_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatuscamera.getBoolean(Manifest.permission.CAMERA,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("Need Camera Permission");
                builder.setMessage("This app needs Camera permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Camera Permission", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.CAMERA}, EXTERNAL_STORAGE_CAMERA_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatuscamera.edit();
            editor.putBoolean(Manifest.permission.CAMERA,true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.
            // proceedAfterPermission();
        }
    }

    @Override
    public void onPermissionGranted(String[] permissionName) {

    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {

    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {

    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {

    }

    @Override
    public void onNoPermissionNeeded() {

    }
}
