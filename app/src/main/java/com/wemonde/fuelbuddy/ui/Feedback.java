package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.DataModel;
import com.wemonde.fuelbuddy.base.BaseActivity;

import java.util.ArrayList;

public class Feedback extends BaseActivity {






    private ListView newlist;
    private EditText EditTextFeedbackBody_new;
    ArrayList dataModels;
    private SimpleRatingBar myRatingBar;
    ListView listView;
    private TextView tv ;

    private CustomAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setscreenLayout(R.layout.activity_feedback_activity);
        setBasicToolBar("Feedback");

        newlist = (ListView)findViewById(R.id.listView1);
        EditTextFeedbackBody_new = (EditText)findViewById(R.id.EditTextFeedbackBody_new);
        listView = (ListView) findViewById(R.id.listView1);
        myRatingBar = (SimpleRatingBar) findViewById(R.id.ratingView_new);
        tv = (TextView) findViewById(R.id.nameofuser1_new_one);




        SimpleRatingBar.AnimationBuilder builder = myRatingBar.getAnimationBuilder()
                .setRatingTarget(0)
                .setDuration(2000)
                .setInterpolator(new BounceInterpolator());
        builder.start();
        myRatingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {

                if (rating >1 && rating <= 3.0){
                    starrating3();
                }else if (rating > 3.0) {
                    starrating4();
                }else {
                    showToast("Please select rating");
                }
                // showToast("Star rating is "+ String.valueOf(rating));
            }
        });





    }

    public void starrating3 (){

        tv.setText("Tell Us what went wrong ? " );
        listView.setVisibility(View.VISIBLE);
        dataModels = new ArrayList();

        dataModels.add(new DataModel("Delayed delivery ", false));
        dataModels.add(new DataModel("Driver attendant unprofessional", false));
        dataModels.add(new DataModel("Issue faced with ordering", false));
        dataModels.add(new DataModel("Issue faced with vehicle tracking", false));
        dataModels.add(new DataModel("Billing discrepency", false));
        dataModels.add(new DataModel("My reason not listed", false));


        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView .setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                DataModel dataModel= (DataModel) dataModels.get(position);



//                for (int i = 0; i< dataModels.size();i++){
//                    dataModel(i)
//                }
                if ( dataModel.checked){
                    //   subjectofmail = "";



                    dataModel.checked = false;








//                    for(dataModels.)
//                    {
//                        planet.setChecked(false);
//                    }

//                    ( (DataModel) ((DataModel) dataModels.get(position)).checked)
                    // (DataModel) dataModels.get(position).name, false);
                }else {
                    dataModel.checked = !dataModel.checked;
                    //   subjectofmail = ((DataModel) dataModels.get(position)).name;

//                    if (subjectofmail.equals("Other Issues")){
//                        subjectofmail = "";
//                    }


                    for (int i = 0 ; i < dataModels.size();i++){
                        DataModel dataModel1= (DataModel) dataModels.get(i);

                        if (dataModel == dataModel1){
                            dataModel1.checked = true;
                        }else {
                            dataModel1.checked = false;
                        }
                    }


                    // dataModel1.checked = false;


                    //  listView.setItemChecked(listView.getCheckedItemPosition(), true);

                    // listView.setItemChecked(5, true);
                    // listView.getCheckedItemPosition();

                    //  showToast(name);
                }
                adapter.notifyDataSetChanged();


            }
        });


    }

    public void  submit(View v){
        showToast(" Thank you for your feedback ! ");
    }



    public void starrating4 () {
        tv.setText("Great what went perfect for you ? ");
        listView.setVisibility(View.VISIBLE);

        dataModels = new ArrayList();

        dataModels.add(new DataModel("Polite and professional driver & attendent", false));
        dataModels.add(new DataModel("On time delivery", false));
        //dataModels.add(new DataModel("Change in time scheduled", false));
        dataModels.add(new DataModel("Easy ordering", false));
        dataModels.add(new DataModel("My reason not listed", false));
//        dataModels.add(new DataModel("Unable to track driver", false));
//        dataModels.add(new DataModel("Invoice not received", false));


        adapter = new CustomAdapter(dataModels, getApplicationContext());

        listView .setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View view, int position, long id) {

                DataModel dataModel= (DataModel) dataModels.get(position);



//                for (int i = 0; i< dataModels.size();i++){
//                    dataModel(i)
//                }
                if ( dataModel.checked){
                    //   subjectofmail = "";



                    dataModel.checked = false;








//                    for(dataModels.)
//                    {
//                        planet.setChecked(false);
//                    }

//                    ( (DataModel) ((DataModel) dataModels.get(position)).checked)
                    // (DataModel) dataModels.get(position).name, false);
                }else {
                    dataModel.checked = !dataModel.checked;
                    //   subjectofmail = ((DataModel) dataModels.get(position)).name;

//                    if (subjectofmail.equals("Other Issues")){
//                        subjectofmail = "";
//                    }


                    for (int i = 0 ; i < dataModels.size();i++){
                        DataModel dataModel1= (DataModel) dataModels.get(i);

                        if (dataModel == dataModel1){
                            dataModel1.checked = true;
                        }else {
                            dataModel1.checked = false;
                        }
                    }


                    // dataModel1.checked = false;


                    //  listView.setItemChecked(listView.getCheckedItemPosition(), true);

                    // listView.setItemChecked(5, true);
                    // listView.getCheckedItemPosition();

                    //  showToast(name);
                }
                adapter.notifyDataSetChanged();


            }
        });


    }
}
