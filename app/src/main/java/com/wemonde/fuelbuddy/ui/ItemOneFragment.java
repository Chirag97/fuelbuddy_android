package com.wemonde.fuelbuddy.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;

/**
 * Created by Devendra Pandey on 6/21/2017.
 */

public class ItemOneFragment extends Fragment  {
    private RecyclerView rv_asset_ver;
    public static BaseActivity ba;
    LinearLayout linearLayout;

    public static ItemOneFragment newInstance() {
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



            View rootView = inflater.inflate(R.layout.activity_my_assets, container, false);
           // linearLayout = (LinearLayout) rootView.findViewById(R.id.linearlayout);
            return rootView;


//        return inflater.inflate(R.layout.activity_my_assets, container, false);
//
//        //new VerifyDialogue(MainFragmentActivity.getInstance()).execute(dateRange);
//        //ba.getMgrAssets();
//
//        rv_asset_ver = (RecyclerView) getView().findViewById(R.id.rv_asset_ver);
//        rv_asset_ver.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
//        rv_asset_ver.setNestedScrollingEnabled(false);
//        rv_asset_ver.setHasFixedSize(true);
    }
}
