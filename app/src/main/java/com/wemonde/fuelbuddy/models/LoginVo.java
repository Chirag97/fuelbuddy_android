package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class LoginVo {

//            "data": {
//                "assets": {
//                    "PMgrID": 1,
//                    "PMgrUID": "dlfpmgr1",
//                    "PMgrPassword": null,
//                    "PMgrFName": "dlfpmgr1",
//                    "PMgrMName": "dlfpmgr1",
//                    "PMgrLName": "",
//                    "PMgrInfo": "",
//                    "PMgrPhone1": "",
//                    "PMgrPhone2": "",
//                    "PMgrEmail": "",
//                    "PMgrImagePath": "",
//                    "CPMgrID": 1,
//                    "IsFBApproved": 0

    private String PMgrID;
    private String PMgrUID;
    private String PMgrFName;
    private String PMgrMName;
    private String PMgrLName;
    private String PMgrInfo;
    private String PMgrPhone1;
    private String PMgrPhone2;
    private String PMgrEmail;
    private String PMgrImagePath;
    private int CPMgrID;
    private int IsFBApproved;

    private int CorpID;
   // public static String id;


    public String getPMgrID() {
        return PMgrID;
    }

    public void setPMgrID(String PMgrID) {
        this.PMgrID = PMgrID;
    }

    public String getPMgrUID() {
        return PMgrUID;
    }

    public void setPMgrUID(String PMgrUID) {
        this.PMgrUID = PMgrUID;
    }

    public String getPMgrFName() {
        return PMgrFName;
    }

    public void setPMgrFName(String PMgrFName) {
        this.PMgrFName = PMgrFName;
    }

    public String getPMgrMName() {
        return PMgrMName;
    }

    public void setPMgrMName(String PMgrMName) {
        this.PMgrMName = PMgrMName;
    }

    public String getPMgrLName() {
        return PMgrLName;
    }

    public void setPMgrLName(String PMgrLName) {
        this.PMgrLName = PMgrLName;
    }

    public String getPMgrInfo() {
        return PMgrInfo;
    }

    public void setPMgrInfo(String PMgrInfo) {
        this.PMgrInfo = PMgrInfo;
    }

    public String getPMgrPhone1() {
        return PMgrPhone1;
    }

    public void setPMgrPhone1(String PMgrPhone1) {
        this.PMgrPhone1 = PMgrPhone1;
    }

    public String getPMgrPhone2() {
        return PMgrPhone2;
    }

    public void setPMgrPhone2(String PMgrPhone2) {
        this.PMgrPhone2 = PMgrPhone2;
    }

    public String getPMgrEmail() {
        return PMgrEmail;
    }

    public void setPMgrEmail(String PMgrEmail) {
        this.PMgrEmail = PMgrEmail;
    }

    public String getPMgrImagePath() {
        return PMgrImagePath;
    }

    public void setPMgrImagePath(String PMgrImagePath) {
        this.PMgrImagePath = PMgrImagePath;
    }

    public int getCPMgrID() {
        return CPMgrID;
    }

    public void setCPMgrID(int CPMgrID) {
        this.CPMgrID = CPMgrID;
    }

    public int getIsFBApproved() {
        return IsFBApproved;
    }

    public void setIsFBApproved(int isFBApproved) {
        IsFBApproved = isFBApproved;
    }

    public int getCorpID() {
        return CorpID;
    }

    public void setCorpID(int corpID) {
        CorpID = corpID;
    }



}
