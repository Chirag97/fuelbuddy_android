package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 1/23/2018.
 */

public class Sensor implements Serializable {

//       "OrderItemID": 30,
//               "SensorID": 1,
//               "SensorName": "Density",
//               "SensorIndex": 0,
//               "SensorValue": 0.843,
//               "UpdatedOn": "2018-01-20T15:10:39.027"


    private int OrderItemID = 0;
    private int SensorID;
    private String SensorName;
    private int SensorIndex;
    private float SensorValue;
    private String BookedOn;

    public int getOrderItemID() {
        return OrderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        OrderItemID = orderItemID;
    }

    public int getSensorID() {
        return SensorID;
    }

    public void setSensorID(int sensorID) {
        SensorID = sensorID;
    }

    public String getSensorName() {
        return SensorName;
    }

    public void setSensorName(String sensorName) {
        SensorName = sensorName;
    }

    public int getSensorIndex() {
        return SensorIndex;
    }

    public void setSensorIndex(int sensorIndex) {
        SensorIndex = sensorIndex;
    }

    public float getSensorValue() {
        return SensorValue;
    }

    public void setSensorValue(float sensorValue) {
        SensorValue = sensorValue;
    }

    public String getBookedOn() {
        return BookedOn;
    }

    public void setBookedOn(String bookedOn) {
        BookedOn = bookedOn;
    }



}
