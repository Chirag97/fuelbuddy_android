package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 04-04-2017.
 **/

public class State implements Serializable{
    private int StateID;
    private String StateName;

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }
}