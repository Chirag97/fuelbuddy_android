package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 8/8/2017.
 */

public class Tracking implements Serializable {



    private String AssetS_ID;
    private String FuelName;
    private String Location;
    private String PINCode;//": "208801",
    private double Latitude;//": 1.00000000,
    private double Longitude;//": 0.99100000,
    private int FExecID;
    private double VehicleCurrLatitude;//": 1.00000000,
    private double VehicleCurrlongitude;//": 0.99100000,
    private String VehicleNumber;
    private String FExecPhone1;




    private String FExecName;

    public String getAssetS_ID() {
        return AssetS_ID;
    }

    public void setAssetS_ID(String assetS_ID) {
        AssetS_ID = assetS_ID;
    }

    public String getFuelName() {
        return FuelName;
    }

    public void setFuelName(String fuelName) {
        FuelName = fuelName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        this.Location = location;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public int getFExecID() {
        return FExecID;
    }

    public void setFExecID(int FExecID) {
        this.FExecID = FExecID;
    }

    public double getFExecCurrLatitude() {
        return VehicleCurrLatitude;
    }

    public void setFExecCurrLatitude(double FExecCurrLatitude) {
        this.VehicleCurrLatitude = FExecCurrLatitude;
    }

    public double getFExecCurrLongitude() {
        return VehicleCurrlongitude;
    }

    public void setFExecCurrLongitude(double FExecCurrLongitude) {
        this.VehicleCurrlongitude = FExecCurrLongitude;
    }

    public String getFExecVNumber() {
        return VehicleNumber;
    }

    public void setFExecVNumber(String FExecVNumber) {
        this.VehicleNumber = FExecVNumber;
    }

    public String getFExecPhone1() {
        return FExecPhone1;
    }

    public void setFExecPhone1(String FExecPhone1) {
        this.FExecPhone1 = FExecPhone1;
    }


    public String getFExecName() {
        return FExecName;
    }

    public void setFExecName(String FExecName) {
        this.FExecName = FExecName;
    }


}
