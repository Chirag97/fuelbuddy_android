package com.wemonde.fuelbuddy.models;

import java.io.Serializable;
import java.util.ArrayList;

public class MonthlyTankComsumption implements Serializable {

    public ArrayList<TankBuddyConsumption> getCurrentDate() {
        return CurrentDate;
    }

    public void setCurrentDate(ArrayList<TankBuddyConsumption> currentDate) {
        CurrentDate = currentDate;
    }

    public ArrayList<TankBuddyConsumption> getCurrentMonth() {
        return CurrentMonth;
    }

    public void setCurrentMonth(ArrayList<TankBuddyConsumption> currentMonth) {
        CurrentMonth = currentMonth;
    }

    private ArrayList<TankBuddyConsumption> CurrentDate;
    private ArrayList<TankBuddyConsumption> CurrentMonth;

}
