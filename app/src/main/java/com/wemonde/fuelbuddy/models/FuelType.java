package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class FuelType {
//    "FuelID": 1,
//    "FuelName": "HighSpeedDiesel",
//    "FuelPrice": 59.00,
//    "FuelIsActive": 1

    private int FuelID;
    private String FuelName;
    private double FuelPrice;
    private int FuelIsActive;

    public int getFuelID() {
        return FuelID;
    }

    public void setFuelID(int fuelID) {
        FuelID = fuelID;
    }

    public String getFuelName() {
        return FuelName;
    }

    public void setFuelName(String fuelName) {
        FuelName = fuelName;
    }

    public double getFuelPrice() {
        return FuelPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        FuelPrice = fuelPrice;
    }

    public int getFuelIsActive() {
        return FuelIsActive;
    }

    public void setFuelIsActive(int fuelIsActive) {
        FuelIsActive = fuelIsActive;
    }
}
