package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class BookOrderRequestVo implements Serializable{

    /*{
        "PMgrID": 1,
            "AssetID": 1,
            "FuelID": 1,
            "Qty": 142
    }*/

    private int PMgrID;
    private int AssetID;
    private int FuelID;
    private int Qty;
    private String ScheduledDelivery;




    public BookOrderRequestVo(int PMgrID, int assetID, int fuelID, int qty,String scheduledDelivery) {
        this.PMgrID = PMgrID;
        AssetID = assetID;
        FuelID = fuelID;
        Qty = qty;
        ScheduledDelivery = scheduledDelivery;
    }

    public int getPMgrID() {
        return PMgrID;
    }

    public void setPMgrID(int PMgrID) {
        this.PMgrID = PMgrID;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public int getFuelID() {
        return FuelID;
    }

    public void setFuelID(int fuelID) {
        FuelID = fuelID;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public String getScheduledDelivery() {
        return ScheduledDelivery;
    }

    public void setScheduledDelivery(String scheduledDelivery) {
        ScheduledDelivery = scheduledDelivery;
    }
}
