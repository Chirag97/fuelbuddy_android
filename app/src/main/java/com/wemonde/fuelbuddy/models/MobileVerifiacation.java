package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

public class MobileVerifiacation implements Serializable {

   // "MobileVerificationID":18,"MobileNo":"8810538328","OTP":"6857"

    public String getMobileVerificationID() {
        return MobileVerificationID;
    }

    public void setMobileVerificationID(String mobileVerificationID) {
        MobileVerificationID = mobileVerificationID;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }

    private String MobileVerificationID;
    private String MobileNo;
    private String OTP;



}
