package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 1/25/2018.
 */

public class Assettankdetails implements Serializable {

//    {
//        "AssetTankBuddyDeviceID": 7,
//            "TankBuddyDeviceID": 1,
//            "TankIndex": 1,
//            "FuelDepth": 4,
//            "TankLatchStatus": true,
//            "TankDepthPercentageThreshold": 50,
//            "TankDepth": 10,
//            "IsActive": true,
//            "AssetTankDataID": 110,
//            "TankBuddyDeviceName": "D0001"
//    }



    private int AssetTankBuddyDeviceID;
    private int TankBuddyDeviceID;
    private int TankIndex;
    private int FuelDepth;



    private int TankDepth;
    private String TankLatchStatus;
    private  int TankDepthPercentageThreshold;
    private String IsActive;
    private int AssetTankDataID;
    private String TankBuddyDeviceName;

    public int getAssetTankBuddyDeviceID() {
        return AssetTankBuddyDeviceID;
    }

    public void setAssetTankBuddyDeviceID(int assetTankBuddyDeviceID) {
        AssetTankBuddyDeviceID = assetTankBuddyDeviceID;
    }

    public int getTankBuddyDeviceID() {
        return TankBuddyDeviceID;
    }

    public void setTankBuddyDeviceID(int tankBuddyDeviceID) {
        TankBuddyDeviceID = tankBuddyDeviceID;
    }

    public int getTankIndex() {
        return TankIndex;
    }

    public void setTankIndex(int tankIndex) {
        TankIndex = tankIndex;
    }

    public int getFuelDepth() {
        return FuelDepth;
    }

    public void setFuelDepth(int fuelDepth) {
        FuelDepth = fuelDepth;
    }

    public int getTankDepth() {
        return TankDepth;
    }

    public void setTankDepth(int tankDepth) {
        TankDepth = tankDepth;
    }

    public String getTankLatchStatus() {
        return TankLatchStatus;
    }

    public void setTankLatchStatus(String tankLatchStatus) {
        TankLatchStatus = tankLatchStatus;
    }

    public int getTankDepthPercentageThreshold() {
        return TankDepthPercentageThreshold;
    }

    public void setTankDepthPercentageThreshold(int tankDepthPercentageThreshold) {
        TankDepthPercentageThreshold = tankDepthPercentageThreshold;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public int getAssetTankDataID() {
        return AssetTankDataID;
    }

    public void setAssetTankDataID(int assetTankDataID) {
        AssetTankDataID = assetTankDataID;
    }





    public String getTankBuddyDeviceName() {
        return TankBuddyDeviceName;
    }

    public void setTankBuddyDeviceName(String tankBuddyDeviceName) {
        TankBuddyDeviceName = tankBuddyDeviceName;
    }

}
