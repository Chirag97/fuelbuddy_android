package com.wemonde.fuelbuddy.models;

import java.util.ArrayList;

/**
 * Created by Devendra Pandey on 1/23/2018.
 */

public class Sensoravo {


    private ArrayList<Sensor> sensors;

    public ArrayList<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(ArrayList<Sensor> sensors) {
        this.sensors = sensors;
    }

}
