package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 14-03-2017.
 */

public class Asset implements Serializable {

    private boolean dummy= false;

//    			"AssetID": 1,
//                "AssetIdentifierName": "Generator",
//                "AssetLocation": "Sec 135",
//                "AssetFuelCapacity": 1553,
//                "FuelName": "HighSpeedDiesel",
//                "BuildName": "Tower1",
//                "BuildLocation": "Delhi 110096"

    private int AssetID = 0;
    private String image;
    private String FuelName;
    private String BuildName;
    private String AssetS_ID;
    private String AssetLocation;
    private String BuildLocation;
    private int AssetFuelCapacity;
    private String AssetIdentifierName;



    private String AssetManufacturer;
    private String AssetModel;
    private String PINCode;//": "208801",
    private double Latitude;//": 1.00000000,
    private double Longitude;//": 0.99100000,


    private int StateID;//34



    private int DistrictId;

    public Asset() {}

    public Asset(boolean isDummy) {
        this.dummy = isDummy;
    }

    public boolean isDummy() {
        return dummy;
    }

    public void setDummy(boolean dummy) {
        this.dummy = dummy;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
//        String image = "https://www.bajajallianz.com/Corp/images/car-insurance-new.jpg";
        return image;
    }

    public String getAssetS_ID() {
        return AssetS_ID;
    }

    public void setAssetS_ID(String assetS_ID) {
        AssetS_ID = assetS_ID;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public String getAssetIdentifierName() {
        return AssetIdentifierName;
    }

    public void setAssetIdentifierName(String assetIdentifierName) {
        AssetIdentifierName = assetIdentifierName;
    }

    public String getAssetLocation() {
        return AssetLocation;
    }
    public String getAssetManufacturer() {
        return AssetManufacturer;
    }

    public void setAssetManufacturer(String assetManufacturer) {
        AssetManufacturer = assetManufacturer;
    }

    public String getAssetModel() {
        return AssetModel;
    }

    public void setAssetModel(String assetModel) {
        AssetModel = assetModel;
    }

    public void setAssetLocation(String assetLocation) {
        AssetLocation = assetLocation;
    }

    public int getAssetFuelCapacity() {
        return AssetFuelCapacity;
    }

    public void setAssetFuelCapacity(int assetFuelCapacity) {
        AssetFuelCapacity = assetFuelCapacity;
    }

    public String getFuelName() {
        return FuelName;
    }

    public void setFuelName(String fuelName) {
        FuelName = fuelName;
    }

    public String getBuildName() {
        return BuildName;
    }

    public void setBuildName(String buildName) {
        BuildName = buildName;
    }

    public String getBuildLocation() {
        return BuildLocation;
    }

    public void setBuildLocation(String buildLocation) {
        BuildLocation = buildLocation;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public int getStateID() {
        return StateID;
    }

    public int getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(int districtId) {
        DistrictId = districtId;
    }


    public void setStateID(int stateID) {
        StateID = stateID;
    }

}
