package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 1/4/2018.
 */

public class NewUserRegistration {


    private String CompanyName;//0
    private String Email;//"Email"
    private String PhoneNo;//"phoneno"
    private String UserName;//username
    private String UserPassword;//"passord"

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        PhoneNo = phoneNo;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public void setUserPassword(String userPassword) {
        UserPassword = userPassword;
    }



}
