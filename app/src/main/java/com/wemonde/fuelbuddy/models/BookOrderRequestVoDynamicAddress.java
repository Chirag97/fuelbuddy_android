package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 7/29/2017.
 */

public class BookOrderRequestVoDynamicAddress implements Serializable {

      /*{
       {
   "PMgrID":14,
   "AssetID":45,
   "FuelID":1,
   "Qty":15,
   "StateID":1,
   "Location":"275 /22 gali no 3 sadar bazar",
   "Latitude":28.50750000,
   "Longitude":77.41230000,
   "PINCode":"234578"
}
    }*/

    private int PMgrID;
    private int AssetID;
    private int FuelID;
    private int Qty;
    private int StateID;
    private String Location;
    private double Latitude;
    private double Longitude;
    private String PINCode;






    public BookOrderRequestVoDynamicAddress(int PMgrID, int assetID, int fuelID, int qty,int StateID,String Location,Double Latitude,Double Longitude,String PINCode ) {
        this.PMgrID = PMgrID;
        AssetID = assetID;
        FuelID = fuelID;
        Qty = qty;
        this.StateID = StateID;
        this.Location = Location;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.PINCode = PINCode;
    }

    public int getPMgrID() {
        return PMgrID;
    }

    public void setPMgrID(int PMgrID) {
        this.PMgrID = PMgrID;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public int getFuelID() {
        return FuelID;
    }

    public void setFuelID(int fuelID) {
        FuelID = fuelID;
    }

    public int getQty() {
        return Qty;
    }

    public void setQty(int qty) {
        Qty = qty;
    }

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }
}