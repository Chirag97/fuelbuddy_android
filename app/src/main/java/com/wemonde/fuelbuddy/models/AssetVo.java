package com.wemonde.fuelbuddy.models;

import java.util.ArrayList;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class AssetVo {
    private ArrayList<Asset> assets;

    public ArrayList<Asset> getAssets() {
        return assets;
    }

    public void setAssets(ArrayList<Asset> assets) {
        this.assets = assets;
    }
}