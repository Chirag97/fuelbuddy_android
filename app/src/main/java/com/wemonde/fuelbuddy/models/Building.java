package com.wemonde.fuelbuddy.models;

import android.net.sip.SipErrorCode;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 17-03-2017.
 */

public class Building implements Serializable{

    private int BuildID;
    private String BuildName;
    private String BuildLocation;
    private int CorpID;
    private String PINCode;//": "208801",
    private double Latitude;//": 1.00000000,
    private double Longitude;//": 0.99100000,
    private int IsActive;//": 1,
    private int CreatedByPMgrID;//": 1,
    private String PMgrUID;//": "DLFPM1"



    private int StateID;//

    public int getBuildID() {
        return BuildID;
    }

    public void setBuildID(int buildID) {
        BuildID = buildID;
    }

    public String getBuildName() {
        return BuildName;
    }

    public void setBuildName(String buildName) {
        BuildName = buildName;
    }

    public String getBuildLocation() {
        return BuildLocation;
    }

    public void setBuildLocation(String buildLocation) {
        BuildLocation = buildLocation;
    }

    public int getCorpID() {
        return CorpID;
    }

    public void setCorpID(int corpID) {
        CorpID = corpID;
    }


    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    public int getCreatedByPMgrID() {
        return CreatedByPMgrID;
    }

    public void setCreatedByPMgrID(int createdByPMgrID) {
        CreatedByPMgrID = createdByPMgrID;
    }

    public String getPMgrUID() {
        return PMgrUID;
    }

    public void setPMgrUID(String PMgrUID) {
        this.PMgrUID = PMgrUID;
    }

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        this.StateID = stateID;
    }
}
