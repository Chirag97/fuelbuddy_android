package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class AddAssetRequestVo {
//    "{\"AssetIdentifierID\":1,\"AssetManufacturer\":\"plepl\",\"AssetModel\":\"momo\",\"AssetS_ID\":\"asd\"," +
//            "\"AssetFuelCapacity\":800,\"FuelID\":1,\"AssetIsActive\":1,\"AssetLocation\":\"lo\",\"BuildID\":2,\"StateID\":9}"

    private int AssetID;
    private int AssetIdentifierID;
    private String AssetManufacturer;
    private String AssetModel;
    private String AssetS_ID;
    private int AssetFuelCapacity;
    private int FuelID;
    private int AssetIsActive;
    private String PINCode;//"803330"
    private String Latitude;//1.000
    private String Longitude;//0.991
    private String AssetLocation;
    private String AssetLocationType;


    private int BuildID;




    private String StateID;



    private String DistrictId;

    public AddAssetRequestVo() {}

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public int getAssetIdentifierID() {
        return AssetIdentifierID;
    }

    public void setAssetIdentifierID(int assetIdentifierID) {
        AssetIdentifierID = assetIdentifierID;
    }

    public String getAssetManufacturer() {
        return AssetManufacturer;
    }

    public void setAssetManufacturer(String assetManufacturer) {
        AssetManufacturer = assetManufacturer;
    }

    public String getAssetModel() {
        return AssetModel;
    }

    public void setAssetModel(String assetModel) {
        AssetModel = assetModel;
    }

    public String getAssetS_ID() {
        return AssetS_ID;
    }

    public void setAssetS_ID(String assetS_ID) {
        AssetS_ID = assetS_ID;
    }

    public int getAssetFuelCapacity() {
        return AssetFuelCapacity;
    }

    public void setAssetFuelCapacity(int assetFuelCapacity) {
        AssetFuelCapacity = assetFuelCapacity;
    }

    public int getFuelID() {
        return FuelID;
    }

    public void setFuelID(int fuelID) {
        FuelID = fuelID;
    }

    public int getAssetIsActive() {
        return AssetIsActive;
    }

    public void setAssetIsActive(int assetIsActive) {
        AssetIsActive = assetIsActive;
    }

    public String getAssetLocation() {
        return AssetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        AssetLocation = assetLocation;
    }

    public int getBuildID() {
        return BuildID;
    }

    public void setBuildID(int buildID) {
        BuildID = buildID;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getStateID() {
        return StateID;
    }

    public String getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(String districtId) {
        DistrictId = districtId;
    }


    public void setStateID(String stateID) {
        StateID = stateID;
    }

    public String getAssetLocationType() {
        return AssetLocationType;
    }

    public void setAssetLocationType(String assetLocationType) {
        AssetLocationType = assetLocationType;
    }
}
