package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 1/23/2018.
 */

public class Orderstatus implements Serializable {

//       "Table": [
//    {
//        "OrderID": 2,
//            "OrderItemID": 2,
//            "Qty": 50,
//            "FuelPrice": 45.45,
//            "AssetID": 2,
//            "AssetS_ID": "rohitasset",
//            "OrderStatus": "Delivered",
//            "ItemValue": 2272.5,
//            "BookedOn": "2017-12-18T13:50:54.953",
//            "OrderStatusID": 10,
//            "ScheduledDelivery": null
//    }
//            ],
//                    "Table1": [
//    {
//        "TotalRows": 1,
//            "TotalPages": 1
//    }



    private int OrderID;
    private int OrderItemID;
    private double Qty;



    private double FuelPrice;
    private int AssetID;

    private String AssetS_ID;
    private String AssetModel;

    private String OrderStatus;
    private String ItemValue;

    private String BookedOn;
    public int OrderStatusID;
    private String ScheduledDelivery;

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getOrderItemID() {
        return OrderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        OrderItemID = orderItemID;
    }

    public double getQty() {
        return Qty;
    }

    public void setQty(double qty) {
        Qty = qty;
    }

    public double getFuelPrice() {
        return FuelPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        FuelPrice = fuelPrice;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public String getAssetS_ID() {
        return AssetS_ID;
    }

    public void setAssetS_ID(String assetS_ID) {
        AssetS_ID = assetS_ID;
    }

    public String getAssetModel() {
        return AssetModel;
    }

    public void setAssetModel(String assetModel) {
        AssetModel = assetModel;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getItemValue() {
        return ItemValue;
    }

    public void setItemValue(String itemValue) {
        ItemValue = itemValue;
    }

    public String getBookedOn() {
        return BookedOn;
    }

    public void setBookedOn(String bookedOn) {
        BookedOn = bookedOn;
    }

    public int getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(int orderStatusID) {
        OrderStatusID = orderStatusID;
    }

    public String getScheduledDelivery() {
        return ScheduledDelivery;
    }

    public void setScheduledDelivery(String scheduledDelivery) {
        ScheduledDelivery = scheduledDelivery;
    }






}
