package com.wemonde.fuelbuddy.models;

import com.google.gson.Gson;

/**
 * Created by Devendra Pandey on 8/23/2017.
 */

public class AddTokenRequest {


    private String KeyID;//0
    private String DeviceRegID;//"B12345"
    private String OSIndex;//"B1LO"


    public String getKeyID() {
        return KeyID;
    }

    public void setKeyID(String keyID) {
        KeyID = keyID;
    }

    public String getDeviceRegID() {
        return DeviceRegID;
    }

    public void setDeviceRegID(String deviceRegID) {
        DeviceRegID = deviceRegID;
    }

    public String getOSIndex() {
        return OSIndex;
    }

    public void setOSIndex(String OSIndex) {
        this.OSIndex = OSIndex;
    }


    public String getJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
