package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

public class DistrictArray implements Serializable {

   // "StateID":8,"StateName":"Haryana","DistrictId":31,"DistrictName":"Kaithal","FuelID":1,"FuelName":"Diesel","Price":43.38

//"StateID":8,"StateName":"Haryana","DistrictId":26,"DistrictName":"Yamuna Nagar","FuelID":1,"FuelName":"Diesel","Price":1.00

    private int StateID;
    private String StateName;
    private int DistrictId;
    private String DistrictName;
    private String FuelID;
    private String FuelName;
    private String Price;

    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public int getDistrictId() {
        return DistrictId;
    }

    public void setDistrictId(int districtId) {
        DistrictId = districtId;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getFuelID() {
        return FuelID;
    }

    public void setFuelID(String fuelID) {
        FuelID = fuelID;
    }

    public String getFuelName() {
        return FuelName;
    }

    public void setFuelName(String fuelName) {
        FuelName = fuelName;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }



}
