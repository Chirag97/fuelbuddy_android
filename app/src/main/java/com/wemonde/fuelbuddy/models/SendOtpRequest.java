package com.wemonde.fuelbuddy.models;

import com.google.gson.Gson;

/**
 * Created by Devendra Pandey on 10/5/2017.
 */

public class SendOtpRequest {



    private String KeyID;//0
    private String OldKey;//"passord"
    private String NewKey;//"newpassword"


    public String getKeyID() {
        return KeyID;
    }

    public void setKeyID(String keyID) {
        KeyID = keyID;
    }

    public String getOldKey() {
        return OldKey;
    }

    public void setOldKey(String oldKey) {
        OldKey = oldKey;
    }

    public String getNewKey() {
        return NewKey;
    }

    public void setNewKey(String newKey) {
        NewKey = newKey;
    }

    public String getJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
