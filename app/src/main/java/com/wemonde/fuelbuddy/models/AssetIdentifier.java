package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class AssetIdentifier {
    private int AssetIdentifierID;
    private String AssetIdentifierName;

    public int getAssetIdentifierID() {
        return AssetIdentifierID;
    }

    public void setAssetIdentifierID(int assetIdentifierID) {
        AssetIdentifierID = assetIdentifierID;
    }

    public String getAssetIdentifierName() {
        return AssetIdentifierName;
    }

    public void setAssetIdentifierName(String assetIdentifierName) {
        AssetIdentifierName = assetIdentifierName;
    }
}
