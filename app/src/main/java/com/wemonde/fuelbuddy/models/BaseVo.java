package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

/**
 * Created by Devendra Pandey on 16-12-2016.
 */
public class BaseVo implements Serializable{

//            "status": 200,
//            "code": 0,
//            "data":

    private int rs;
    private String msg;
    private String data;

    public BaseVo(int rs, String msg, String data) {
        this.rs = rs;
        this.msg = msg;
        this.data = data;
    }

    public int getRs() {
        return rs;
    }

    public void setRs(int rs) {
        this.rs = rs;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
