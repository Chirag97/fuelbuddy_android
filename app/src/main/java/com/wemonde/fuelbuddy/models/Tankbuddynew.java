package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

public class Tankbuddynew implements Serializable {

//
//      "id":5,
//              "tank_id":"T001",
//              "tank_depth":"ashas",
//              "tank_temp":"0.55",
//              "capacity":"c.66",
//              "createdat":"2018-09-11T17:16:30.127"




    private int id;
    private String tank_id;
    private String tank_temp;



    private String tank_depth;
    private String capacity;
    private String createdat;



    public String getTank_depth() {
        return tank_depth;
    }

    public void setTank_depth(String tank_depth) {
        this.tank_depth = tank_depth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTank_id() {
        return tank_id;
    }

    public void setTank_id(String tank_id) {
        this.tank_id = tank_id;
    }

    public String getTank_temp() {
        return tank_temp;
    }

    public void setTank_temp(String tank_temp) {
        this.tank_temp = tank_temp;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }


}
