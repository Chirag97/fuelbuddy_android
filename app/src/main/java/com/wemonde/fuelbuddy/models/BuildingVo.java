package com.wemonde.fuelbuddy.models;

import java.util.ArrayList;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class BuildingVo {
    private ArrayList<Building> assets;
    public ArrayList<Building> getAssets() {
        return assets;
    }
    public void setAssets(ArrayList<Building> assets) {
        this.assets = assets;
    }
}