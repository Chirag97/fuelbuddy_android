package com.wemonde.fuelbuddy.models;

import com.google.gson.Gson;

/**
 * Created by Devendra Pandey on 04-04-2017.
 */

public class AddBuildRequestVo {

    private String BuildID;//0
    private String BuildName;//"B12345"
    private String BuildLocation;//"B1LO"
    private String StateID;//5
    private String PINCode;//"803330"
    private String Latitude;//1.000
    private String Longitude;//0.991
    private String CreatedByPMgrID;//2

    public String getBuildID() {
        return BuildID;
    }

    public void setBuildID(String buildID) {
        BuildID = buildID;
    }

    public String getBuildName() {
        return BuildName;
    }

    public void setBuildName(String buildName) {
        BuildName = buildName;
    }

    public String getBuildLocation() {
        return BuildLocation;
    }

    public void setBuildLocation(String buildLocation) {
        BuildLocation = buildLocation;
    }

    public String getStateID() {
        return StateID;
    }

    public void setStateID(String stateID) {
        StateID = stateID;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getCreatedByPMgrID() {
        return CreatedByPMgrID;
    }

    public void setCreatedByPMgrID(String createdByPMgrID) {
        CreatedByPMgrID = createdByPMgrID;
    }

    public String getJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
