package com.wemonde.fuelbuddy.models;

import java.io.Serializable;

public class TankBuddyConsumption implements Serializable {

    public String getColumn1() {
        return Column1;
    }

    public void setColumn1(String column1) {
        Column1 = column1;
    }

    public String getColumn2() {
        return Column2;
    }

    public void setColumn2(String column2) {
        Column2 = column2;
    }

    private String Column1;
    private String Column2;
}
