package com.wemonde.fuelbuddy.models;

import java.util.ArrayList;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class FuelTypeVo {
    private ArrayList<FuelType> assets;

    public ArrayList<FuelType> getAssets() {
        return assets;
    }

    public void setAssets(ArrayList<FuelType> assets) {
        this.assets = assets;
    }
}
