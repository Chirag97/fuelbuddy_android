package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class BookingVo {
    
            private int OrderID;
            private int OrderItemID;
            private double Qty;
            private double FuelPrice;
            private int AssetID;
            private String AssetLocation;
            private String AssetS_ID;
            private String AssetModel;
            private String AssetIdentifierName;
            private int AssetIdentifierID;
            private String FuelName;
            private String OrderStatus;
            private String ItemValue;
            private String FExecVNumber;
            private String FExecVDetail;
            private String FExecPhone1;
            private String FExecPhone2;
            private String BookedOn;
            private String Location;



    private double Latitude;
    private double Longitude;


    private String PINCode;

    private int StateID;




    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getOrderItemID() {
        return OrderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        OrderItemID = orderItemID;
    }

    public double getQty() {
        return Qty;
    }

    public void setQty(double qty) {
        Qty = qty;
    }

    public double getFuelPrice() {
        return FuelPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        FuelPrice = fuelPrice;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public String getAssetLocation() {
        return AssetLocation;
    }

    public void setAssetLocation(String assetLocation) {
        AssetLocation = assetLocation;
    }

    public String getAssetS_ID() {
        return AssetS_ID;
    }

    public void setAssetS_ID(String assetS_ID) {
        AssetS_ID = assetS_ID;
    }

    public String getAssetModel() {
        return AssetModel;
    }

    public void setAssetModel(String assetModel) {
        AssetModel = assetModel;
    }

    public String getAssetIdentifierName() {
        return AssetIdentifierName;
    }

    public void setAssetIdentifierName(String assetIdentifierName) {
        AssetIdentifierName = assetIdentifierName;
    }

    public int getAssetIdentifierID() {
        return AssetIdentifierID;
    }

    public void setAssetIdentifierID(int assetIdentifierID) {
        AssetIdentifierID = assetIdentifierID;
    }

    public String getFuelName() {
        return FuelName;
    }

    public void setFuelName(String fuelName) {
        FuelName = fuelName;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getItemValue() {
        return ItemValue;
    }

    public void setItemValue(String itemValue) {
        ItemValue = itemValue;
    }

    public String getFExecVNumber() {
        return FExecVNumber;
    }

    public void setFExecVNumber(String FExecVNumber) {
        this.FExecVNumber = FExecVNumber;
    }

    public String getFExecVDetail() {
        return FExecVDetail;
    }

    public void setFExecVDetail(String FExecVDetail) {
        this.FExecVDetail = FExecVDetail;
    }

    public String getFExecPhone1() {
        return FExecPhone1;
    }

    public void setFExecPhone1(String FExecPhone1) {
        this.FExecPhone1 = FExecPhone1;
    }

    public String getFExecPhone2() {
        return FExecPhone2;
    }

    public void setFExecPhone2(String FExecPhone2) {
        this.FExecPhone2 = FExecPhone2;
    }

    public String getBookedOn() {
        return BookedOn;
    }

    public void setBookedOn(String bookedOn) {
        BookedOn = bookedOn;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }


    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

    public String getPINCode() {
        return PINCode;
    }

    public void setPINCode(String PINCode) {
        this.PINCode = PINCode;
    }




    public int getStateID() {
        return StateID;
    }

    public void setStateID(int stateID) {
        StateID = stateID;
    }




}
