package com.wemonde.fuelbuddy.models;

/**
 * Created by Devendra Pandey on 1/22/2018.
 */

public class



Getorderstatus {

//    {
//        "PMgrID": 0,
//            "OrderID": 2,
//            "OrderItemID": 0,
//            "OrderStatusID": 0,
//            "CompactDataLevel": 1,
//            "PageNo": 1,
//            "RecordPerPage": 20
//    }




    private String PMgrID;//0
    private int OrderID;//"B12345"
    private int OrderItemID;//"B1LO"

    private int OrderStatusID;//0
    private int CompactDataLevel;//"B12345"
    private int PageNo;//"B1LO"
    private int RecordPerPage;


    public String getPMgrID() {
        return PMgrID;
    }

    public void setPMgrID(String PMgrID) {
        this.PMgrID = PMgrID;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public int getOrderItemID() {
        return OrderItemID;
    }

    public void setOrderItemID(int orderItemID) {
        OrderItemID = orderItemID;
    }

    public int getOrderStatusID() {
        return OrderStatusID;
    }

    public void setOrderStatusID(int orderStatusID) {
        OrderStatusID = orderStatusID;
    }

    public int getCompactDataLevel() {
        return CompactDataLevel;
    }

    public void setCompactDataLevel(int compactDataLevel) {
        CompactDataLevel = compactDataLevel;
    }

    public int getPageNo() {
        return PageNo;
    }

    public void setPageNo(int pageNo) {
        PageNo = pageNo;
    }

    public int getRecordPerPage() {
        return RecordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        RecordPerPage = recordPerPage;
    }




}
