package com.wemonde.fuelbuddy.base;

import com.wemonde.fuelbuddy.BuildConfig;
import com.wemonde.fuelbuddy.adapters.OrderRecVerAdapter;

/**
 * Created by Devendra Pandey on 16-03-2017.
 **/

public class Apis {

//
//    OrderStatusID	OrderStatus
//1	Opened
//2	Re-Opened
//4	Assigned
//6	Accepted
//8	Arriving
//10	Delivered
//20	Declined


    //https://www.fuelbuddyapp.com/FBSecure/AuthService.svc/
    private static String baseurl = "http://fuelbuddyapp.com/FuelBuddyServicesCor/FuelBuddy.svc/";

    //testurl
    private static String baseurllogin ="https://www.fuelbuddyapp.com/FBSecureStage/AuthService.svc/";

    //liveurl
   //private static String baseurllogin = "https://www.fuelbuddyapp.com/FBSecure/AuthService.svc/";
    //live
    //private static String baseurlorder = "https://www.fuelbuddyapp.com/FBSecure/CorporateAPIs.svc/";

    //test
private static String baseurlorder = "https://www.fuelbuddyapp.com/FBSecureStage/CorporateAPIs.svc/";

    //testing url
    private static String baseurlnew = "https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/";
    //live url
// private static String baseurlnew =    "https://www.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/";

    private static String baseurlnewforimage = "https://www.fuelbuddyapp.com/";
   // private static String baseurl = "http://fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/";
    //https://www.live.fuelbuddyapp.com/FBSecure/CorporateAPIs.svc/GetPMgrOrder
    private static String getBaseUrl(){
        return BuildConfig.BASE_URL;
    }

    public static String URL_LOGIN = baseurllogin + "ValidatePurchaseMgr/<UNAME>/<PWD>";
    public static String URL_GET_ASSETS = baseurlnew + "GetPMgrBuildingAsset/<UID>";
    public static String URL_GET_BUILDS = baseurlnew + "GetPMgrBuilding/<UID>";
    public static String URL_GET_BUILDS_ALL = baseurlnew + "PMgrCreatedBuildings/<UID>";
  //  public static String URL_FUEL_TYPES = getBaseUrl() + "GetFuelType";
    public static String URL_FUEL_TYPES = baseurlnew + "GetFuelType";
    public static String URL_GET_STATES = baseurlnew + "GetStates";


  //  public static String URL_BOOK_ORDER = "https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/BookOrder";
//    Updated price list on daily basis, district wise pricing
//
//     Well, we did some bug fixes too

            //baseurlnew + "BookOrder";

    //https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/BookOrder

   public static String URL_BOOK_ORDER = baseurlorder + "BookOrder";
    public static String URL_ASSET_IDENTIFIER = baseurlnew + "GetAssetIdentifier";
    public static String URL_ADD_ASSET = baseurlnew + "AddAsset";
    public static String URL_ADD_BUILD = baseurlnew + "Addbuilding";
    public static String URL_GET_ORDERS = baseurlnew + "GetPMgrOrders/<UID>/0";

    public static String URL_GET_ORDERS_OPEN = baseurlnew + "GetPMgrOrders/<UID>/1";
    public static String URL_GET_ORDERS_Accepted = baseurlnew + "GetPMgrOrders/<UID>/6";
    public static String URL_GET_ORDERS_Declined = baseurlnew + "GetPMgrOrders/<UID>/20";
    public static String URL_GET_ORDERS__delivered = baseurlnew + "GetPMgrOrders/<UID>/10";

    public static String URL_GET_TRACKING_DETAILS = baseurlnew + "GetOrderDeliveryStatus/<UID>";


   // https://www.live.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/UploadPMgrImage/1

    public static String URL_FOR_POSTING_TOKEN = baseurlnew +"RegisterPMgrDevice";
    public static String URL_FOR_POSTING_IMAGE = baseurlnew+ "UploadPMgrImage/<UID>";
    public static String URL_FOR_IMAGE_DOWNLOADING = baseurlnewforimage+ "<UID>";

    //https://www.live.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/UpdatePMgrPassword
    public static String URL_FOR_PASSWORD_CHANGE = baseurllogin+ "UpdatePMgrPassword";

   // http://fuelbuddyapp.com/FuelBuddyServicesCor/FuelBuddy.svc/PMgrResetPasswordOTPRequest/DLFPM3/8299132891
    public static String URL_FOR_OTP_REQUEST = baseurlnew+ "PMgrResetPasswordOTPRequest/<UID>/<PHONE>";

    //http://fuelbuddyapp.com/FuelBuddyServicesCor/FuelBuddy.svc/ResetPMgrPassword
    public static String URL_FOR_RESET_PASSWORD = baseurlnew+ "ResetPMgrPassword";

    //https://www.live.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/GetPMgrAssetTopQtyOrdered/1/-5
    public static String URL_FOR_GET_DASHBOARD_DETAILS = baseurlnew+ "GetPMgrAssetTopQtyOrdered/<UID>/-30";

    public static String URL_FOR_NEW_USER_REGISTRATION = baseurllogin + "OnlineCompanyRegistrationRequest";

    //https://www.live.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/GetPurchaseManagerOrders
    public static String URL_FOR_ORDER_FILLING = baseurlnew + "GetPurchaseManagerOrders";

   // GetSensorValueForDeliveredOrder/OrdrItemID

    public static String URL_FOR_SENSOR_DATA = baseurlnew + "GetSensorValueForDeliveredOrder/<UID>";

    //get tank details

    //GetAssetsTanksDetail/1
    public static String URL_FOR_TANK_DATA = baseurlnew + "GetAssetsTanksDetail/<UID>";


    //public static String URL_FOR_TANK_DATA_NEW = baseurlnew + "GetTankBuddy/<UID>";

    public static String URL_FOR_TANK_DATA_NEW =  "https://www.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/GetTankBuddy/<UID>";

    public static String URL_FOR_GET_DISTRICT_BY_STATE = baseurlnew + "GetDistrictByState/<UID>";
    public static String URL_FOR_UNLOCK_ELOCK = "https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/InAppUnLock";
    public static String URL_FOR_LOCK = "https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/InAppLock";

//    public static String URL_FOR_UNLOCK_ELOCK = "https://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1";
//    public static String URL_FOR_LOCK = "https://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1";

   // "https://nominatim.openstreetmap.org/reverse?format=json&lat=52.5487429714954&lon=-1.81602098644987&zoom=18&addressdetails=1"



    //https://www.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/RegistrationOTPVerification/


    public static String URL_FOR_OTP_REGISTRATION ="https://www.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/RegistrationOTPVerification/<UID>";
   // https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/GetDistrictByState/8

    public static String URL_FOR_VERIFY_OTP_REGISTRATION ="https://www.fuelbuddyapp.com/FuelBuddyServices/FuelBuddy.svc/MobVerifyRegistrationOTPcheck/<UID>/<OTP>";


    public static String URL_FOR_DAILY_TANKCONSUMPTION = "https://www.fuelbuddyapp.com/FuelBuddyServicesStage/FuelBuddy.svc/GetTankBuddyConsumptionFilling/<UID>";

}