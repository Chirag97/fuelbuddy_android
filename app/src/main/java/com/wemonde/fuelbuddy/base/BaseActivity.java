package com.wemonde.fuelbuddy.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wemonde.fuelbuddy.BuildConfig;
import com.wemonde.fuelbuddy.MapActivity;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.adapters.ConnectivityReceiver;
import com.wemonde.fuelbuddy.interfaces.VolleyInterface;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.AssetIdentifier;
import com.wemonde.fuelbuddy.models.AssetVo;
import com.wemonde.fuelbuddy.models.Assettankdetails;
import com.wemonde.fuelbuddy.models.BaseVo;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.models.Dashboard;
import com.wemonde.fuelbuddy.models.DistrictArray;
import com.wemonde.fuelbuddy.models.FuelType;
import com.wemonde.fuelbuddy.models.FuelTypeVo;
import com.wemonde.fuelbuddy.models.LoginVo;
import com.wemonde.fuelbuddy.models.MyLoc;
import com.wemonde.fuelbuddy.models.Orderstatus;
import com.wemonde.fuelbuddy.models.OtpClass;
import com.wemonde.fuelbuddy.models.Sensor;
import com.wemonde.fuelbuddy.models.State;
import com.wemonde.fuelbuddy.models.TankBuddyConsumption;
import com.wemonde.fuelbuddy.models.Tankbuddynew;
import com.wemonde.fuelbuddy.models.Tracking;
//import com.wemonde.fuelbuddy.payment.UIActivity;
import com.wemonde.fuelbuddy.ui.AddAssetActivity;
import com.wemonde.fuelbuddy.ui.AddBuildingActivity;
import com.wemonde.fuelbuddy.ui.Animatedviewfuelfilling;
//import com.wemonde.fuelbuddy.ui.Cardpaymenui;
import com.wemonde.fuelbuddy.ui.ContactActivity;
import com.wemonde.fuelbuddy.ui.Dashboardnewui;
import com.wemonde.fuelbuddy.ui.Dashboardui;
import com.wemonde.fuelbuddy.ui.Elocks;
import com.wemonde.fuelbuddy.ui.Feedback;
import com.wemonde.fuelbuddy.ui.HomeActivity;
import com.wemonde.fuelbuddy.ui.IndividualActivity;
import com.wemonde.fuelbuddy.ui.Livefueldispensing;
import com.wemonde.fuelbuddy.ui.LoginActivity;
import com.wemonde.fuelbuddy.ui.MyAssetsActivity;
import com.wemonde.fuelbuddy.ui.MyBuildingActivity;
import com.wemonde.fuelbuddy.ui.OrderCompletionFinal;
import com.wemonde.fuelbuddy.ui.OrderListActivity;
import com.wemonde.fuelbuddy.ui.OrderSummaryActivity;
import com.wemonde.fuelbuddy.ui.OtpRegistrationPhone;
import com.wemonde.fuelbuddy.ui.PaymentHistoryActivity;
import com.wemonde.fuelbuddy.ui.PlaceOrder;
import com.wemonde.fuelbuddy.ui.PriceListDetails;
import com.wemonde.fuelbuddy.ui.Privacypolicy;
import com.wemonde.fuelbuddy.ui.ProfileActivity;
import com.wemonde.fuelbuddy.ui.SchedulingActivity;
import com.wemonde.fuelbuddy.ui.SignupActivity;
import com.wemonde.fuelbuddy.ui.TankLevelshowing;
import com.wemonde.fuelbuddy.ui.Termsofuse;
import com.wemonde.fuelbuddy.ui.TrackingActivity;
import com.wemonde.fuelbuddy.ui.UserRegistrationActivity;
import com.wemonde.fuelbuddy.utils.GPSTracker;
import com.wemonde.fuelbuddy.utils.JsonUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;
import com.wemonde.fuelbuddy.utils.VolleyHelper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

//import butterknife.Unbinder;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static com.wemonde.fuelbuddy.MapActivity.mapactivitynw;
import static com.wemonde.fuelbuddy.adapters.OrderRecVerAdapter.orderid;
import static com.wemonde.fuelbuddy.base.Apis.URL_ASSET_IDENTIFIER;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_GET_DASHBOARD_DETAILS;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_SENSOR_DATA;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_TANK_DATA_NEW;
import static com.wemonde.fuelbuddy.base.Apis.URL_FUEL_TYPES;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_ASSETS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_BUILDS;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_STATES;
import static com.wemonde.fuelbuddy.base.Apis.URL_GET_TRACKING_DETAILS;
import static com.wemonde.fuelbuddy.base.Apis.URL_FOR_TANK_DATA;
import static com.wemonde.fuelbuddy.ui.HomeActivity.assetId;
import static com.wemonde.fuelbuddy.ui.HomeActivity.bottomNavigationView;
import static com.wemonde.fuelbuddy.ui.HomeActivity.fuelId;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_FUEL_TYPES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSET_IDENTIFIER;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_BUILDING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DASHBOARD_DETAILS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_DISTRICT;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_LOCATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_STATES;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_TRACKING;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_NEW_USER_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_OTP_PASSWORD_CHANGE;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_ORDER_STATUS_NEW;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_SENSOR_DATA;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_TANK_DETAILS;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_VERIFY_OTP_REGISTRATION;
import static com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper.getDefaultPref;

/**
 * Created by Devendra Pandey on 12-03-2017.
 **/

public class BaseActivity extends AppCompatActivity implements VolleyInterface,View.OnClickListener,LocationListener {

    public static final LatLng DELHI = new LatLng(28.7041, 77.1025);
    private static LoginVo lvo;
    private static UserRegistrationActivity userregister;
    public String RSymbol = "₹";
    protected Marker marker;
    protected Marker markernew;
    protected GoogleMap googleMap;
    protected float zoom = 15;
    protected Context bContext;
    protected Toolbar toolbar;
    protected LayoutInflater inflater;
    protected ProgressDialog progressD;
    public   String uid = null;
    public static String imagepath;
    public static String checkotp = null;
    protected static ArrayList<Asset>  assets;

    public static ArrayList<Sensor>  sensors;

    public static ArrayList<Assettankdetails>  tankdetails;

    public static ArrayList<Tankbuddynew> newtankbuddy;

    protected static ArrayList<LoginVo> profileupdate;

    protected static ArrayList<State> states;

    protected static ArrayList<Tracking> track;

    protected static ArrayList<Dashboard> dashboarddetails;

    protected static ArrayList<Orderstatus> orderstatusdetails;

    protected static ArrayList<OtpClass> otparray;


    protected static ArrayList<DistrictArray> districtarray;




    protected static ArrayList<com.wemonde.fuelbuddy.models.Location> locations;



    protected ArrayList<FuelType> fuelTypes;
    protected static ArrayList<Building> buildings;



    protected static ArrayList<AssetIdentifier> identifiers;

    protected static ArrayList<TankBuddyConsumption> consumptiondaily;
    protected static ArrayList<TankBuddyConsumption> consumptionmonthly;
    protected int gpsCount = 0;
    private RelativeLayout screen_area;
    private LinearLayout ll_footer_area;
    private TabLayout tabs_base;
    private View parentLayout;
    private int onStartCount;
    private GPSTracker gps;

    private MyLoc myLoc;
    private ImageView imgMyLocation;
    public static String AddressFromGoogleApi = " ";
    public static String PinfromGoogleApi = " ";

    public  static double Ltdnew;
    public  static double Lngnew;


    public static final String MyPREFERENCES = "MyPrefs";

    private String Address1 = "", Address2 = "", City = "", State = "", Country = "", County = "", PIN = "";

    InputStream is = null;
    String result = "";
    JSONObject jObject = null;

    public SharedPreferences sharedpreferences;


    private static BaseActivity instance;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);




        inflater = getLayoutInflater();

        myLoc = new MyLoc();

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);




        bContext = this;
        getLoginVo();

        try {




            uid = getLoginVo().getPMgrID();

            imagepath =getLoginVo().getPMgrImagePath();


        } catch (Exception e) {
            e.printStackTrace();
        }

        Slog.d("Class Name: " + this.getLocalClassName());

        initUi();
        onStartCount = 1;
        if (savedInstanceState == null) { // 1st time
            this.overridePendingTransition(R.anim.animation_entry_right, R.anim.stay_still);
        } else { // already created so reverse animation
            onStartCount = 2;
        }

        try {
            parentLayout = findViewById(android.R.id.content);
        } catch (Exception e) {
            Slog.e("error in parent layout" + e.getMessage());
        }


//        mTfRegular = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
//        mTfLight = Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf");
    }



    public static BaseActivity getInstance(){
        return instance;
    }





    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }




    public void getGpsLoc() {

        gps = new GPSTracker(bContext);




// Register the listener with the Location Manager to receive location updates
        //  locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);



        // check if GPS enabled
        if (gps.canGetLocation()) {




            gps.getLatitude();
            gps.getLocation();
            gps.getFLocation();
            gps.getLongitude();
            // getLocation(gps);
            canGetLocation(gps);
            // \n is for new line

        } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            if (gpsCount < 1) {
                //  gps.showSettingsAlert();
            }
        }
        gpsCount++;
    }

    protected void getLocation(GPSTracker gps){

    }


    protected void canGetLocation(GPSTracker gps) {

    }//override this for location

    public TabLayout getTabs_base() {
        tabs_base = (TabLayout) findViewById(R.id.base_tabs);
        tabs_base.setVisibility(View.VISIBLE);
        return tabs_base;
    }


    public void gettankdetailsnew () {
        String url = URL_FOR_TANK_DATA_NEW.replace("<UID>", "TB00001");
        volleyGetHit(url,REQUEST_TANK_DETAILS );
    }


    private void initUi() {
        screen_area = (RelativeLayout) findViewById(R.id.screen_area);
        ll_footer_area = (LinearLayout) findViewById(R.id.ll_footer);
//        loading_view_ll = (RelativeLayout) findViewById(R.id.loading_view_ll);

        // First we need to check availability of play services
//        if (checkPlayServices()) {
        // Building the GoogleApi client
//        }
    }

    protected void gotoHome() {
        if (!isGuest()) {
            openHome();
        } else {
            Intent intent = new Intent(bContext, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        finish();
    }


    protected void gotopricedetails () {
        Intent intent = new Intent(bContext, PriceListDetails.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void openStartupnew() {
        Intent intent = new Intent(bContext, Animatedviewfuelfilling.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    protected void openHome() {
        Intent intent = new Intent(bContext, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    protected void gotoLogin() {
        if (!isGuest()) {
            openHome();
            return;
        }

        Intent intent = new Intent(bContext, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void gotoSignup() {
        Intent intent = new Intent(bContext, SignupActivity.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void gotoCardpaymenui(String total) {


//        Intent intent = new Intent(bContext, UIActivity.class);
//        String amount = String.valueOf(total);
//        intent.putExtra("value", amount);
//        bContext.startActivity(intent);


//        Intent intent = new Intent(bContext, Cardpaymenui.class);
//         intent.putExtra("Total",total);
//        startActivity(intent);
    }



    protected void gotoProfile() {
        Intent intent = new Intent(bContext, ProfileActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void gotoNewUserRegistration(String phone) {
        Intent intent = new Intent(bContext, UserRegistrationActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("Mobno",phone);
        startActivity(intent);
    }


    protected void gotoNewUserRegistrationPhone() {
        Intent intent = new Intent(bContext, OtpRegistrationPhone.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        startActivity(intent);
    }

    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    protected void gotoContact() {
        Intent intent = new Intent(bContext, ContactActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    protected void gotoindividual(){
        Intent intent= new Intent(bContext, IndividualActivity.class);
        startActivity(intent);
    }




//    public void gotoPlaceOrdernew(String assetname,String assetid) {
//        Intent intent = new Intent(bContext, PlaceOrder.class);
//        intent.putExtra("borv", assetname +"@&"+ assetid);
//        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//
//
//        // startActivity(intent);
//        // finish();
//    }

    protected void gotoOrderHistory(String name,String url) {
        Intent intent = new Intent(bContext, OrderListActivity.class);
        intent.putExtra("borv", name +"@&"+ url);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void gotoAddAsset() {
        // getBuildings();
        Intent intent = new Intent(bContext, AddAssetActivity.class);
        startActivity(intent);
    }

    public void gotoOrdercompletionfinal() {
        // getBuildings();
        Intent intent = new Intent(bContext, OrderCompletionFinal.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();


    }

    public void gotoAddAsset(Asset asset) {
        //   getBuildings();
        Intent intent = new Intent(bContext, AddAssetActivity.class);
        intent.putExtra("asset", asset);
        startActivity(intent);







    }

    public void gotoAddBuilding() {
        Intent intent = new Intent(bContext, AddBuildingActivity.class);
        startActivity(intent);
    }

    public void gotoAddBuilding(Building build) {
        Intent intent = new Intent(bContext, AddBuildingActivity.class);


        intent.putExtra("build", build);
        startActivity(intent);
    }

    public void gotoMyAsset() {
        Intent intent = new Intent(bContext, MyAssetsActivity.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);


    }

    public void gotoBuilding() {
        Intent intent = new Intent(bContext, MyBuildingActivity.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void gotoOrderSummary(BookOrderRequestVo borv) {
        Intent intent = new Intent(bContext, OrderSummaryActivity.class);
        intent.putExtra("borv", borv);
        startActivity(intent);
        bottomNavigationView.setVisibility(View.GONE);
        assetId = -1;
        fuelId = -1;

    }

    public void gotoOrderSummaryfordynamicaddress(BookOrderRequestVo borv) {
        Intent intent = new Intent(bContext, OrderSummaryActivity.class);
        intent.putExtra("borv", borv);
        startActivity(intent);
        bottomNavigationView.setVisibility(View.VISIBLE);
        assetId = -1;
        fuelId = -1;

    }


    public void gotoScheduling() {
        Intent intent = new Intent(bContext, SchedulingActivity.class);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }
    public void gotoDashboard() {
        Intent intent = new Intent(bContext, Dashboardui.class);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }

    public void gototermsofuse() {
        Intent intent = new Intent(bContext, Termsofuse.class);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }
    public void gotoprivacypolicy() {
        Intent intent = new Intent(bContext, Privacypolicy.class);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }

    public void gotonewDashboard() {

//        getdashboard();
        Intent intent = new Intent(bContext, Dashboardnewui.class);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }

public void gotoPaymentHistory(){
        Intent intent= new Intent(bContext, PaymentHistoryActivity.class);
        startActivity(intent);
}

    public void gotofeedbackactivity() {

//        getdashboard();
        Intent intent = new Intent(bContext, Feedback.class);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }

    public void gotoPlaceOrder() {
        Intent intent = new Intent(bContext, PlaceOrder.class);
        intent.putExtra("borv", "");
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        //  startActivity(intent);
        //  finish();
    }



    public void gotoTankLevelsenseing() {
        Intent intent = new Intent(bContext, TankLevelshowing.class);
       // intent.putExtra("borv", assetname +"@&"+ assetid);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        // startActivity(intent);
        // finish();
    }


    public void gotoelocksactivity() {
        Intent intent = new Intent(bContext, Elocks.class);
        //  intent.putExtra("borv", assetname +"@&"+ assetid);
        // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        // startActivity(intent);
        // finish();
    }

    public void gotoLivefueldispensing() {
        Intent intent = new Intent(bContext, Livefueldispensing.class);
      //  intent.putExtra("borv", assetname +"@&"+ assetid);
       // intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        // startActivity(intent);
        // finish();
    }




    public void gotoPlaceOrdernew(String assetname,String assetid) {
        Intent intent = new Intent(bContext, PlaceOrder.class);
        intent.putExtra("borv", assetname +"@&"+ assetid);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


        // startActivity(intent);
        // finish();
    }
    public void gotoTracking() {
        Intent intent = new Intent(bContext, TrackingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        //  intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        //  intent.putExtra("borv", borv);
        startActivity(intent);
    }

    public void logout() {
        clearUserData();
    }




    public void clearUserData() {
        lvo = null;

        SharedPreferences.Editor editor = getDefaultPref(bContext).edit();
        editor.remove("com.sarkari");
        editor.remove("location-lat");
        editor.remove("location-lng");
        editor.remove("company-id");
        editor.remove("facebook-id");
        editor.remove("lvo");
        editor.remove("fuels");
        editor.remove("account-pass");
        editor.remove("account-uuid");
        editor.remove("order-id");
        editor.remove("account-uname");
        editor.remove("account-email");
        editor.remove("account-phone");
        editor.remove("account-login");
        editor.remove("referral-code");

        editor.commit();

        //  SharedPrefrenceHelper.clearAll(bContext);
        SharedPrefrenceHelper.setIsLogin(bContext, false);
        SharedPrefrenceHelper.setUUID(bContext, "-1");
        SharedPrefrenceHelper.setLvo(bContext, " ");
        SharedPrefrenceHelper.setFuels(bContext, "");
        SharedPrefrenceHelper.setNotificationCount(bContext,"");
        if (assets != null) {
            assets.clear();
        }
        if (dashboarddetails != null) {
            dashboarddetails.clear();
        }

        uid = "-1";


        if (fuelTypes != null) {
            fuelTypes.clear();
        }

        this.profileupdate = null;
        // profileupdate.clear();
        gotoHome();
    }

    protected boolean isGuest() {
        return !SharedPrefrenceHelper.getIsLogin(this);
    }

    protected void setBasicToolBar(int resId) {
        setBasicToolBar(getString(resId), R.id.toolbar);
    }

    protected void setBasicToolBar(String title) {
        setBasicToolBar(title, R.id.toolbar);
    }

    private void setBasicToolBar(String title, int toolbar_id) {
        try {
            toolbar = (Toolbar) findViewById(toolbar_id);
            setSupportActionBar(toolbar);
            toolbar.setTitle(title);

            TextView tv_title = (TextView) findViewById(R.id.home_label_current_location);
            tv_title.setText("" + title);

            getSupportActionBar().setTitle("" + title);
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (NullPointerException ne) {
            Slog.e("error in getsupportActionbar centerlist on create");
        }
    }

    // Acquire a reference to the system Location Manager



//    protected void setTransparentToolbar(int rid) {
//
//        toolbar = (Toolbar) findViewById(rid);
//        setSupportActionBar(toolbar);
//        toolbar.setTitle("");
//
////        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
////        getSupportActionBar().setDisplayShowCustomEnabled(true);
////        getSupportActionBar().setDisplayShowHomeEnabled(false);
////        getSupportActionBar().setDisplayShowTitleEnabled(false);
////        getSupportActionBar().setDisplayUseLogoEnabled(false);
//
//        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_back_white);
//        upArrow.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(upArrow);
//
//        setStatusBarTranslucent(true);
//    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().setStatusBarColor(Color.TRANSPARENT);
//        }
    }

    public void volleyGetHit(String url, int rcode) {
        //trustEveryone();
        VolleyHelper.getRequestVolley(this, url, rcode);
    }

    public void volleyPostHit(String url, HashMap hm, int rcode) {
        VolleyHelper.postRequestVolley(bContext, url, hm, rcode);
    }

    public void volleyBodyHit(String url, String json, int rcode) {
        try {
            // trustEveryone();
            JSONObject jObj = new JSONObject(json);
            VolleyHelper.postRequestByBodyVolley(bContext, url, jObj, rcode);
        } catch (Exception e) {
            Slog.e("exception creating json object");
            e.printStackTrace();
        }
    }

    public LoginVo getLoginVo() {
        if (lvo != null) {
            return lvo;
        }

        String asset = SharedPrefrenceHelper.getLvo(bContext);

        Gson gson = new Gson();
        lvo = gson.fromJson(asset.toString(), LoginVo.class);
        return lvo;
    }

    @Override
    public void requestStarted(int request_code) {
        Slog.d("reqquest started");
        showProgress("Loading");
    }

    protected void onResponse(int request_code, String response, String data) {
        Slog.d("data : in base activity : " + data.toString());

        switch (request_code) {
            case REQUEST_GET_ASSETS:
                try {
                    Gson gson = new Gson();
                    AssetVo assetVo = gson.fromJson(data, AssetVo.class);
                    assets = assetVo.getAssets();
                    //HomeActivity.assetsdetails = assets;
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case REQUEST_FUEL_TYPES:
                try {
                    SharedPrefrenceHelper.setFuels(bContext, data);
                    Gson gson = new Gson();
                    FuelTypeVo assetVo = gson.fromJson(data, FuelTypeVo.class);
                    fuelTypes = assetVo.getAssets();
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }

            case REQUEST_GET_BUILDING:
                try {

                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Building>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    buildings = gson.fromJson(jobj.get("assets").toString(), type);
                    // setBuildingSpinner();
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    showLogToast("unable to parse buildings");
                    e.printStackTrace();
                }
                break;

            case REQUEST_GET_ASSET_IDENTIFIER:
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<AssetIdentifier>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    identifiers = gson.fromJson(jobj.get("assets").toString(), type);
                    // setAssetSpinner();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;


            case REQUEST_GET_STATES:
                try {
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<State>>() {}.getType();
                    states = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
                    // setStateSpinner();
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;

            case REQUEST_GET_TRACKING:
                try {
                    Gson gson = new Gson();

                    //  Tracking track = gson.fromJson(data, Tracking.class);
                    //assets = assetVo.getAssets();

                    Type type = new TypeToken<ArrayList<Tracking>>() {}.getType();
                    track = gson.fromJson(new JSONObject(data).get("assets").toString(), type);

                    gotoTracking();
                    // setBuildAdapter();
                } catch (Exception e) {
                    showToast("No Building found!");
                    e.printStackTrace();
                }
                break;

//            case REQUEST_PASSWORD_CHANGE :
//                Gson gson = new Gson();
//
//                Type type = new TypeToken<ArrayList<LoginVo>>() {}.getType();
//                try {
//                    profileupdate = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }


//            case REQUEST_OTP_PASSWORD_CHANGE:
//                try {
//
//
////                    Intent intent = new Intent("finish_activity_password_change");
////                    sendBroadcast(intent);
//
//                    checkotp = "yes";
////                    SignupActivity pro = new SignupActivity();
////                    pro.otpdialog();
//                    //   Gson gson = new Gson();
//
//
////                    Type type = new TypeToken<ArrayList<OtpClass>>() {}.getType();
////                    otparray = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
//
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//                break;
//


            case REQUEST_GET_DASHBOARD_DETAILS:

                try {
                    Gson gson1 = new Gson();

                    //  Tracking track = gson.fromJson(data, Tracking.class);
                    //assets = assetVo.getAssets();

                    Type type1 = new TypeToken<ArrayList<Dashboard>>() {}.getType();



                    JSONObject jsonObject = new JSONObject(data);
//                JSONArray events = jsonObject.getJSONArray("assets");
//                String jsonString = events.toString();



                    JSONObject getassetarray = jsonObject.getJSONObject("assets");


                    // String innerJson = gson.toJson(new JSONObject(data).get("assets"));


                    dashboarddetails = gson1.fromJson(new JSONObject(getassetarray.toString()).get("Table").toString(), type1);

                    //  showToast("Scuess");
                    // gotoTracking();
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;

//
////            case REQUEST_NEW_USER_REGISTRATION :
////                try {
////
////
////
////
////
////                    Intent intent = new Intent("finish_activity");
////                    sendBroadcast(intent);
////
////
////
////                }catch (Exception e) {
////
////                }
////                break;

            case REQUEST_ORDER_STATUS_NEW :


//                "Table": [
//            {
//                "OrderID": 2,
//                    "OrderItemID": 2,
//                    "Qty": 50,
//                    "FuelPrice": 45.45,
//                    "AssetID": 2,
//                    "AssetS_ID": "rohitasset",
//                    "OrderStatus": "Delivered",
//                    "ItemValue": 2272.5,
//                    "BookedOn": "2017-12-18T13:50:54.953",
//                    "OrderStatusID": 10,
//                    "ScheduledDelivery": null
//            }

                try {

                    Gson gson1 = new Gson();

                    //  Tracking track = gson.fromJson(data, Tracking.class);
                    //assets = assetVo.getAssets();

                    Type type1 = new TypeToken<ArrayList<Orderstatus>>() {}.getType();



                    JSONObject jsonObject = new JSONObject(data);
//                JSONArray events = jsonObject.getJSONArray("assets");
//                String jsonString = events.toString();



                    JSONObject getassetarray = jsonObject.getJSONObject("assets");


                    // String innerJson = gson.toJson(new JSONObject(data).get("assets"));



                    orderstatusdetails = gson1.fromJson(new JSONObject(getassetarray.toString()).get("Table").toString(), type1);

                    //    showToast("" + orderstatusdetails);

                }catch (Exception e){

                }
                break;
            case REQUEST_SENSOR_DATA :

                try {

                    if (sensors == null) {
                        sensors = new ArrayList<>();
                    }
                    Gson gson = new Gson();
                    Type type = new TypeToken<ArrayList<Sensor>>() {}.getType();
                    JSONObject jobj = new JSONObject(data);
                    sensors = gson.fromJson(jobj.get("assets").toString(), type);


                    gotoOrdercompletionfinal();

                    //showToast(""+sensors);
//                    Gson gson = new Gson();
//                    Sensoravo sensorvo = gson.fromJson(data, Sensoravo.class);
//                    sensors = sensorvo.getSensors();
                    //HomeActivity.assetsdetails = assets;
                } catch (Exception e) {
                    Slog.e("exception in assets -====---  " + e.getMessage());
                    e.printStackTrace();
                }
                break;
//            case REQUEST_TANK_DETAILS:



//                try {
//                    if (newtankbuddy == null) {
//                        newtankbuddy = new ArrayList<>();
//                    }
//                    Gson gson = new Gson();
//                    Type type = new TypeToken<ArrayList<Tankbuddynew>>() {}.getType();
//
//                    newtankbuddy = gson.fromJson(new JSONObject(data).get("assets").toString(), type);
//
//                    gotoTankLevelsenseing("Asset","56");
//                    // JSONObject jobj = new JSONObject(data);
//                    // newtankbuddy = gson.fromJson(data.toString(), type);
//
//                    // tankdetailsshow ();
//
//
//                }catch (Exception e) {
//                    Slog.e("exception in assets -====---  " + e.getMessage());
//                    e.printStackTrace();
//                }
//                try {
//                    if (tankdetails == null) {
//                        tankdetails = new ArrayList<>();
//                    }
//                    Gson gson = new Gson();
//                    Type type = new TypeToken<ArrayList<Assettankdetails>>() {}.getType();
//                    // JSONObject jobj = new JSONObject(data);
//                    tankdetails = gson.fromJson(data.toString(), type);
//
//
//                }catch (Exception e) {
//                    Slog.e("exception in assets -====---  " + e.getMessage());
//                    e.printStackTrace();
//                }
//                break;


        }
    }



    public void showoverlaydialog (String msg){


        final PrettyDialog dialog = new PrettyDialog(this);
        dialog
                .setTitle("FuelBuddy ")
                .setIcon(R.mipmap.circularfb)
                .setIconTint(R.color.colortransparent)
                .setMessage(msg)
                .setAnimationEnabled(true)

                .addButton(
                        "Okay",     // button text
                        R.color.pdlg_color_white,  // button text color
                        R.color.colorPrimary,  // button background color
                        new PrettyDialogCallback() {  // button OnClick listener
                            @Override
                            public void onClick() {

                                // onResume();
                                // Dismiss

                                //finish();
                                dialog.dismiss();
                                try {
                                  //  UserRegistrationActivity.usa.finish();
                                }catch (Exception e) {
                                    e.printStackTrace();
                                }


                                // Do what you gotta do
                            }
                        }
                )
                .show();

    }


    public void finishactivity (){
        try {
            userregister.finish();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    //request reponsedata for the vmair

    @Override
    public void requestCompleted(int request_code, String response) {
        Slog.d("request completed");
        try {
            JSONObject jobj = new JSONObject(response);
            int rs = JsonUtils.getIntFromJSON(jobj, "rs");
            String msg = JsonUtils.getStringFromJSON(jobj, "msg");
            String data = JsonUtils.getStringFromJSON(jobj, "data");
            BaseVo baseVo = new BaseVo(rs, msg, data);




            if (baseVo != null) {
                if (baseVo.getRs() == 0) {



                    if (request_code == REQUEST_NEW_USER_REGISTRATION){

                        showoverlaydialog ("" + baseVo.getMsg());
                       // showToast("" + msg);
                    }
//                    else if(request_code == REQUEST_VERIFY_OTP_REGISTRATION){
//                        showoverlaydialog ("" + baseVo.getMsg());
//                    }


                    else {
                        onResponse(request_code, response, data);
                    }


//                    if (request_code ==  REQUEST_LOGIN){
//                        showLogToast("Invalid user name password");
//                    }
                } else {
                    //showoverlaydialog ("" + baseVo.getMsg());

                    if (request_code == REQUEST_GET_DISTRICT){


                       // showoverlaydialog ("" + baseVo.getMsg());
                        // showToast("" + msg);
                    }


                    if (baseVo.getMsg().equalsIgnoreCase("Asset not found")){

                    }else {
                        showToast("" + baseVo.getMsg());
                    }




//                    new AlertDialog.Builder(bContext)
//                            .setTitle(R.string.app_name)
//                            .setMessage(baseVo.getMsg())
//                            .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    dialogInterface.dismiss();
//                                }
//                            })
                    //                           .show();
                }
            } else {
                //  showToast("Some Error occured");
            }
        } catch (Exception e) {
            e.printStackTrace();
            showToast("Some Error occured");
        }

        hideProgress();
    }






    @Override
    public void requestEndedWithError(int request_code, VolleyError error) {
        Slog.d("reqqquest error");
        hideProgress();
        onFailed(request_code, error);
    }

    protected void onFailed(int request_code, VolleyError error) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.stay_still, R.anim.animation_exit_right);
        } else if (onStartCount == 1) {
            onStartCount++;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.overridePendingTransition(R.anim.stay_still, R.anim.animation_exit_right);
    }

    @Override
    protected void onResume() {
        super.onResume();

        String screen_name = getClass().getSimpleName();
        Slog.d("screen : " + screen_name);
//        AppController.getInstance().trackScreenView(screen_name);

        // Resuming the periodic location updates
    }

    protected void setscreenLayout(int layout_id) {

        View llSubLayout = new View(this);
        if (inflater == null) {
            inflater = getLayoutInflater();
        }
        llSubLayout = (View) inflater.inflate(layout_id, null);
        try {
            screen_area.removeAllViews();
            screen_area.removeAllViewsInLayout();
            screen_area = (RelativeLayout) findViewById(R.id.screen_area);
        } catch (Exception e) {
            e.printStackTrace();
        }
        screen_area.addView(llSubLayout, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    protected void addFooter(int footerId) {
        View footer = (View) inflater.inflate(footerId, null);
        ll_footer_area.setVisibility(View.VISIBLE);
        try {
            ll_footer_area.removeAllViews();
            ll_footer_area.removeAllViewsInLayout();
            ll_footer_area = (LinearLayout) findViewById(R.id.ll_footer);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ll_footer_area.addView(footer);
    }

    protected int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Slog.d("RegisterActivity", "I never expected this! Going down, going down!" + e);
            throw new RuntimeException(e);
        }
    }

    public void clickGetLocation(View v) {



        Intent intent = new Intent(this, MapActivity.class);
      //  startActivity(intent);
        startActivityForResult(intent, REQUEST_GET_LOCATION);
    }


//    public void gotolocation (){
//        Intent intent = new Intent(this, MapActivity.class);
//        startActivityForResult(intent, REQUEST_GET_LOCATION);
//    }

//    protected void showSnackbar(String msg) {
//        if (parentLayout != null) {
//            Snackbar snack = Snackbar.make(parentLayout, "" + msg, Snackbar.LENGTH_LONG);
//
//            View view = snack.getView();
//            view.setBackgroundResource(R.color.colorPrimary);
//            TextView tv = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
////        tv.setBackgroundResource(R.color.primary);
//            tv.setTextColor(Color.WHITE);
//            snack.show();
//        }
//    }

    public void showProgress() {
        showProgress("Loading...");
    }


    protected void getStates() {
        //  trustEveryone();
        volleyGetHit(URL_GET_STATES, REQUEST_GET_STATES);
    }



    public void showProgress(String msg) {
        //create progress progressD and show
        if (null == progressD) {
            progressD = new ProgressDialog(BaseActivity.this, ProgressDialog.STYLE_SPINNER);
            progressD.setMessage(msg);
            progressD.setCanceledOnTouchOutside(false);
            progressD.setIndeterminate(false);
        }

        if (!progressD.isShowing())
            progressD.show();
    }

    public void hideProgress() {
        if (null != progressD && progressD.isShowing()) {
            progressD.dismiss();
        }
    }

    public void showToast(String msg) {
        if (null != msg && !TextUtils.isEmpty(msg))


            Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
    }

    public void showLogToast(String msg) {
        if (BuildConfig.DEBUG)
            if (null != msg && !TextUtils.isEmpty(msg)) {
//                Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_SHORT).show();
            }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(R.anim.stay_still, R.anim.animation_exit_right);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return false;
    }

    public void getMgrAssets() {
        // trustEveryone();
        String url = URL_GET_ASSETS.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_ASSETS);
    }

    public void gettrackingdetails() {
        //  trustEveryone();

        String url = URL_GET_TRACKING_DETAILS.replace("<UID>" ,String.valueOf(orderid) );
        //String url = URL_GET_TRACKING_DETAILS;
        volleyGetHit(url, REQUEST_GET_TRACKING);
    }



    public void getdashboard() {
        // trustEveryone();
        String url = URL_FOR_GET_DASHBOARD_DETAILS.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_DASHBOARD_DETAILS);
    }

    public void getAssetIdentifier() {
        String url = URL_ASSET_IDENTIFIER;
        volleyGetHit(url, REQUEST_GET_ASSET_IDENTIFIER);
    }
    public ArrayList<FuelType> getFuelTypes() {
        String data = SharedPrefrenceHelper.getFuels(bContext);
        if (data.length() > 0) {
            Gson gson = new Gson();
            FuelTypeVo assetVo = gson.fromJson(data, FuelTypeVo.class);
            fuelTypes = assetVo.getAssets();
            return fuelTypes;
        } else {
            String url = URL_FUEL_TYPES;
            volleyGetHit(url, REQUEST_FUEL_TYPES);
            return null;
        }
    }

    public void getBuildings() {
        // trustEveryone();
        String url = URL_GET_BUILDS.replace("<UID>", uid);
        volleyGetHit(url, REQUEST_GET_BUILDING);
    }


    public void gettankdetails () {
        String url = URL_FOR_TANK_DATA.replace("<UID>", "1");
        volleyGetHit(url,REQUEST_TANK_DETAILS );
    }

    public void getsensors() {
        // trustEveryone();
        String url = URL_FOR_SENSOR_DATA.replace("<UID>",  String.valueOf(orderid));
        volleyGetHit(url, REQUEST_SENSOR_DATA);
    }

    protected void showExitDialog() {
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle(R.string.app_name)
                .setMessage("Do you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BaseActivity.super.onBackPressed();
                        //HomeActivity.turnGPSOff();
                        dialog.dismiss();


                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                })
                .create();
        dialog.show();
    }




    protected Marker addmarkerpoint(LatLng loc) {

        if (markernew != null) {
            markernew.remove();
        }

        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(loc)
                .title("My Location")
                .snippet("Asset Selected Location")
        );

        return markernew;
    }

    protected Marker addPointToassetlocation(LatLng loc) {

        if (marker != null) {
            marker.remove();
        }

        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(loc)
                .title("My Location")
                .snippet("Asset Selected Location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pointer)));


        return marker;
    }

    protected Marker remove () {

        marker.remove();

        return marker;
    }

    protected Marker addPointToMap(LatLng loc) {



        if (marker != null) {
            marker.remove();
        }

        marker = googleMap.addMarker(new MarkerOptions()
                .position(loc)
                .title("My Location")
                .snippet("Selected Location")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.location_pointer)));

//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(loc));
        updateAddress(loc);

        Slog.d("zoom 1: = " + zoom);

        if (zoom < 10) {
            zoom = 15;
        } else {
            zoom = googleMap.getCameraPosition().zoom;
        }
        Slog.d("zoom 2: = " + zoom);


        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(loc)      // Sets the center of the map to Mountain View
                .zoom(googleMap.getCameraPosition().zoom)                   // Sets the zoom
                .zoom(zoom)                   // Sets the zoom
//                .bearing(90)                // Sets the orientation of the camera to east
//                .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        return marker;
    }

    protected void updateAddress(LatLng loc) {

    }






//    private void trustEveryone() {
//
//
//
//
//
////        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
////        {
////            @Override
////            public boolean verify(String hostname,SSLSession arg1)
////            {
////                if (! hostname.equalsIgnoreCase("www.asdasdad.com"))
////                    return true;
////                else
////                    return false;
////            }
////        });
//
//        //method 2
//
////        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
////            @Override
////            public boolean verify(String hostname, SSLSession arg1) {
////                if (hostname.equalsIgnoreCase("api.my.com") ||
////                        hostname.equalsIgnoreCase("api.crashlytics.com") ||
////                        hostname.equalsIgnoreCase("settings.crashlytics.com")) {
////                    return true;
////                } else {
////                    return false;
////                }
////            }
////        });
//
//        //working
//
//        try {
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier(){
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }});
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, new X509TrustManager[]{new X509TrustManager(){
//                public void checkClientTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public void checkServerTrusted(X509Certificate[] chain,
//                                               String authType) throws CertificateException {}
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }}}, new SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(
//                    context.getSocketFactory());
//        } catch (Exception e) { // should never happen
//            e.printStackTrace();
//        }
//    }


    protected String getCompleteAddressString(double LATITUDE, double LONGITUDE) {

        String strAdd = "";

        Ltdnew = LATITUDE;
        Lngnew = LONGITUDE;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null && addresses.size() != 0 ) {
                Address returnedAddress = addresses.get(0);
                // addresses.get(1);

                strAdd = addresses.get(0).getAddressLine(0);
                // strAdd = addresses.get(0).getAddressLine(1); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                StringBuilder strReturnedAddress = new StringBuilder("");

                // strAdd = strAdd +""+city+""+state+""+country+""+postalCode+""+knownName;

                if(returnedAddress.getMaxAddressLineIndex() > 0) {
                    for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                    }
                    strAdd = strReturnedAddress.toString();
                }

                else {

                    //   strAdd = addresses.get(0).getAddressLine(1);
//                    try {
//                        addressFragments.add(address.getAddressLine(0));
//                    } catch (Exception ignored) { }
                }










                // strAdd = strReturnedAddress.toString();

                Slog.d("My Current loction address" + "" + strReturnedAddress.toString());
            } else {
                try {
                    if (isInternetOnbase()){
                        new JSONParse().execute();
                    }else {
                        showToast("No internet connectivity,unable to fetch address");
                    }
                }catch (Exception e){
                    Slog.d("My Current loction address" + "No Net Connectivity");
                }

                Slog.d("My Current loction address" + "No Address returned!");
                // new JSONParse().execute();
            }
        } catch (Exception e) {
            // new JSONParse().execute();
            e.printStackTrace();
            Slog.d("My Current loction address" + "Canont get Address!");
        }
        return strAdd;
    }

    protected  String getcompletepincode (double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        String postalCode = "";
        // new JSONParse().execute();
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null && addresses.size() != 0) {

                postalCode =  addresses.get(0).getPostalCode();
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");




                Slog.d("My Current loction address" + "" + strReturnedAddress.toString());
            } else {
                // new JSONParse().execute();
                Slog.d("My Current loction address" + "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Slog.d("My Current loction address" + "Canont get Address!");

            //  new JSONParse().execute();
        }
        return postalCode;
    }

    protected  String getcompletestateaddress (double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        String postalCode = "";
        // new JSONParse().execute();
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);

            if (addresses != null && addresses.size() != 0) {

                postalCode =  addresses.get(0).getAdminArea();
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");




                Slog.d("My Current loction address" + "" + strReturnedAddress.toString());
            } else {
                // new JSONParse().execute();
                Slog.d("My Current loction address" + "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Slog.d("My Current loction address" + "Canont get Address!");

            //  new JSONParse().execute();
        }
        return postalCode;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }




    public  class JSONParse extends AsyncTask<String, String, JSONObject> {
        private ProgressDialog pDialog;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Address1 = "";
            Address2 = "";
            City = "";
            State = "";
            Country = "";
            County = "";
            PIN = "";

//            AddressFromGoogleApi = " ";
//            PinfromGoogleApi = " ";


            pDialog = new ProgressDialog(BaseActivity.this);
            pDialog.setMessage("Getting Location ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();

        }

        @Override
        protected JSONObject doInBackground(String... args) {


            String url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + Ltdnew + ","
                    + Lngnew + "&sensor=true";

            // http post
            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(url);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();

            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (Exception e) {
                Log.e("log_tag", "Error in http connection " + e.toString());
            }

            // convert response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("log_tag", "Error converting result " + e.toString());
            }

            // try parse the string to a JSON object
            try {
                jObject = new JSONObject(result);
            } catch (JSONException e) {
                Log.e("log_tag", "Error parsing data " + e.toString());
            }

            return jObject;
        }
        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();
            try {
                String Status = json.getString("status");
                if (Status.equalsIgnoreCase("OK")) {
                    JSONArray Results = json.getJSONArray("results");
                    JSONObject zero = Results.getJSONObject(0);
                    JSONArray address_components = zero.getJSONArray("address_components");


                    for (int i = 0; i < address_components.length(); i++) {
                        JSONObject zero2 = address_components.getJSONObject(i);
                        JSONObject result = Results.getJSONObject(i);

                        String long_name = zero2.getString("long_name");
                        JSONArray mtypes = zero2.getJSONArray("types");
                        String Type = mtypes.getString(0);
//                        String Type1 = mtypes.getString(1);
//                        String Type2 = mtypes.getString(0);

                        if (TextUtils.isEmpty(long_name) == false || !long_name.equals(null) || long_name.length() > 0 || long_name != "") {
                            if(Type.equalsIgnoreCase("premise"))
                            {
                                Address1 = long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("political"))
                            {
                                Address1 = Address1 + long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("political"))
                            {
                                Address2 = long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("locality"))
                            {
//                          Address2 = Address2 + long_name + ", ";
                                City = long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("administrative_area_level_2"))
                            {
                                // County = long_name;
                            }
                            else if(Type.equalsIgnoreCase("administrative_area_level_1"))
                            {
                                State = long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("country"))
                            {
                                Country = long_name + " ";
                            }
                            else if(Type.equalsIgnoreCase("postal_code"))
                            {
                                PIN = long_name;
                            }
                        }

                        // JSONArray mtypes = zero2.getJSONArray("types");
                        // String Type = mtypes.getString(0);
                        // Log.e(Type,long_name);
                    }


                    AddressFromGoogleApi = Address1 + Address2 + State + Country + PIN ;
                    PinfromGoogleApi = PIN;




                    HomeActivity.autocompleteFragment.setText(AddressFromGoogleApi);

                    SharedPreferences.Editor editor = sharedpreferences.edit();

                    editor.putString("Keyaddress", AddressFromGoogleApi);

                    editor.commit();

                    if ( MapActivity.mapactivity == 1){

                        MapActivity.autocompleteFragment.setText(AddressFromGoogleApi);


                    }




                    // Toast.makeText(BaseActivity.this, AddressFromGoogleApi, Toast.LENGTH_SHORT).show();

                    // showToast(AddressFromGoogleApi);


//                    showToast(Address1);
//                    showToast(Address2);
//                    showToast(State);
//                    showToast(Country);
//                    showToast(PIN);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }




//    public void getAddress(double LATITUDE, double LONGITUDE) {
//        Address1 = "";
//        Address2 = "";
//        City = "";
//        State = "";
//        Country = "";
//        County = "";
//        PIN = "";
//
//        try {
//
//            JSONObject jsonObj = parser_Json.getJSONfromURL("http://maps.googleapis.com/maps/api/geocode/json?latlng=" + LATITUDE + ","
//                    + LONGITUDE + "&sensor=true");
//
//            JSONObject json = parser_Json.getJSONfromURL();
//            String Status = jsonObj.getString("status");
//            if (Status.equalsIgnoreCase("OK")) {
//                JSONArray Results = jsonObj.getJSONArray("results");
//                JSONObject zero = Results.getJSONObject(0);
//                JSONArray address_components = zero.getJSONArray("address_components");
//
//                for (int i = 0; i < address_components.length(); i++) {
//                    JSONObject zero2 = address_components.getJSONObject(i);
//                    String long_name = zero2.getString("long_name");
//                    JSONArray mtypes = zero2.getJSONArray("types");
//                    String Type = mtypes.getString(0);
//
//                    if (TextUtils.isEmpty(long_name) == false || !long_name.equals(null) || long_name.length() > 0 || long_name != "") {
//                        if (Type.equalsIgnoreCase("street_number")) {
//                            Address1 = long_name + " ";
//                        } else if (Type.equalsIgnoreCase("route")) {
//                            Address1 = Address1 + long_name;
//                        } else if (Type.equalsIgnoreCase("sublocality")) {
//                            Address2 = long_name;
//                        } else if (Type.equalsIgnoreCase("locality")) {
//                            // Address2 = Address2 + long_name + ", ";
//                            City = long_name;
//                        } else if (Type.equalsIgnoreCase("administrative_area_level_2")) {
//                            County = long_name;
//                        } else if (Type.equalsIgnoreCase("administrative_area_level_1")) {
//                            State = long_name;
//                        } else if (Type.equalsIgnoreCase("country")) {
//                            Country = long_name;
//                        } else if (Type.equalsIgnoreCase("postal_code")) {
//                            PIN = long_name;
//                        }
//                    }
//
//                    // JSONArray mtypes = zero2.getJSONArray("types");
//                    // String Type = mtypes.getString(0);
//                    // Log.e(Type,long_name);
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }



    public String getAddress1() {
        return Address1;

    }

    public String getAddress2() {
        return Address2;

    }

    public String getCity() {
        return City;

    }

    public String getState() {
        return State;

    }

    public String getCountry() {
        return Country;

    }

    public String getCounty() {
        return County;

    }

    public String getPIN() {
        return PIN;

    }

    public String getstatename (int stateid) {


        if (stateid != 0) {


            if (stateid == 1) {
                return "ANDAMAN AND NICOBAR ISLANDS";
            } else if (stateid == 2) {
                return "ANDHRA PRADESH";
            } else if (stateid == 3) {
                return "ARUNACHAL PRADESH";
            } else if (stateid == 4) {
                return "ASSAM";
            } else if (stateid == 5) {
                return "BIHAR";
            } else if (stateid == 7) {
                return "CHANDIGARH";
            } else if (stateid == 6) {
                return "CHATTISGARH";
            } else if (stateid == 10) {
                return "DADRA AND NAGAR HAVELI";
            } else if (stateid == 8) {
                return "DAMAN AND DIU";
            } else if (stateid == 9) {
                return "DELHI";
            } else if (stateid == 11) {
                return "GOA";
            } else if (stateid == 12) {
                return "GUJARAT";
            } else if (stateid == 14) {
                return "HARYANA";
            } else if (stateid == 13) {
                return "HIMACHAL PRADESH";
            } else if (stateid == 15) {
                return "JAMMU AND KASHMIR";
            } else if (stateid == 16) {
                return "JHARKHAND";
            } else if (stateid == 18) {
                return "KARNATAKA";
            } else if (stateid == 17) {
                return "KERALA";
            } else if (stateid == 19) {
                return "LAKSHADWEEP";
            } else if (stateid == 23) {
                return "MADHYA PRADESH ";
            } else if (stateid == 21) {
                return "MAHARASHTRA";
            } else if (stateid == 22) {
                return "MANIPUR";
            } else if (stateid == 20) {
                return "MEGHALAYA";
            } else if (stateid == 24) {
                return "MIZORAM";
            } else if (stateid == 25) {
                return "NAGALAND";
            } else if (stateid == 26) {
                return "ORISSA";
            } else if (stateid == 28) {
                return "PONDICHERRY";
            } else if (stateid == 27) {
                return "PUNJAB ";
            } else if (stateid == 29) {
                return "RAJASTHAN";
            } else if (stateid == 30) {
                return "SIKKIM";
            } else if (stateid == 31) {
                return "TAMIL NADU";
            } else if (stateid == 32) {
                return "TRIPURA";
            } else if (stateid == 34) {
                return "UTTAR PRADESH";
            } else if (stateid == 33) {
                return "UTTARAKHAND";
            } else if (stateid == 35) {
                return "WEST BENGAL";
            }
        }else {
            showToast("Error in Getting State, please click on Map to get Your Location");
            return "";
        }

        return "";

    }


//    public void UploadTwoImages() {
//
////
////        Bitmap bitmap1 = imageView1.getDrawingCache();
////        Bitmap bitmap2 = imageView2.getDrawingCache();
//
//        final String imageOne = ProfileActivity.image;
//       // final String imageTwo = getStringImage(bitmap2);
//
//        Log.e("Image One", imageOne);
//       // Log.e("Image Twol", imageTwo);
//
//        final ProgressDialog pDialog = new ProgressDialog(this);
//        pDialog.setMessage("Registration is in Process Please wait...");
//        pDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST,
//                URL_FOR_POSTING_IMAGE,
//                new Response.Listener<String>() {
//
//                    @Override
//                    public void onResponse(String response) {
//                        pDialog.hide();
//                        String result = response;
//                        Log.e("Result", response);
//
//                    }
//                }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("Error", error.getMessage());
//                pDialog.hide();
//
//            }
//        }) {
//
//            @Override
//            protected Map<String, String> getParams() {
//
//
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("getdata", "UploadImage");
//
//
//                params.put("insert_image_one", imageOne);
//
//
//
//                //Bank Information
//
//                return params;
//            }URL
//
//
//        };
//
////Adding request to request queue
//        Voll.getInstance().addToRequestQueue(stringRequest);
//
//    }

    public final boolean isInternetOnbase() {

        // get Connectivity Manager object to check connection
        ConnectivityManager connec =
                (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        // Check for network connections
        if ( connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED ) {

            // if connected with internet

            // Toast.makeText(this, " Connected ", Toast.LENGTH_LONG).show();
            return true;

        } else if (
                connec.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {

            //Toast.makeText(this, " No internet connectivity ! ", Toast.LENGTH_LONG).show();
            return false;
        }
        return false;
    }


}