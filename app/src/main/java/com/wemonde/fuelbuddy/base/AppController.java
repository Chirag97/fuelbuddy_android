package com.wemonde.fuelbuddy.base;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.wemonde.fuelbuddy.ui.HomeActivity;
import com.wemonde.fuelbuddy.utils.Slog;

//import net.danlew.android.joda.JodaTimeAndroid;

import java.io.File;

/**
 * Created by sagar on 04-08-2016.
 */

public class AppController extends Application {


    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    public static final String cacheFolder = "fuel";

    public static synchronized AppController getInstance() {
        return mInstance;
    }
    public static final String TAG = AppController.class.getSimpleName();
    private Thread.UncaughtExceptionHandler defaultUEH;

//    private ArrayList<Product> myProducts = new ArrayList<Product>();
//    private static ModelCart myCart = new ModelCart();
//    public ModelCart getCart() {
//        return myCart;
//    }
//    public int getProductsArraylistSize() {
//        return myProducts.size();
//    }

    @Override
    public void onCreate() {
        super.onCreate();
//        JodaTimeAndroid.init(this);

        mInstance = this;
//        AnalyticsTrackers.initialize(this);
//        AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        mRequestQueue = Volley.newRequestQueue(this);
    }

    private Thread.UncaughtExceptionHandler _unCaughtExceptionHandler =
            new Thread.UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(Thread thread, Throwable ex){
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent myActivity = PendingIntent.getActivity(getApplicationContext(), 192837, intent, PendingIntent.FLAG_ONE_SHOT);

                    AlarmManager alarmManager;
                    alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, 15000, myActivity);
                    System.exit(2);

                    // re-throw critical exception further to the os (important)
                    defaultUEH.uncaughtException(thread, ex);
                }
            };

    public AppController() {
//        Thread.setDefaultUncaughtExceptionHandler(_unCaughtExceptionHandler);
    }

    public static String getCacheDir(Context context) {
        String path;

        try {
            path = "" + context.getExternalFilesDir(cacheFolder).getAbsolutePath() + "/";
//            FitnLog.d("path in try controller : " + path);
        } catch (Exception e) {
            path = Environment.getExternalStoragePublicDirectory(cacheFolder).getAbsolutePath() + "/";
            Slog.d("error getting path  : " + e.getMessage());
        }

        File f = new File(path);
        if (!f.exists()) {
            f.mkdirs();
        }

        return path;
    }

/*    public static String getTutDir(Context context) {
        String path;

        try {
            path = "" + context.getExternalFilesDir(tutFolder).getAbsolutePath() + "/";
//            FitnLog.d("path in try controller : " + path);
        } catch (Exception e) {
            path = Environment.getExternalStoragePublicDirectory(tutFolder).getAbsolutePath() + "/";
            Slog.d("error getting path  : " + e.getMessage());
        }

        File f = new File(path);
        if (!f.exists()) {
            f.mkdirs();
        }

        return path;
    }*/

//    protected void attachBaseContext(Context base) {
//        super.attachBaseContext(base);
//        MultiDex.install(this);
//    }

//    public boolean getbaseUrl() {
//        return getResources().getBoolean(R.bool.base_url);
//    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue.getCache().clear();
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        // rQueue.getCache().clear();

        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

/*    public synchronized Tracker getGoogleAnalyticsTracker() {
        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
    }*/

    /***
     * Tracking screen view
     *
     * @param screenName screen name to be displayed on GA dashboard
     */
//    public void trackScreenView(String screenName) {
//        Tracker t = getGoogleAnalyticsTracker();
//
//        // Set screen name.
//        t.setScreenName(screenName);
//
//        // Send a screen view.
//        t.send(new HitBuilders.ScreenViewBuilder().build());
//
//        GoogleAnalytics.getInstance(this).dispatchLocalHits();
//    }

    /***
     * Tracking exception
     *
     * @param e exception to be tracked
     */
    /*public void trackException(Exception e) {
        if (e != null) {
            Tracker t = getGoogleAnalyticsTracker();

            t.send(new HitBuilders.ExceptionBuilder()
                    .setDescription(
                            new StandardExceptionParser(this, null)
                                    .getDescription(Thread.currentThread().getName(), e))
                    .setFatal(false)
                    .build()
            );
        }
    }*/

    /***
     * Tracking event
     *
     * @param category event category
     * @param action   action of the event
     * @param label    label
     */
    /*public void trackEvent(String category, String action, String label) {
        Tracker t = getGoogleAnalyticsTracker();

        // Build and send an Event.
        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
    }*/

}