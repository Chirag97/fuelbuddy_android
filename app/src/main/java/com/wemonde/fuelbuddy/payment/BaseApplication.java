package com.wemonde.fuelbuddy.payment;

import android.app.Application;

//import com.crashlytics.android.Crashlytics;

/**
 * Created by vijay on 16/8/16.
 */
public class BaseApplication extends Application {

    com.wemonde.fuelbuddy.payment.AppEnvironment appEnvironment;

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        appEnvironment = com.wemonde.fuelbuddy.payment.AppEnvironment.SANDBOX;
    }

    public com.wemonde.fuelbuddy.payment.AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }

    public void setAppEnvironment(com.wemonde.fuelbuddy.payment.AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }
}
