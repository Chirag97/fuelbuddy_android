package com.wemonde.fuelbuddy.payment;

import android.support.v4.content.ContextCompat;

import com.citrus.sdk.Environment;
import com.citruspay.sdkui.ui.utils.CitrusFlowManager;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.ui.OrderSummaryActivity;

/**
 * Created by vijay on 14/7/16.
 */
public enum AppEnvironment {
    SANDBOX {
        @Override
        public String getBillUrl() {
            return "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.php";
        }

//        CitrusFlowManager.initCitrusConfig("jt2iu8vpi9-js-merchant",
//                "a926f867fbf922deb794bb04288369bc", "jt2iu8vpi9-js-consumer",
//                "64c21c020a0fb59dfb1c26246dd98b8d", ContextCompat.getColor(this, R.color.white),
//        OrderSummaryActivity.this, Environment.SANDBOX, "fuelbuddy","https://www.fuelbuddyapp.com/payment/GenerateBill.aspx",
//        returnUrlLoadMoney);

        @Override
        public String getVanity() {
            return "fuelbuddyapp";
        }

        @Override
        public String getSignUpId() {
            return "jt2iu8vpi9-js-merchant";
        }

        @Override
        public String getSignUpSecret() {
            return "a926f867fbf922deb794bb04288369bc";
        }

        @Override
        public String getSignInId() {
            return "jt2iu8vpi9-js-consumer";
        }

        @Override
        public String getSignInSecret() {
            return "64c21c020a0fb59dfb1c26246dd98b8d";
        }

        @Override
        public Environment getEnvironment() {
            return Environment.SANDBOX;
        }
    },
    PRODUCTION {
        @Override
        public String getBillUrl() {
            return "https://salty-plateau-1529.herokuapp.com/billGenerator.production.php";
        }

        @Override
        public String getVanity() {
            return "testing";
        }

        @Override
        public String getSignUpId() {
            return "kkizp9tsqg-signup";
        }

        @Override
        public String getSignUpSecret() {
            return "39c50a32eaabaf382223fdd05f331e1c";
        }

        @Override
        public String getSignInId() {
            return "kkizp9tsqg-signin";
        }

        @Override
        public String getSignInSecret() {
            return "1fc1f57639ec87cf4d49920f6b3a2c9d";
        }

        @Override
        public Environment getEnvironment() {
            return Environment.PRODUCTION;
        }
    };

    public abstract String getBillUrl();

    public abstract String getVanity();

    public abstract String getSignUpId();

    public abstract String getSignUpSecret();

    public abstract String getSignInId();

    public abstract String getSignInSecret();

    public abstract Environment getEnvironment();
}
