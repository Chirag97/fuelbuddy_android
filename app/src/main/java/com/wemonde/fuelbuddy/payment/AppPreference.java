package com.wemonde.fuelbuddy.payment;

/**
 * Created by vijay on 6/12/16.
 */

public class AppPreference {

    static String dummyMobile = "";
    static String dummyEmail = "";

    private boolean isDisableWallet, isDisableSavedCards, isDisableNetBanking;
    private int selectedTheme = -1;

    boolean isDisableWallet() {
        return isDisableWallet;
    }

    public void setDisableWallet(boolean disableWallet) {
        isDisableWallet = disableWallet;
    }

    boolean isDisableSavedCards() {
        return isDisableSavedCards;
    }

    void setDisableSavedCards(boolean disableSavedCards) {
        isDisableSavedCards = disableSavedCards;
    }

    boolean isDisableNetBanking() {
        return isDisableNetBanking;
    }

    void setDisableNetBanking(boolean disableNetBanking) {
        isDisableNetBanking = disableNetBanking;
    }

    int getSelectedTheme() {
        return selectedTheme;
    }

    void setSelectedTheme(int selectedTheme) {
        this.selectedTheme = selectedTheme;
    }
}
