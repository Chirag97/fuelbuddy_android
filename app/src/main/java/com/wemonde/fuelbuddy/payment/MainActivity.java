
package com.wemonde.fuelbuddy.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.citrus.sdk.Callback;
import com.citrus.sdk.CitrusClient;
import com.citrus.sdk.CitrusUser;
import com.citrus.sdk.TransactionResponse;
import com.citrus.sdk.logger.CitrusLogger;
import com.citrus.sdk.response.CitrusError;
import com.citrus.sdk.response.CitrusResponse;
import com.citruspay.sdkui.ui.utils.CitrusFlowManager;
import com.citruspay.sdkui.ui.utils.PPConfig;
import com.citruspay.sdkui.ui.utils.ResultModel;
import com.wemonde.fuelbuddy.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MainActivity extends com.wemonde.fuelbuddy.payment.BaseActivity implements View.OnClickListener {

//    public static final String returnUrlLoadMoney = "https://salty-plateau-1529.herokuapp" +
//            ".com/redirectUrlLoadCash.php";
public static final String returnUrlLoadMoney = "http://www.fuelbuddyapp.com/payment/returnData.aspx";
    public static final String TAG = "MainActivity";
    private static final String USER_EMAIL = "user_email";
    private static final String USER_MOBILE = "user_mobile";
    private static String USER_DETAILS = "user_details";
    private static final String PHONE_PATTERN = "^[987]\\d{9}$";
    private static final long MENU_DELAY = 300;
    public static String dummyAmount = "5";
    String userMobile, userEmail;
    private TextView logoutBtn;
    SharedPreferences settings;
    SharedPreferences.Editor editor;
    SharedPreferences userDetailsPreference;
    EditText email_et, mobile_et, amount_et;
    TextInputLayout email_til, mobile_til;
    RadioGroup radioGroup_color_theme, radioGroup_select_env;
    SwitchCompat switch_disable_wallet, switch_disable_netBanks, switch_disable_cards;
    int style = -1;
    private boolean isOverrideResultScreen;
    private AppCompatRadioButton radio_btn_default;
    private AppPreference app_stored_pref;
    private AppCompatRadioButton radio_btn_theme_purple, radio_btn_theme_pink, radio_btn_theme_green, radio_btn_theme_grey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        app_stored_pref = new AppPreference();
        Toolbar toolbar = (Toolbar) findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(getString(R.string.app_name));
        settings = getSharedPreferences("settings", MODE_PRIVATE);
        logoutBtn = (TextView) findViewById(R.id.logout_button);
        email_et = (EditText) findViewById(R.id.email_et);
        mobile_et = (EditText) findViewById(R.id.mobile_et);
        amount_et = (EditText) findViewById(R.id.amount_et);
        email_til = (TextInputLayout) findViewById(R.id.email_til);
        mobile_til = (TextInputLayout) findViewById(R.id.mobile_til);
        radioGroup_color_theme = (RadioGroup) findViewById(R.id.radio_grp_color_theme);

        radio_btn_default = (AppCompatRadioButton) findViewById(R.id.radio_btn_theme_default);
        radio_btn_theme_pink = (AppCompatRadioButton) findViewById(R.id.radio_btn_theme_pink);
        radio_btn_theme_purple = (AppCompatRadioButton) findViewById(R.id.radio_btn_theme_purple);
        radio_btn_theme_green = (AppCompatRadioButton) findViewById(R.id.radio_btn_theme_green);
        radio_btn_theme_grey = (AppCompatRadioButton) findViewById(R.id.radio_btn_theme_grey);


        switch_disable_wallet = (SwitchCompat) findViewById(R.id.switch_disable_wallet);
        switch_disable_netBanks = (SwitchCompat) findViewById(R.id.switch_disable_netbanks);
        switch_disable_cards = (SwitchCompat) findViewById(R.id.switch_disable_cards);
        AppCompatRadioButton radio_btn_sandbox = (AppCompatRadioButton) findViewById(R.id.radio_btn_sandbox);
        AppCompatRadioButton radio_btn_production = (AppCompatRadioButton) findViewById(R.id.radio_btn_production);
        radioGroup_select_env = (RadioGroup) findViewById(R.id.radio_grp_env);

        initListeners();

        //Set Up SharedPref
        setUpUserDetails();
        if (settings.getBoolean("is_prod_env", false)) {
            ((com.wemonde.fuelbuddy.payment.BaseApplication) getApplication()).setAppEnvironment(com.wemonde.fuelbuddy.payment.AppEnvironment.PRODUCTION);
            radio_btn_production.setChecked(true);
        } else {
            ((com.wemonde.fuelbuddy.payment.BaseApplication) getApplication()).setAppEnvironment(com.wemonde.fuelbuddy.payment.AppEnvironment.SANDBOX);
            radio_btn_sandbox.setChecked(true);
        }
        setupCitrusConfigs();
    }


    private void setUpUserDetails() {
        userDetailsPreference = getSharedPreferences(USER_DETAILS, MODE_PRIVATE);
        userEmail = userDetailsPreference.getString(USER_EMAIL, AppPreference.dummyEmail);
        userMobile = userDetailsPreference.getString(USER_MOBILE, AppPreference.dummyMobile);
        email_et.setText(userEmail);
        mobile_et.setText(userMobile);
        amount_et.setText(dummyAmount);
        restoreAppPref();
    }

    private void restoreAppPref() {
        //Set Up Disable Options
        switch_disable_wallet.setChecked(app_stored_pref.isDisableWallet());
        switch_disable_cards.setChecked(app_stored_pref.isDisableSavedCards());
        switch_disable_netBanks.setChecked(app_stored_pref.isDisableNetBanking());

        //Set Up saved theme pref
        switch (app_stored_pref.getSelectedTheme()) {
            case -1:
                radio_btn_default.setChecked(true);
                break;
//            case R.style.AppTheme_pink:
//                radio_btn_theme_pink.setChecked(true);
//                break;
//            case R.style.AppTheme_Grey:
//                radio_btn_theme_grey.setChecked(true);
//                break;
//            case R.style.AppTheme_purple:
//                radio_btn_theme_purple.setChecked(true);
//                break;
//            case R.style.AppTheme_Green:
//                radio_btn_theme_green.setChecked(true);
//                break;
//            default:
//                radio_btn_default.setChecked(true);
//                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isFinishing()) {
            initViews();
        }
    }

    private void initViews() {
        try {
            if (CitrusClient.getInstance(this) != null) {
                CitrusClient.getInstance(this).isUserSignedIn(new Callback<Boolean>() {
                    @Override
                    public void success(Boolean aBoolean) {
                        if (aBoolean) {
                            logoutBtn.setVisibility(View.VISIBLE);
                        } else {
                            logoutBtn.setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void error(CitrusError citrusError) {
                        CitrusLogger.d("IsUserSignedInError", citrusError);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
            logoutBtn.setVisibility(View.GONE);
        }
    }

//    private void selectProdEnv() {
//        new Handler(getMainLooper()).postDelayed(() -> {
//            ((com.wemonde.fuelbuddy.payment.BaseApplication) getApplication()).setAppEnvironment(com.wemonde.fuelbuddy.payment.);
//            editor = settings.edit();
//            editor.putBoolean("is_prod_env", true);
//            editor.apply();
//            logoutBtn.setVisibility(View.GONE);
//            CitrusClient.getInstance(MainActivity.this).signOut(new Callback<CitrusResponse>() {
//                @Override
//                public void success(CitrusResponse citrusResponse) {
//
//                }
//
//                @Override
//                public void error(CitrusError error) {
//
//                }
//            });
//            CitrusClient.getInstance(MainActivity.this).destroy();
//            setupCitrusConfigs();
//        }, MENU_DELAY);
//    }

//    private void selectSandBoxEnv() {
//        new Handler(getMainLooper()).postDelayed(() -> {
//            ((com.wemonde.fuelbuddy.payment.BaseApplication) getApplication()).setAppEnvironment(com.wemonde.fuelbuddy.payment.AppEnvironment.SANDBOX));
//            editor = settings.edit();
//            editor.putBoolean("is_prod_env", false);
//            editor.apply();
//            logoutBtn.setVisibility(View.GONE);
//            CitrusClient.getInstance(MainActivity.this).signOut(new Callback<CitrusResponse>() {
//                @Override
//                public void success(CitrusResponse citrusResponse) {
//
//                }
//
//                @Override
//                public void error(CitrusError error) {
//
//                }
//            });
//            CitrusClient.getInstance(MainActivity.this).destroy();
//            setupCitrusConfigs();
//        }, MENU_DELAY);
//    }

    private void setupCitrusConfigs() {
        com.wemonde.fuelbuddy.payment.AppEnvironment appEnvironment = ((com.wemonde.fuelbuddy.payment.BaseApplication) getApplication()).getAppEnvironment();
        if (appEnvironment == AppEnvironment.SANDBOX) {
            Toast.makeText(MainActivity.this, "Environment Set to Production", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Environment Set to SandBox", Toast.LENGTH_SHORT).show();
        }

        CitrusFlowManager.initCitrusConfig(appEnvironment.getSignUpId(),
                appEnvironment.getSignUpSecret(), appEnvironment.getSignInId(),
                appEnvironment.getSignInSecret(), ContextCompat.getColor(this, R.color.white),
                MainActivity.this, appEnvironment.getEnvironment(), appEnvironment.getVanity(), appEnvironment.getBillUrl(),
                returnUrlLoadMoney);

        //To Set the Log Level of Core SDK & Plug & Play
        PPConfig.getInstance().setLogLevel(this, CitrusLogger.LogLevel.DEBUG);

        //To Set the User details
        CitrusUser.Address customAddress = new CitrusUser.Address("Street1", "Street2", "City", "State", "Country", "411045");
        PPConfig.getInstance().setUserDetails("Custom_FName", "Custom_LName", customAddress);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("MainActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == CitrusFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            // You will get data here if transaction flow is started through pay options other than wallet
            TransactionResponse transactionResponse = data.getParcelableExtra(CitrusFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);
            // You will get data here if transaction flow is started through wallet
            ResultModel resultModel = data.getParcelableExtra(CitrusFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getJsonResponse() != null) {
                // Decide what to do with this data
                Log.d(TAG, "Transaction response : " + transactionResponse.getJsonResponse());
            } else if (resultModel != null && resultModel.getTransactionResponse() != null) {
                // Decide what to do with this data
                Log.d(TAG, "Result response : " + resultModel.getTransactionResponse().getTransactionId());
            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(TAG, "Both objects are null!");
            }
        }
    }


    @Override
    public void onClick(View v) {
        userEmail= "adnan.kidwai@fuelbuddy.in";
        userMobile = "9911683358";


//        userEmail = email_et.getText().toString().trim();
//        userMobile = mobile_et.getText().toString().trim();
//        if (v.getId() == R.id.logout_button || validateDetails(userEmail, userMobile)) {
//            switch (v.getId()) {
////                case R.id.quick_pay:
////                    startShoppingFlow();
////                    break;
////                case R.id.wallet_button:
////                    startWalletFlow();
////                    break;
////                case R.id.logout_button:
////                    logoutUser();
////                    break;
//            }
//        }
    }

    private void startShoppingFlow() {
        CitrusClient.getInstance(this).isUserSignedIn(new Callback<Boolean>() {
            @Override
            public void success(Boolean success) {
                if (success) {
                    if (isDifferentUser(userMobile)) {
                     //   showUserLoggedInDialog(true);
                    } else {
                        initQuickPayFlow();
                    }
                } else {
                    initQuickPayFlow();
                }
            }

            @Override
            public void error(CitrusError error) {

            }
        });

    }

    private void initQuickPayFlow() {
        saveUserDetails(userEmail, userMobile);
        if (isOverrideResultScreen) {
            Toast.makeText(MainActivity.this, "Result Screen will Override", Toast.LENGTH_SHORT).show();
        }
        if (style != -1) {
            CitrusFlowManager.startShoppingFlowStyle(MainActivity.this,
                    userEmail, userMobile,
                    TextUtils.isEmpty(amount_et.getText().toString()) ? dummyAmount : amount_et.getText().toString(),
                    style, isOverrideResultScreen);
        } else {
            CitrusFlowManager.startShoppingFlowStyle(MainActivity.this,
                    userEmail, userMobile,
                    TextUtils.isEmpty(amount_et.getText().toString()) ? dummyAmount : amount_et.getText().toString(),
                    R.style.AppTheme_default, isOverrideResultScreen);
        }
    }


    public void startWalletFlow() {
        CitrusClient.getInstance(this).isUserSignedIn(new Callback<Boolean>() {
            @Override
            public void success(Boolean success) {
                if (success) {
                    if (isDifferentUser(userMobile)) {
                      //  showUserLoggedInDialog(false);
                    } else {
                        initWalletFlow();
                    }
                } else {
                    initWalletFlow();
                }
            }

            @Override
            public void error(CitrusError error) {

            }
        });
    }

    private void initWalletFlow() {
        saveUserDetails(userEmail, userMobile);
        if (style != -1) {
            CitrusFlowManager.startWalletFlowStyle(MainActivity.this, userEmail, userMobile, style);
        } else {
            CitrusFlowManager.startWalletFlow(MainActivity.this, userEmail, userMobile);
        }
    }

    private void storeAppPref() {
        app_stored_pref.setDisableWallet(switch_disable_wallet.isChecked());
        app_stored_pref.setDisableSavedCards(switch_disable_cards.isChecked());
        app_stored_pref.setDisableNetBanking(switch_disable_netBanks.isChecked());
        app_stored_pref.setSelectedTheme(style);
    }

    private void logoutUser() {
        logoutUser(MainActivity.this);
        logoutBtn.setVisibility(View.GONE);
    }

    private void initListeners() {
        email_et.addTextChangedListener(new EditTextInputWatcher(email_til));
        mobile_et.addTextChangedListener(new EditTextInputWatcher(mobile_til));
        setupCitrusConfigs();
       // selectSandBoxEnv();
//        radioGroup_color_theme.setOnCheckedChangeListener((group, checkedId) -> {
//            switch (checkedId) {
//                case R.id.radio_btn_theme_default:
//                    style = -1;
//                    isOverrideResultScreen = false;
//                    break;
//                case R.id.radio_btn_theme_pink:
//                    style = R.style.AppTheme_pink;
//                    isOverrideResultScreen = true;
//                    break;
//                case R.id.radio_btn_theme_grey:
//                    style = R.style.AppTheme_Grey;
//                    isOverrideResultScreen = false;
//                    break;
//                case R.id.radio_btn_theme_purple:
//                    style = R.style.AppTheme_purple;
//                    isOverrideResultScreen = false;
//                    break;
//                case R.id.radio_btn_theme_green:
//                    style = R.style.AppTheme_Green;
//                    isOverrideResultScreen = false;
//                    break;
//                default:
//                    style = -1;
//                    isOverrideResultScreen = false;
//                    break;
//            }
//        });
//
//        radioGroup_select_env.setOnCheckedChangeListener((group, checkedId) -> {
//            switch (checkedId) {
//                case R.id.radio_btn_sandbox:
//                    selectSandBoxEnv();
//                    break;
//                case R.id.radio_btn_production:
//                    selectProdEnv();
//                    break;
//            }
//        });
//        switch_disable_cards.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            PPConfig.getInstance().disableSavedCards(isChecked);
//        });
//
//        switch_disable_netBanks.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            PPConfig.getInstance().disableNetBanking(isChecked);
//        });
//
//        switch_disable_wallet.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            PPConfig.getInstance().disableWallet(isChecked);
//        });

    }

    public static class EditTextInputWatcher implements TextWatcher {

        private TextInputLayout textInputLayout;

        EditTextInputWatcher(TextInputLayout textInputLayout) {
            this.textInputLayout = textInputLayout;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() > 0) {
                textInputLayout.setError(null);
                textInputLayout.setErrorEnabled(false);
            }
        }
    }

    public boolean validateDetails(String email, String mobile) {
        email = email.trim();
        mobile = mobile.trim();

        if (TextUtils.isEmpty(mobile)) {
            setErrorInputLayout(mobile_et, getString(R.string.err_phone_empty), mobile_til);
            return false;
        } else if (!isValidPhone(mobile)) {
            setErrorInputLayout(mobile_et, getString(R.string.err_phone_not_valid), mobile_til);
            return false;
        } else if (TextUtils.isEmpty(email)) {
            setErrorInputLayout(email_et, getString(R.string.err_email_empty), email_til);
            return false;
        } else if (!isValidEmail(email)) {
            setErrorInputLayout(email_et, getString(R.string.email_not_valid), email_til);
            return false;
        } else {
            return true;
        }
    }

    public static void setErrorInputLayout(EditText editText, String msg, TextInputLayout textInputLayout) {
        textInputLayout.setError(msg);
        editText.requestFocus();
    }

    public static boolean isValidEmail(String strEmail) {
        return strEmail != null && android.util.Patterns.EMAIL_ADDRESS.matcher(strEmail).matches();
    }

    public static boolean isValidPhone(String phone) {
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone);
        return matcher.matches();
    }

//    private void showUserLoggedInDialog(boolean isQuickPayFlow) {
//        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
//        alertDialog.setTitle("Already logged in.");
//        alertDialog.setMessage("Different User is already logged in\n do you want to logout ?");
//        alertDialog.setPositiveButton("Yes", (dialog, which) -> {
//            logoutUser();
//            if (isQuickPayFlow) {
//                initQuickPayFlow();
//            } else {
//                initWalletFlow();
//            }
//        });
//        alertDialog.setNegativeButton("No", (dialog, which) -> CitrusFlowManager.startWalletFlow(MainActivity.this, userEmail, userMobile));
//        alertDialog.show();
//    }

    public boolean isDifferentUser(String userMobile) {
        String savedUserMobile = userDetailsPreference.getString(USER_MOBILE, "");
        return !savedUserMobile.equals(userMobile);

    }

    private void saveUserDetails(String email, String mobile) {
        SharedPreferences.Editor userDetailsEditor = userDetailsPreference.edit();
        userDetailsEditor.putString(USER_EMAIL, email);
        userDetailsEditor.putString(USER_MOBILE, mobile);
        userDetailsEditor.apply();

        storeAppPref();
    }

    public void logoutUser(final Context context) {

        final ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Logging user out...");
        mProgressDialog.show();
        CitrusClient.getInstance(context).signOut(new Callback<CitrusResponse>() {
            @Override
            public void success(CitrusResponse citrusResponse) {
                mProgressDialog.dismiss();
                Toast.makeText(context, "User is successfully logged out", Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void error(CitrusError error) {
                mProgressDialog.dismiss();
                Toast.makeText(context, "User could not be logged out", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
