package com.wemonde.fuelbuddy.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.wemonde.fuelbuddy.R;

import java.util.ArrayList;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class SpinnerAdapter extends ArrayAdapter<String> {

    public Resources res;
    private Context context;
    private ArrayList data;
    private LayoutInflater inflater;

    /*************  CustomAdapter Constructor *****************/
    public SpinnerAdapter(Context context, int textViewResourceId, ArrayList objects) {
        super(context, textViewResourceId, objects);

        /********** Take passed values **********/
        this.context = context;
        data = objects;

        /***********  Layout inflator to call external xml layout () **********************/
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    // This funtion called for each row ( Called data.size() times )
    public View getCustomView(int position, View convertView, ViewGroup parent) {

        /********** Inflate spinner_rows.xml file for each row ( Defined below ) ************/
        View row = inflater.inflate(R.layout.item_spinner, parent, false);

        /***** Get each Model object from Arraylist ********/
        String text = (String) data.get(position);

        TextView label =  row.findViewById(R.id.tv_label);

        label.setText(text);

//        if (position == 0) {
//
//            // Default selected Spinner item
//            label.setText("Please select company");
//            sub.setText("");
//        } else {
//            // Set values for spinner each row
//            label.setText(tempValues.getCompanyName());
//            sub.setText(tempValues.getUrl());
//            companyLogo.setImageResource(res.getIdentifier
//                    ("com.androidexample.customspinner:drawable/"
//                            + tempValues.getImage(), null, null));
//
//        }

        return row;
    }
}