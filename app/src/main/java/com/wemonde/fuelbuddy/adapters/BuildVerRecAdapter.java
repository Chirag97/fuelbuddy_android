package com.wemonde.fuelbuddy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.models.Building;
import com.wemonde.fuelbuddy.ui.MyBuildingActivity;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

/**
 * Created by Almond7 on 06-09-2016.
 */
public class BuildVerRecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Building> dataList;

    public BuildVerRecAdapter(Context context, List<Building> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_build_rec_ver, parent, false);
        return new DetailItemViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        setAssetData(holder, position);
    }

    private void setAssetData(RecyclerView.ViewHolder holder, final int position) {

        String habitnumber = "<b>" + "Address :" + "</b> ";
        String str = "<b>Address :</b> ";

        //setText  (""+ WordUtils.capitalize (dataList.get(position).getAssetS_ID()) )
        // tv.setText(Html.fromHtml(habitnumber ));
//        AppUtils.setImageUrl(((DetailItemViewHolder) holder).mItemImage, dataList.get(position).getImage(), R.drawable.def_box);
        ((DetailItemViewHolder) holder).tv_1.setText(""+ WordUtils.capitalize ( dataList.get(position).getBuildName()) );
        ((DetailItemViewHolder) holder).tv_2.setText((Html.fromHtml("<B>Address :  </B>"+ dataList.get(position).getBuildLocation())));
        //((DetailItemViewHolder) holder).tv_3.setText("Corp Id     : " + dataList.get(position).getCorpID());
        //((DetailItemViewHolder) holder).tv_4.setText("PinCode     : " + dataList.get(position).getPINCode());

        ((DetailItemViewHolder) holder).ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MyBuildingActivity)context).gotoAddBuilding(dataList.get(position));
            }
        });
    }

    private void setDummyData(RecyclerView.ViewHolder holder, int position) {
        // no specific action
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class DetailItemViewHolder extends RecyclerView.ViewHolder {
        TextView tv_1;
        TextView tv_2;
       // TextView tv_3;
      //  TextView tv_4;
        LinearLayout ll_main;

        private DetailItemViewHolder(View itemView) {
            super(itemView);
            tv_1 = (TextView) itemView.findViewById(R.id.tv_1);
            tv_2 = (TextView) itemView.findViewById(R.id.tv_2);
          //  tv_3 = (TextView) itemView.findViewById(R.id.tv_3);
         //   tv_4 = (TextView) itemView.findViewById(R.id.tv_4);
            ll_main = (LinearLayout) itemView.findViewById(R.id.ll_main);
        }
    }

//    private class DummyItemViewHolder extends RecyclerView.ViewHolder {
//        ImageView iv_add;
//
//        private DummyItemViewHolder(View itemView) {
//            super(itemView);
//            iv_add = (ImageView) itemView.findViewById(R.id.iv_asset_dp);
//            iv_add.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    ((BaseActivity) context).gotoAddAsset();
//                }
//            });
//        }
//    }
}