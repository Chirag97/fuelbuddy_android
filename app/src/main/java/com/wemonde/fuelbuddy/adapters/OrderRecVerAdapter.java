package com.wemonde.fuelbuddy.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.BookOrderRequestVoDynamicAddress;
import com.wemonde.fuelbuddy.models.BookingVo;
import com.wemonde.fuelbuddy.ui.HomeActivity;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.wemonde.fuelbuddy.utils.Slog;
import android.app.AlertDialog.Builder;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import static com.wemonde.fuelbuddy.ui.HomeActivity.assetId;
import static com.wemonde.fuelbuddy.ui.HomeActivity.fuelId;

/**
 * Created by Almond7 on 06-09-2016.
 */
public class OrderRecVerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<BookingVo> dataList;
    private static OrderRecVerAdapter instance;
    private  static AlertDialog alertdynamicbooking;




    public static int orderid;
    public static String quantity;
    public OrderRecVerAdapter(Context context, List<BookingVo> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    public static OrderRecVerAdapter getInstance(){
        return instance;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_ver, parent, false);
        return new ItemViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        setAssetData(holder, position);
    }

    private void setAssetData(RecyclerView.ViewHolder holder, final int position) {

        Slog.d("date : " + dataList.get(position).getBookedOn());

        String date =  AppUtils.getFormattedDate(dataList.get(position).getBookedOn());




        // ("<B>Order Id </B> - " + dataList.get(position).getOrderItemID())
        // setText  (""+ WordUtils.capitalize (dataList.get(position).getAssetS_ID()) )
        ((ItemViewHolder) holder).tv_o_id.setText(Html.fromHtml(""+ WordUtils.capitalize (dataList.get(position).getAssetS_ID()))  );
        ((ItemViewHolder) holder).tv_o_name.setText(Html.fromHtml("<B>Order Status - </B>" + dataList.get(position).getOrderStatus()));
        //  ((ItemViewHolder) holder).tv_o_date.setText(Html.fromHtml("<B> </B>" +date.substring(0, date.indexOf(' '))));
        // ((ItemViewHolder) holder).tv_o_date.setText(date);
        ((ItemViewHolder) holder).tv_o_date.setText(Html.fromHtml("<B> </B>" +date));
        ((ItemViewHolder) holder).tv_o_status.setText(Html.fromHtml("Order Id  - " + dataList.get(position).getOrderItemID()));
        ((ItemViewHolder) holder).tv_o_quantity.setText(Html.fromHtml("" + dataList.get(position).getQty()+ " Litres"));

        if (dataList.get(position).getOrderStatus().equals("Opened")){
            //setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0097a7")))
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.GONE);

            // ((ItemViewHolder) holder).reorder_button.setBackgroundColor(Color.WHITE);
            ((ItemViewHolder) holder).reorder_button.setVisibility(View.GONE);
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#0097a7"));
            //   ((ItemViewHolder) holder).tv_o_status.setBackgroundDrawable (new ColorDrawable(Color.parseColor("#0097a7")));
//           // ((ItemViewHolder) holder).tv_o_status.setTextColor (Color.parseColor("2196f3"));
        }else if (dataList.get(position).getOrderStatus().equals("Declined")){
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.GONE);
            ((ItemViewHolder) holder).reorder_button.setVisibility(View.VISIBLE);
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#ff5252"));
        }
        else if (dataList.get(position).getOrderStatus().equals("Delivered")){
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.GONE);
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#689f38"));

            ((ItemViewHolder) holder).reorder_button.setVisibility(View.VISIBLE);
            ((ItemViewHolder) holder).ordersummary.setVisibility(View.VISIBLE);



            ((ItemViewHolder) holder).ordersummary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    orderid =  dataList.get(position).getOrderItemID();

                    ((BaseActivity) context).getsensors();
                    orderid =  dataList.get(position).getOrderItemID();

                    quantity =  String.valueOf(dataList.get(position).getQty());
                    SharedPrefrenceHelper.setCompanyId(((BaseActivity) context), quantity);


                    // ((BaseActivity) context).gotoTracking();
                }
            });

        }
        else if (dataList.get(position).getOrderStatus().equals("Assigned")){
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#ef6c00"));
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.GONE);
            ((ItemViewHolder) holder).reorder_button.setVisibility(View.GONE);

            ((ItemViewHolder) holder).iv_o_dp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    orderid =  dataList.get(position).getOrderItemID();



                    ((BaseActivity) context).gettrackingdetails();

                    // ((BaseActivity) context).gotoTracking();
                }
            });



        } else if (dataList.get(position).getOrderStatus().equals("Arriving")){
            ((ItemViewHolder) holder).reorder_button.setVisibility(View.GONE);
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.VISIBLE);
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#8B008B"));

        }else if (dataList.get(position).getOrderStatus().equals("Accepted")){
            ((ItemViewHolder) holder).reorder_button.setVisibility(View.GONE);
            ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.VISIBLE);
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#8B008B"));
            ((ItemViewHolder) holder).iv_o_dp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    orderid =  dataList.get(position).getOrderItemID();

                    ((BaseActivity) context).gettrackingdetails();

                    // ((BaseActivity) context).gotoTracking();
                }
            });

        }


        else {
            ((ItemViewHolder) holder).tv_o_name.setTextColor(Color.parseColor("#8B008B"));
            //  ((ItemViewHolder) holder).iv_o_dp.setVisibility(View.VISIBLE);


            ((ItemViewHolder) holder).iv_o_dp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    orderid =  dataList.get(position).getOrderItemID();

                    ((BaseActivity) context).gettrackingdetails();

                    // ((BaseActivity) context).gotoTracking();
                }
            });




        }

//        ((ItemViewHolder) holder).iv_o_dp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                orderid =  dataList.get(position).getOrderItemID();
//
//                ((BaseActivity) context).gettrackingdetails();
//
//                // ((BaseActivity) context).gotoTracking();
//            }
//        });



        ((ItemViewHolder) holder).reorder_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int fuelid = 1;


                String uid = ((BaseActivity) context).getLoginVo().getPMgrID();

                //imagepath =getLoginVo().getPMgrImagePath();
                int uuid = Integer.parseInt(uid);

               // int uuid = Integer.parseInt(BaseActivity.uid);

                showalertreorder (uuid, dataList.get(position).getAssetID(), fuelid, (int) dataList.get(position).getQty(),dataList.get(position).getAssetS_ID(),dataList.get(position).getLocation(),dataList.get(position).getLatitude(),dataList.get(position).getLongitude(),dataList.get(position).getStateID(),dataList.get(position).getPINCode());



            }
        });




//        AppUtils.setImageUrl(((ItemViewHolder) holder).iv_o_dp, dataList.get(position).getImage(), R.drawable.def_box);
    }

    private void showalertreorder( int uuid, final int assetID, final int fuelid, final int qty, final String assetsname,final String Location,final double latitude,final  double longitude,final  int StateID, final  String PINcode) {



        final int uid = uuid;
        final int assetid =assetID;
        final int fuel_id = fuelid;
        final int quantity = qty;
        final String assetn = assetsname;

        final String location = Location;
        final double Latitude =  latitude;
        final double Longitude = longitude;
        final int Stateid = StateID;
        final String Pincode = PINcode;



        // Integer x = Integer.valueOf(str);









        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customdialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        TextView text = (TextView) dialog.findViewById(R.id.text_message);
        text.setText("Do you want to confirm order for : "+ quantity+ "litres , for the asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //  String scheduleorder = SharedPrefrenceHelper.getscheduledate(BaseActivity.getInstance());
                BookOrderRequestVo borv = new BookOrderRequestVo(uid, assetID, fuelid,qty,"");
                ((BaseActivity) context).gotoOrderSummary(borv);
//
//                        BookOrderRequestVoDynamicAddress borv = new BookOrderRequestVoDynamicAddress(uid, assetID, fuelid,qty,Stateid,location,Latitude,Longitude,Pincode);
//        ((BaseActivity) context).gotoOrderSummaryfordynamicaddress(borv);

                HomeActivity.flagfordynamicbooking = 1;
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                dialog.cancel();
            }
        });

        dialog.show();
//        BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetID, fuelid,qty);
//        ((BaseActivity) context).gotoOrderSummary(borv);
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }



    private class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView iv_o_dp;
        private TextView tv_o_id;
        private TextView tv_o_name;
        private TextView tv_o_date;
        private TextView tv_o_status;
        private TextView tv_o_quantity;
        private Button reorder_button;
        private Button ordersummary;

        private ItemViewHolder(View itemView) {
            super(itemView);

            iv_o_dp = (ImageView) itemView.findViewById(R.id.iv_o_dp);
            tv_o_id = (TextView) itemView.findViewById(R.id.tv_o_id);
            tv_o_name = (TextView) itemView.findViewById(R.id.tv_o_name);
            tv_o_date = (TextView) itemView.findViewById(R.id.tv_o_date);
            tv_o_status = (TextView) itemView.findViewById(R.id.tv_o_status);
            tv_o_quantity = (TextView) itemView.findViewById(R.id.tv_o_quantity);
            reorder_button = (Button) itemView.findViewById(R.id.reorder_button);
            ordersummary = (Button) itemView.findViewById(R.id.ordersummary);
        }
    }
}