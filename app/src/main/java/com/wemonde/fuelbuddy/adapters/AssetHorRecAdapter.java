package com.wemonde.fuelbuddy.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.ui.ProfileActivity;
import com.wemonde.fuelbuddy.utils.AppUtils;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;

import static android.R.id.input;

/**
 * Created by Almond7 on 06-09-2016.
 */
public class AssetHorRecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Asset> dataList;
    private String baseUrl;
    private final int TYPE_NORMAL = 1;
    private final int TYPE_DUMMY = 2;

    public AssetHorRecAdapter(Context context, List<Asset> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.baseUrl = baseUrl;

        Asset asset = new Asset(true);
        dataList.add(asset);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_DUMMY){
           View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asset_rec_hor_dummy, parent, false);
            return new DummyItemViewHolder(itemView);
        }else{
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asset_rec_hor, parent, false);
            return new DetailItemViewHolder(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if(dataList.get(position).isDummy()){
            return TYPE_DUMMY;
        }else{
            return TYPE_NORMAL;
        }


    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_DUMMY:
                setDummyData(holder, position);
                break;
            case TYPE_NORMAL:
                setAssetData(holder, position);
                break;
        }
    }

    private void setAssetData(RecyclerView.ViewHolder holder, final int position){




        if  (dataList.get(position).getAssetIdentifierName().equals("Immovable Asset")){
            ((DetailItemViewHolder) holder).mItemImage.setImageResource(R.drawable.genset_selected);
        }else {
            ((DetailItemViewHolder) holder).mItemImage.setImageResource(R.drawable.car_selected);
        }


        if(dataList.get(position).getAssetS_ID() != null){
            ((DetailItemViewHolder) holder).tv_asset_name.setText("" +  WordUtils.capitalize(dataList.get(position).getAssetS_ID()));
        }else{
            ((DetailItemViewHolder) holder).tv_asset_name.setText("");
        }
        ((DetailItemViewHolder) holder).ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity) context).gotoAddAsset(dataList.get(position));
            }
        });
        AppUtils.setImageUrl(((DetailItemViewHolder) holder).mItemImage, dataList.get(position).getImage(), R.drawable.def_box);
    }

    private void setDummyData(RecyclerView.ViewHolder holder, int position){
        // no specific action
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class DetailItemViewHolder extends RecyclerView.ViewHolder {
        ImageView mItemImage;
        TextView tv_asset_name;
        RelativeLayout ll_main;

        private DetailItemViewHolder(View itemView) {
            super(itemView);
            mItemImage = (ImageView) itemView.findViewById(R.id.iv_asset_dp);
            tv_asset_name = (TextView) itemView.findViewById(R.id.tv_asset_name);
            ll_main = (RelativeLayout) itemView.findViewById(R.id.ll_main);
        }
    }

    private class DummyItemViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_add;

        private DummyItemViewHolder(View itemView) {
            super(itemView);
            iv_add = (ImageView) itemView.findViewById(R.id.iv_asset_dp);
            iv_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity)context).gotoAddAsset();
                }
            });
        }
    }
}