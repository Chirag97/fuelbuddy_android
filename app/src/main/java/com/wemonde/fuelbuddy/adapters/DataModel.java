package com.wemonde.fuelbuddy.adapters;

/**
 * Created by Devendra Pandey on 12/6/2017.
 */

public class DataModel {
    public String name;
    public boolean checked;

    public DataModel(String name, boolean checked) {
        this.name = name;
        this.checked = checked;

    }
}
