package com.wemonde.fuelbuddy.adapters;

import android.animation.Animator;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gelitenight.waveview.library.WaveView;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.Asset;
import com.wemonde.fuelbuddy.models.Assettankdetails;
import com.wemonde.fuelbuddy.models.BookOrderRequestVo;
import com.wemonde.fuelbuddy.models.MyLoc;
import com.wemonde.fuelbuddy.ui.MyAssetsActivity;
import com.wemonde.fuelbuddy.utils.AppUtils;
import com.wemonde.fuelbuddy.utils.SharedPrefrenceHelper;
import com.willowtreeapps.spruce.Spruce;
import com.willowtreeapps.spruce.animation.DefaultAnimations;
import com.willowtreeapps.spruce.sort.DefaultSort;
import com.willowtreeapps.spruce.sort.LinearSort;

import org.apache.commons.lang3.text.WordUtils;

import java.util.List;
import java.util.Random;

//import static com.wemonde.fuelbuddy.base.BaseActivity.uid;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_ASSETS;

/**
 * Created by Almond7 on 06-09-2016.
 */
public class AssetVerRecAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TYPE_NORMAL = 1;
    private final int TYPE_DUMMY = 2;
    private Context context;
    private List<Asset> dataList;
    private   Dialog dialog ;


    public static String assetname;
    public static int quantityofasset;
    public static int assetid = -1;
    private static EditText et_country;
    private static int quantityselected = -1;
    private static int assetidfordialog = -1;
    private static int capacitydialog = -1;
    public static int assetiddialog = -1;
    private int lastPosition = -1;
    private WavehelperNew mWaveHelper;
    private static int fuldepth;
    private static int tankdepth;
    private static int   percentagethreshold;
    private int mBorderColor = Color.parseColor("#44FFFFFF");
    private List<Assettankdetails> dataListnew;

    public AssetVerRecAdapter(Context context, List<Asset> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_DUMMY) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asset_rec_hor_dummy, parent, false);
            return new DummyItemViewHolder(itemView);
        } else {



            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asset_rec_ver, parent, false);
            return new DetailItemViewHolder(itemView);

        }
    }

    @Override
    public int getItemViewType(int position) {
        if (dataList.get(position).isDummy()) {
            return TYPE_DUMMY;
        } else {
            return TYPE_NORMAL;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        switch (getItemViewType(position)) {
            case TYPE_DUMMY:
                setDummyData(holder, position);
                break;
            case TYPE_NORMAL:
                setAssetData(holder, position);
           //     setAnimation(holder.itemView, position);
                break;
        }
    }


    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            anim.setDuration(new Random().nextInt(501));//to make duration random number between [0,501)
            viewToAnimate.startAnimation(anim);
            lastPosition = position;
        }
    }
    private void setAssetData(RecyclerView.ViewHolder holder, final int position) {
        if  (dataList.get(position).getAssetIdentifierName().equals("Immovable Asset")){
            ((DetailItemViewHolder) holder).mItemImage.setBackgroundResource(R.drawable.genset_selected);
        }else {
            ((DetailItemViewHolder) holder).mItemImage.setBackgroundResource(R.drawable.car_selected);
        }






        //""+ WordUtils.capitalize (bookingVo.getAssetModel())
       // AppUtils.setImageUrl(((DetailItemViewHolder) holder).mItemImage, dataList.get(position).getImage(), R.drawable.def_box);
       // ((DetailItemViewHolder) holder).tv_a_id.setText   ("ID        : "+ dataList.get(position).getAssetID());
        ((DetailItemViewHolder) holder).tv_a_sid.setText  (""+ WordUtils.capitalize (dataList.get(position).getAssetS_ID()) );
        ((DetailItemViewHolder) holder).tv_a_cap.setText  ((Html.fromHtml("<B>Capacity :  </B>" + dataList.get(position).
                getAssetFuelCapacity() + " Litres")));
     //   ((DetailItemViewHolder) holder).tv_a_ident.setText("Identifier: "+ dataList.get(position).getAssetIdentifierName());


        ((DetailItemViewHolder) holder).ll_asset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // ((BaseActivity) context).gotoAddAsset(dataList.get(position));
            }
        });


        ((DetailItemViewHolder) holder).morderbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capacitydialog = (dataList.get(position).getAssetFuelCapacity());
                assetiddialog = (dataList.get(position).getAssetID());

                ((BaseActivity) context).gotoAddAsset(dataList.get(position));
             //   orderfuel();
            }
        });

        ((DetailItemViewHolder) holder).mItemImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Assetdetails(position);

              //  ((BaseActivity) context).gettankdetails();
              //  Assetdetails(Integer.parseInt(dataList.get(position).getAssetS_ID()));

            //   ((BaseActivity) context).gotoTankLevelsenseing(assetname, toString().valueOf(assetid));

            //    ((BaseActivity) context).gettankdetailsnew();

              //  ((BaseActivity) context).gotoLivefueldispensing();

                try {
                    Handler h = new Handler();
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            // getMgrAssets();
//                    getFuelTypes();
                            // getdashboard();




                            //getAssetIdentifier();
                            //  getStates();
                            // gettrackingdetails();

                        }
                    };
                    h.postDelayed(r, 1000);
                    ((BaseActivity) context).hideProgress();
                } catch (Exception e) {
                    e.printStackTrace();
                }




               // dialogview ();
            }


        });

    }

//    @Override
//    protected void onResponse(int request_code, String response, String data) {
//        super.onResponse(request_code, response, data);
//        switch (request_code) {
//            case REQUEST_GET_ASSETS:
//                setAssetAdapternew();
//                break;
//        }
//    }
    private void dialogview() {

       //TankLevelshowing

        tankdetailsshow();

        MaterialDialog dialog;

        boolean wrapInScrollView = true;



         dialog = new MaterialDialog.Builder(((BaseActivity) context))
                .title(R.string.TankBuddy)

                .iconRes(R.mipmap.ic_launcher_app)
                .customView(R.layout.custom_dialog_box_for_water_level, wrapInScrollView)


                .positiveText(R.string.place_order)
                 .onPositive(new MaterialDialog.SingleButtonCallback() {
                     @Override
                     public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                       //  showdialogmaterialnew ();

                      //   BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetID, fuelid,qty);
//        ((BaseActivity) context).gotoOrderSummary(borv)
                       //  Assetdetails(positionorder);

                        // int assetidnew = Integer.parseInt(assetid);


                         ((BaseActivity) context).gotoPlaceOrdernew(assetname, toString().valueOf(assetid));


                        // gotoPlaceOrdernew(assetidentifier,assetname);
                     }
                 })
                 .negativeText("Back")

                 .build();






        final WaveView waveView = (WaveView) dialog.findViewById(R.id.wave);
        waveView.setWaveColor(
                Color.parseColor("#ffffff"),
                Color.parseColor("#45b879"));
        mBorderColor = Color.parseColor("#0c2f33");
        waveView.setBorder(5, mBorderColor);
        waveView.setShapeType(WaveView.ShapeType.SQUARE);
        mWaveHelper = new WavehelperNew(waveView);
        mWaveHelper.start();

      //  View actualreadingnew = (View)dialog.findViewById(R.id.threshold_indicator_one);

      //  LinearLayout actualreading = (LinearLayout)dialog.findViewById(R.id.actual_reading);

        LinearLayout layout = (LinearLayout)dialog.findViewById(R.id.actual_reading);
        int paddingPixel = 780;
        layout.setPadding(0,paddingPixel,0,0);


        LinearLayout layoutthreshold = (LinearLayout)dialog.findViewById(R.id.threshhold_reading);
        int paddingPixelnew = 100;
        layoutthreshold.setPadding(0,0,0,percentagethreshold);
//        float density = context.getResources().getDisplayMetrics().density;
//        int paddingDp = (int)(paddingPixel * density);
//        layout.setPadding(0,paddingDp,0,0);


        dialog.show();


    }


    public void tankdetailsshow (){



         fuldepth = ((BaseActivity) context).tankdetails.get(0).getFuelDepth();
         percentagethreshold = ((BaseActivity) context).tankdetails.get(0).getTankDepthPercentageThreshold();
         tankdepth = ((BaseActivity) context).tankdetails.get(0).getTankDepth();

        ((BaseActivity) context).  showToast(""+ tankdepth);

        int realqtypercent = (fuldepth/tankdepth)*100;

       // ((BaseActivity) context).  showToast(""+ realqtypercent);



    }









    private void setDummyData(RecyclerView.ViewHolder holder, int position) {
        // no specific action

    }


    private void initDialog(){

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.customorderingdialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        et_country = (EditText) dialog.findViewById(R.id.et_country);
        et_country.setText("30");
//        if (et_country.hasFocus()){
//            et_country.setCursorVisible(true);
//        }
        et_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                et_country.setCursorVisible(true);
            }
        });



        // et_country.setSelection(et_country.getText().length());
        // text.setText("Do You Want To confirm Order For : "+ quantity+ "litres , For the Asset :"+assetn);

        Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Cancel" ,Toast.LENGTH_SHORT).show();
                dialog.dismiss();
               // getMgrAssets();

            }
        });

        Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btn_okay);
        dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validate();


//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();
                // dialog.cancel();
            }
        });

        dialog.show();
//        BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetID, fuelid,qty);
//        ((BaseActivity) context).gotoOrderSummary(borv);
    }

    public void validate(){

        try {
            quantityselected = Integer.parseInt(et_country.getText().toString());
        }catch (Exception e){
            ((BaseActivity) context). showToast("Please enter quantity");
            return;
        }


        if (capacitydialog  < quantityselected){
            ((BaseActivity) context).  showToast("Ordered capacity is greater than asset's capacity");
            et_country.setTextColor(Color.parseColor("#FF0000"));

            // setTextColor(Color.parseColor("#8B008B")

            return;
        }



        try {
            try {
                quantityselected = Integer.parseInt(et_country.getText().toString());
            }catch (Exception e){
                ((BaseActivity) context). showToast("Please enter quantity");
                return;
            }
            if (quantityselected < 1) {
                ((BaseActivity) context). showToast("Please enter quantity");
                et_country.setTextColor(Color.parseColor("#000000"));
                //    speakOut("Please enter quantity");
                return;
            } else if (quantityselected < 30) {
                ((BaseActivity) context).  showToast("Minimum Limit of ordering is 30 litres, please order within limit ");
                et_country.setTextColor(Color.parseColor("#FF0000"));
                return;
            } else if (quantityselected > 5000) {
                ((BaseActivity) context). showToast("Maximum Limit of ordering is 5000 litres, please order within limit ");
                et_country.setTextColor(Color.parseColor("#FF0000"));
                return;
            }else  {

               String uid = ((BaseActivity) context).getLoginVo().getPMgrID();

                //imagepath =getLoginVo().getPMgrImagePath();
                int uuid = Integer.parseInt(uid);

              //  String scheduleorder = SharedPrefrenceHelper.getscheduledate(BaseActivity.getInstance());

                int quantity = Integer.parseInt(et_country.getText().toString());
                int fuelid = 1;
                BookOrderRequestVo borv = new BookOrderRequestVo(uuid, assetiddialog, fuelid,quantity,"");
                ((BaseActivity) context).gotoOrderSummary(borv);
                dialog.cancel();

                ((BaseActivity) context). showToast("Thank you for ordering with Fuelbuddy! you will  be receiving your order soon ");

            }
        }catch (Exception e) {
        }
    }

    public void orderfuel(){
        initDialog();
        //((BaseActivity) context).showToast("new asset");

    }

    public void Assetdetails (int position ){
        try {
            assetname = dataList.get(position).getAssetS_ID();
            quantityofasset = dataList.get(position).getAssetFuelCapacity();
            assetid = dataList.get(position).getAssetID();
        }catch (Exception e){

        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class DetailItemViewHolder extends RecyclerView.ViewHolder {
        ImageView mItemImage;
        ImageView morderbutton;
      //  TextView tv_a_id;
        TextView tv_a_sid;
        TextView tv_a_cap;
      //  TextView tv_a_ident;
        LinearLayout ll_asset;

        private DetailItemViewHolder(View itemView) {
            super(itemView);
            mItemImage = (ImageView) itemView.findViewById(R.id.iv_a_dp);
            morderbutton = (ImageView)itemView.findViewById(R.id.iv_a_order);


           // tv_a_id = (TextView) itemView.findViewById(R.id.tv_a_id);
            tv_a_sid = (TextView) itemView.findViewById(R.id.tv_a_sid);
            tv_a_cap = (TextView) itemView.findViewById(R.id.tv_a_cap);
          //  tv_a_ident = (TextView) itemView.findViewById(R.id.tv_a_ident);
            ll_asset = (LinearLayout) itemView.findViewById(R.id.ll_asset);
        }
    }

    private class DummyItemViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_add;


        private DummyItemViewHolder(View itemView) {
            super(itemView);
            iv_add = (ImageView) itemView.findViewById(R.id.iv_asset_dp);
            iv_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((BaseActivity ) context).gotoAddAsset();
                }
            });
        }
    }
}