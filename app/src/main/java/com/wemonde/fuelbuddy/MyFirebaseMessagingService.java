//package com.wemonde.fuelbuddy;
//
//import android.app.Service;
//
//import android.app.NotificationManager;
//import android.app.PendingIntent;
//import android.content.Context;
//import android.content.Intent;
//import android.media.RingtoneManager;
//import android.net.Uri;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.google.firebase.messaging.FirebaseMessagingService;
//import com.google.firebase.messaging.RemoteMessage;
//import com.wemonde.fuelbuddy.ui.HomeActivity;
//
///**
// * Created by Devendra
// */
//
//public class MyFirebaseMessagingService extends FirebaseMessagingService {
//
//    private static final String TAG = "MyFirebaseMsgService";
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        //Displaying data in log
//        //It is optional
//        Log.d(TAG, "From: " + remoteMessage.getFrom());
//        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
//
//
//        //Calling method to generate notification
//        sendNotification(remoteMessage.getNotification().getBody());
//    }
//
//    //This method is only generating push notification
//    //It is same as we did in earlier posts
//    private void sendNotification(String messageBody) {
//        Intent intent = new Intent(this, HomeActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//
//
//        String CHANNEL_ID = "my_channel_01";
//
//
//
//
//
//
//        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//
//
//
//
//
//
//
//    //    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.fuel_buddy_logo_00021)
//                .setContentTitle("FuelBuddy")
//
//
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setPriority(NotificationCompat.PRIORITY_HIGH)
//
//                .setContentText(messageBody)
//
//                .setContentIntent(pendingIntent);
//
//
//
//
//        NotificationCompat.InboxStyle inboxStyle =
//                new NotificationCompat.InboxStyle();
//
//        String[] events = new String[15];
//
//
//// Sets a title for the Inbox in expanded layout
//// Sets a title for the Inbox in expanded layout
//        inboxStyle.setBigContentTitle("Event tracker details:");
//
//// Moves events into the expanded layout
//        for (int i=0; i < events.length; i++) {
//            inboxStyle.addLine(events[i]);
//        }
//// Moves the expanded layout object into the notification object.
//        notificationBuilder.setStyle(inboxStyle);
//
//      //  notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(0, notificationBuilder.build());
//    }
//}