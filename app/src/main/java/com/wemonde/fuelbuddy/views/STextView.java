package com.wemonde.fuelbuddy.views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class STextView extends AppCompatTextView {

    public STextView(Context context) {
        super(context);
        onInitTypeface(context, null, 0);
    }

    public STextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        onInitTypeface(context, attrs, 0);
    }

    public STextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        onInitTypeface(context, attrs, defStyle);
    }

    private void onInitTypeface(Context context, AttributeSet attrs, int defStyle) {
//	super(context, attrs, defStyle);
//        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fuel.ttf");
//        setTypeface(typeface);
    }
}