package com.wemonde.fuelbuddy;

import android.content.Context;
import android.graphics.Typeface;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConstantClass {
    public static Typeface getFont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "OpenSans.ttf");
        return typeface;
    }

    public static Typeface getItalicfont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "garamonditalic.ttf");
        return typeface;
    }

    public static Typeface getBoldfont(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "garamondbold.ttf");
        return typeface;
    }

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
// Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }


    public static String dateTimeFormat(String oldDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = sdf.parse(oldDate);

            SimpleDateFormat sd1f = new SimpleDateFormat("dd MMM, yyyy  hh:mm a");
            return sd1f.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}

