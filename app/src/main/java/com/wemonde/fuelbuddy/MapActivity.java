package com.wemonde.fuelbuddy;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fastaccess.permission.base.PermissionHelper;
import com.fastaccess.permission.base.callback.OnPermissionCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.wemonde.fuelbuddy.base.BaseActivity;
import com.wemonde.fuelbuddy.models.MyLoc;
import com.wemonde.fuelbuddy.utils.GPSTracker;
import com.wemonde.fuelbuddy.utils.Slog;
import com.google.android.gms.common.ConnectionResult;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import static com.wemonde.fuelbuddy.utils.ApiRequestCodes.REQUEST_GET_LOCATION;
// OnMapReadyCallback, OnPermissionCallback, PlaceSelectionListener, GoogleMap.OnMarkerClickListener, TextToSpeech.OnInitListener, GoogleApiClient.ConnectionCallbacks, OnConnectionFailedListener

public class MapActivity extends BaseActivity implements OnMapReadyCallback, OnPermissionCallback, GoogleApiClient.ConnectionCallbacks, OnConnectionFailedListener {

    final String PERMISSION = Manifest.permission.ACCESS_FINE_LOCATION;
    final String DIALOG_TITLE = "Access Location";
    final String DIALOG_MESSAGE = "We need to access your Location to show you proper distance to the centers.";
    PermissionHelper permissionHelper;
    private TextView tv_location;
    private MyLoc myLoc;
    private MapFragment mapFragment;

    private static int locationcheck = 1 ;

    private static int locationformapfromautocomplete = 1 ;

    private static String postalCode;
    private static String selectedaddress;

    private static String addresfromautocomplete;

    public static final String MyPREFERENCES = "MyPrefs";

    public static  SharedPreferences settings;

    public  static int mapactivity = 0;

    private int currentapiVersion ;
    protected Location mLastLocation;
    public static Activity mapactivitynw;

    protected GoogleApiClient mGoogleApiClient;

    private static String addressmapactivity ;

    public static PlaceAutocompleteFragment autocompleteFragment;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setscreenLayout(R.layout.activity_map);



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);


        mapactivitynw = this;

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks((GoogleApiClient.ConnectionCallbacks) this)
                    .addOnConnectionFailedListener((GoogleApiClient.OnConnectionFailedListener) this)
                    .addApi(LocationServices.API)
                    .build();
        }


        currentapiVersion = android.os.Build.VERSION.SDK_INT;

        myLoc = new MyLoc();
        tv_location = (TextView) findViewById(R.id.tv_location);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        settings = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        setMyButton();

        autocompleteFragment  = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.autocomplete_fragment);


        // placeAutocompleteFragment - is my PlaceAutocompleteFragment instance
        ((EditText)autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).setTextSize(14.0f);

        //  address = String.valueOf(((EditText)autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input)).getText());

        AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(Place.TYPE_ADMINISTRATIVE_AREA_LEVEL_1)
                .setCountry("IN")
                .build();

        autocompleteFragment.setFilter(autocompleteFilter);


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.

                locationcheck= 0 ;

                double workLatitude = place.getLatLng().latitude;
                double workLongitude = place.getLatLng().longitude;

                final CharSequence address = place.getAddress();

                String firstname = (String) place.getName();
                String add = String.valueOf(place.getAddress());


                showToast(firstname);

                showToast(add);

               if ( add.toLowerCase().contains(firstname.toLowerCase())){
                 //  addresfromautocomplete = String.valueOf((String) place.getName() +" , " + (String) place.getAddress());
                   addresfromautocomplete = String.valueOf(  (String) place.getAddress());
               }else {
                  // addresfromautocomplete = String.valueOf(  (String) place.getAddress());
                   addresfromautocomplete = String.valueOf((String) place.getName() +" , " + (String) place.getAddress());
               }

              //  addresfromautocomplete = String.valueOf((String) place.getName() +" , " + (String) place.getAddress());

                selectedaddress = " ";

               // String firstname = (String) place.getName();
                //  new JSONParse().execute();
                selectedaddress =  String.valueOf(place.getAddress());
               // selectedaddress = getCompleteAddressString(workLatitude, workLongitude);

                if (selectedaddress != null && selectedaddress.length() > 0){

                    autocompleteFragment.setText (selectedaddress);
                    locationformapfromautocomplete = 0;
                   // autocompleteFragment.setText(getCompleteAddressString(workLatitude, workLongitude));
                    //  autocompleteFragment.setText ("Hi");
                }else {
                    // autocompleteFragment.setText (BaseActivity.AddressFromGoogleApi);
                    selectedaddress = BaseActivity.AddressFromGoogleApi;
                    locationformapfromautocomplete = 1;
                }
                //  getAddress(loc.latitude,loc.longitude);


                //   Toast.makeText(getApplicationContext(),"Your Selected Address is " +add , Toast.LENGTH_LONG).show();


                try {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(workLatitude, workLongitude)));
                    marker = addPointToMap(new LatLng(workLatitude, workLongitude));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // Toast.makeText(get)
                //Log.i(TAG, "Place: " + place.getName());
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                //  Log.i(TAG, "An error occurred: " + status);
            }
        });
    }



    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Slog.i("onRequestPermissionsResult() ", Arrays.toString(permissions)
                + "\nRequest Code: " + requestCode
                + "\nGrand Results: " + grantResults);

        permissionHelper.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onMapReady(GoogleMap gmap) {
        this.googleMap = gmap;

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        String str = sdf.format(new Date());

        String[] time = str.split ( ":" );
        int hour = Integer.parseInt ( time[0].trim() );



        //  outputtime.setText(time);
        // showToast(""+hour);

        if (hour>= 18 || hour < 6 ){
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.mapstyle_night);
            gmap.setMapStyle(style);




            // fab.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            //fab_btn.setbac
        }else {
            MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.mapstyle_retro);
            gmap.setMapStyle(style);

        }
//        googleMap.moveCamera(CameraUpdateFactory.newLatLng(DELHI));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(DELHI)      // Sets the center of the map to Mountain View
                .zoom(15)                   // Sets the zoom
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        if (status != ConnectionResult.SUCCESS) {
            ///if play services are not available
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            // Enabling MyLocation Layer of Google Map
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
           // googleMap.setTrafficEnabled(true);
            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {

                    if (currentapiVersion <= 21) {
                        // Do something for 14 and above versions

                        try {
                            getGpsLoc();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {

                        if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return false;
                        }
                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                mGoogleApiClient);
                        if (mLastLocation != null) {
//                            showToast(String.valueOf(mLastLocation.getLatitude()));
//                            showToast(String.valueOf(mLastLocation.getLongitude()));


                            LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                            updateAddress(DELHI6);
                            marker = addPointToMap(DELHI6);
                          //  showToast(String.valueOf(currentapiVersion));
                        }
                        // do something for phones running an SDK before 14

                    }



//
//                    double current_lat = mLastLocation.getLatitude();
//                    double current_longi = mLastLocation.getLongitude();
//
//                    String numberAsString = Double.toString(current_lat);
//                    showToast(numberAsString);
                    // getGpsLoc();


                    //  getGpsLoc();
                    return false;
                }
            });

            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
           // googleMap.getUiSettings().setZoomControlsEnabled(true);
            Handler h = new Handler();
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    {

                        if (currentapiVersion <= 24) {
                            // Do something for 14 and above versions

                            try {
                                getGpsLoc();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } else {

                            if (ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(bContext, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                // TODO: Consider calling
                                //    ActivityCompat#requestPermissions
                                // here to request the missing permissions, and then overriding
                                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                //                                          int[] grantResults)
                                // to handle the case where the user grants the permission. See the documentation
                                // for ActivityCompat#requestPermissions for more details.
                                return ;
                            }
                            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                                    mGoogleApiClient);
                            if (mLastLocation != null) {
//                            showToast(String.valueOf(mLastLocation.getLatitude()));
//                            showToast(String.valueOf(mLastLocation.getLongitude()));


                                LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                                updateAddress(DELHI6);
                                marker = addPointToMap(DELHI6);
                               // showToast(String.valueOf(currentapiVersion));
                            }
                            // do something for phones running an SDK before 14

                        }



//
//                    double current_lat = mLastLocation.getLatitude();
//                    double current_longi = mLastLocation.getLongitude();
//
//                    String numberAsString = Double.toString(current_lat);
//                    showToast(numberAsString);
                        // getGpsLoc();

                        hideProgress();

                        //  getGpsLoc();
                      //  return ;
                    }
//                    getGpsLoc();
//                    hideProgress();
                }
            };

            h.postDelayed(r, 1000);
            showProgress();
        }

        googleMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
//                Slog.d("map moving");
            }
        });

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Slog.d("Map clicked");
                locationcheck=1;
                marker = addPointToMap(latLng);
                locationformapfromautocomplete = 1;
            }
        });
    }

    @Override
    protected void canGetLocation(GPSTracker gps) {
//        super.canGetLocation(gps);
     //  setMyButton();
        if (locationcheck == 1) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            LatLng DELHI6 = new LatLng(latitude,longitude);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(DELHI6)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom
                    .build();

            try {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude)));
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                marker = addPointToMap(new LatLng(latitude, longitude));
            } catch (Exception e) {
                e.printStackTrace();
            }
            showLogToast("Your Location is - \nLat: " + latitude + "\nLong: " + longitude);

        }

    }

    private void setMyButton(){
        try {
            View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));


            //  View locationButton = ((View) mapFragment.getView().findViewById(1).getParent()).findViewById(2);

            // and next place it, for exemple, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
            // position on right bottom
//            rlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, 0);
//            rlp.addRule(RelativeLayout.ALIGN_BOTTOM, RelativeLayout.TRUE);
//            rlp.setMargins(50, 50, 30, 210);

            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            rlp.setMargins(0, 180, 180, 0);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void updateAddress(LatLng loc) {
        super.updateAddress(loc);


       // address = getCompleteAddressString(loc.latitude, loc.longitude);



        selectedaddress = " ";
        postalCode = " ";

        myLoc.setLat(loc.latitude);
        myLoc.setLng(loc.longitude);

       // flagcheck = 1;

        mapactivity = 1;

        selectedaddress = getCompleteAddressString(loc.latitude, loc.longitude);
         postalCode = getcompletepincode(loc.latitude, loc.longitude);

        autocompleteFragment.setText(selectedaddress);

      //  address != null && address.length() > 0
        if (selectedaddress != null && selectedaddress.length() > 0) {
            mapactivity = 0;


            if (locationformapfromautocomplete == 0){
                myLoc.setAddress(addresfromautocomplete);
                autocompleteFragment.setText(selectedaddress);
            }else {
                myLoc.setAddress(selectedaddress);
              //  autocompleteFragment.setText(selectedaddress);
            }
          //  Toast.makeText(getApplicationContext(), "Your Selected Address is " + selectedaddress, Toast.LENGTH_LONG).show();


        }else {

            selectedaddress = settings.getString("Keyaddress", "defaultValue");;
          //  myLoc.setAddress(BaseActivity.AddressFromGoogleApi);
            autocompleteFragment.setText(selectedaddress);

          //  Toast.makeText(getApplicationContext(), "Your Selected Address is " + selectedaddress, Toast.LENGTH_LONG).show();
        }

//        else {
//            Toast.makeText(getApplicationContext(),"Unable to Fetch Address of this Place Please Enter Address Manually"  , Toast.LENGTH_LONG).show();
//        }
       // myLoc.setAddress(selectedaddress);
        if (postalCode != null && !postalCode.isEmpty() && !postalCode.equals("null")) {
            mapactivity = 0;
            myLoc.setPincode(postalCode);
        }



        else {
            Toast.makeText(getApplicationContext(),"Please enter pin code for selected place manually"  , Toast.LENGTH_LONG).show();

        }

    }

    @Override
    public void onPermissionGranted(String[] permissionName) {
        Slog.i("onPermissionGranted() ", "" + Arrays.toString(permissionName));
    }

    @Override
    public void onPermissionDeclined(String[] permissionName) {
        Slog.i("onPermissionDeclined() ", "" + Arrays.toString(permissionName));
    }

    @Override
    public void onPermissionPreGranted(String permissionsName) {
        Slog.i("onPermissionPreGranted() ", permissionsName);
    }

    @Override
    public void onPermissionNeedExplanation(String permissionName) {
        Slog.i("onPermissionNeedExplanation() ", permissionName);

        /*
        Show dialog here and ask permission again. Say why
         */

        showAlertDialog(DIALOG_TITLE, DIALOG_MESSAGE, PERMISSION);

    }

    @Override
    public void onPermissionReallyDeclined(String permissionName) {
    }

    @Override
    public void onNoPermissionNeeded() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MapActivity.this.finish();
        locationformapfromautocomplete = 1;
    }

    public void doneClick(View v){
        if ( mapactivity == 1) {

            if (locationformapfromautocomplete == 0) {
                myLoc.setAddress (addresfromautocomplete);
               // myLoc.setAddress(BaseActivity.AddressFromGoogleApi);
            }else {
                myLoc.setAddress(BaseActivity.AddressFromGoogleApi);
            }
            myLoc.setPincode(BaseActivity.PinfromGoogleApi);
            Intent intent = new Intent();
            intent.putExtra("mloc", myLoc);
            setResult(RESULT_OK, intent);
        }else {
            Intent intent = new Intent();
           intent.putExtra("mloc", myLoc);
            setResult(RESULT_OK, intent);
        }
        mapactivity = 0;

      // finish();
        locationformapfromautocomplete = 1;
        MapActivity.this.finish();
    }

    private void showAlertDialog(String title, String message, final String permission) {

        AlertDialog settingDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Request", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionHelper.requestAfterExplanation(permission);
                        dialog.dismiss();
                    }
                })
                .create();
        settingDialog.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapactivity = 0 ;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {

              // Toast.makeText(getApplicationContext(),"Your Selected Address is " +(String.valueOf(mLastLocation.getLatitude())) , Toast.LENGTH_LONG).show();
          //showToast(String.valueOf(mLastLocation.getLatitude()));
         //  showToast(String.valueOf(mLastLocation.getLongitude()));

            LatLng DELHI6 = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

            updateAddress(DELHI6);
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(DELHI6)      // Sets the center of the map to Mountain View
                    .zoom(zoom)                   // Sets the zoom
                    .build();
           // addPointToMap(DELHI6);
                          // Creates a CameraPosition from the builder
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}