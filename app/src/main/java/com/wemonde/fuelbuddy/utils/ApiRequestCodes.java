package com.wemonde.fuelbuddy.utils;

/**
 * Created by Devendra Pandey on 16-03-2017.
 */

public class ApiRequestCodes {
    public static final int REQUEST_LOGIN = 2001;
    public static final int REQUEST_GET_ASSETS = 2005;
    public static final int REQUEST_FUEL_TYPES = 2006;
    public static final int REQUEST_GET_BUILDING = 2007;
    public static final int REQUEST_GET_ALL_BUILDING = 2008;
    public static final int REQUEST_GET_STATES = 2009;
    public static final int REQUEST_BOOK_ORDER = 2011;
    public static final int REQUEST_GET_ASSET_IDENTIFIER = 2016;
    public static final int REQUEST_ADD_ASSET = 2018;
    public static final int REQUEST_ADD_BUILD = 2019;
    public static final int REQUEST_GET_ORDERS = 2017;
    public static final int REQUEST_GET_LOCATION = 2211;
    public static final int REQUEST_GET_TRACKING = 2212;

    public static final int REQ_CODE_PICK_IMAGE = 2214;


    public static final int REQUEST_TOKEN_REGISTRATION = 2213;
    public static final int REQUEST_POST_IMAGE = 2215;
    public static final int REQUEST_PASSWORD_CHANGE = 2217;
    public static final int REQUEST_OTP_PASSWORD_CHANGE = 2218;
    public static final int REQUEST_RESET_PASSWORD_CHANGE = 2219;

    public static final int REQUEST_GET_DASHBOARD_DETAILS = 2220;

    public static final int REQUEST_NEW_USER_REGISTRATION = 2221;

    public static final int REQUEST_ORDER_STATUS_NEW = 2222;

    public static final int REQUEST_SENSOR_DATA = 2223;

    public static final int REQUEST_TANK_DETAILS = 2224;

    public static final int REQUEST_GET_DISTRICT = 2225;
    public static final int REQUEST_GET_LOCK_ELOCK = 2226;
    public static final int REQUEST_GET_UNLOCK_ELOCK = 2227;


    public static final int REQUEST_GET_OTP_REGISTRATION = 2228;
    public static final int REQUEST_VERIFY_OTP_REGISTRATION = 2229;

    public static final int REQUEST_TANK_BUDDY_CURRENT_CONSUMPTION = 2230;


}