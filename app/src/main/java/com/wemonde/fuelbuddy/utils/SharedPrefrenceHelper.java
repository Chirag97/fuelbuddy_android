package com.wemonde.fuelbuddy.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.HashMap;

/**
 * Created by Almond7 on 22-08-2016.
 */
public class SharedPrefrenceHelper {

    public static final String PREF_KEY = "com.sarkari";
    public static final String LAT_KEY = "location-lat";
    public static final String LNG_KEY = "location-lng";
    private static final String COMPANY_ID = "company-id";
    private static final String ACCOUNT_FBID = "facebook-id";
    private static final String ACCOUNT_LVO = "lvo";
    private static final String FUELS = "fuels";
    private static final String ACCOUNT_PASS = "account-pass";
    private static final String ACCOUNT_UUID = "account-uuid";
    private static final String ORDER_UUID = "order-id";
    private static final String ACCOUNT_FNAME = "account-uname";
    private static final String ACCOUNT_EMAIL = "account-email";
    private static final String ACCOUNT_PHONE = "account-phone";
    private static final String ACCOUNT_LOGIN = "account-login";
    private static final String ACCOUNT_REF_CODE = "referral-code";
    private static final String ACCOUNT_KEY_TOKEN = "token-code";
    private static final String NOTIFICATION_COUNT = "notification-count";
    private static final String SCHDULE_DATE = "schedule-date";

    public static SharedPreferences defaultPrefrences;

    public static SharedPreferences getDefaultPref(Context context) {
        if (defaultPrefrences == null) {
            defaultPrefrences = context.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        }
        return defaultPrefrences;
    }

    public static boolean clearAll(Context context){
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.clear();
        return editor.commit();
    }

    public static void setLatlng(Context context, Location location) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putLong(LAT_KEY, Double.doubleToLongBits(location.getLatitude()));
        editor.putLong(LNG_KEY, Double.doubleToLongBits(location.getLongitude()));
        editor.commit();

        Log.d("SharedPref", " @mky:setLatlng :" + " {" + location.getLatitude() + "," + location.getLatitude() + "}");
    }

    public static HashMap<String, Double> getLatLng(Context context) {
        HashMap<String, Double> latlng = new HashMap<>();
        double latitude = Double.longBitsToDouble(getDefaultPref(context).getLong(LAT_KEY, 0));
        double longitude = Double.longBitsToDouble(getDefaultPref(context).getLong(LNG_KEY, 0));
        latlng.put(LAT_KEY, latitude);
        latlng.put(LNG_KEY, longitude);
        Log.d("SharedPref", " @mky:getLatLng :" + " {" + latitude + "," + longitude + "}");
        return latlng;

    }

    public static String getDeviceId(Context context) {
        String device_uuid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (device_uuid == null) {
            device_uuid = "12356789"; // for emulator testing
        } else {
            try {
                byte[] _data = device_uuid.getBytes();
                MessageDigest _digest = MessageDigest.getInstance("MD5");
                _digest.update(_data);
                _data = _digest.digest();
                BigInteger _bi = new BigInteger(_data).abs();
                device_uuid = _bi.toString(36);
            } catch (Exception e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        }
        return device_uuid;
    }

    public static void setCompanyId(Context context, String cid) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(COMPANY_ID, cid);
        editor.commit();
        Log.d("SharedPref", " @mky:setCompanyId : " + "CID : " + cid);
    }

    public static String getCompanyID(Context context) {
        return getDefaultPref(context).getString(COMPANY_ID, "0");
    }

    public static void setFbid(Context context, String fbid) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_FBID, fbid);
        editor.commit();
        Log.d("SharedPref", " @mky:setFbid : " + "FBID : " + fbid);
    }

    public static String getUFbid(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_FBID, "0");
    }

    public static void setUUID(Context context, String uuid) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_UUID, uuid);
        editor.commit();



        Log.d("SharedPref", " @mky:setUUID : " + "UUID : " + uuid);
    }

    public static void setORDERID(Context context, String orderid) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ORDER_UUID, orderid);
        editor.commit();



        Log.d("SharedPref", " @mky:setUUID : " + "UUID : " + orderid);
    }




    public static void setUNAME(Context context, String name) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_FNAME, name);
        editor.commit();
        Log.d("SharedPref", " @mky:setUNAME : " + "UNAME : " + name);
    }

    public static void setUpass(Context context, String name) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_PASS, name);
        editor.commit();
        Log.d("SharedPref", " @mky:setUNAME : " + "UNAME : " + name);
    }

    public static void setUEMAIL(Context context, String email) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_EMAIL, email);
        editor.commit();
        Log.d("SharedPref", " @mky:setUEMAIL : " + "UEMAIL : " + email);
    }

    public static boolean setLvo(Context context, String lvo) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_LVO, lvo);
        Log.d("SharedPref", " @mky:setUEMAIL : " + "UEMAIL : " + lvo);
        return editor.commit();
    }

    public static boolean setFuels(Context context, String fuels) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(FUELS, fuels);
        Log.d("SharedPref", " @mky:setUEMAIL : " + "fuels : " + fuels);
        return editor.commit();
    }

    public static void setReferralCode(Context context, String ref) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_REF_CODE, ref);
        editor.commit();
        Log.d("SharedPref", " @mky:setUEMAIL : " + "referrall : " + ref);
    }

    public static void setUPHONE(Context context, String mobile) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_PHONE, mobile);
        editor.commit();
        Log.d("SharedPref", " @mky:setUPHONE : " + "UPHONE : " + mobile);
    }


    public static void setTOKEN(Context context, String key) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(ACCOUNT_KEY_TOKEN, key);
        editor.commit();
        Log.d("SharedPref", " @mky:setTOKEN : " + "TOKEN : " + key);
    }



    public static void setNotificationCount (Context context,String count){
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(NOTIFICATION_COUNT, count);
        editor.commit();
        Log.d("SharedPref", " @mky:setTOKEN : " + "TOKEN : " + count);
    }




    public static String getNotificationCount (Context context){
        return getDefaultPref(context).getString(NOTIFICATION_COUNT, "");
        //return getNotificationCount(context).getString(NOTIFICATION_COUNT, "0");
    }


    public static void setscheduledate (Context context,String count){
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putString(SCHDULE_DATE, count);
        editor.commit();
        Log.d("SharedPref", " @mky:setTOKEN : " + "TOKEN : " + count);
    }




    public static String getscheduledate (Context context){
        return getDefaultPref(context).getString(SCHDULE_DATE, "0");
        //return getNotificationCount(context).getString(NOTIFICATION_COUNT, "0");
    }


    public static String getTOKEN(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_KEY_TOKEN, "0");

    }


    public static String getUNAME(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_FNAME, "0");
    }

    public static String getUPass(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_PASS, "0");
    }

    public static String getURefCode(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_REF_CODE, "0");
    }

    public static String getUEMAIL(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_EMAIL, "0");
    }

    public static String getUPHONE(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_PHONE, "0");
    }

   public static String getLvo(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_LVO, "");
    }

   public static String getFuels(Context context) {
        return getDefaultPref(context).getString(FUELS, "");
    }

    public static String getUUID(Context context) {
        return getDefaultPref(context).getString(ACCOUNT_UUID, "-1");
    }

    public static void setIsLogin(Context context, boolean islogin) {
        SharedPreferences.Editor editor = getDefaultPref(context).edit();
        editor.putBoolean(ACCOUNT_LOGIN, islogin);
        editor.commit();
        Log.d("SharedPref", " @mky:setIsLogin : " + "ISLOGIN : " + islogin);
    }

    public static boolean getIsLogin(Context context) {
        return getDefaultPref(context).getBoolean(ACCOUNT_LOGIN, false);
    }
}
