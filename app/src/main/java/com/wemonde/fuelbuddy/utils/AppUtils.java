package com.wemonde.fuelbuddy.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ScaleDrawable;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.wemonde.fuelbuddy.R;
import com.wemonde.fuelbuddy.base.AppController;
import com.wemonde.fuelbuddy.base.BaseActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Devendra Pandey on 14-03-2017.
 */

public class AppUtils {

    // Error Messages
    public static final String REQUIRED_MSG = "required";
    public static final String EMAIL_MSG = "invalid email";
    public static final String PHONE_MSG = "##########";
    public static final String PASWD_MSG = "minimum 8 characters";
    public static final String PASWD_RE_MSG = "passwords do not match";
    private static Context context;
    // Regular Expression
    // you can change the expression based on your need
    private static final String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static final String PHONE_REGEX = "(\\d{10})";

    public static int dpToPx(int dp, DisplayMetrics displayMetrics) {
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static boolean isLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    public static boolean isEmpty(TextView tv){

        if(tv == null) return true;
        try {
            if (tv.getText().toString().isEmpty()) return true;
            if (tv.getText().toString().trim().length() <= 0) return true;

        }catch (Exception e){
            return true;
        }

        return false;

    }

    public static void setImageUrl(final View view, final String url) {
        if (null == url)
            return;
        setImageUrl(view, url, R.drawable.def_wide);
    }

    public static void setImageUrl(final View view, String url, int error) {
        if (null == url)
            return;

        double token=0;
        token=Math.random();
        Picasso.with(view.getContext()).invalidate("");
        Picasso.with(view.getContext())
                .load(Uri.parse(url))
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(((ImageView) view));

//        Picasso.with(view.getContext())
//                .load(Uri.parse(url))
//                .placeholder(R.drawable.def_user)
//                .into(((ImageView) view));


      //  Picasso.with(view.getContext()).load(Uri.parse(url)).error(error).into(((ImageView) view));


    }

    public static boolean isValidMobile(String phone) {
        return !TextUtils.isEmpty(phone) && Pattern.matches(PHONE_REGEX, phone);
    }

    public static boolean isValidName(String name) {
        return !name.isEmpty();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Pattern.matches(EMAIL_REGEX, target);
    }

    public static String getformattedDateTime(long create_time) {
        Date date = new Date(create_time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.getDefault());
        String time = simpleDateFormat.format(date);
        return time;
    }

    public static void setSeekBarColor(SeekBar seekBar, int newColor) {
        seekBar.getThumb().setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
        LayerDrawable ld = (LayerDrawable) seekBar.getProgressDrawable().getCurrent();
        ScaleDrawable d1 = (ScaleDrawable) ld.findDrawableByLayerId(android.R.id.progress);
        d1.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);

    }

    public static String getFormattedCurrency(String price) {
        boolean iAmInIndia = "IN".equals(System.getProperty("user.country"));
        DecimalFormat formatter = iAmInIndia ? new DecimalFormat("\u20B9 000.00/-") : new DecimalFormat("\u00A4 000.00/-");
        return formatter.format(Double.parseDouble(price));
    }

    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    public static void saveStringToFile(Context context, String dataStr, String name) {
        String path = AppController.getCacheDir(context);
        Slog.d("path in save : " + path);
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.mkdirs();
            }

            File myFile = new File(file, name);
            if (!myFile.exists())
                myFile.createNewFile();
            FileOutputStream fos;
            byte[] data = dataStr.getBytes();
            try {
                fos = new FileOutputStream(myFile);
                fos.write(data);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getStringFromFile(Context context, String name) {
        String path = AppController.getCacheDir(context);
//        File file = new File(path);
//        if (!file.exists())
//            new File(path).mkdirs();
        File file = new File(path, name);

        if (!file.exists()){
            return "";
        }

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
        } catch (IOException e) {
            Slog.e("unable to read files : " + e.getMessage());
            e.printStackTrace();
        }

        return text.toString();
    }

    public static void copyAssets(Context context, String filename) {
        AssetManager assetManager = context.getAssets();
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(filename);
            File outFile = new File(AppController.getCacheDir(context), filename);
            out = new FileOutputStream(outFile);
            copyFile(in, out);
        } catch (IOException e) {
            Slog.e("Failed to copy asset file: " + filename);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    // NOOP
                }
            }
        }
    }

    public static String getFormattedDate(String date_s) {

        // *** note that it's "yyyy-MM-dd hh:mm:ss" not "yyyy-mm-dd hh:mm:ss", YYYY-MM-DDThh:mm:ss.s
       // SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date date = null;
        try {
            date = dt.parse(date_s);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        // *** same for the format String below
        SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
//        System.out.println(dt1.format(date));
        return dt1.format(date);
    }


    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}